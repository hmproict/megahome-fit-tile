package com.homepro.megahome.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.*
import com.google.gson.annotations.SerializedName
import java.lang.reflect.Type
import java.util.*


data class Size(val width: Double, val length: Double) {
    var show = "${width}x${length}"
}

//"size_inch": "10x16",
//"size_cm": "25x40",
//"box": "1.000",
//"type": "Wall"

@Entity(tableName = "tile_size_table")
data class TileSize(

    @PrimaryKey
    val id: String,

    @ColumnInfo(name = "size_inch")
    @SerializedName("size_inch")
    val sizeInch: String,

    @ColumnInfo(name = "size_cm")
    @SerializedName("size_cm")
    val sizeCm: String,

    @SerializedName("type")
    @ColumnInfo(name = "type")
    val tileType: String,

    @SerializedName("box")
    @ColumnInfo(name = "box")
    val box: Double


) {
    public val cmSize: Size
        get() {
            val component = sizeCm.toUpperCase(Locale.US).split("X")
            val width = component.first().toDouble()
            val length = component.last().toDouble()
            return Size(width, length)
        }

    public val inchSize: Size
        get() {
            val component = sizeInch.toUpperCase(Locale.US).split("X")
            val width = component.first().toDouble()
            val length = component.last().toDouble()
            return Size(width, length)
        }
}

class TileSizeDeserializerTypeAdapter : JsonDeserializer<TileSize?> {
    @Throws(JsonParseException::class)
    override fun deserialize(
        jsonElement: JsonElement,
        type: Type?,
        jsonDeserializationContext: JsonDeserializationContext?
    ): TileSize? {

        if (jsonElement.isJsonObject) {
            val jObject = jsonElement.asJsonObject
            return try {
                val inch = jObject.get("size_inch").asString
                val tileType = jObject.get("type").asString
                val cm = jObject.get("size_cm").asString
                val box = jObject.get("box").asDouble
                TileSize(inch + tileType, inch, cm, tileType, box)
            } catch (e: NumberFormatException) {
                null
            }
        }
        return null
    }
}