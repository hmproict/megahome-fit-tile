package com.homepro.megahome.database

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.*
import com.homepro.megahome.model.Tile

@Dao
interface TileDatabaseDao {
    @Insert
    fun insert(tile: Tile)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(tiles: List<Tile>)

    @Update
    fun update(tile: Tile)

    @Query("SELECT * FROM tile_table ORDER BY type, id ASC")
    fun getTiles(): LiveData<List<Tile>>

    @Query("SELECT * FROM tile_table ORDER BY id")
    fun tilesFactory(): DataSource.Factory<Int, Tile>

    @Query("SELECT * FROM tile_table WHERE type = :type ORDER BY id")
    fun tilesByType(type: String): DataSource.Factory<Int, Tile>

    @Query("SELECT * FROM tile_table WHERE width = :width AND length = :length ORDER BY id")
    fun tilesBySize(width: Double, length: Double): DataSource.Factory<Int, Tile>

    @Query("SELECT * FROM tile_table WHERE type = :type AND width = :width AND length = :length ORDER BY id")
    fun tilesByTypeAndSize(type: String, width: Double, length: Double): DataSource.Factory<Int, Tile>

    @Query("SELECT * FROM tile_table ORDER BY type, id ASC")
    suspend fun tilesSuspend(): List<Tile>

    @Query("SELECT * FROM tile_table WHERE id = :tileId")
    suspend fun tileById(tileId: String): Tile?

    @Query("SELECT * FROM tile_table WHERE width = :width AND length = :length LIMIT 1")
    suspend fun tileBySize(width: Double, length: Double): Tile?

    @Query("SELECT * FROM tile_table ORDER BY id DESC LIMIT 1")
    fun getLastedTile(): Tile?
}