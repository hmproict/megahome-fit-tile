package com.homepro.megahome.model

import com.google.gson.annotations.SerializedName

data class TileSizeResponse(
    @SerializedName("list") val sizes: List<TileSize>
)
