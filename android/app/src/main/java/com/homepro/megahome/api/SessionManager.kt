package com.homepro.megahome.api

import android.content.Context
import android.content.SharedPreferences
import com.homepro.megahome.R
import com.homepro.megahome.model.Token

class SessionManager(context: Context) {
    private var prefs: SharedPreferences = context.getSharedPreferences(
        context.getString(R.string.app_name),
        Context.MODE_PRIVATE
    )

    companion object {
        const val TOKEN_KEY = "access_token"
        const val TOKEN_EXPIRED_AT = "expired_at"
        const val CLIENT_ID = "client_id"
    }

    fun setClientId(clientId: String) {
        val editor = prefs.edit()
        editor.putString(CLIENT_ID, clientId)
        editor.apply()
    }

    fun saveAuthToken(token: Token) {
        val currentUnixTimestamp = System.currentTimeMillis() / 1000
        token.expiredAt = currentUnixTimestamp + token.expiresIn

        val editor = prefs.edit()
        editor.putString(TOKEN_KEY, token.accessToken)
        editor.putLong(TOKEN_EXPIRED_AT, token.expiredAt)
        editor.apply()
    }

    fun fetchAuthToken(): String? {
        return prefs.getString(TOKEN_KEY, null)
    }

    fun fetchAuthExpiredAt(): Long {
        return prefs.getLong(TOKEN_EXPIRED_AT, 0L)
    }

    fun fetchClientId(): String? {
        return prefs.getString(CLIENT_ID, null)
    }
}