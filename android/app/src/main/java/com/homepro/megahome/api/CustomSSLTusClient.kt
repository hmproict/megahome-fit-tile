package com.homepro.megahome.api

import io.tus.java.client.TusClient
import java.net.HttpURLConnection
import java.security.SecureRandom
import javax.net.ssl.*

class CustomSSLTusClient : TusClient() {
    private val customSSLFactory: SSLSocketFactory
        get() {
            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                override fun checkClientTrusted(
                    chain: Array<java.security.cert.X509Certificate>,
                    authType: String
                ) {
                }

                override fun checkServerTrusted(
                    chain: Array<java.security.cert.X509Certificate>,
                    authType: String
                ) {
                }

                override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
                    return arrayOf()
                }
            })

            val sslContext: SSLContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, SecureRandom())

            return sslContext.socketFactory
        }

    override fun prepareConnection(connection: HttpURLConnection) {
        super.prepareConnection(connection)

        if (connection is HttpsURLConnection) {
            val secureConnection: HttpsURLConnection = connection
            secureConnection.sslSocketFactory = this.customSSLFactory
        }
    }
}