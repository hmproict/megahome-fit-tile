package com.homepro.megahome.ui.calculate

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.homepro.megahome.data.TileRepository
import com.homepro.megahome.database.TileDatabaseDao
import com.homepro.megahome.ui.catalog.CatalogViewModel

class CalculateViewModelFactory(
    private val dataSource: TileDatabaseDao,
    private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CalculateViewModel::class.java)) {
            return CalculateViewModel(dataSource, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}