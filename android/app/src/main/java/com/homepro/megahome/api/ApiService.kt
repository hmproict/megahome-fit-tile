package com.homepro.megahome.api

import com.homepro.megahome.realm.Calc
import com.homepro.megahome.realm.Tile
import com.homepro.megahome.realm.TileAll
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface ApiService {
    @GET("calc.jsp")
    fun calculate(
        @Query("id") id: String,
        @Query("area") area: String,
        @Query("length") length: String,
        @Query("width") width: String,
        @Query("extra") extra: String?
    ): Call<Calc>

    @GET("get.jsp")
    fun tile(
        @Query("id") id: String?,
        @Query("flag") flag: String?,
        @Query("from") from: String,
        @Query("size") size: String
    ): Call<TileAll>

    @GET("pre-install.jsp")
    fun preTile(): Call<List<Tile>>

    //    https://estore.homepro.co.th/service/fittile/get.jsp?id=122623
    @GET("get.jsp")
    fun tileWithId(@Query("id") id: String): Call<Tile>
}