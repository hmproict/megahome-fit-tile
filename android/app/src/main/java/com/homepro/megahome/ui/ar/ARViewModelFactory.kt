package com.homepro.megahome.ui.ar

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.homepro.megahome.data.TileRepository

@Suppress("UNCHECKED_CAST")
class ARViewModelFactory(
    private val repository: TileRepository
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ARViewModel::class.java)) {
            return ARViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}