package com.homepro.megahome.realm

import com.google.gson.annotations.SerializedName

open class Default {
    @SerializedName("status")
    var status: String? = null

    @SerializedName("message")
    var message: String? = null
}