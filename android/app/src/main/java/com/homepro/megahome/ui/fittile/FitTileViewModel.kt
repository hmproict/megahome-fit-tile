package com.homepro.megahome.ui.fittile

import android.app.Application
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import androidx.lifecycle.*
import androidx.paging.PagedList
import coil.Coil
import coil.request.GetRequest
import com.homepro.megahome.api.FitTileApiService
import com.homepro.megahome.data.TileRepository
import com.homepro.megahome.database.TileDatabaseDao
import com.homepro.megahome.model.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

class FitTileViewModel(
    private val service: FitTileApiService,
    val database: TileDatabaseDao,
    repository: TileRepository,
    application: Application
) : AndroidViewModel(application) {
    private val _navigateToScanner = MutableLiveData<Boolean>()
    val navigateToScanner: LiveData<Boolean>
        get() = _navigateToScanner

    private val tilesFilterLiveData = MutableLiveData<TilesFilter>()
    private val tilesResult: LiveData<TilesResult> = Transformations.map(tilesFilterLiveData) {
        repository.getTiles(it.tileType, it.size)
    }

    val tiles: LiveData<PagedList<Tile>> = Transformations.switchMap(tilesResult) { it.data }
    val networksError: LiveData<String> = Transformations.switchMap(tilesResult) { it.networksError }

    lateinit var tilesCache: List<Tile>
    lateinit var tilesWallCache: List<Tile>
    lateinit var tilesFloorCache: List<Tile>

    lateinit var selectedTile: Tile
    private val _selectedBitmap = MutableLiveData<Bitmap>()
    val selectedBitmap: LiveData<Bitmap>
        get() = _selectedBitmap

    val tilesDefault = runBlocking {
        withContext(Dispatchers.IO) {
            database.tilesSuspend()
        }
    }

    var isCompletePin: Boolean = false
    var perspectiveProgress: Int = 0
    var scaleProgress: Int = 0

    fun completeSelectedArea() {
        isCompletePin = true
}

    fun onQRScannerClicked() {
        _navigateToScanner.value = true
    }

    fun onScannerNavigate() {
        _navigateToScanner.value = false
    }

    fun selectTile(tile: Tile) {
        selectedTile = tile
        viewModelScope.launch {
            _selectedBitmap.value = withContext(Dispatchers.IO) {
                val downloadURL = if (tile.imagePatternURLPath.isNotEmpty()) {
                    tile.imagePatternURLPath
                } else {
                    tile.imageURL
                }

                val context = getApplication<Application>().applicationContext
                val imageLoader = Coil.imageLoader(context)
                val request = GetRequest.Builder(context)
                    .data(downloadURL)
                    .allowHardware(false)
                    .build()
                (imageLoader.execute(request).drawable as BitmapDrawable).bitmap
            }!!
        }
    }

    fun changeTilesByTypeAndSize(tileType: TileType, size: Size? = null) {
        val isTheSame = lastFilter()?.tileType == tileType && lastFilter()?.size == size
        if (isTheSame) {
            return
        } else {
            tilesFilterLiveData.postValue(TilesFilter(tileType, size))
        }
    }

    fun lastFilter(): TilesFilter? = tilesFilterLiveData.value

    suspend fun getTileById(id: String): Tile? {
        return withContext(Dispatchers.IO) {
            try {
                database.tileById(id) ?: service.tile(id)
            } catch(e: Exception) {
                null
            }
        }
    }
}