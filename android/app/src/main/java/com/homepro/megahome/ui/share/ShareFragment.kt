package com.homepro.megahome.ui.share

import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.google.android.material.button.MaterialButton
import com.homepro.megahome.R
import com.homepro.megahome.api.AppService
import com.homepro.megahome.database.FitTileDatabase
import com.homepro.megahome.databinding.ShareFragmentBinding
import com.homepro.megahome.glide.GlideApp
import com.homepro.megahome.manager.DateManager
import com.homepro.megahome.manager.UploadImageManager
import com.homepro.megahome.model.Tile
import com.homepro.megahome.model.TileType
import com.homepro.megahome.realm.MyProject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File

class ShareFragment : Fragment() {
    private val TAG = "ShareFragment"
    private val args by navArgs<ShareFragmentArgs>()

    private lateinit var viewModel: ShareViewModel
    private lateinit var binding: ShareFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.share_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val application = requireNotNull(this.activity).application
        val dataSource = FitTileDatabase.getInstance(application).tileDatabaseDao
        val viewModelFactory = ShareViewModelFactory(dataSource, application)
        viewModel = ViewModelProvider(this, viewModelFactory).get(ShareViewModel::class.java)

        binding.project = args.project

        binding.beforeAfterInclude.beforePanelTransparentButton.setOnClickListener {
            selectBeforeMenu(args.project)
        }

        binding.beforeAfterInclude.afterPanelTransparentButton.setOnClickListener {
            selectAfterMenu(args.project)
        }

        binding.backTextView.setOnClickListener {
            findNavController().navigateUp()
        }

        binding.cancelButton.setOnClickListener {
            findNavController().navigate(ShareFragmentDirections.actionShareFragmentToProjectFragment())
        }

        binding.saveButton.setOnClickListener {
            it.isEnabled = false
            args.project.imagePath?.let { imagePath ->
                val contentURI = FileProvider.getUriForFile(requireContext(), "${requireContext().packageName}.FileProvider", File(imagePath))
                binding.progressBarFrameLayout.visibility = View.VISIBLE
                disableTouch()
                AppService().uploadProject(object : UploadImageManager.UploadListener {
                    override fun setUploadProgress(progress: Int) {}

                    override fun setStatus(obj: UploadImageManager.Status) {
                        if (obj.status == "finish") {
                            it.isEnabled = true
                            binding.progressBarFrameLayout.visibility = View.INVISIBLE
                            enableTouch()
                            findNavController().navigate(ShareFragmentDirections.actionShareFragmentToProjectFragment())
                        }
                    }

                    override fun showError(ex: Exception) {
                        ex.message?.let {
                            Log.e(TAG, it)
                        }
                    }
                }, requireActivity(), args.project.transactionId!!, "complete", args.project.tileId!!, contentURI)
            }
        }

        binding.beforeAfterInclude.afterPanelTransparentButton.callOnClick()

        val tileIds = args.project.tileId?.split(",")?.toSet()
        tileIds?.let {
            it.forEach { tileId ->
                lifecycleScope.launch(Dispatchers.Main) {
                    val tile = viewModel.getTileById(tileId)
                    tile?.let {
                        val tileCell = createTileCell(tile, args.project)
                        binding.tileDetailLinearLayout.addView(tileCell)
                    }
                }
            }
        }
    }

    private fun disableTouch() {
        requireActivity().window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    private fun enableTouch() {
        requireActivity().window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    private fun selectBeforeMenu(project: MyProject) {
        binding.beforeAfterInclude.iconBeforeImageView.setColorFilter(ContextCompat.getColor(requireContext(), R.color.colorAccent), PorterDuff.Mode.SRC_IN)
        binding.beforeAfterInclude.beforeTextView.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorAccent))

        binding.beforeAfterInclude.iconAfterImageView.setColorFilter(ContextCompat.getColor(requireContext(), R.color.colorTextOnWhite), PorterDuff.Mode.SRC_IN)
        binding.beforeAfterInclude.afterTextView.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorTextOnWhite))

        project.rawImage?.let { imagePath ->
            showBeforeImage(imagePath)
        }
    }

    private fun showBeforeImage(imagePath: String) {
        GlideApp.with(this)
            .load(imagePath)
            .skipMemoryCache(false)
            .placeholder(ColorDrawable(Color.LTGRAY))
            .transition(DrawableTransitionOptions.withCrossFade())
            .centerInside()
            .into(binding.roomImageView)
    }

    private fun selectAfterMenu(project: MyProject) {
        binding.beforeAfterInclude.iconBeforeImageView.setColorFilter(ContextCompat.getColor(requireContext(), R.color.colorTextOnWhite), PorterDuff.Mode.SRC_IN)
        binding.beforeAfterInclude.beforeTextView.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorTextOnWhite))

        binding.beforeAfterInclude.iconAfterImageView.setColorFilter(ContextCompat.getColor(requireContext(), R.color.colorAccent), PorterDuff.Mode.SRC_IN)
        binding.beforeAfterInclude.afterTextView.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorAccent))

        project.imagePath?.let { imagePath ->
            showAfterImage(imagePath)
        }
    }

    private fun showAfterImage(imagePath: String) {
        GlideApp.with(this)
            .load(File(imagePath))
            .skipMemoryCache(false)
            .placeholder(ColorDrawable(Color.LTGRAY))
            .transition(DrawableTransitionOptions.withCrossFade())
            .centerInside()
            .into(binding.roomImageView)
    }

    private fun createTileCell(tile: Tile, project: MyProject): View {
        val tileCellView = layoutInflater.inflate(R.layout.tile_detail_cell, null)
        val tileType: TileType = if (tile.type ==  "Wall") TileType.WALL else TileType.FLOOR

        if (tileType == TileType.WALL) {
            tileCellView.findViewById<MaterialButton>(R.id.calculateButton).visibility = View.GONE
        }

        tileCellView.findViewById<AppCompatTextView>(R.id.createdDateDetailTextView).text = DateManager.getInstance().getDateFormatProject(project.dateCreate!!)
        tileCellView.findViewById<AppCompatTextView>(R.id.artNumberDetailTextView).text = tile.id
        tileCellView.findViewById<AppCompatTextView>(R.id.brandDetailTextView).text = tile.vendor
        tileCellView.findViewById<AppCompatTextView>(R.id.sizeDetailTextView).text = getString(R.string.size_format, tile.length.toString(), tile.width.toString())
        tileCellView.findViewById<AppCompatTextView>(R.id.featureDetailTextView).text = tile.feature
        tileCellView.findViewById<AppCompatTextView>(R.id.nameDetailTextView).text = tile.shortDescription

        tileCellView.findViewById<MaterialButton>(R.id.calculateButton).visibility = View.GONE

        tileCellView.findViewById<AppCompatButton>(R.id.buyNowButton).visibility = View.GONE

        tileCellView.findViewById<AppCompatButton>(R.id.roomSceneButton).setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(tile.qrURL)))
        }

        return tileCellView
    }
}