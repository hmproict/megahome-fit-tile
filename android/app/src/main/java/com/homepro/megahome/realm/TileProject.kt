package com.homepro.megahome.realm

import android.os.Parcel
import android.os.Parcelable
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.Required

open class TileProject protected constructor(`in`: Parcel?) : Parcelable, RealmObject() {
    @PrimaryKey
    @Required
    var id: String? = null

    @Required
    var tileId: String? = null

    @Required
    var shortDesc: String? = null

    @Required
    var longDesc: String? = null

    @Required
    var width: String? = null

    @Required
    var length: String? = null

    @Required
    var ramdom: String? = null

    @Required
    var box: String? = null

    @Required
    var feature: String? = null

    @Required
    var vendor: String? = null

    @Required
    var pattern: String? = null

    @Required
    private var imagePattern: String? = null

    @Required
    var image: String? = null

    @Required
    var url: String? = null

    var type: String? = null

    var appflag: String? = null

    var imageByte: ByteArray? = null

    constructor() : this(null)


    fun getImagePattern(): String? {
        return if (imagePattern == null || imagePattern == "") {
            image
        } else imagePattern
    }

    fun setImagePattern(imagePattern: String) {
        this.imagePattern = imagePattern
    }

    fun setObject(tile: Tile) {
        //        id = tile.getId();
        tileId = tile.id
        shortDesc = tile.shortDesc
        longDesc = tile.longDesc
        width = tile.width
        length = tile.length
        ramdom = tile.ramdom
        box = tile.box
        feature = tile.feature
        vendor = tile.vendor
        pattern = tile.pattern
        image = tile.image
        url = tile.url
        appflag = tile.appflag
        image = tile.image
        imagePattern = if (tile.image_pattern == null) "" else tile.image_pattern
    }

    fun setTileData(tile: com.homepro.megahome.model.Tile) {
        tileId = tile.id
        shortDesc = tile.shortDescription
        longDesc = tile.longDescription
        width = tile.width.toString()
        length = tile.length.toString()
        tile.random?.let {
            ramdom = it
        }
        box = tile.box.toString()
        feature = tile.feature
        vendor = tile.vendor
        pattern = tile.pattern
        image = tile.imageURL
        url = tile.qrURL
        imagePattern = ""

        type = tile.type
    }

    init {
        if (`in` == null) {

        } else {
            id = `in`.readString()
            tileId = `in`.readString()
            shortDesc = `in`.readString()
            longDesc = `in`.readString()
            width = `in`.readString()
            length = `in`.readString()
            ramdom = `in`.readString()
            box = `in`.readString()
            feature = `in`.readString()
            vendor = `in`.readString()
            pattern = `in`.readString()
            imagePattern = `in`.readString()
            image = `in`.readString()
            url = `in`.readString()
            appflag = `in`.readString()
            imageByte = `in`.createByteArray()
        }

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(tileId)
        parcel.writeString(shortDesc)
        parcel.writeString(longDesc)
        parcel.writeString(width)
        parcel.writeString(length)
        parcel.writeString(ramdom)
        parcel.writeString(box)
        parcel.writeString(feature)
        parcel.writeString(vendor)
        parcel.writeString(pattern)
        parcel.writeString(imagePattern)
        parcel.writeString(image)
        parcel.writeString(url)
        parcel.writeString(appflag)
        parcel.writeByteArray(imageByte)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TileProject> {
        override fun createFromParcel(parcel: Parcel): TileProject {
            return TileProject(parcel)
        }

        override fun newArray(size: Int): Array<TileProject?> {
            return arrayOfNulls(size)
        }
    }
}