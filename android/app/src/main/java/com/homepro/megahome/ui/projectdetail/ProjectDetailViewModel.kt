package com.homepro.megahome.ui.projectdetail

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.homepro.megahome.api.FitTileApiService
import com.homepro.megahome.database.TileDatabaseDao
import com.homepro.megahome.model.Tile
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ProjectDetailViewModel(
    private val service: FitTileApiService,
    val database: TileDatabaseDao,
    application: Application
) : AndroidViewModel(application) {

    private val _navigateToCalculate = MutableLiveData<String>()
    val navigateToCalculate: LiveData<String>
        get() = _navigateToCalculate

    fun onCalculateClicked(tileId: String) {
        _navigateToCalculate.value = tileId
    }

    fun onNavigateToCalculate() {
        _navigateToCalculate.value = null
    }

    suspend fun getTileById(id: String): Tile? {
        return withContext(Dispatchers.IO) {
            database.tileById(id) ?: service.tile(id)
        }
    }

}
