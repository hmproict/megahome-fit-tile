package com.homepro.megahome.extensions

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.homepro.megahome.glide.GlideApp

fun AppCompatImageView.changeTintColor(context: Context, color: Int) {
    ImageViewCompat.setImageTintList(
        this,
        ColorStateList.valueOf(
            ContextCompat.getColor(context, color)
        )
    )
}

@BindingAdapter("imageURL")
fun AppCompatImageView.setImage(imageURL: String?) {
    imageURL?.let { imageURL ->
        GlideApp.with(this.context)
            .load(imageURL)
            .centerInside()
            .placeholder(ColorDrawable(Color.LTGRAY))
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(this)
    }
}