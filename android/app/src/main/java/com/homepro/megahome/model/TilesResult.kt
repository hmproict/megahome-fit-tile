package com.homepro.megahome.model

import androidx.lifecycle.LiveData
import androidx.paging.PagedList

data class TilesResult(
    val data: LiveData<PagedList<Tile>>,
    val networksError: LiveData<String>
)