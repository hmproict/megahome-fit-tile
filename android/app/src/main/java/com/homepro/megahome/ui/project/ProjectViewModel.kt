package com.homepro.megahome.ui.project

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.homepro.megahome.realm.MyProject
import com.homepro.megahome.realm.ProjectShare

class ProjectViewModel : ViewModel() {
    private val _navigateToProjectDetail = MutableLiveData<MyProject>()
    val navigateToProjectDetail: LiveData<MyProject>
        get() = _navigateToProjectDetail

    private val _navigateToScanner = MutableLiveData<Boolean>()
    val navigateToScanner: LiveData<Boolean>
        get() = _navigateToScanner

    private val _navigateToFitTile = MutableLiveData<Boolean>()
    val navigateToFitTile: LiveData<Boolean>
        get() = _navigateToFitTile

    private val _navigateToProjectDetailFromScanner = MutableLiveData<ProjectShare.Project>()
    val navigateToProjectDetailFromScanner: LiveData<ProjectShare.Project>
        get() = _navigateToProjectDetailFromScanner

    fun onProjectClicked(project: MyProject) {
        _navigateToProjectDetail.value = project
    }

    fun onProjectDetailNavigate() {
        _navigateToProjectDetail.value = null
    }

    fun onQRScanClicked() {
        _navigateToScanner.value = true
    }

    fun onScannerNavigate() {
        _navigateToScanner.value = false
    }

    fun onScannerResult(project: ProjectShare.Project) {
        _navigateToProjectDetailFromScanner.value = project
    }

    fun onProjectDetailFromScannerNavigate() {
        _navigateToProjectDetailFromScanner.value = null
    }

    fun onFitTileClicked() {
        _navigateToFitTile.value = true
    }

    fun onFitTileNavigate() {
        _navigateToFitTile.value = false
    }
}