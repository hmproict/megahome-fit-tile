package com.homepro.megahome.ui.ar

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import androidx.lifecycle.*
import androidx.paging.PagedList
import coil.Coil
import coil.request.GetRequestBuilder
import com.google.ar.sceneform.rendering.Texture
import com.homepro.megahome.data.TileRepository
import com.homepro.megahome.model.Tile
import com.homepro.megahome.model.TileType
import com.homepro.megahome.model.TilesFilter
import com.homepro.megahome.model.TilesResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.concurrent.CompletableFuture

class ARViewModel(
    private val tileRepository: TileRepository
) : ViewModel() {
    // Tiles
    private val tilesFilterLiveData = MutableLiveData<TilesFilter>(TilesFilter(TileType.ALL, null))
    private val tilesResult: LiveData<TilesResult> = Transformations.map(tilesFilterLiveData) {
        tileRepository.getTiles(it.tileType, it.size)
    }
    val tiles: LiveData<PagedList<Tile>> = Transformations.switchMap(tilesResult) { it.data }
    val networksError: LiveData<String> = Transformations.switchMap(tilesResult) { it.networksError }

    // Selected Tile
    private val _selectedTile = MutableLiveData<Tile>()
    val selectedTile: LiveData<Tile>
        get() = _selectedTile

    private val _selectedTileBitmap = MutableLiveData<Bitmap>()
    val selectedTileBitmap: LiveData<Bitmap>
        get() = _selectedTileBitmap

    // Navigation
    private val _navigateToScanner = MutableLiveData<Boolean>()
    val navigateToScanner: LiveData<Boolean>
        get() = _navigateToScanner

    // Constant
    private val _baseURL = "https://s3-ap-southeast-1.amazonaws.com/qas-public-bucket/homepro/FITTILE_IMAGE/"

    // lateinit
    lateinit var triGrid: CompletableFuture<Texture>


    fun selectTile(context: Context, tile: Tile) {
        _selectedTile.value = tile
        viewModelScope.launch(Dispatchers.IO) {
            val downloadURL = if (tile.imagePatternURLPath.isNotEmpty()) {
                _baseURL + tile.imagePatternURLPath
            } else {
                tile.imageURL
            }

            val imageLoader = Coil.imageLoader(context)
            val getRequest = GetRequestBuilder(context)
                .data(downloadURL)
                .allowHardware(false)
                .build()
            imageLoader.execute(getRequest).drawable?.let {
                _selectedTileBitmap.postValue((it as BitmapDrawable).bitmap)
            }
        }
    }

    fun changeTilesFilter(tilesFilter: TilesFilter) {
        tilesFilterLiveData.value = tilesFilter
    }

    fun lastFilter(): TilesFilter = tilesFilterLiveData.value ?: TilesFilter(TileType.ALL, null)

    fun getTileById(id: String): LiveData<Tile?> {
        return tileRepository.getTile(id)
    }

    fun onQRScannerClicked() {
        _navigateToScanner.value = true
    }

    fun onScannerNavigate() {
        _navigateToScanner.value = false
    }
}