package com.homepro.megahome.ui.project

import android.graphics.drawable.BitmapDrawable
import android.net.UrlQuerySanitizer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import coil.Coil
import coil.request.LoadRequest
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.homepro.megahome.R
import com.homepro.megahome.api.AppService
import com.homepro.megahome.api.ServiceListener
import com.homepro.megahome.databinding.ProjectFragmentBinding
import com.homepro.megahome.manager.DateManager
import com.homepro.megahome.manager.ExternalFileManager
import com.homepro.megahome.realm.ProjectShare
import com.homepro.megahome.singleton.AppData
import com.homepro.megahome.ui.scanner.ScannerFragment
import retrofit2.Call
import retrofit2.Response

class ProjectFragment : Fragment() {
    private lateinit var viewModel: ProjectViewModel
    private lateinit var binding: ProjectFragmentBinding

    private val navController by lazy {
        findNavController()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener(SCAN_GET_PROJECT_REQUEST_KEY) { _, bundle ->
            val sanitizer = UrlQuerySanitizer(bundle.getString(ScannerFragment.SCANNER_RESULT_KEY))
            val qrCodeQuery = sanitizer.getValue("qrcode")
            if (qrCodeQuery == null) {
                MaterialAlertDialogBuilder(requireContext())
                    .setTitle(getString(R.string.error))
                    .setMessage(getString(R.string.not_valid_qrcode))
                    .setPositiveButton(getString(R.string.ok), null)
                    .show()

                return@setFragmentResultListener
            }

            if (qrCodeQuery.isEmpty() || qrCodeQuery.isBlank()) {
                return@setFragmentResultListener
            }

            AppService().getProject(object : ServiceListener<ProjectShare> {
                override fun onFailure(call: Call<ProjectShare>, t: Throwable) {
                    MaterialAlertDialogBuilder(requireContext())
                        .setTitle(getString(R.string.error))
                        .setMessage(getString(R.string.not_valid_qrcode))
                        .setPositiveButton(getString(R.string.ok), null)
                        .show()
                }

                override fun onResponse(
                    call: Call<ProjectShare>,
                    response: Response<ProjectShare>
                ) {
                    AppData.project = response.body()?.project ?: return
                    AppData.project?.let { project ->
                        project.id = qrCodeQuery
                        val rawImageURL = project.raw_image ?: return@onResponse

                        val imageLoader = Coil.imageLoader(requireContext())
                        val request = LoadRequest.Builder(requireContext())
                            .data(rawImageURL)
                            .allowHardware(false)
                            .target(
                                onStart = { drawable ->
                                    val bitmap = (drawable as BitmapDrawable).bitmap
                                    ExternalFileManager.saveBitmapToFile(
                                        requireContext(),
                                        bitmap,
                                        "${System.currentTimeMillis()}HP.jpg"
                                    )
                                }
                            ).build()
                        imageLoader.execute(request)
                        viewModel.onScannerResult(project)
                    }
                }
            }, qrCodeQuery)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =  DataBindingUtil.inflate(inflater, R.layout.project_fragment, container, false)
        viewModel = ViewModelProvider(this).get(ProjectViewModel::class.java)

        val projects = AppService().retrieveProject()
        val adapter = ProjectAdapter(ProjectListener { project ->
            viewModel.onProjectClicked(project)
        })

        viewModel.navigateToProjectDetail.observe(viewLifecycleOwner, Observer { project ->
            project?.let {
                navController.navigate(
                    ProjectFragmentDirections.actionProjectFragmentToProjectDetailFragment(
                        type = "project",
                        project = it
                    )
                )
                viewModel.onProjectDetailNavigate()
            }
        })

        binding.projectRecyclerView.adapter = adapter
        adapter.submitList(projects)

        if (projects.count() > 0) {
            binding.greetingConstraintLayout.visibility = View.GONE
            binding.projectRecyclerView.visibility = View.VISIBLE
        }

        binding.qrScanImageView.setOnClickListener {
            viewModel.onQRScanClicked()
        }

        viewModel.navigateToScanner.observe(viewLifecycleOwner, Observer {
            if (it) {
                navController.navigate(ProjectFragmentDirections.actionProjectFragmentToScannerFragment(requestKey = SCAN_GET_PROJECT_REQUEST_KEY))
                viewModel.onScannerNavigate()
            }
        })

        viewModel.navigateToProjectDetailFromScanner.observe(viewLifecycleOwner, Observer { shareProject ->
            shareProject?.id?.let { shareProjectId ->
                val myProject = AppService().retriveProject(shareProjectId) ?: AppService().saveProjectTypeShare(
                    id = shareProjectId,
                    name = shareProject.title.takeIf { it != null } ?: "",
                    tileId = "",
                    fitTileImage = shareProject.fittile_image ?: "",
                    rawImage = shareProject.raw_image ?: "",
                    date = DateManager.getInstance().currentDate(),
                    transactionId = shareProjectId,
                    qrUrl = "",
                    stage = "fittile"
                )
                findNavController().navigate(
                    ProjectFragmentDirections.actionProjectFragmentToProjectDetailFragment(
                        type = "share",
                        project = myProject
                    )
                )

                viewModel.onProjectDetailFromScannerNavigate()
            }
        })

        viewModel.navigateToFitTile.observe(viewLifecycleOwner, Observer {
            if (it) {
                navController.navigate(ProjectFragmentDirections.actionProjectFragmentToCameraFragment())
                viewModel.onFitTileNavigate()
            }
        })

        binding.imvCamera.setOnClickListener {
            viewModel.onFitTileClicked()
        }

        return binding.root
    }

    companion object {
        const val SCAN_GET_PROJECT_REQUEST_KEY = "requestProject"
    }
}