package com.homepro.megahome.ui.fittile

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.homepro.megahome.api.FitTileApiService
import com.homepro.megahome.data.TileRepository
import com.homepro.megahome.database.TileDatabaseDao

class FitTileViewModelFactory(
    private val service: FitTileApiService,
    private val dataSource: TileDatabaseDao,
    private val repository: TileRepository,
    private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(FitTileViewModel::class.java)) {
            return FitTileViewModel(service, dataSource, repository, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}