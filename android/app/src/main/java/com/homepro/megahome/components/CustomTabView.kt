package com.homepro.megahome.components

import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.annotation.RequiresApi
import com.homepro.megahome.R

interface OnTabListener {
    fun onTabLeft()
    fun onTabRight()
}


class CustomTabView : FrameLayout {
    private var layoutTabLeft: RelativeLayout? = null
    private var layoutTabRight: RelativeLayout? = null
    private var tvLeft: TextView? = null
    private var tvRight: TextView? = null
    private var onTabListener: OnTabListener? = null

    constructor(context: Context) : super(context) {
        initInflate(context)
        initInstance()
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?) : super(context, attrs) {
        initInflate(context)
        initWithAttrs(attrs, 0, 0)
        initInstance()
    }

    constructor(
        context: Context,
        @Nullable attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        initInflate(context)
        initWithAttrs(attrs, defStyleAttr, 0)
        initInstance()
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context,
        @Nullable attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        initInflate(context)
        initWithAttrs(attrs, defStyleAttr, defStyleRes)
        initInstance()
    }

    fun setOnTabListener(onTabListener: OnTabListener?) {
        this.onTabListener = onTabListener
    }

    private fun initInflate(context: Context) {
        View.inflate(context, R.layout.custom_tab, this)
        layoutTabLeft = findViewById(R.id.layoutTabLeft)
        layoutTabRight = findViewById(R.id.layoutTabRight)
        tvLeft = findViewById(R.id.tvLeft)
        tvRight = findViewById(R.id.tvRight)
    }

    fun setTextLeft(text: String?) {
        tvLeft!!.text = text
    }

    fun setTextRight(text: String?) {
        tvRight!!.text = text
    }

    private fun selectTab(tab: Int) {
        layoutTabLeft!!.setBackgroundResource(0)
        layoutTabRight!!.setBackgroundResource(0)
        tvLeft!!.setTextColor(resources.getColor(R.color.colorNonSelectBottomNavigation))
        tvRight!!.setTextColor(resources.getColor(R.color.colorNonSelectBottomNavigation))
        if (tab == 0) {
            layoutTabLeft!!.setBackgroundResource(R.drawable.shape_positive_btn)
            tvLeft!!.setTextColor(resources.getColor(R.color.colorWhite))
        } else if (tab == 1) {
            layoutTabRight!!.setBackgroundResource(R.drawable.shape_positive_btn)
            tvRight!!.setTextColor(resources.getColor(R.color.colorWhite))
        }
    }

    private fun initInstance() {
        layoutTabLeft!!.setOnClickListener {
            selectTab(0)
            onTabListener?.onTabLeft()
        }
        layoutTabRight!!.setOnClickListener {
            selectTab(1)
            onTabListener?.onTabRight()
        }
    }

    private fun initWithAttrs(
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) {
    }
}