package com.homepro.megahome.model

object TileEvent {
    const val PIN_AREA = 1
    const val SELECT_TILE = 2
    const val SCALE_TILE = 3
    const val TILT_TILE = 4
    const val ACTION_TILE = 5
    const val TILT_LEFT_WALL = 6
    const val TILT_RIGHT_WALL = 7
    const val TILT_FLOOR = 8
}