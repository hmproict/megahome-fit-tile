package com.homepro.megahome.ui.camera

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.Surface.ROTATION_0
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.camera.core.*
import androidx.camera.core.ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import coil.Coil
import coil.request.LoadRequest
import com.google.ar.core.ArCoreApk
import com.google.common.util.concurrent.ListenableFuture
import com.homepro.megahome.databinding.CameraFragmentBinding
import com.homepro.megahome.ui.fittile.FitTileFragment
import com.kbeanie.multipicker.api.ImagePicker
import java.io.File

private const val REQUEST_IMAGE_GET = 11

private const val REQUEST_CODE_PERMISSIONS = 10
private val REQUIRED_PERMISSIONS = arrayOf(
    Manifest.permission.CAMERA,
    Manifest.permission.WRITE_EXTERNAL_STORAGE,
    Manifest.permission.READ_EXTERNAL_STORAGE
)

class CameraFragment : Fragment() {
    private val TAG = "CameraFragment"
    private val args by navArgs<CameraFragmentArgs>()
    private lateinit var cameraProviderFuture: ListenableFuture<ProcessCameraProvider>
    private lateinit var imagePicker: ImagePicker

    private val navController by lazy {
        findNavController()
    }

    private val binding: CameraFragmentBinding by lazy {
        CameraFragmentBinding.inflate(layoutInflater)
    }

    private val executor by lazy {
        ContextCompat.getMainExecutor(requireContext())
    }

    private var container: ViewGroup? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        this.container = container
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (allPermissionGranted()) {
            cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())
            binding.previewView.post { startCamera() }
        } else {
            requestPermissions(REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS)
        }

        binding.btnSelect.setOnClickListener {
            sendIntentOpenDocument()
        }

        maybeEnableARButton()
    }

    private fun maybeEnableARButton() {
        val availability = ArCoreApk.getInstance().checkAvailability(requireContext())
        if (availability.isTransient) {
            Handler().postDelayed(java.lang.Runnable { maybeEnableARButton() }, 200)
        }

        if (availability.isSupported) {
            binding.btnAr.visibility = View.VISIBLE
            binding.btnAr.isEnabled = true
            binding.btnAr.setOnClickListener {
                startARMode()
            }
        } else {
            binding.btnAr.visibility = View.INVISIBLE
            binding.btnAr.isEnabled = false
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionGranted()) {
                cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())
                binding.previewView.post { startCamera() }
            } else {
                Toast.makeText(requireContext(), "ไม่สามารถใช้ฟีเจอร์นี้ได้", Toast.LENGTH_LONG)
                    .show()
                navController.navigateUp()
            }
        }
    }

    private fun allPermissionGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(requireContext(), it) == PackageManager.PERMISSION_GRANTED
    }

    private fun startCamera() {
        val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA
        val preview = Preview.Builder().apply {
            setTargetRotation(ROTATION_0)
            setTargetAspectRatio(AspectRatio.RATIO_4_3)
        }.build()


        val imageCapture = ImageCapture.Builder().apply {
            setCaptureMode(CAPTURE_MODE_MINIMIZE_LATENCY)
            setTargetRotation(ROTATION_0)
            setTargetAspectRatio(AspectRatio.RATIO_4_3)
        }.build()

        binding.cameraCaptureButton.setOnClickListener {
            it.isEnabled = false
            it.postDelayed({ it.isEnabled = true }, 500)

            val file = File(
                requireActivity().externalMediaDirs.first(),
                "${System.currentTimeMillis()}.jpg"
            )

            imageCapture.takePicture(
                ImageCapture.OutputFileOptions.Builder(file).build(),
                executor,
                object : ImageCapture.OnImageSavedCallback {
                    override fun onImageSaved(outputFileResults: ImageCapture.OutputFileResults) {
                        val imageLoader = Coil.imageLoader(requireContext())
                        val request = LoadRequest.Builder(requireContext())
                            .data(file)
                            .allowHardware(false)
                            .target(
                                onSuccess = { drawable ->
                                    val bitmap = (drawable as BitmapDrawable).bitmap

                                    if (args.tileIdFromCatalog == null) {
                                        FitTileFragment.captureBitmap = bitmap
                                        navController.navigate(
                                            CameraFragmentDirections.actionCameraFragmentToFitTileFragment(
                                                tileIds = "",
                                                captureBitmapFilePath = file.absolutePath,
                                                roomImagePath = file.absolutePath
                                            )
                                        )
                                    } else {
                                        FitTileFragment.captureBitmap = bitmap
                                        navController.navigate(
                                            CameraFragmentDirections.actionCameraFragmentToFitTileFragment(
                                                tileIds = "",
                                                captureBitmapFilePath = file.absolutePath,
                                                tileIdFromCatalog = args.tileIdFromCatalog,
                                                roomImagePath = file.absolutePath
                                            )
                                        )
                                    }
                                }
                            ).build()
                        imageLoader.execute(request)
                    }

                    override fun onError(exception: ImageCaptureException) {
                        Toast.makeText(
                            requireContext(),
                            "There is error when capture image. Application will be closed. with error: ${exception.message}",
                            Toast.LENGTH_LONG
                        )
                            .show()
                        navController.navigateUp()
                    }
                })
        }

        cameraProviderFuture.addListener(Runnable {
            val cameraProvider = cameraProviderFuture.get()
            val camera = cameraProvider.bindToLifecycle(viewLifecycleOwner, cameraSelector, preview, imageCapture)
            binding.previewView.preferredImplementationMode = PreviewView.ImplementationMode.TEXTURE_VIEW
            preview.setSurfaceProvider(binding.previewView.createSurfaceProvider())
        }, executor)
    }

    private fun sendIntentOpenDocument() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
            type = "image/*"
            addCategory(Intent.CATEGORY_OPENABLE)
        }

        startActivityForResult(intent, REQUEST_IMAGE_GET)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val contentResolver = requireActivity().contentResolver

        if (requestCode == REQUEST_IMAGE_GET && resultCode == Activity.RESULT_OK) {
            data?.data?.also { uri ->
                try {
                    contentResolver.takePersistableUriPermission(uri, Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    contentResolver.takePersistableUriPermission(uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                } catch (e: SecurityException) {
                    Log.e(TAG, "onActivityResult: cannot take permission")
                    return
                }

                val imageLoader = Coil.imageLoader(requireContext())
                val request = LoadRequest.Builder(requireContext())
                    .data(uri)
                    .allowHardware(false)
                    .target(
                        onSuccess = {
                            val bitmap = (it as BitmapDrawable).bitmap
                            FitTileFragment.captureBitmap = bitmap
                            navController.navigate(
                                CameraFragmentDirections.actionCameraFragmentToFitTileFragment(
                                    tileIds = "",
                                    captureBitmapFilePath = uri.toString(),
                                    roomImagePath = uri.toString()
                                )
                            )
                        }
                    ).build()
                imageLoader.execute(request)
            }
        }
    }

    private fun startARMode() {
        findNavController().navigate(CameraFragmentDirections.actionCameraFragmentToARFragment())
    }
}