package com.homepro.megahome.data

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import com.homepro.megahome.api.FitTileApiService
import com.homepro.megahome.database.TileLocalCache
import com.homepro.megahome.model.Tile
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class TileBoundaryCallback(
    private val service: FitTileApiService,
    private val cache: TileLocalCache
) : PagedList.BoundaryCallback<Tile>() {
    private var lastedRequestTile = 21
    private var isRequestInProgress = false
    private var totalTiles: Int = 0

    private val _networkErrors = MutableLiveData<String>()
    val networkErrors: LiveData<String>
        get() = _networkErrors

    override fun onZeroItemsLoaded() {
        requestAndSaveData()
    }

    override fun onItemAtEndLoaded(itemAtEnd: Tile) {
        requestAndSaveData()
    }

    private fun requestAndSaveData() {
        if (totalTiles != 0 && lastedRequestTile == totalTiles) return
        if (isRequestInProgress) return

        isRequestInProgress = true

        val handler = CoroutineExceptionHandler { _, exception ->
            Log.d("Flutter to App", "service error : $exception")
            _networkErrors.postValue(exception.message)
            isRequestInProgress = false
        }

        GlobalScope.launch(Dispatchers.IO + handler) {
            val size = if (totalTiles == 0 || (lastedRequestTile + NETWORK_PAGE_SIZE) <= totalTiles)
                NETWORK_PAGE_SIZE else totalTiles - lastedRequestTile
            val tilesResponse = service.tilesWithSize(
                from = lastedRequestTile,
                size = size,
                type = null
            )

            cache.insert(tilesResponse.tiles) {
                totalTiles = tilesResponse.total
                lastedRequestTile += tilesResponse.tiles.count()
                isRequestInProgress = false
            }
        }
    }

    companion object {
        private const val NETWORK_PAGE_SIZE = 51
    }
}