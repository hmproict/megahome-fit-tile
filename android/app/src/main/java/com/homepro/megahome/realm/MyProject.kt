package com.homepro.megahome.realm

import android.os.Parcel
import android.os.Parcelable
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.Required
import java.util.*

open class MyProject protected constructor(`in`: Parcel?) : Parcelable, RealmObject() {

    @PrimaryKey
    @Required
    var id: String? = null

    @Required
    var productName: String? = null //title

    @Required
    var imagePath: String? = null

    @Required
    var dateCreate: Date? = null // date

    @Required
    var tileId: String? = null // tile_id

    @Required
    var type: String = "project" // share

    @Required
    var stage: String = "initial" // fittile

    @Required
    var rawImage: String? = null

    @Required
    var transactionId: String? = null

    @Required
    var qrUrl: String? = null

    constructor() : this(null)


    init {
        if (`in` != null) {
            id = `in`.readString()
            productName = `in`.readString()
            imagePath = `in`.readString()
            tileId = `in`.readString()
            type = `in`.readString()!!
            stage = `in`.readString()!!
            rawImage = `in`.readString()
            transactionId = `in`.readString()
            qrUrl = `in`.readString()
        }

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(productName)
        parcel.writeString(imagePath)
        parcel.writeString(tileId)
        parcel.writeString(type)
        parcel.writeString(stage)
        parcel.writeString(rawImage)
        parcel.writeString(transactionId)
        parcel.writeString(qrUrl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MyProject> {
        override fun createFromParcel(parcel: Parcel): MyProject {
            return MyProject(parcel)
        }

        override fun newArray(size: Int): Array<MyProject?> {
            return arrayOfNulls(size)
        }
    }

}