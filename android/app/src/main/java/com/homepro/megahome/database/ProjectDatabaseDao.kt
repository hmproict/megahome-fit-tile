package com.homepro.megahome.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.homepro.megahome.model.Project

@Dao
interface ProjectDatabaseDao {
    @Insert
    fun insert(project: Project)

    @Update
    fun update(project: Project)

    @Query("SELECT * FROM project_table WHERE projectId = :key")
    fun get(key: String): Project?

    @Query("SELECT * FROM project_table ORDER BY projectId DESC LIMIT 1")
    fun getLastedProject(): Project?

    @Query("SELECT * FROM project_table ORDER BY projectId DESC")
    fun getAllProjects(): LiveData<List<Project>>

    @Query("DELETE FROM project_table WHERE projectId = :key")
    fun delete(key: String)
}