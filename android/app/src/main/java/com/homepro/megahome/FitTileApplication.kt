package com.homepro.megahome

import android.app.Application
import android.content.Context
import androidx.camera.camera2.Camera2Config
import androidx.camera.core.CameraXConfig
import io.realm.Realm
import io.realm.RealmConfiguration

class FitTileApplication : Application(), CameraXConfig.Provider {

    lateinit var context: Context

    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
        context = this
        val mRealmConfiguration = RealmConfiguration.Builder()
            .name("$packageName.realm")
            .schemaVersion(1)
            .deleteRealmIfMigrationNeeded()
            .build()

        Realm.getInstance(mRealmConfiguration)
        Realm.setDefaultConfiguration(mRealmConfiguration)
    }

    override fun getCameraXConfig(): CameraXConfig {
        return Camera2Config.defaultConfig()
    }

    private fun getRealmDatabaseConfiguration(): RealmConfiguration {
        return RealmConfiguration.Builder().name("$packageName.realm").deleteRealmIfMigrationNeeded().build()
    }
}