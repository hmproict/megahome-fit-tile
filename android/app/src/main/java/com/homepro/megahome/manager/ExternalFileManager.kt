package com.homepro.megahome.manager

import android.content.Context
import android.graphics.Bitmap
import android.media.MediaScannerConnection
import android.util.Log
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class ExternalFileManager {
    companion object {
        fun saveBitmapToFile(context: Context, bitmap: Bitmap, fileName: String): File {
            if (hasExternalStoragePrivatePicture(context, fileName)) {
                return File(context.externalMediaDirs.first(), fileName)
            }

            return createExternalStoragePrivatePicture(context, bitmap, fileName)
        }

        private fun createExternalStoragePrivatePicture(
            context: Context,
            bitmap: Bitmap,
            fileName: String
        ): File {
            val file = File(context.externalMediaDirs.first(), fileName)

            try {
                val outputStream = FileOutputStream(file)
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
                outputStream.flush()
                outputStream.close()
            } catch (e: IOException) {
                Log.w("ExternalStorage", "Error writing $file", e);
            }

            // Tell the media scanner about the new file so that it is
            // immediately available to the user.
            MediaScannerConnection.scanFile(
                context,
                arrayOf(file.toString()),
                null,
                null
            )

            return file
        }

        private fun hasExternalStoragePrivatePicture(context: Context, fileName: String): Boolean {
            val file = File(context.externalMediaDirs.first(), fileName)
            return file.exists()
        }
    }
}