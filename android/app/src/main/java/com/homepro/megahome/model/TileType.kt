package com.homepro.megahome.model

enum class TileType {
    ALL, FLOOR, WALL
}