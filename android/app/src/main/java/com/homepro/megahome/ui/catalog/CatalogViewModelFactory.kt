package com.homepro.megahome.ui.catalog

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.homepro.megahome.api.FitTileApiService
import com.homepro.megahome.data.TileRepository
import com.homepro.megahome.database.TileDatabaseDao

class CatalogViewModelFactory(
    private val service: FitTileApiService,
    private val tilesDao: TileDatabaseDao,
    private val repository: TileRepository,
    private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CatalogViewModel::class.java)) {
            return CatalogViewModel(service, tilesDao, repository, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}