package com.homepro.megahome.ui.ar

import android.app.Activity
import android.app.ActivityManager
import android.app.Dialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.net.UrlQuerySanitizer
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.ar.core.Config
import com.google.ar.sceneform.rendering.PlaneRenderer
import com.google.ar.sceneform.rendering.Texture
import com.google.ar.sceneform.ux.ArFragment
import com.homepro.megahome.R
import com.homepro.megahome.api.AppService
import com.homepro.megahome.api.FitTileApi
import com.homepro.megahome.data.TileRepository
import com.homepro.megahome.database.FitTileDatabase
import com.homepro.megahome.database.TileLocalCache
import com.homepro.megahome.databinding.ARFragmentBinding
import com.homepro.megahome.manager.DateManager
import com.homepro.megahome.manager.ExternalFileManager
import com.homepro.megahome.model.Size
import com.homepro.megahome.model.Tile
import com.homepro.megahome.model.TileType
import com.homepro.megahome.model.TilesFilter
import com.homepro.megahome.ui.catalog.TileAdapter
import com.homepro.megahome.ui.catalog.TileListener
import com.homepro.megahome.ui.fittile.FitTileFragment
import com.homepro.megahome.ui.scanner.ScannerFragment
import kotlinx.coroutines.launch
import java.io.IOException
import java.util.concurrent.CompletableFuture

class ARFragment : Fragment() {
    private val TAG: String = "ARFragment"
    private val MIN_OPENGL_VERSION = 3.0

    private val navController by lazy {
        findNavController()
    }

    private lateinit var binding: ARFragmentBinding
    private lateinit var viewModel: ARViewModel

    private var tilesCacheMap: Map<String, Tile> = mapOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (!checkDeviceCompatible(requireActivity())) {
            navController.navigateUp()
        }

        val dataSource = FitTileDatabase.getInstance(requireContext()).tileDatabaseDao
        val repository = TileRepository(FitTileApi.service(requireContext()), TileLocalCache(dataSource))
        val viewModelFactory = ARViewModelFactory(repository)
        viewModel = ViewModelProvider(this, viewModelFactory).get(ARViewModel::class.java)

        setFragmentResultListener(SCAN_GET_TILE_REQUEST_KEY) { _, bundle ->
            val sanitizer = UrlQuerySanitizer(bundle.getString(ScannerFragment.SCANNER_RESULT_KEY))
            val tileId = sanitizer.getValue("id_scan")

            if (tileId == null) {
                MaterialAlertDialogBuilder(requireContext())
                    .setTitle(getString(R.string.error))
                    .setMessage(getString(R.string.not_valid_qrcode))
                    .setPositiveButton(getString(R.string.ok), null)
                    .show()

                return@setFragmentResultListener
            }

            lifecycleScope.launch {
                viewModel.getTileById(tileId).observe(viewLifecycleOwner, Observer { tile ->
                    tile?.let {
                        viewModel.selectTile(requireContext(), it)
                        return@Observer
                    }

                    Toast.makeText(
                        requireContext(),
                        getString(R.string.cannot_find_tile_from_scan),
                        Toast.LENGTH_SHORT).show()
                })
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.a_r_fragment, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initARCore(childFragmentManager.findFragmentById(R.id.ar_ux_fragment) as ArFragment)
        initUIEvent()
        initObserveData(viewModel)
        initTilesBottomSheet()
    }

    private fun checkDeviceCompatible(activity: Activity): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            Toast.makeText(activity, "Sceneform requires Android N or later", Toast.LENGTH_LONG)
                .show()
            return false
        }

        val openGlVersionString =
            (activity.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager)
                .deviceConfigurationInfo
                .glEsVersion

        if (openGlVersionString.toDouble() < MIN_OPENGL_VERSION) {
            Log.e(TAG, "Sceneform requires OpenGL ES 3.0 later")
            Toast.makeText(activity, "Sceneform requires OpenGL ES 3.0 or later", Toast.LENGTH_LONG)
                .show()
            activity.finish()
            return false
        }
        return true
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun initARCore(arFragment: ArFragment) {
        arFragment.arSceneView.session?.apply {
            val changeConfig = Config(this)
            changeConfig.updateMode = Config.UpdateMode.LATEST_CAMERA_IMAGE
            changeConfig.planeFindingMode = Config.PlaneFindingMode.HORIZONTAL
            configure(changeConfig)
        }

        arFragment.arSceneView.scene.addOnUpdateListener { frameTime ->
            arFragment.onUpdate(frameTime)
            arFragment.arSceneView.planeRenderer.material.thenAccept { material ->
                material.setFloat(PlaneRenderer.MATERIAL_SPOTLIGHT_RADIUS, 100f)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun initUIEvent() {
        binding.btnSave.setOnClickListener { saveProject() }
        binding.btnCancel.setOnClickListener { findNavController().navigate(ARFragmentDirections.actionARFragmentToProjectFragment()) }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun initObserveData(viewModel: ARViewModel) {
        val arFragment = childFragmentManager.findFragmentById(R.id.ar_ux_fragment) as ArFragment

        viewModel.selectedTileBitmap.observe(viewLifecycleOwner, Observer { tileBitmap ->
            val tileBitmapWithGrout = addGrout(tileBitmap)
            viewModel.triGrid = createNewTriGrid(tileBitmapWithGrout)
            arFragment.arSceneView
                .planeRenderer
                .material
                .thenAcceptBoth(viewModel.triGrid) { material, texture ->
                    material.setTexture(PlaneRenderer.MATERIAL_TEXTURE, texture)
                    material.setFloat2(PlaneRenderer.MATERIAL_UV_SCALE, 8F, 8F)
                    material.setFloat(PlaneRenderer.MATERIAL_SPOTLIGHT_RADIUS, 100f);
                }
        })
    }

    private fun initTilesBottomSheet() {
        val sheetBehavior = BottomSheetBehavior.from(binding.includeBottomSheet.bottomSheet)
        sheetBehavior.isHideable = false
        sheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED

        initRecyclerView()
        initQRCodeScanner()
        initFilterButton()
    }

    private fun initRecyclerView() {
        val gridLayoutManager = GridLayoutManager(requireActivity(), 3)
        binding.includeBottomSheet.tileList.layoutManager = gridLayoutManager

        val recyclerViewAdapter = TileAdapter(TileListener { tileId ->
            tilesCacheMap[tileId]?.let {
                viewModel.selectTile(requireContext(), it)
                return@TileListener
            }

            viewModel.getTileById(tileId).observe(viewLifecycleOwner, Observer { tile ->
                tile?.let {
                    viewModel.selectTile(requireContext(), tile)
                }
            })
        })

        binding.includeBottomSheet.tileList.adapter = recyclerViewAdapter
        viewModel.networksError.observe(viewLifecycleOwner, Observer {
            Log.d("Flutter to App", "service error : $it")
            Toast.makeText(requireContext(), "\uD83D\uDE28 Wooops $it", Toast.LENGTH_LONG).show()
        })

        viewModel.tiles.observe(viewLifecycleOwner, Observer {
            tilesCacheMap = tilesCacheMap + it.filterNotNull().associateBy({ tile -> tile.id }, { tile -> tile })
            recyclerViewAdapter.submitList(it)

            if (viewModel.selectedTile.value == null) {
                viewModel.selectTile(requireContext(), tilesCacheMap.values.first())
            }
        })
    }

    private fun initQRCodeScanner() {
        binding.includeBottomSheet.qrScanTileImageView.setOnClickListener { viewModel.onQRScannerClicked() }
        viewModel.navigateToScanner.observe(viewLifecycleOwner, Observer {
            if (it) {
                navController.navigate(ARFragmentDirections.actionARFragmentToScannerFragment(requestKey = FitTileFragment.SCAN_GET_TILE_REQUEST_KEY))
                viewModel.onScannerNavigate()
            }
        })
    }

    private fun initFilterButton() {
        binding.includeBottomSheet.tvFilter.setOnClickListener {
            val tiles = tilesCacheMap.values.toList()
            val tileSizes = tiles.filterNotNull().associateBy(
                { tile -> "${tile.width}x${tile.length}" },
                { tile -> Size(tile.width.toDouble(), tile.length.toDouble()) }
            )

            tileSizes.values.toList().also {
                val mutableShowTiles = it.toMutableList()
                mutableShowTiles.sortBy { tile -> tile.show }

                val showTilesString = mutableShowTiles.map { tile -> "${tile.show}cm." }.toMutableList()

                showTilesString.add(0, "All")

                val showTilesStringArray = showTilesString.toTypedArray()

                MaterialAlertDialogBuilder(requireContext())
                    .setTitle(getString(R.string.choose_filter))
                    .setItems(showTilesStringArray) { _, which ->
                        if (which == 0) {
                            viewModel.changeTilesFilter(TilesFilter(TileType.ALL, null))
                        } else {
                            viewModel.changeTilesFilter(TilesFilter(
                                tileType = TileType.ALL,
                                size = Size(it[which].width, it[which].length)
                            ))
                        }
                    }
                    .show()
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private fun createNewTriGrid(bitmap: Bitmap): CompletableFuture<Texture> {
        val sampler = Texture.Sampler.builder()
            .setMinFilter(Texture.Sampler.MinFilter.LINEAR)
            .setMagFilter(Texture.Sampler.MagFilter.LINEAR)
            .setWrapMode(Texture.Sampler.WrapMode.REPEAT)
            .build()

        return Texture.builder()
            .setSource(bitmap)
            .setSampler(sampler).build()
    }

    private fun addGrout(bitmap: Bitmap): Bitmap {
        val bitmapWithGrout = Bitmap.createBitmap(bitmap.width + 8, bitmap.height + 8, bitmap.config)
        val canvas = Canvas(bitmapWithGrout)
        canvas.drawColor(Color.WHITE)
        canvas.drawBitmap(bitmap, 8F, 8F, null)
        return bitmapWithGrout
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun saveProject() {
        val arFragment = childFragmentManager.findFragmentById(R.id.ar_ux_fragment) as ArFragment
        openSaveDialog(arFragment)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun openSaveDialog(arFragment: ArFragment) {
        val originalMode = activity?.window?.attributes?.softInputMode
        activity?.window?.setSoftInputMode(
            WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING
        )

        val dialog = Dialog(requireContext())
        dialog.setTitle("Save")
        dialog.setContentView(R.layout.save_dialog)

        val saveEditText = dialog.findViewById<EditText>(R.id.edtName)
        val btnSave = dialog.findViewById<Button>(R.id.btnSave)

        btnSave.setOnClickListener { _ ->
            val imm = saveEditText.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(saveEditText.windowToken, 0)

            dialog.dismiss()

            val projectName = saveEditText.text.toString()
            takePhoto(projectName, ::saveToProject)

            originalMode?.let {
                activity?.window?.setSoftInputMode(
                    it
                )
            }

            navController.navigate(ARFragmentDirections.actionARFragmentToProjectFragment())
        }

        dialog.show()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun takePhoto(
        projectName: String,
        takePhotoFinished: (String, Bitmap) -> Unit
    ) {
        val arFragment = childFragmentManager.findFragmentById(R.id.ar_ux_fragment) as ArFragment
        val view = arFragment.arSceneView

        val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)

        val handlerThread = HandlerThread("PixelCopier")
        handlerThread.start()

        PixelCopy.request(view, bitmap, { copyResult ->
            when (copyResult) {
                PixelCopy.SUCCESS -> {
                    try {
                        takePhotoFinished(projectName, bitmap)
                    } catch (e: IOException) {
                        val toast = Toast.makeText(requireContext(), e.toString(), Toast.LENGTH_LONG)
                        toast.show()
                        return@request
                    }
                }
                else -> {
                    val toast = Toast.makeText(requireContext(), "Failed to take a photo: $copyResult", Toast.LENGTH_LONG)
                    toast.show()
                }
            }
            handlerThread.quitSafely()
        }, Handler(handlerThread.looper))
    }

    private fun saveToProject(projectName: String, captureBitmap: Bitmap) {
        val fileName = "${System.currentTimeMillis()}_HomePro_FitTile.jpg"
        val file = ExternalFileManager.saveBitmapToFile(requireContext(), captureBitmap, fileName)

        val id = viewModel.selectedTile.value?.id
        id?.let { id ->
            AppService().saveProject(
                id = id + "_" + DateManager.getInstance().getDateWithFormat(DateManager.getInstance().currentDate()),
                name = projectName,
                tileId = id,
                imagePath = file.absolutePath,
                date = DateManager.getInstance().currentDate())
        }
    }

    companion object {
        private val SCAN_GET_TILE_REQUEST_KEY = "requestTile"
    }
}