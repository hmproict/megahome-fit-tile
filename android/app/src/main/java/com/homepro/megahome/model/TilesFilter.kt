package com.homepro.megahome.model

data class TilesFilter(
    val tileType: TileType,
    val size: Size? = null
)