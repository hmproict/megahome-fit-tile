package com.homepro.megahome.model

import androidx.room.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

@Entity(tableName = "project_table")
@TypeConverters(TimestampConverter::class)
data class Project(
    @PrimaryKey
    var projectId: String,

    @ColumnInfo(name = "name")
    val name: String,

    @ColumnInfo(name = "image_path")
    val imagePath: String,

    @ColumnInfo(name = "tile_ids")
    val tileIds: String,

    @ColumnInfo(name = "type")
    var type: String = "project",

    @ColumnInfo(name = "stage")
    var stage: String = "initial",

    @ColumnInfo(name = "raw_image")
    var rawImage: String? = null,

    @ColumnInfo(name = "transaction_id")
    var transactionId: String? = null,

    @ColumnInfo(name = "qr_url")
    var qrURL: String? = null,

    @ColumnInfo(name = "created_date")
    val createdDate: Date = Date()
)

class TimestampConverter {
    private val formatter: DateFormat by lazy {
        SimpleDateFormat.getDateInstance()
    }

    private val timeZone: TimeZone by lazy {
        TimeZone.getTimeZone("Asia/Bangkok")
    }

    @TypeConverter
    fun fromTimestamp(timestamp: String): Date? {
        formatter.timeZone = timeZone
        return formatter.parse(timestamp)
    }

    @TypeConverter
    fun dateToTimestamp(date: Date): String {
        formatter.timeZone = timeZone
        return formatter.format(date)
    }
}