package com.homepro.megahome.database

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.*
import com.homepro.megahome.model.TileSize

@Dao
interface TileSizeDatabaseDao {
    @Insert
    fun insert(size: TileSize)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(sizes: List<TileSize>)

    @Update
    fun update(size: TileSize)

    @Query("SELECT * FROM tile_size_table ORDER BY id ASC")
    fun getSizes(): LiveData<List<TileSize>>

    @Query("SELECT * FROM tile_size_table ORDER BY id ASC")
    fun sizesFactory(): LiveData<List<TileSize>>

    @Query("SELECT * FROM tile_size_table WHERE type = :type ORDER BY id ASC")
    fun sizesByType(type: String): LiveData<List<TileSize>>
}