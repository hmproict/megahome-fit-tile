package com.homepro.megahome.api

import com.homepro.megahome.realm.Tile
import com.homepro.megahome.realm.TileAll
import retrofit2.http.GET
import retrofit2.http.Query

interface HomeProService {
    @GET("get.jsp")
    suspend fun tiles(
        @Query("id") id: String?,
        @Query("flag") flag: String?,
        @Query("from") from: String,
        @Query("size") size: String
    ): TileAll

    @GET("pre-install.jsp")
    suspend fun preTile(): List<Tile>
}