package com.homepro.megahome.api

import com.google.gson.GsonBuilder
import com.homepro.megahome.constant.AppConfig
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.SecureRandom
import java.util.*
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.X509TrustManager

class DgtHttpManager private constructor() {
    companion object {
        @Volatile
        private var INSTANCE: DgtHttpManager? = null

        fun getInstance(): DgtHttpManager {
            return INSTANCE ?: synchronized(this) {
                DgtHttpManager().also {
                    INSTANCE = it
                }
            }
        }
    }

    private val doNotVerifyHostname: HostnameVerifier
    private val client: OkHttpClient
    private val retrofit: Retrofit
    val apiService: DgtApiService

    init {
        val gson = GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .create()

        val trustManager = trustManagerForAllCertificates()
        val sslContext = SSLContext.getInstance("TLS")
        sslContext.init(null, arrayOf(trustManager), SecureRandom())
        val sslSocketFactory = sslContext.socketFactory

        doNotVerifyHostname = HostnameVerifier { _, _ -> true }

        client = OkHttpClient.Builder()
            .sslSocketFactory(sslSocketFactory, trustManager)
            .hostnameVerifier(doNotVerifyHostname)
            .addInterceptor { chain ->
                val request = chain.request().newBuilder()
                    .addHeader("Content-Type", "application/x-www-form")
                    .addHeader("Authorization", "Basic " + AppConfig.AUTH)
                    .addHeader("Accept-Language", supportLanguage())
                    .build()
                chain.proceed(request)
            }
            .build()

        retrofit = Retrofit.Builder()
            .baseUrl(AppConfig.FIT_TILE_SERVICE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client)
            .build()

        apiService = retrofit.create(DgtApiService::class.java)
    }

    private fun trustManagerForAllCertificates(): X509TrustManager {
        return object : X509TrustManager {
            override fun checkClientTrusted(
                chain: Array<java.security.cert.X509Certificate>,
                authType: String
            ) {
            }

            override fun checkServerTrusted(
                chain: Array<java.security.cert.X509Certificate>,
                authType: String
            ) {
            }

            override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
                return arrayOf()
            }
        }
    }

    private fun supportLanguage(): String {
        return if (Locale.getDefault().displayLanguage.toString().contains("ไทย")) {
            "th-TH"
        } else {
            "en-US"
        }
    }
}