package com.homepro.megahome.ui.catalog

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.homepro.megahome.databinding.CatalogTileCellBinding

import com.homepro.megahome.model.Tile

class TileAdapter(private val clickListener: TileListener) :
    PagedListAdapter<Tile, TileAdapter.ViewHolder>(TileDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val tile: Tile? = getItem(position)
        tile?.let {
            holder.bind(it, clickListener)
        }
    }

    class ViewHolder private constructor(private val binding: CatalogTileCellBinding) :
        RecyclerView.ViewHolder(binding.root) {
        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = CatalogTileCellBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }

        fun bind(item: Tile, clickListener: TileListener) {
            binding.tile = item
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }
    }
}

class TileDiffCallback : DiffUtil.ItemCallback<Tile>() {
    override fun areItemsTheSame(oldItem: Tile, newItem: Tile): Boolean = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Tile, newItem: Tile): Boolean = oldItem == newItem
}

class TileListener(val clickListener: (tileId: String) -> Unit) {
    fun onClick(tile: Tile) = clickListener(tile.id)
}