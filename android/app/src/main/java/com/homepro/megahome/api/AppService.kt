package com.homepro.megahome.api

import android.app.Activity
import android.net.Uri
import com.homepro.megahome.manager.UploadImageManager
import com.homepro.megahome.realm.*
import io.realm.Realm
import io.realm.Sort
import io.realm.kotlin.where
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

interface ServiceListener<T> {
    fun onFailure(call: Call<T>, t: Throwable)
    fun onResponse(call: Call<T>, response: Response<T>)
}

class AppService {
    fun retrieveTileProject(): MutableList<TileProject>? {

        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        val test = realm.where(TileProject::class.java).sort("id").findAll()
        var tileProjects = realm.copyFromRealm(test)
        realm.commitTransaction()

        return tileProjects
    }

    fun retrieveTile(listener: ServiceListener<List<Tile>>) {

        var call = HttpManager.getInstance().apiService.preTile()

        call.enqueue(object : Callback<List<Tile>> {
            override fun onFailure(call: Call<List<Tile>>, t: Throwable) {
                listener.onFailure(call, t)
            }

            override fun onResponse(call: Call<List<Tile>>, response: Response<List<Tile>>) {
                listener.onResponse(call, response)
            }

        })
    }

    fun retrieveOptionalTile(
        listener: ServiceListener<TileAll>,
        id: String?,
        flag: String?,
        start: String,
        size: String
    ) {

        var call = HttpManager.getInstance().apiService.tile(id, flag, start, size)

        call.enqueue(object : Callback<TileAll> {
            override fun onFailure(call: Call<TileAll>, t: Throwable) {
                listener.onFailure(call, t)
            }

            override fun onResponse(call: Call<TileAll>, response: Response<TileAll>) {
                listener.onResponse(call, response)
            }

        })
    }


    fun retrieveCalculate(
        listener: ServiceListener<Calc>,
        id: String,
        area: String,
        length: String,
        width: String,
        extra: String
    ) {


        var call = HttpManager.getInstance().apiService.calculate(id, area, length, width, extra)

        call.enqueue(object : Callback<Calc> {
            override fun onFailure(call: Call<Calc>, t: Throwable) {
                listener.onFailure(call, t)
            }

            override fun onResponse(call: Call<Calc>, response: Response<Calc>) {
                listener.onResponse(call, response)
            }

        })
    }


    fun retrieveOptionalTile(listener: ServiceListener<Tile>, id: String) {

        var call = HttpManager.getInstance().apiService.tileWithId(id)

        call.enqueue(object : Callback<Tile> {
            override fun onFailure(call: Call<Tile>, t: Throwable) {
                listener.onFailure(call, t)
            }

            override fun onResponse(call: Call<Tile>, response: Response<Tile>) {
                listener.onResponse(call, response)
            }

        })
    }


    fun retrieveTile(l: String, w: String): MutableList<TileProject>? {

        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        val test = realm.where(TileProject::class.java).contains("length", l).contains("width", w)
            .sort("id").findAll()
        var tileProjects = realm.copyFromRealm(test)
        realm.commitTransaction()

        return tileProjects
    }


    fun saveProject(id: String, name: String, tileId: String, imagePath: String, date: Date) {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        val myProject = realm.createObject(MyProject::class.java, id)

        myProject.productName = name
        myProject.tileId = tileId
        myProject.imagePath = imagePath
        myProject.dateCreate = date
        myProject.type = "project"
        myProject.stage = "finished"

        realm.commitTransaction()
    }

    fun saveProject(
        id: String,
        name: String,
        tileId: String,
        rawImage: String,
        imagePath: String,
        date: Date
    ) {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        val myProject = realm.createObject(MyProject::class.java, id)

        myProject.productName = name
        myProject.tileId = tileId
        myProject.rawImage = rawImage
        myProject.imagePath = imagePath
        myProject.dateCreate = date
        myProject.type = "project"
        myProject.stage = "finished"

        realm.commitTransaction()
    }

    fun saveProjectTypeShare(
        id: String,
        name: String,
        tileId: String,
        fitTileImage: String,
        rawImage: String,
        date: Date,
        transactionId: String,
        qrUrl: String,
        stage: String
    ): MyProject {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        val myProject = realm.createObject(MyProject::class.java, id)
        myProject.productName = name
        myProject.tileId = tileId
        myProject.imagePath = fitTileImage
        myProject.rawImage = rawImage
        myProject.dateCreate = date
        myProject.transactionId = transactionId
        myProject.type = "share"
        myProject.qrUrl = qrUrl
        myProject.stage = stage
        realm.commitTransaction()
        return myProject
    }

    fun updateProject(
        id: String,
        tileId: String,
        fitTileImage: String
    ): MyProject? {

        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        val myProject = realm.where(MyProject::class.java)
            .equalTo("id", id)
            .findFirst()

        if (null == myProject) {
            realm.commitTransaction()
            return null
        }
        myProject.tileId = tileId
        myProject.imagePath = fitTileImage
        myProject.type = "project"
        realm.copyToRealmOrUpdate(myProject)
        realm.commitTransaction()

        return myProject
    }


    fun saveTileProject(tile: Tile) {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        val tileProject = realm.createObject(TileProject::class.java, tile.id)
        tileProject.setObject(tile)

        realm.commitTransaction()
    }

    fun saveTileProject(tile: com.homepro.megahome.model.Tile) {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        val tileProject = realm.createObject(TileProject::class.java, tile.id)
        tileProject.setTileData(tile)

        realm.commitTransaction()
    }


    fun removeProject(id: String) {

        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()

        val check = realm.where(MyProject::class.java)
            .equalTo("id", id)
            .findAll()

        check?.deleteFirstFromRealm()

        realm.commitTransaction()

    }

    fun haveTileProject(tile: Tile): Boolean {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        val check = realm.where(TileProject::class.java)
            .equalTo("id", tile.id)
            .findFirst()

        realm.commitTransaction()

        return check != null
    }

    fun haveTileProject(id: String): Boolean {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        val check = realm.where(TileProject::class.java)
            .equalTo("id", id)
            .findFirst()

        realm.commitTransaction()

        return check != null
    }


    fun haveProject(): Boolean {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        val check = realm.where(MyProject::class.java)
            .findAll()

        realm.commitTransaction()

        return check.size <= 0
    }

    fun haveProject(id: String): Boolean {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        val check = realm.where(MyProject::class.java)
            .equalTo("id", id)
            .findAll()

        realm.commitTransaction()

        return check.size > 0
    }

    fun retriveProject(id: String): MyProject? {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        val check = realm.where(MyProject::class.java)
            .equalTo("id", id)
            .findFirst()

        realm.commitTransaction()

        return check
    }

    fun retrieveProject(): List<MyProject> {

        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        val projects = realm.where<MyProject>().sort("dateCreate", Sort.DESCENDING).findAll()

        realm.commitTransaction()
        return projects
    }


    fun retrieveTileProject(id: String): TileProject {


        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        val check = realm.where(TileProject::class.java)
            .equalTo("id", id)
            .findFirst()

        realm.commitTransaction()

        return check!!
    }

    fun retrieveProject(id: String): MyProject? {

        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        val test = realm.where(MyProject::class.java)
            .equalTo("id", id)
            .findFirst()

        realm.commitTransaction()
        return test
    }


    fun createProject(listener: ServiceListener<ProjectUrl>, transactionId: String) {

        var body = DgtApiService.ProjectBody()
        body.transaction_id = transactionId
        var call = DgtHttpManager.getInstance().apiService.createProject(body)

        call.enqueue(object : Callback<ProjectUrl> {
            override fun onFailure(call: Call<ProjectUrl>, t: Throwable) {
                listener.onFailure(call, t)
            }

            override fun onResponse(call: Call<ProjectUrl>, response: Response<ProjectUrl>) {
                listener.onResponse(call, response)
            }

        })
    }

    fun getProject(listener: ServiceListener<ProjectShare>, transactionId: String) {

        var call = DgtHttpManager.getInstance().apiService.getProject(transactionId)

        call.enqueue(object : Callback<ProjectShare> {
            override fun onFailure(call: Call<ProjectShare>, t: Throwable) {
                listener.onFailure(call, t)
            }

            override fun onResponse(call: Call<ProjectShare>, response: Response<ProjectShare>) {
                listener.onResponse(call, response)
            }

        })
    }

    fun editProject(
        listener: ServiceListener<Default>,
        transactionId: String,
        title: String,
        date: String
    ) {

        var edit = DgtApiService.EditProjectBody()
        edit.date = date
        edit.title = title

        var call = DgtHttpManager.getInstance().apiService.editProject(transactionId, edit)

        call.enqueue(object : Callback<Default> {
            override fun onFailure(call: Call<Default>, t: Throwable) {
                listener.onFailure(call, t)
            }

            override fun onResponse(call: Call<Default>, response: Response<Default>) {
                listener.onResponse(call, response)
            }

        })
    }


    fun uploadProject(
        upload: UploadImageManager.UploadListener,
        activity: Activity,
        transactionId: String,
        stage: String,
        tileId: String?,
        uri: Uri
    ) {
        TusImgUpload().test2(activity, uri, transactionId, stage, tileId, upload)
    }
}