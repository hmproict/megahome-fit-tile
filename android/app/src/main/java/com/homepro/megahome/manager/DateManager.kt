package com.homepro.megahome.manager

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class DateManager private constructor() {
    private val sdf: SimpleDateFormat =
        SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000'Z'", Locale.getDefault())

    init {
        date = currentDate()
    }

    fun currentDate(): Date {
        return Calendar.getInstance().time
    }

    fun getDateWithFormat(date: Date): String {
        return instance!!.sdf.format(date)
    }

    @Throws(ParseException::class)
    fun getDateWithFormat(dateTmp: String): Date {
        return instance!!.sdf.parse(dateTmp)!!
    }

    fun getDateFormatProject(date: Date): String {
        val sdf = SimpleDateFormat("yyyy MMM dd", Locale.getDefault())
        return sdf.format(date)
    }

    companion object {
        private lateinit var date: Date
        private var instance: DateManager? = null

        fun getInstance(): DateManager {
            if (instance == null)
                instance = DateManager()
            return instance as DateManager
        }

        val currentDate: Date
            get() = instance!!.currentDate()
    }
}