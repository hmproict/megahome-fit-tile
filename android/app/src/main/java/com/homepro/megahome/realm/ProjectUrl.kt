package com.homepro.megahome.realm

import com.google.gson.annotations.SerializedName

class ProjectUrl : Default() {
    @SerializedName("url")
    var url: String? = null
}
