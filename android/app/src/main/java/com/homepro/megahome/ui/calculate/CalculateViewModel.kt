package com.homepro.megahome.ui.calculate

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.homepro.megahome.database.TileDatabaseDao
import com.homepro.megahome.model.Size
import com.homepro.megahome.model.Tile
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CalculateViewModel(
    private val database: TileDatabaseDao,
    application: Application
) : AndroidViewModel(application) {
    suspend fun getTileById(id: String): Tile? {
        return withContext(Dispatchers.IO) {
            database.tileById(id)
        }
    }

    suspend fun getTileBySize(size: Size): Tile? {
        return withContext(Dispatchers.IO) {
            database.tileBySize(size.width, size.length)
        }
    }
}