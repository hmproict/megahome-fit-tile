package com.homepro.megahome.ui.project

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.homepro.megahome.glide.GlideApp
import com.homepro.megahome.manager.DateManager
import com.homepro.megahome.realm.MyProject
import java.io.File


@BindingAdapter("setDateCreatedText")
fun AppCompatTextView.setDateCreatedText(project: MyProject?) {
    project?.let {
        if (it.type == "share" && it.stage == "initial") {
            this.text = "Pending";
        } else if (it.type == "share" && it.stage == "fittile") {
            this.text = "FitTile";
        } else {
            it.dateCreate?.let { date ->
                this.text = (DateManager.getInstance().getDateFormatProject(date))
            }
        }
    }
}

@BindingAdapter("setProjectImage")
fun AppCompatImageView.setProjectImage(project: MyProject?) {
    project?.let {
        if (it.type == "share") {
            GlideApp.with(this.context)
                .load(project.rawImage!!)
                .centerCrop()
                .placeholder(ColorDrawable(Color.LTGRAY))
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(this)
        } else {
            GlideApp.with(this.context)
                .load(File(it.imagePath!!))
                .centerCrop()
                .placeholder(ColorDrawable(Color.LTGRAY))
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(this)
        }
    }
}