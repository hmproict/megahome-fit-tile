package com.homepro.megahome.ui.room

import android.Manifest
import android.content.ContentValues
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.net.UrlQuerySanitizer
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.gson.*
import com.google.gson.JsonParseException
import com.google.gson.internal.LinkedTreeMap
import com.google.gson.reflect.TypeToken
import com.homepro.megahome.R
import com.homepro.megahome.api.*
import com.homepro.megahome.database.FitTileDatabase
import com.homepro.megahome.database.TileLocalCache
import com.homepro.megahome.database.TileSizeLocalCache
import com.homepro.megahome.manager.DateManager
import com.homepro.megahome.manager.ExternalFileManager
import com.homepro.megahome.model.Tile
import com.homepro.megahome.model.TileSize
import com.homepro.megahome.model.TileType
import com.homepro.megahome.ui.scanner.ScannerFragment
import io.flutter.embedding.android.FlutterFragment
import io.flutter.embedding.android.RenderMode
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import kotlinx.coroutines.*
import java.lang.reflect.Type
import android.provider.MediaStore
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.navigation.fragment.navArgs
import com.homepro.megahome.ui.catalog.CatalogFragmentArgs
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream


class RoomFragment : FlutterFragment() {

    private lateinit var service: FitTileApiService
    private lateinit var cache: TileLocalCache
    private lateinit var sizeCache: TileSizeLocalCache

    private var lastedRequestTile = 0
    private var isRequestInProgress = false
    private var totalTiles: Int = 0
    private var tiles = ArrayList<Tile>()
    private var tileSizes = ArrayList<TileSize>()

    private val gson = GsonBuilder()
        .setPrettyPrinting()
        .registerTypeAdapter(object : TypeToken<List<Map<String, Any>>>() {}.type, ListMapDeserializerDoubleAsIntFix())
        .registerTypeAdapter(object : TypeToken<Map<String, Any>>() {}.type, MapDeserializerDoubleAsIntFix())
        .create()

    private var topFileName: String? = null
    private var topBitmap: Bitmap? = null

    private var flutterResult: MethodChannel.Result? = null

    private val REQUIRED_PERMISSIONS = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
    )

    private val REQUEST_CODE_PERMISSIONS = 19

    //Permission Request Handler
    private lateinit var requestPermissionLauncher: ActivityResultLauncher<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val application = requireActivity().application
        service = FitTileApi.service(requireContext())

        val dataSource = FitTileDatabase.getInstance(application).tileDatabaseDao
        cache =  TileLocalCache(dataSource)

        val sizeDataSource = FitTileDatabase.getInstance(application).sizeDatabaseDao
        sizeCache =  TileSizeLocalCache(sizeDataSource)

        setPermissionCallback()

        setFragmentResultListener(SCAN_GET_TILE_ID_REQUEST_KEY) { _, bundle ->
            val sanitizer =
                UrlQuerySanitizer(bundle.getString(ScannerFragment.SCANNER_RESULT_KEY))
            val tileId = sanitizer.getValue("id_scan")

            if (tileId == null) {
                MaterialAlertDialogBuilder(requireContext())
                    .setTitle(getString(R.string.error))
                    .setMessage(getString(R.string.not_valid_qrcode))
                    .setPositiveButton(getString(R.string.ok), null)
                    .show()

                return@setFragmentResultListener
            }

            if (tileId.isNotBlank() || tileId.isNotEmpty()) {
                lifecycleScope.launch {
                    getTileById(tileId)?.let {
                        val json = gson.toJson(it)
                        val mapType = object : TypeToken<Map<String, Any>>() {}.type
                        val mapData: Map<String, Any> = gson.fromJson(json, mapType)
                        activity?.runOnUiThread {
                            flutterResult?.success(mapData)
                            flutterResult = null
                        }
                        return@launch
                    }

                    Toast.makeText(
                        requireContext(),
                        getString(R.string.cannot_find_tile_from_scan),
                        Toast.LENGTH_SHORT
                    ).show()
                    activity?.runOnUiThread {
                        flutterResult?.error("04", "QR Code Error", null)
                        flutterResult = null
                    }
                }
            }
        }
    }

    override fun getCachedEngineId(): String {
        return "fittile_flutter_engine"
    }

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        MethodChannel(
            flutterEngine.dartExecutor.binaryMessenger,
            "com.homepro.megahome.flutter"
        ).setMethodCallHandler { call, result ->
            when (call.method) {
                "pop" -> try {

                } catch (e: Exception) {
                    handleException(e, result)
                }
                "changeToWhiteStatusBarStyle" -> try {

                } catch (e: Exception) {
                    handleException(e, result)
                }
                "changeToBlackStatusBarStyle" -> try {

                } catch (e: Exception) {
                    handleException(e, result)
                }
                "tileList" -> try {
                    Log.d("Flutter To App", "call method tileList")
                    val roomId = call.argument<String>("room_id")
                    requestTileData(result, roomId)
                } catch (e: Exception) {
                    handleException(e, result)
                }
                "resetTileList" -> try {

                } catch (e: Exception) {
                    handleException(e, result)
                }
                "tileSizes" -> try {
                    Log.d("Flutter To App", "call method tileSizes")
                    val tileType = call.argument<String>("type")
                    requestTileSize(result, tileType)
                } catch (e: Exception) {
                    handleException(e, result)
                }
                "qrCodeScanner" -> try {
                    Log.d("Flutter To App", "qrCodeScanner")
                    var resultTemp: MethodChannel.Result? = result
                    findNavController().navigate(RoomFragmentDirections.actionRoomflutterfragmentToScannerFragment(requestKey = SCAN_GET_TILE_ID_REQUEST_KEY))
                    this.flutterResult = result
                } catch (e: Exception) {
                    handleException(e, result)
                }
                "defaultRoomNo" -> try {
                    result.success("1")
                } catch (e: Exception) {
                    handleException(e, result)
                }
                "saveProject" -> try {
                    val image = call.argument<String>("image")
                    val name = call.argument<String>("name")
                    val tiles = call.argument<String>("tiles")
                    val topImage = call.argument<String>("top_image")

                    Log.v("Flutter to App", "image : $image")
                    Log.v("Flutter to App", "topImage : $topImage")

                    if (image != null && name != null && tiles != null) {
                        val bitmap: Bitmap = BitmapFactory.decodeFile(image)

                        val fileName = "${System.currentTimeMillis()}_HomePro_FitTile.png"
                        val screenshotFile =
                            ExternalFileManager.saveBitmapToFile(requireContext(), bitmap, fileName)

                        val mapType = object : TypeToken<List<Map<String, Any>>>() {}.type
                        val mapData: List<Map<String, Any>> = gson.fromJson(tiles, mapType)
                        val tileIds = mapData.map { it["id"] }

                        AppService().saveProject(
                            id = "Flutter_" + DateManager.getInstance()
                                .getDateWithFormat(DateManager.getInstance().currentDate()),
                            name = name,
                            tileId = tileIds.joinToString(separator = ","),
                            rawImage = "",
                            imagePath = screenshotFile.absolutePath,
                            date = DateManager.getInstance().currentDate()
                        )

                        if (topImage != null) {
                            val topBitmap: Bitmap = BitmapFactory.decodeFile(topImage)
                            val topImageFileName = "${name}_TopView_HomePro_FitTile(${System.currentTimeMillis()}).png"

                            checkPermission {
                                if (it) {
                                    ExternalFileManager.saveBitmapToFile(requireContext(), topBitmap, topImageFileName)
                                    result.success(true)
                                } else {
                                    this.topFileName = topImageFileName
                                    this.topBitmap = topBitmap
                                    this.flutterResult = result
                                }
                            }
                        } else {
                            result.success(true)
                        }
                    }
                } catch (e: Exception) {
                    handleException(e, result)
                }
                else -> result.notImplemented()
            }
        }
    }

    override fun shouldAttachEngineToActivity(): Boolean {
        return true
    }

    override fun shouldRestoreAndSaveState(): Boolean {
        return true
    }

    override fun getRenderMode(): RenderMode {
        return RenderMode.texture
    }

    override fun shouldDestroyEngineWithHost(): Boolean {
        return false
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    private fun handleException(
        exception: Exception,
        result: MethodChannel.Result
    ) {
        throw (exception as RuntimeException)
    }

    private fun requestTileData(result: MethodChannel.Result, roomId: String?) {
        Log.d("Flutter To App", "requestAndSaveData : $roomId")
        if (totalTiles != 0 && lastedRequestTile >= totalTiles) {

            val filterTiles = if (roomId != null) {
                tiles.filter { it.area.isEmpty() || it.area.contains(roomId) }
            } else {
                tiles
            }

            val json = gson.toJson(filterTiles)
            val mapType = object : TypeToken<List<Map<String, Any>>>() {}.type
            val mapData: List<Map<String, Any>> = gson.fromJson(json, mapType)
            activity?.runOnUiThread {
                result.error("00", "Downloaded", mapData)
            }
            return
        }
        if (isRequestInProgress) {
            activity?.runOnUiThread {
                result.error("02", "Is Loading", null)
            }
            return
        }

        isRequestInProgress = true

        val handler = CoroutineExceptionHandler { _, exception ->
            activity?.runOnUiThread {
                result.error("99", exception.localizedMessage, null)
            }
            isRequestInProgress = false
        }

        GlobalScope.launch(Dispatchers.IO + handler) {
            val size = if (totalTiles == 0 || (lastedRequestTile + 20) <= totalTiles)
                20 else totalTiles - lastedRequestTile
            val tilesResponse = service.tilesWithSize(
                from = lastedRequestTile,
                size = size,
                type = null
            )

            tiles.addAll(tilesResponse.tiles)
            cache.insert(tilesResponse.tiles) {
                totalTiles = tilesResponse.total
                lastedRequestTile += tilesResponse.tiles.count()
                isRequestInProgress = false

                val filterTiles = if (roomId != null) {
                    tiles.filter { it.area.isEmpty() || it.area.contains(roomId) }
                } else {
                    tiles
                }

                val json = gson.toJson(filterTiles)
                val mapType = object : TypeToken<List<Map<String, Any>>>() {}.type
                val mapData: List<Map<String, Any>> = gson.fromJson(json, mapType)
                activity?.runOnUiThread {
                    result.success(mapData)
                }
            }
        }
    }

    private fun requestTileSize(result: MethodChannel.Result, tileType: String?) {

        if (tileSizes.isEmpty()) {
            val handler = CoroutineExceptionHandler { _, exception ->

                sizeCache.sizes(TileType.ALL).observe(viewLifecycleOwner, Observer {
                    tileSizes.addAll(it)
                    val json = gson.toJson(tileSizes)
                    val mapType = object : TypeToken<List<Map<String, Any>>>() {}.type
                    val mapData: List<Map<String, Any>> = gson.fromJson(json, mapType)
                    activity?.runOnUiThread {
                        if (tileSizes.isEmpty()) {
                            result.error("99", exception.localizedMessage, null)
                        } else {
                            result.success(mapData)
                        }
                    }
                })
            }

            GlobalScope.launch(Dispatchers.IO + handler) {
                val response = service.tileSize()
                tileSizes.addAll(response.sizes)

                sizeCache.insert(response.sizes) {
                    val filterTiles = if (tileType != null) {
                        tileSizes.filter { it.tileType == tileType }
                    } else {
                        tileSizes
                    }

                    val json = gson.toJson(filterTiles)
                    val mapType = object : TypeToken<List<Map<String, Any>>>() {}.type
                    val mapData: List<Map<String, Any>> = gson.fromJson(json, mapType)
                    activity?.runOnUiThread {
                        result.success(mapData)
                    }
                }
            }
        } else {
            val filterTiles = if (tileType != null) {
                tileSizes.filter { it.tileType == tileType }
            } else {
                tileSizes
            }

            val json = gson.toJson(filterTiles)
            val mapType = object : TypeToken<List<Map<String, Any>>>() {}.type
            val mapData: List<Map<String, Any>> = gson.fromJson(json, mapType)
            activity?.runOnUiThread {
                result.success(mapData)
            }
        }
    }

    suspend fun getTileById(id: String): Tile? {
        val application = requireActivity().application
        val dataSource = FitTileDatabase.getInstance(application).tileDatabaseDao
        return withContext(Dispatchers.IO) {
            try {
                dataSource.tileById(id) ?: service.tile(id)
            } catch (e: Exception) {
                null
            }
        }
    }

    companion object {
        const val SCAN_GET_TILE_ID_REQUEST_KEY = "roomRequestTile"
    }

    private fun allPermissionGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(requireContext(), it) == PackageManager.PERMISSION_GRANTED
    }

    //Allowing activity to automatically handle permission request
    private fun setPermissionCallback() {
        requestPermissionLauncher =
            registerForActivityResult(
                ActivityResultContracts.RequestPermission()
            ) { isGranted: Boolean ->
                if (isGranted) {
                    if (topFileName != null && topBitmap != null) {
                        ExternalFileManager.saveBitmapToFile(requireContext(), topBitmap!!, topFileName!!)
                        if (flutterResult != null) {
                            flutterResult?.success(true)
                            flutterResult = null
                        }
                    }
                }
            }
    }

    //function to check and request storage permission
    private fun checkPermission(complete: (Boolean) -> Unit) {
        when {
            allPermissionGranted() -> {
                complete(true)
            }
            shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE) -> {
                showPermissionRequestDialog(
                    getString(R.string.permission_title),
                    getString(R.string.write_permission_request)
                ) {
                    requestPermissionLauncher.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    complete(false)
                }
            }
            else -> {
                requestPermissionLauncher.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                complete(false)
            }
        }
    }

    private fun showPermissionRequestDialog(
        title: String,
        body: String,
        callback: () -> Unit
    ) {
        AlertDialog.Builder(this.context).also {
            it.setTitle(title)
            it.setMessage(body)
            it.setPositiveButton("Ok") { _, _ ->
                callback()
            }
        }.create().show()
    }

    private fun saveImageToGallery(fileName: String, bitmap: Bitmap) {
        var fos: OutputStream? = null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            context.contentResolver?.also { resolver ->
                val contentValues = ContentValues().apply {
                    put(MediaStore.MediaColumns.DISPLAY_NAME, fileName)
                    put(MediaStore.MediaColumns.MIME_TYPE, "image/png")
                    put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES)
                }
                val imageUri: Uri? =
                    resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)
                fos = imageUri?.let { resolver.openOutputStream(it) }
            }
        } else {
            val imagesDir =
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
            val image = File(imagesDir, fileName)
            fos = FileOutputStream(image)
        }
        fos?.use {
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, it)
        }
    }
}

class ListMapDeserializerDoubleAsIntFix :
    JsonDeserializer<List<Map<String, Any>>?> {
    @Throws(JsonParseException::class)
    override fun deserialize(
        json: JsonElement,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): List<Map<String, Any>>? {
        return read(json) as List<Map<String, Any>>?
    }

    fun read(element: JsonElement): Any? {
        if (element.isJsonArray) {
            val list: MutableList<Any?> = ArrayList()
            val arr = element.asJsonArray
            for (anArr in arr) {
                list.add(read(anArr))
            }
            return list
        } else if (element.isJsonObject) {
            val map: MutableMap<String, Any?> = LinkedTreeMap()
            val obj = element.asJsonObject
            val entitySet = obj.entrySet()
            for ((key, value) in entitySet) {
                map[key] = read(value)
            }
            return map
        } else if (element.isJsonPrimitive) {
            val prim = element.asJsonPrimitive
            if (prim.isBoolean) {
                return prim.asBoolean
            } else if (prim.isString) {
                return prim.asString
            } else if (prim.isNumber) {
                val num = prim.asNumber
                // here you can handle double int/long values
                // and return any type you want
                // this solution will transform 3.0 float to long values
                return if (Math.ceil(num.toDouble()) == num.toLong().toDouble()) num.toLong() else {
                    num.toDouble()
                }
            }
        }
        return null
    }
}

class MapDeserializerDoubleAsIntFix :
    JsonDeserializer<Map<String, Any>?> {
    @Throws(JsonParseException::class)
    override fun deserialize(
        json: JsonElement,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): Map<String, Any>? {
        return read(json) as Map<String, Any>?
    }

    fun read(element: JsonElement): Any? {
        if (element.isJsonArray) {
            val list: MutableList<Any?> = ArrayList()
            val arr = element.asJsonArray
            for (anArr in arr) {
                list.add(read(anArr))
            }
            return list
        } else if (element.isJsonObject) {
            val map: MutableMap<String, Any?> = LinkedTreeMap()
            val obj = element.asJsonObject
            val entitySet = obj.entrySet()
            for ((key, value) in entitySet) {
                map[key] = read(value)
            }
            return map
        } else if (element.isJsonPrimitive) {
            val prim = element.asJsonPrimitive
            if (prim.isBoolean) {
                return prim.asBoolean
            } else if (prim.isString) {
                return prim.asString
            } else if (prim.isNumber) {
                val num = prim.asNumber
                // here you can handle double int/long values
                // and return any type you want
                // this solution will transform 3.0 float to long values
                return if (Math.ceil(num.toDouble()) == num.toLong().toDouble()) num.toLong() else {
                    num.toDouble()
                }
            }
        }
        return null
    }
}