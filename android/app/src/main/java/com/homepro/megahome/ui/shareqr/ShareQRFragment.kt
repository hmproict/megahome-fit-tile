package com.homepro.megahome.ui.shareqr

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.homepro.megahome.R
import com.homepro.megahome.databinding.ShareQRFragmentBinding
import com.homepro.megahome.manager.AppUtilites

class ShareQRFragment : Fragment() {
    private val args by navArgs<ShareQRFragmentArgs>()
    private val navController by lazy { findNavController() }

    private lateinit var viewModel: ShareQRViewModel
    private lateinit var binding: ShareQRFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.share_q_r_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ShareQRViewModel::class.java)

        binding.project = args.project
        binding.executePendingBindings()

        val qrBitmap = AppUtilites().createQRcode(args.project.qrUrl)
        binding.qrImageView.setImageBitmap(qrBitmap)

        binding.backConstraintLayout.setOnClickListener { navController.navigate(ShareQRFragmentDirections.actionShareQRFragmentToProjectFragment()) }
        binding.doneButton.setOnClickListener { navController.navigate(ShareQRFragmentDirections.actionShareQRFragmentToProjectFragment()) }
    }
}