package com.homepro.megahome.ui.share

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.homepro.megahome.database.TileDatabaseDao
import com.homepro.megahome.model.Tile
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ShareViewModel(
    val database: TileDatabaseDao,
    application: Application) : AndroidViewModel(application) {
    suspend fun getTileById(id: String): Tile? {
        return withContext(Dispatchers.IO) {
            database.tileById(id)
        }
    }
}
