package com.homepro.megahome.ui.fittile

import android.app.Dialog
import android.content.Context
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.net.UrlQuerySanitizer
import android.os.Bundle
import android.provider.DocumentsContract
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.core.net.toUri
import androidx.core.view.doOnLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.homepro.megahome.R
import com.homepro.megahome.api.AppService
import com.homepro.megahome.api.FitTileApi
import com.homepro.megahome.api.ServiceListener
import com.homepro.megahome.components.DrawViewListener
import com.homepro.megahome.components.OnMoveListener
import com.homepro.megahome.data.TileRepository
import com.homepro.megahome.database.FitTileDatabase
import com.homepro.megahome.database.TileLocalCache
import com.homepro.megahome.databinding.FitTileFragmentBinding
import com.homepro.megahome.extensions.changeTintColor
import com.homepro.megahome.glide.GlideApp
import com.homepro.megahome.manager.*
import com.homepro.megahome.model.*
import com.homepro.megahome.realm.Default
import com.homepro.megahome.realm.ProjectUrl
import com.homepro.megahome.realm.TileProject
import com.homepro.megahome.ui.catalog.TileAdapter
import com.homepro.megahome.ui.catalog.TileListener
import com.homepro.megahome.ui.scanner.ScannerFragment
import com.jaygoo.widget.OnRangeChangedListener
import com.jaygoo.widget.RangeSeekBar
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Response
import java.io.File
import kotlin.math.absoluteValue

class FitTileFragment : Fragment(), UploadImageManager.UploadListener {
    private val args: FitTileFragmentArgs by navArgs()

    private val binding: FitTileFragmentBinding by lazy {
        FitTileFragmentBinding.inflate(layoutInflater)
    }

    private lateinit var adapter: TileAdapter

    private lateinit var tilesMap: Map<String, Tile>

    private lateinit var viewModel: FitTileViewModel

    private var showTileType = TileType.ALL

    private var isAlreadyShowWallAppliedOnlyDialog = false

    private var tileSelectType = ""
    private var mPosX = 0
    private var mPosY = 0

    private var baseX = 0f
    private var baseY = 0f
    internal var tile: TileProject? = null

    private var progress = 0f
    private var width = 0
    private var scale = 0
    private var scaleX = 50
    private var scaleY = 50
    private var tileBitmap: Bitmap? = null
    private var tileEventId = TileEvent.PIN_AREA
    private var screenSize: Point? = null
    private var tileProjects: MutableList<TileProject>? = null
    private var tileAllProjects: List<TileProject>? = null
    private var tileId = ""
    private var tutorSeq = 0
    private var multiple = 1
    private var resultTile: TileProject? = null

    private val tileLayoutMultiplier = 3

    private var defaultTileLayoutWidth: Int = 0
    private var defaultTileLayoutHeight: Int = 0

    private var multipliedTileLayoutWidth: Int = 0
    private var multipliedTileLayoutHeight: Int = 0

    private var additionalHeight: Int = 0
    private var additionalWidth: Int = 0

    private val layoutParamsCenterWith3TimesDimension: FrameLayout.LayoutParams by lazy {
        defaultTileLayoutWidth = binding.imvRepeatTile.width
        defaultTileLayoutHeight = binding.imvRepeatTile.height

        multipliedTileLayoutWidth = defaultTileLayoutWidth * tileLayoutMultiplier
        multipliedTileLayoutHeight = defaultTileLayoutHeight * tileLayoutMultiplier

        additionalWidth = (multipliedTileLayoutWidth - defaultTileLayoutWidth) / 2
        additionalHeight = (multipliedTileLayoutHeight - defaultTileLayoutHeight) / 2

        val params = FrameLayout.LayoutParams(
            binding.imvRepeatTile.width * tileLayoutMultiplier,
            binding.imvRepeatTile.height * tileLayoutMultiplier
        )
        params.gravity = Gravity.CENTER

        params
    }

    private var selectTileTarget: CustomTarget<Bitmap> = object : CustomTarget<Bitmap>() {
        override fun onLoadCleared(placeholder: Drawable?) {
        }

        override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
            scaleY = 50
            scaleX = 50

            val tileBitmapWithWhiteBorder = addWhiteBorder(resource, 4)

            enableTileMode()
            setTile(tileBitmapWithWhiteBorder)
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        setFragmentResultListener(SCAN_GET_TILE_REQUEST_KEY) { _, bundle ->
            val sanitizer = UrlQuerySanitizer(bundle.getString(ScannerFragment.SCANNER_RESULT_KEY))
            val tileId = sanitizer.getValue("id_scan")

            if (tileId == null) {
                MaterialAlertDialogBuilder(requireContext())
                    .setTitle(getString(R.string.error))
                    .setMessage(getString(R.string.not_valid_qrcode))
                    .setPositiveButton(getString(R.string.ok), null)
                    .show()

                return@setFragmentResultListener
            }

            lifecycleScope.launch {
                tileId?.let {
                    viewModel.getTileById(it)?.let { tile ->
                        viewModel.selectTile(tile)
                        return@launch
                    }
                }

                Toast.makeText(
                    requireContext(),
                    getString(R.string.cannot_find_tile_from_scan),
                    Toast.LENGTH_SHORT).show()
            }
        }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val application = requireNotNull(this.activity).application
        val service = FitTileApi.service(requireContext())
        val dataSource = FitTileDatabase.getInstance(application).tileDatabaseDao
        val repository = TileRepository(FitTileApi.service(requireContext()), TileLocalCache(dataSource))
        val viewModelFactory = FitTileViewModelFactory(service, dataSource, repository, application)
        viewModel = ViewModelProvider(this, viewModelFactory).get(FitTileViewModel::class.java)

        val tilesWithoutWrongImageURL =
            viewModel.tilesDefault.filter { tile -> tile.imageURL != "https://s3-ap-southeast-1.amazonaws.com/qas-public-bucket/homepro/FITTILE_IMAGE/" }
        tilesMap = tilesWithoutWrongImageURL.associateBy({ it.id }, { it })

        viewModel.tilesCache = tilesWithoutWrongImageURL
        viewModel.tilesWallCache = tilesWithoutWrongImageURL.filter { tile -> tile.type == "Wall" }
        viewModel.tilesFloorCache =
            tilesWithoutWrongImageURL.filter { tile -> tile.type == "Floor" }

        if (args.tileIdFromCatalog == null) {
            viewModel.selectTile(tilesWithoutWrongImageURL[0])
        } else {
            args.tileIdFromCatalog?.let { tileId ->
                tilesMap[tileId]?.let {
                    viewModel.selectTile(it)
                }
            }
        }

        initAdapter(viewModel)
        val tilesFilter = viewModel.lastFilter() ?: DEFAULT_FILTER
        viewModel.changeTilesByTypeAndSize(tilesFilter.tileType, tilesFilter.size)


        binding.includeBottomSheet.qrScanTileImageView.setOnClickListener { viewModel.onQRScannerClicked() }
        viewModel.navigateToScanner.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(FitTileFragmentDirections.actionFitTileFragmentToScannerFragment(requestKey = SCAN_GET_TILE_REQUEST_KEY))
                viewModel.onScannerNavigate()
            }
        })

        if (viewModel.isCompletePin) {
            completeSelectArea()
        }
    }

    private fun initAdapter(viewModel: FitTileViewModel) {
        val manager = GridLayoutManager(requireActivity(), 3)
        binding.includeBottomSheet.tileList.layoutManager = manager

        adapter = TileAdapter(TileListener { tileId ->
            tilesMap[tileId]?.let {
                viewModel.selectTile(it)
            }
        })

        binding.includeBottomSheet.tileList.adapter = adapter

        viewModel.networksError.observe(viewLifecycleOwner, Observer {
            Log.d("Flutter to App", "service error : $it")
            Toast.makeText(requireContext(), "\uD83D\uDE28 Wooops $it", Toast.LENGTH_LONG).show()
        })

        viewModel.tiles.observe(viewLifecycleOwner, Observer {
            tilesMap = tilesMap + it.filterNotNull().associateBy({ tile -> tile.id }, { tile -> tile })
            adapter.submitList(it)
        })
    }

    private fun selectTile(imageUrl: String, tileId: String, pat: String) {
        tileSelectType = imageUrl
        this.tileId = tileId

        tilesMap[tileId]?.let {
            if (it.type == "Wall" && !isAlreadyShowWallAppliedOnlyDialog) {
                isAlreadyShowWallAppliedOnlyDialog = true
                MaterialAlertDialogBuilder(requireContext())
                    .setTitle(getString(R.string.wall_apply_only_message_dialog))
                    .setPositiveButton(getString(R.string.ok), null)
                    .show()
            }
        }

        multiple = try {
            val patA = pat.split("x".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val p = Integer.parseInt(patA[0])
            p
        } catch (e: Exception) {
            1
        }

        GlideApp.with(this)
            .asBitmap()
            .load(imageUrl)
            .into(selectTileTarget)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()

        initEvent()

        fetchData()

        fetchTutorial()

        showCheck()

        bottomSheet()
    }

    private fun init() {

        hideBottomSheet()
        binding.includeTutorialDialog.layoutTutorial.visibility = View.GONE
        binding.imvRepeatTile.visibility = View.VISIBLE

        binding.imvArrowPin.visibility = View.GONE
        binding.imvArrowTilt.visibility = View.GONE
        binding.imvArrowScale.visibility = View.GONE
        binding.imvArrowMove.visibility = View.GONE

        val display = requireActivity().windowManager.defaultDisplay
        screenSize = Point()
        display.getSize(screenSize)
        binding.sbScale.visibility = View.GONE
        binding.sbTilt.visibility = View.GONE

        binding.btnSave.visibility = View.GONE
        binding.btnAddMore.visibility = View.GONE

        disableTileMode()

        setRoomImage(captureBitmap)

        binding.imvRepeatTile.doOnLayout {
            it.layoutParams = layoutParamsCenterWith3TimesDimension
        }
    }

    private fun enableTileMode() {
        binding.pinRelativeLayout.isEnabled = true
        binding.perspectiveRelativeLayout.isEnabled = true
        binding.scaleRelativeLayout.isEnabled = true
        binding.dragRelativeLayout.isEnabled = true
    }


    private fun setTile(bitmap: Bitmap) {
        tileBitmap = bitmap

        val tileMe = BitmapDrawable(
            resources,
            Bitmap.createScaledBitmap(bitmap, scaleX * multiple, scaleY * multiple, true)
        )
        tileMe.tileModeX = Shader.TileMode.REPEAT
        tileMe.tileModeY = Shader.TileMode.REPEAT
        tileMe.setBounds(2, 2, 2, 2)

        binding.imvRepeatTile.background = tileMe
    }

    private fun addWhiteBorder(bmp: Bitmap, borderSize: Int): Bitmap {
        val bmpWithBorder =
            Bitmap.createBitmap(bmp.width + borderSize * 2, bmp.height + borderSize * 2, bmp.config)
        val canvas = Canvas(bmpWithBorder)
        canvas.drawColor(Color.WHITE)
        canvas.drawBitmap(bmp, borderSize.toFloat(), borderSize.toFloat(), null)
        return bmpWithBorder
    }

    private fun createShareProject(transactionId: String, uri: Uri, name: String) {
        AppService().createProject(object : ServiceListener<ProjectUrl> {
            override fun onFailure(call: Call<ProjectUrl>, t: Throwable) {
                Toast.makeText(requireContext(), "Cannot upload share project. Please try again later", Toast.LENGTH_SHORT).show()
                findNavController().navigateUp()
            }

            override fun onResponse(call: Call<ProjectUrl>, response: Response<ProjectUrl>) {
                if (response.body()?.status != "200") {
                    Toast.makeText(requireContext(), "Cannot upload share project. Please try again later", Toast.LENGTH_SHORT).show()
                    findNavController().navigateUp()

                    return
                }

                uploadShareProject(
                    transactionId,
                    "initial",
                    null,
                    uri,
                    name,
                    response.body()!!.url!!
                )
            }

        }, transactionId)
    }


    private fun uploadShareProject(
        transactionId: String,
        stage: String,
        tileId: String?,
        imgUri: Uri,
        name: String,
        qrUrl: String
    ) {
        AppService().uploadProject(this, requireActivity(), transactionId, stage, tileId, imgUri)

        val currentDate = DateManager.getInstance().currentDate()
        AppService().editProject(object : ServiceListener<Default> {
            override fun onFailure(call: Call<Default>, t: Throwable) {
            }

            override fun onResponse(call: Call<Default>, response: Response<Default>) {
            }

        }, transactionId, name, DateManager.getInstance().getDateWithFormat(currentDate))

        val project = AppService().saveProjectTypeShare(
            transactionId,
            name,
            "",
            args.captureBitmapFilePath,
            args.captureBitmapFilePath,
            currentDate,
            transactionId,
            qrUrl,
            stage
        )
        findNavController().navigate(FitTileFragmentDirections.actionFitTileFragmentToShareQRFragment(project))
    }


    private fun showCheck() {
        if (tileSelectType.equals("", ignoreCase = true)) {
            binding.btnScale.isEnabled = false
            binding.btnTilt.isEnabled = false
            binding.btnAction.isEnabled = false
        } else {
            binding.btnScale.isEnabled = true
            binding.btnTilt.isEnabled = true
            binding.btnAction.isEnabled = true
        }
    }

    internal var TAG = "DATA_TILE"
    var isShare = false
    var transactionId = ""
    private fun fetchData() {
        if (args.isShare) {
            isShare = args.isShare
            binding.btnShare.visibility = View.GONE
        }

        args.transactionId?.let {
            transactionId = it
        }

        if (args.data.equals("tile", ignoreCase = true)) {
            tile = args.tileProject
        }
    }

    private fun isFirstTutorial(): Boolean {

        if (requireActivity().getSharedPreferences(
                "${requireContext().packageName}.isTutorial",
                AppCompatActivity.MODE_PRIVATE
            )!!.getBoolean("firstrun", true)
        ) {
            requireActivity().getSharedPreferences(
                "${requireContext().packageName}.isTutorial",
                AppCompatActivity.MODE_PRIVATE
            )!!.edit().putBoolean("firstrun", false).apply()
            return true
        }

        return false
    }

    private fun bottomSheet() {
        val sheetBehavior = BottomSheetBehavior.from(binding.includeBottomSheet.bottomSheet)
        sheetBehavior.isHideable = false
        sheetBehavior.peekHeight = 350
        sheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED

    }

    private fun fetchTutorial() {
        if (isFirstTutorial()) {
            binding.includeTutorialDialog.layoutTutorial.visibility = View.VISIBLE
            binding.imvArrowPin.visibility = View.INVISIBLE
            binding.imvArrowTilt.visibility = View.INVISIBLE
            binding.imvArrowScale.visibility = View.INVISIBLE
            binding.imvArrowMove.visibility = View.INVISIBLE
            showTutorial(tutorSeq)
            binding.layoutTile.isEnabled = false
            binding.includeTutorialDialog.layoutTutorial.setOnClickListener({ })
        }
    }

    private fun initEvent() {
        binding.includeBottomSheet.tvFilter.setOnClickListener { showFilterDialog() }

        binding.btnCancel.setOnClickListener { findNavController().navigateUp() }

        binding.btnSave.setOnClickListener {
            val isThisSharedProject = isShare
            if (isThisSharedProject) {
                beforeCapture()
                val bitmap = Screenshot.takeWithCanvas(binding.captureFrameLayout)
                val fileName = "${System.currentTimeMillis()}_HomePro_FitTile.jpg"
                val imageFile =
                    ExternalFileManager.saveBitmapToFile(requireContext(), bitmap, fileName)

                var tileIds = args.tileIds
                tileIds = if (tileIds.isEmpty()) {
                    tileId
                } else {
                    "$tileIds,$tileId"
                }

                val updatedProject =
                    AppService().updateProject(transactionId, tileIds, imageFile.absolutePath)

                updatedProject?.let { project ->
                    findNavController().navigate(FitTileFragmentDirections.actionFitTileFragmentToShareFragment(project))
                }
            } else {
                openSaveNameDialog(false)
            }
        }

        binding.btnShare.setOnClickListener {
            openSaveNameDialog(true)
        }

        binding.btnAddMore.setOnClickListener {
            beforeCapture()
            val bitmap = Screenshot.takeWithCanvas(binding.captureFrameLayout)
            val fileName = "${System.currentTimeMillis()}_HomePro_FitTile.jpg"
            val file = ExternalFileManager.saveBitmapToFile(requireContext(), bitmap, fileName)

//            BitmapManager.setLastedBitmapCapture(bitmap, file.toUri())

            var tileIds = args.tileIds
            tileIds = if (tileIds.isEmpty()) {
                tileId
            } else {
                "$tileIds,$tileId"
            }
            FitTileFragment.captureBitmap = bitmap
            findNavController().navigate(
                FitTileFragmentDirections.actionFitTileFragmentSelf(
                    tileIds = tileIds,
                    isShare = isShare,
                    transactionId = transactionId,
                    captureBitmapFilePath = file.absolutePath,
                    roomImagePath = args.roomImagePath
                )
            )
        }


        binding.pinRelativeLayout.setOnClickListener {
            binding.pinBackgroundImageView.changeTintColor(requireContext(), R.color.colorAccent)
            binding.perspectiveBackgroundImageView.changeTintColor(
                requireContext(),
                R.color.colorTileModeBackgroundDefault
            )
            binding.scaleBackgroundImageView.changeTintColor(
                requireContext(),
                R.color.colorTileModeBackgroundDefault
            )
            binding.dragBackgroundImageView.changeTintColor(
                requireContext(),
                R.color.colorTileModeBackgroundDefault
            )

            tileEventId = TileEvent.PIN_AREA
            binding.drawView.setTileEventId(tileEventId)
            binding.drawView.invalidate()
            hideBottomSheet()
            binding.imvMove.visibility = View.GONE

            binding.sbScale.visibility = View.GONE
            binding.sbTilt.visibility = View.GONE

        }

        binding.scaleRelativeLayout.setOnClickListener {
            binding.pinBackgroundImageView.changeTintColor(
                requireContext(),
                R.color.colorTileModeBackgroundDefault
            )
            binding.perspectiveBackgroundImageView.changeTintColor(
                requireContext(),
                R.color.colorTileModeBackgroundDefault
            )
            binding.scaleBackgroundImageView.changeTintColor(requireContext(), R.color.colorAccent)
            binding.dragBackgroundImageView.changeTintColor(
                requireContext(),
                R.color.colorTileModeBackgroundDefault
            )

            binding.imvMove.visibility = View.GONE
            tileEventId = TileEvent.SCALE_TILE
            binding.drawView.setTileEventId(tileEventId)
            binding.drawView.invalidate()
            binding.sbScale.visibility = View.VISIBLE
            binding.sbTilt.visibility = View.GONE
            binding.sbScale.setProgress(viewModel.scaleProgress.toFloat())
            showBottomSheet()
            disableMark()
        }

        binding.perspectiveRelativeLayout.setOnClickListener {
            binding.pinBackgroundImageView.changeTintColor(
                requireContext(),
                R.color.colorTileModeBackgroundDefault
            )
            binding.perspectiveBackgroundImageView.changeTintColor(
                requireContext(),
                R.color.colorAccent
            )
            binding.scaleBackgroundImageView.changeTintColor(
                requireContext(),
                R.color.colorTileModeBackgroundDefault
            )
            binding.dragBackgroundImageView.changeTintColor(
                requireContext(),
                R.color.colorTileModeBackgroundDefault
            )

            binding.imvMove.visibility = View.GONE
            if (tileEventId == TileEvent.PIN_AREA) {
                if (this.rotationX > 0F) {
                    tileEventId = TileEvent.TILT_FLOOR
                } else if (this.rotationY > 0F) {
                    tileEventId = TileEvent.TILT_LEFT_WALL
                } else {
                    tileEventId = TileEvent.TILT_RIGHT_WALL
                }

                binding.drawView.setTileEventId(tileEventId)
                binding.drawView.invalidate()
            }
            binding.sbTilt.visibility = View.VISIBLE
            binding.sbScale.visibility = View.GONE
            binding.sbTilt.setProgress(viewModel.perspectiveProgress.toFloat())
            showBottomSheet()
            disableMark()

            val items = arrayOf(
                getString(R.string.left_perspective),
                getString(R.string.floor_perspective),
                getString(R.string.right_perspective)
            )

            MaterialAlertDialogBuilder(requireContext())
                .setTitle(getString(R.string.choose_perspective_title))
                .setCancelable(false)
                .setItems(items) { _, which ->
                    when (which) {
                        0 -> {
                            when (tileEventId) {
                                TileEvent.TILT_RIGHT_WALL -> {
                                    rotationY = rotationY.absoluteValue.coerceIn(0F, 32F)
                                    rotationX = 0F
                                }
                                TileEvent.TILT_FLOOR -> {
                                    rotationY = rotationX.absoluteValue.coerceIn(0F, 32F)
                                    rotationX = 0F
                                }
                            }

                            viewModel.changeTilesByTypeAndSize(TileType.ALL, viewModel.lastFilter()?.size)
                            showTileType = TileType.ALL
                            tileEventId = TileEvent.TILT_LEFT_WALL

                            binding.drawView.setTileEventId(tileEventId)
                            binding.drawView.invalidate()

                            setPerspectiveLeftWall(rotationY)
                        }
                        1 -> {
                            if (rotationY != 0F) {
                                rotationX = rotationY.absoluteValue.coerceIn(0F, 24F)
                                rotationY = 0F
                            }

                            viewModel.changeTilesByTypeAndSize(TileType.FLOOR, viewModel.lastFilter()?.size)
                            if (tilesMap[tileId]?.type == "Wall") {
                                MaterialAlertDialogBuilder(requireContext())
                                    .setTitle(getString(R.string.attention_to_change_to_floor_tile))
                                    .setPositiveButton(getString(R.string.ok)) { _, _ ->
                                        selectTile(
                                            viewModel.tilesFloorCache.first().imageURL,
                                            viewModel.tilesFloorCache.first().id,
                                            viewModel.tilesFloorCache.first().pattern
                                        )
                                    }.show()
                            }
                            showTileType = TileType.FLOOR

                            tileEventId = TileEvent.TILT_FLOOR
                            binding.drawView.setTileEventId(tileEventId)
                            binding.drawView.invalidate()

                            setPerspective(rotationX)
                        }
                        2 -> {
                            when (tileEventId) {
                                TileEvent.TILT_LEFT_WALL -> {
                                    rotationY = (rotationY.absoluteValue).coerceIn(0F, 32F)
                                    rotationX = 0F
                                }
                                TileEvent.TILT_FLOOR -> {
                                    rotationY = rotationX.absoluteValue.coerceIn(0F, 32F)
                                    rotationX = 0F
                                }
                            }

                            viewModel.changeTilesByTypeAndSize(TileType.ALL, viewModel.lastFilter()?.size)
                            showTileType = TileType.ALL

                            tileEventId = TileEvent.TILT_RIGHT_WALL
                            binding.drawView.setTileEventId(tileEventId)
                            binding.drawView.invalidate()

                            setPerspectiveRightWall(rotationY)
                        }
                        else -> {
                        }
                    }

                    binding.imvRepeatTile.rotationX = rotationX
                    binding.imvRepeatTile.rotationY = rotationY
                }.show()
        }

        binding.dragRelativeLayout.setOnClickListener {
            binding.imvMove.visibility = View.VISIBLE
            binding.pinBackgroundImageView.changeTintColor(
                requireContext(),
                R.color.colorTileModeBackgroundDefault
            )
            binding.perspectiveBackgroundImageView.changeTintColor(
                requireContext(),
                R.color.colorTileModeBackgroundDefault
            )
            binding.scaleBackgroundImageView.changeTintColor(
                requireContext(),
                R.color.colorTileModeBackgroundDefault
            )
            binding.dragBackgroundImageView.changeTintColor(requireContext(), R.color.colorAccent)

            tileEventId = TileEvent.ACTION_TILE
            binding.drawView.setTileEventId(tileEventId)
            binding.drawView.invalidate()
            binding.sbScale.visibility = View.GONE
            binding.sbTilt.visibility = View.GONE
            showBottomSheet()
            disableMark()
        }

        binding.sbTilt.setOnRangeChangedListener(object : OnRangeChangedListener {

            override fun onStartTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {
            }

            override fun onRangeChanged(
                view: RangeSeekBar?,
                leftValue: Float,
                rightValue: Float,
                isFromUser: Boolean
            ) {
                if (isFromUser) {
                    viewModel.perspectiveProgress = leftValue.toInt()
                }
                when (tileEventId) {
                    TileEvent.TILT_LEFT_WALL -> setPerspectiveLeftWall(leftValue.toFloat())
                    TileEvent.TILT_FLOOR -> setPerspective(leftValue.toFloat())
                    TileEvent.TILT_RIGHT_WALL -> setPerspectiveRightWall(leftValue.toFloat())
                    else -> {
                    }
                }
            }

            override fun onStopTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {
            }
        })

        binding.sbScale.setOnRangeChangedListener(object : OnRangeChangedListener {

            override fun onStartTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {

            }

            override fun onRangeChanged(
                view: RangeSeekBar?,
                leftValue: Float,
                rightValue: Float,
                isFromUser: Boolean
            ) {
                if (isFromUser) {
                    viewModel.scaleProgress = leftValue.toInt()
                }
                width = leftValue.coerceIn(0F, 220F).toInt()
                setScale(width)
            }

            override fun onStopTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {

            }
        })

        binding.drawView.setDrawViewListener(object : DrawViewListener {
            override fun onSelectComplete() {
                completeSelectArea()
            }
        })

        binding.drawView.setOnMoveListener(object : OnMoveListener {
            override fun onMove(posX: Int, posY: Int) {
                mPosX = posX
                mPosY = posY

                updateLocation(posX.toFloat(), posY.toFloat())
            }
        })


        binding.includeTutorialDialog.layoutNext.setOnClickListener {
            tutorSeq++
            showTutorial(tutorSeq)
        }
        binding.includeTutorialDialog.layoutBack.setOnClickListener {
            tutorSeq--
            showTutorial(tutorSeq)
        }
    }

    private fun updateLocation(x: Float, y: Float) {
        if (scale == 0) scale = 1

        val tmp = ((scaleX * scaleY + scaleX * scale) / scaleY - scaleX).toDouble()

        val positionX = x % (scaleX + tmp.toInt() + rotationX.toInt() * 2 * multiple) + baseX
        val positionY = (y % ((scaleY + scale) * multiple)) + baseY + rotationX * 6f * -1f
        binding.imvRepeatTile.x = positionX - additionalWidth
        binding.imvRepeatTile.y = positionY - additionalHeight
    }

    private fun showTutorial(seq: Int) {
        binding.imvArrowPin.visibility = View.INVISIBLE
        binding.imvArrowTilt.visibility = View.INVISIBLE
        binding.imvArrowScale.visibility = View.INVISIBLE
        binding.imvArrowMove.visibility = View.INVISIBLE

        when (seq) {
            0 -> {
                binding.includeTutorialDialog.tvTutorDes.text = getString(R.string.message_use_pin)
                GlideApp.with(this)
                    .load(R.raw.pin_selected)
                    .placeholder(R.drawable.pin_selected)
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .into(binding.includeTutorialDialog.imvTutor)
                binding.imvArrowPin.visibility = View.VISIBLE
                binding.includeTutorialDialog.layoutBack.visibility = View.GONE
            }
            1 -> {
                binding.includeTutorialDialog.tvTutorDes.text =
                    getString(R.string.message_use_perspective)
                GlideApp.with(this)
                    .load(R.raw.tilt_tile)
                    .placeholder(R.drawable.tilt_tile)
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .into(binding.includeTutorialDialog.imvTutor)
                binding.imvArrowTilt.visibility = View.VISIBLE
                binding.includeTutorialDialog.layoutBack.visibility = View.VISIBLE
            }
            2 -> {
                binding.includeTutorialDialog.tvTutorDes.text =
                    getString(R.string.message_use_size_tile)
                GlideApp.with(this)
                    .load(R.raw.scale_tile)
                    .placeholder(R.drawable.scale_tile)
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .into(binding.includeTutorialDialog.imvTutor)
                binding.imvArrowScale.visibility = View.VISIBLE
                binding.includeTutorialDialog.layoutBack.visibility = View.VISIBLE

            }
            3 -> {

                binding.includeTutorialDialog.tvTutorDes.text =
                    getString(R.string.message_move_tile)
                GlideApp.with(this)
                    .load(R.raw.move_tile)
                    .placeholder(R.drawable.move_tile)
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .into(binding.includeTutorialDialog.imvTutor)
                binding.imvArrowMove.visibility = View.VISIBLE
                binding.includeTutorialDialog.layoutBack.visibility = View.VISIBLE
            }
            else -> {
                binding.includeTutorialDialog.layoutTutorial.visibility = View.GONE
                binding.imvArrowPin.visibility = View.GONE
                binding.imvArrowTilt.visibility = View.GONE
                binding.imvArrowScale.visibility = View.GONE
                binding.imvArrowMove.visibility = View.GONE
                binding.layoutTile.isEnabled = true
            }
        }
    }


    private fun completeSelectArea() {
        showFirstBottomSheet()
        viewModel.completeSelectedArea()
    }

    private fun showFirstBottomSheet() {
        binding.imvMove.visibility = View.GONE
        tileEventId = TileEvent.SELECT_TILE
        binding.drawView.setTileEventId(tileEventId)
        binding.drawView.invalidate()
        binding.drawView.setComplete(true)
        binding.btnSave.visibility = View.VISIBLE


        binding.btnAddMore.visibility = View.VISIBLE

        disableMark()
        binding.sbScale.visibility = View.GONE
        binding.sbTilt.visibility = View.GONE
        binding.includeBottomSheet.bottomSheet.visibility = View.VISIBLE

        enableTileMode()

        if (tile != null) {
            tileId = tile!!.id.toString()

            GlideApp.with(this)
                .asBitmap()
                .load(tile!!.getImagePattern())
                .into(selectTileTarget)
        } else {
            tileId = viewModel.tilesCache.first().id

            scaleY = 50
            scaleX = 50

            viewModel.selectedBitmap.observe(
                viewLifecycleOwner,
                androidx.lifecycle.Observer { bitmap ->
                    tileSelectType = viewModel.selectedTile.imageURL
                    this.tileId = viewModel.selectedTile.id

                    tilesMap[tileId]?.let {
                        if (it.type == "Wall" && !isAlreadyShowWallAppliedOnlyDialog) {
                            isAlreadyShowWallAppliedOnlyDialog = true
                            MaterialAlertDialogBuilder(requireContext())
                                .setTitle(getString(R.string.wall_apply_only_message_dialog))
                                .setPositiveButton(getString(R.string.ok), null)
                                .show()
                        }
                    }

                    multiple = try {
                        val patA = viewModel.selectedTile.pattern.split("x".toRegex())
                            .dropLastWhile { it.isEmpty() }.toTypedArray()
                        val p = Integer.parseInt(patA[0])
                        p
                    } catch (e: Exception) {
                        1
                    }

                    val bitmapWithWhiteBorder = addWhiteBorder(bitmap, 4)

                    enableTileMode()

                    setTile(bitmapWithWhiteBorder)
                })
        }
    }

    private fun setScale(scale: Int) {
        this.scale = scale
        tileBitmap?.let {
            val bitmap = it.copy(it.config, it.isMutable)
            val scaleTemp = ((scaleX * scaleY + scaleX * scale) / scaleY - scaleX).toDouble()
            val tileMe = BitmapDrawable(
                resources,
                Bitmap.createScaledBitmap(
                    bitmap!!,
                    (scaleX + scaleTemp.toInt()) * multiple + rotationX.toInt() * 2,
                    (scaleY + scale) * multiple,
                    true
                )
            ).apply {
                tileModeX = Shader.TileMode.REPEAT
                tileModeY = Shader.TileMode.REPEAT
                setBounds(2, 2, 2, 2)
            }

            binding.imvRepeatTile.background = tileMe
        }
    }

    private var rotationX = 0f
    private var rotationY = 0f
    fun setPerspective(rotationX: Float) {
        // Condition
        tileBitmap?.let {
            val bitmap = it.copy(it.config, it.isMutable)
            val scaleTemp = ((scaleX * scaleY + scaleX * scale) / scaleY - scaleX).toDouble()
            val tileMe = BitmapDrawable(
                resources,
                Bitmap.createScaledBitmap(
                    bitmap!!,
                    (scaleX + scaleTemp.toInt()) * multiple,
                    (scaleY + scale) * multiple,
                    true
                )
            ).apply {
                tileModeX = Shader.TileMode.REPEAT
                tileModeY = Shader.TileMode.REPEAT
                setBounds(2, 2, 2, 2)
            }

            // Effect
            this.rotationX = rotationX.coerceIn(0F, 23.5F)
            binding.imvRepeatTile.pivotX = binding.imvRepeatTile.width / 2F
            binding.imvRepeatTile.pivotY =
                (multipliedTileLayoutHeight + defaultTileLayoutHeight) / 2F
            binding.imvRepeatTile.background = tileMe
            binding.imvRepeatTile.rotationX = rotationX.coerceIn(0F, 23.5F)
            binding.imvRepeatTile.rotationY = 0F
        }
    }

    fun setPerspectiveLeftWall(rotationY: Float) {
        // Condition
        tileBitmap?.let {
            val bitmap = it.copy(it.config, it.isMutable)
            val scaleTemp = ((scaleX * scaleY + scaleX * scale) / scaleY - scaleX).toDouble()
            val tileMe = BitmapDrawable(
                resources,
                Bitmap.createScaledBitmap(
                    bitmap!!,
                    (scaleX + scaleTemp.toInt()) * multiple,
                    (scaleY + scale) * multiple,
                    true
                )
            ).apply {
                tileModeX = Shader.TileMode.REPEAT
                tileModeY = Shader.TileMode.REPEAT
                setBounds(2, 2, 2, 2)
            }

            // Effect
            this.rotationY = rotationY.coerceIn(0F, 32F)
            binding.imvRepeatTile.pivotX = (multipliedTileLayoutWidth - defaultTileLayoutWidth) / 2F
            binding.imvRepeatTile.pivotY = binding.imvRepeatTile.height / 2F
            binding.imvRepeatTile.background = tileMe
            binding.imvRepeatTile.rotationX = 0F
            binding.imvRepeatTile.rotationY = rotationY.coerceIn(0F, 32F)
        }
    }

    fun setPerspectiveRightWall(rotationY: Float) {
        // Condition
        tileBitmap?.let {
            val bitmap = it.copy(it.config, it.isMutable)
            val scaleTemp = ((scaleX * scaleY + scaleX * scale) / scaleY - scaleX).toDouble()
            val tileMe = BitmapDrawable(
                resources,
                Bitmap.createScaledBitmap(
                    bitmap!!,
                    (scaleX + scaleTemp.toInt()) * multiple,
                    (scaleY + scale) * multiple,
                    true
                )
            ).apply {
                tileModeX = Shader.TileMode.REPEAT
                tileModeY = Shader.TileMode.REPEAT
                setBounds(2, 2, 2, 2)
            }

            // Effect
            this.rotationY = rotationY.absoluteValue.coerceIn(0F, 32F).unaryMinus()
            binding.imvRepeatTile.background = tileMe
            binding.imvRepeatTile.pivotX = (multipliedTileLayoutWidth + defaultTileLayoutWidth) / 2F
            binding.imvRepeatTile.pivotY = binding.imvRepeatTile.height / 2F
            binding.imvRepeatTile.rotationX = 0F
            binding.imvRepeatTile.rotationY = this.rotationY
        }
    }

    private fun showBottomSheet() {
        binding.includeBottomSheet.bottomSheet.visibility = View.VISIBLE
    }

    private fun disableMark() {
        binding.pinBackgroundImageView.changeTintColor(
            requireContext(),
            R.color.colorTileModeBackgroundDefault
        )
    }

    private fun beforeCapture() {
        binding.imvMove.visibility = View.GONE
    }

    private fun openSaveNameDialog(isShared: Boolean) {
        val originalMode = activity?.window?.attributes?.softInputMode
        activity?.window?.setSoftInputMode(
            WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING
        )

        val dialog = Dialog(requireContext()).apply {
            setTitle("Save")
            setContentView(R.layout.save_dialog)
        }

        val projectNameEditText = dialog.findViewById<EditText>(R.id.edtName)
        val btnSave = dialog.findViewById<Button>(R.id.btnSave)

        btnSave.setOnClickListener {
            val imm =
                projectNameEditText.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(projectNameEditText.windowToken, 0)
            dialog.dismiss()

            beforeCapture()
            saveProject(projectNameEditText.text.toString(), isShared)

            originalMode?.let {
                activity?.window?.setSoftInputMode(
                    it
                )
            }
        }

        dialog.show()
    }

    private fun saveProject(name: String, isShared: Boolean) {
        if (isShared) {
            val isDocumentUri = DocumentsContract.isDocumentUri(requireContext(), args.captureBitmapFilePath.toUri())
            if (isDocumentUri) {
                createShareProject(AppUtilites().createTransactionId(), args.captureBitmapFilePath.toUri(), name)
            } else {
                val contentURI = FileProvider.getUriForFile(
                    requireContext(),
                    "${requireContext().packageName}.FileProvider",
                    File(args.captureBitmapFilePath)
                )

                createShareProject(AppUtilites().createTransactionId(), contentURI, name)
            }
        } else {
            val bitmap = Screenshot.takeWithCanvas(binding.captureFrameLayout)
            val fileName = "${System.currentTimeMillis()}_HomePro_FitTile.jpg"
            val screenshotFile =
                ExternalFileManager.saveBitmapToFile(requireContext(), bitmap, fileName)
//            val projectId = tileSelectType + "_" + DateManager.getInstance().getDateWithFormat(DateManager.getInstance().currentDate())

//            viewModel.onSaveProject(Project(
//                    projectId = projectId,
//                    name = name,
//                    imagePath = screenshotFile.absolutePath,
//                    tileIds = tileId
//            ))

            var tileIds = args.tileIds
            tileIds = if (tileIds.isEmpty()) {
                tileId
            } else {
                "$tileIds,$tileId"
            }

            AppService().saveProject(
                id = tileSelectType + "_" + DateManager.getInstance()
                    .getDateWithFormat(DateManager.getInstance().currentDate()),
                name = name,
                tileId = tileIds,
                rawImage = args.roomImagePath,
                imagePath = screenshotFile.absolutePath,
                date = DateManager.getInstance().currentDate()
            )

            navigateToMyProjectFragment()
        }
    }

    private fun navigateToMyProjectFragment() {
        findNavController().navigate(FitTileFragmentDirections.actionFitTileFragmentToProjectFragment())
    }

    private fun showFilterDialog() {
        val tiles = tilesMap.values.toList()
        val tileSizes = tiles.filterNotNull().associateBy(
            { tile -> "${tile.width.toInt()}x${tile.length.toInt()}" },
            { tile -> Size(tile.width.toDouble(), tile.length.toDouble()) }
        )

        tileSizes.values.toList().also {
            val mutableShowTiles = it.toMutableList()
            mutableShowTiles.sortBy { tile -> tile.show }

            val showTilesString = mutableShowTiles.map { tile -> "${tile.show}cm." }.toMutableList()

            showTilesString.add(0, "All")

            val showTilesStringArray = showTilesString.toTypedArray()

            MaterialAlertDialogBuilder(requireContext())
                .setTitle(getString(R.string.choose_filter))
                .setItems(showTilesStringArray) { _, which ->
                    if (which == 0) {
                        viewModel.changeTilesByTypeAndSize(showTileType)
                    } else {
                        viewModel.changeTilesByTypeAndSize(
                            tileType = showTileType,
                            size = Size(it[which].width, it[which].length)
                        )
                    }
                }
                .show()
        }
    }

    private fun setRoomImage(captureBitmap: Bitmap) {
        binding.drawView.setRoomBitmap(captureBitmap)
        binding.drawView.invalidate()
    }

    private fun disableTileMode() {
        binding.pinRelativeLayout.isEnabled = false
        binding.perspectiveRelativeLayout.isEnabled = false
        binding.scaleRelativeLayout.isEnabled = false
        binding.dragRelativeLayout.isEnabled = false
        binding.sbScale.visibility = View.GONE
        binding.sbTilt.visibility = View.GONE
    }

    private fun hideBottomSheet() {
        binding.includeBottomSheet.bottomSheet.visibility = View.GONE
    }

    override fun setUploadProgress(progress: Int) {

    }

    override fun setStatus(obj: UploadImageManager.Status) {

    }

    override fun showError(ex: Exception) {

    }

    companion object {
        lateinit var captureBitmap: Bitmap
        private val DEFAULT_FILTER: TilesFilter = TilesFilter(TileType.ALL)
        const val SCAN_GET_TILE_REQUEST_KEY = "requestTile"
    }
}
