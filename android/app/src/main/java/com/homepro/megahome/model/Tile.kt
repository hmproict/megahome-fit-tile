package com.homepro.megahome.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "tile_table")
data class Tile(
    @PrimaryKey
    val id: String,

    @ColumnInfo(name = "short_description")
    @SerializedName("short_desc")
    val shortDescription: String,

    @ColumnInfo(name = "long_description")
    @SerializedName("long_desc")
    val longDescription: String,

    @SerializedName("width")
    @ColumnInfo(name = "width")
    val width: Int,

    @SerializedName("length")
    @ColumnInfo(name = "length")
    val length: Int,

    @SerializedName("ramdom")
    @ColumnInfo(name = "random")
    val random: String?,

    @SerializedName("box")
    @ColumnInfo(name = "box")
    val box: Double,

    @SerializedName("feature")
    @ColumnInfo(name = "feature")
    val feature: String,

    @SerializedName("vendor")
    @ColumnInfo(name = "vendor")
    val vendor: String,

    @SerializedName("pattern")
    @ColumnInfo(name = "pattern")
    val pattern: String,

    @SerializedName("type")
    @ColumnInfo(name = "type")
    val type: String,

    @SerializedName("image")
    @ColumnInfo(name = "image_url")
    val imageURL: String,

    @SerializedName("url")
    @ColumnInfo(name = "qr_url")
    val qrURL: String,

    @SerializedName("image_pattern")
    @ColumnInfo(name = "image_pattern")
    val imagePatternURLPath: String,

    @SerializedName("floor")
    @ColumnInfo(name = "floor")
    val floor: String,

    @SerializedName("wall")
    @ColumnInfo(name = "wall")
    val wall: String,

    @SerializedName("left_wall")
    @ColumnInfo(name = "left_wall")
    val leftWall: String,

    @SerializedName("right_wall")
    @ColumnInfo(name = "right_wall")
    val rightWall: String,

    @SerializedName("inch_width")
    @ColumnInfo(name = "inch_width")
    val inchWidth: Double,

    @SerializedName("inch_length")
    @ColumnInfo(name = "inch_length")
    val inchLength: Double,

    @SerializedName("category")
    @ColumnInfo(name = "category")
    val category: Int,

    @SerializedName("area")
    @ColumnInfo(name = "area")
    val area: String
)