package com.homepro.megahome.ui.projectdetail

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.core.widget.ContentLoadingProgressBar
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.request.target.CustomTarget
import com.google.android.material.button.MaterialButton
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.homepro.megahome.R
import com.homepro.megahome.api.AppService
import com.homepro.megahome.api.FitTileApi
import com.homepro.megahome.api.ServiceListener
import com.homepro.megahome.components.NavBarListener
import com.homepro.megahome.database.FitTileDatabase
import com.homepro.megahome.databinding.ProjectDetailFragmentBinding
import com.homepro.megahome.glide.GlideApp
import com.homepro.megahome.manager.AppUtilites
import com.homepro.megahome.manager.DateManager
import com.homepro.megahome.manager.ExternalFileManager
import com.homepro.megahome.model.Tile
import com.homepro.megahome.model.TileType
import com.homepro.megahome.realm.MyProject
import com.homepro.megahome.realm.ProjectShare
import com.homepro.megahome.realm.TileProject
import com.homepro.megahome.ui.fittile.FitTileFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Response
import java.io.File

class ProjectDetailFragment : Fragment() {
    private val navController by lazy {
        findNavController()
    }

    private val args: ProjectDetailFragmentArgs by navArgs()
    private lateinit var binding: ProjectDetailFragmentBinding
    private lateinit var viewModel: ProjectDetailViewModel

    private var listener: NavBarListener? = null
    private var tileId = ""
    private var projectId = ""
    private var transactionId = ""

    private var tile: TileProject? = null
    private var project: MyProject? = null
    private var imagePath = ""

    enum class DataType {
        TILE,
        PROJECT
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater, R.layout.project_detail_fragment, container, false
        )

        binding.btnFittile.visibility = View.VISIBLE

        // for observe LiveData
        binding.lifecycleOwner = this

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val application = requireNotNull(this.activity).application
        val dataSource = FitTileDatabase.getInstance(application).tileDatabaseDao
        val service = FitTileApi.service(requireContext())
        val viewModelFactory = ProjectDetailViewModelFactory(service, dataSource, application)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(ProjectDetailViewModel::class.java)

        val projectType =
            if (args.type.equals("tile", ignoreCase = true)) DataType.TILE else DataType.PROJECT
        when (projectType) {
            DataType.TILE -> {
                lifecycleScope.launch {
                    args.tileId?.let { tileId ->
                        val tile = viewModel.getTileById(tileId)
                        tile?.let {
                            prepareTileUI(tile)
                            binding.imvTryFit.setOnClickListener { openFitTile(tile.id) }
                        }
                    }
                }
            }

            DataType.PROJECT -> {
                args.project?.let { project ->
                    this.project = project
                    this.transactionId = project.transactionId!!

                    binding.beforePanelTransparentButton.setOnClickListener {
                        selectBeforeMenu(
                            project
                        )
                    }
                    binding.afterPanelTransparentButton.setOnClickListener { selectAfterMenu(project) }

                    prepareProjectUI(project)
                }
            }
        }

        initEvent()
    }

    var shouldStopLoop = false
    var mHandler = Handler()

    private fun receiver() {

        mHandler.post(object : Runnable {
            override fun run() {
                receiveStatusProject()
                if (!shouldStopLoop) {
                    mHandler.postDelayed(this, 5000)
                }
            }

        })

    }

    private fun initEvent() {
        binding.btnBuyNow.setOnClickListener { openBuyNow() }

        binding.layoutRemove.setOnClickListener { removeAll() }

        binding.layoutSeeMore.setOnClickListener { openSeeMore() }

        binding.layoutBack.setOnClickListener {
            findNavController().navigateUp()
        }

        binding.tvOpenBarcode.setOnClickListener { openBarcodeDialog() }

        binding.btnReceive.setOnClickListener { receiveStatusProject() }

        binding.btnFittile.setOnClickListener { fitTileProject() }
    }

    private fun prepareTileUI(tile: Tile) {
        val goneList = mutableListOf(
            binding.layoutButton2,
            binding.layoutQr,
            binding.beforeAfterConstraintLayout,
            binding.layoutButton,
            binding.layoutRemove
        )
        val visibleList = mutableListOf(binding.layoutDetail, binding.layoutSeeMore)

        GlideApp.with(this).load(tile.imageURL).into(binding.imvImage).also {
            binding.loadingPanel.visibility = View.GONE
        }

        binding.layout.removeAllViewsInLayout()
        val tileCell = createTileCell(tile)
        binding.layout.addView(tileCell)

        goneList.forEach { view -> view.visibility = View.GONE }
        visibleList.forEach { view -> view.visibility = View.VISIBLE }

//        tvDateSet.text = "-"
//        tvNumberSet.text = tile.id
//        tvBrandSet.text = tile.vendor
//        tvSizeSet.text = "${tile.length} x ${tile.width} ${getString(R.string.text_cm)}"
//        tvProductSet.text = tile.shortDescription
//        tvFeaturedSet.text = tile.feature
    }

    private fun prepareProjectUI(project: MyProject) {
        val goneList = mutableListOf<View>(binding.imvTryFit)
        val visibleList = mutableListOf<View>()

        when (project.type) {
            "project" -> {
                with(goneList) {
                    this.add(binding.layoutButton2)
                    this.add(binding.layoutQr)
                }
                with(visibleList) {
                    this.add(binding.beforeAfterConstraintLayout)
                    this.add(binding.layoutButton)
                    this.add(binding.layoutDetail)
                }

                imagePath = project.imagePath!!

                selectAfterMenu(project)

//                GlideApp.with(this).load(file).skipMemoryCache(false).centerInside().into(imvImage).also {
//                    binding.loadingPanel.visibility = View.GONE
//                }

                val tileIds = project.tileId?.split(",")?.toSet()
                tileIds?.let { it ->
                    binding.layout.removeAllViewsInLayout()
                    with(goneList) {
                        this.add(binding.edge)
                        this.add(binding.layoutButton)
                    }

                    visibleList.remove(binding.layoutButton)

                    val isHaveRawImage = (project.rawImage?.length ?: 0) != 0
                    if (!isHaveRawImage) {
                        visibleList.remove(binding.beforeAfterConstraintLayout)
                        goneList.add(binding.beforeAfterConstraintLayout)
                    }

                    it.forEach { tileId ->
                        lifecycleScope.launch {
                            val tile = viewModel.getTileById(tileId)
                            tile?.let { tileData ->
                                val tileCell = createTileCell(tileData)
                                binding.layout.addView(tileCell)
                            }
                        }

                        // side effect
                        projectId = project.id.toString()
                        // end side effect

                        goneList.add(binding.layoutSeeMore)
                        visibleList.add(binding.layoutRemove)

                        binding.projectNameTextView.text = project.productName
                    }
                }
            }
            "share" -> {
                with(goneList) {
                    this.add(binding.beforeAfterConstraintLayout)
                    this.add(binding.layoutButton)
                    this.add(binding.layoutDetail)
                    this.add(binding.tvOpenBarcode)
                    this.add(binding.btnFittile)
                    this.add(binding.btnReceive)
                }

                with(visibleList) {
                    this.add(binding.layoutButton2)
                    this.add(binding.layoutRemove)
                }

                // side effect
                imagePath = project.rawImage!!
                projectId = project.id.toString()
                // end side effect

                when (project.stage) {
                    "initial" -> {
                        with(visibleList) {
                            this.add(binding.layoutQr)
                            this.add(binding.btnReceive)
                        }

                        GlideApp.with(this).load(project.rawImage).into(binding.imvImage).also {
                            binding.loadingPanel.visibility = View.GONE
                        }

                        val bitmap = AppUtilites().createQRcode(project.qrUrl)
                        binding.imvQr.setImageBitmap(bitmap)

//                        receiver()
                    }
                    "fittile" -> {
                        with(goneList) {
                            this.add(binding.layoutQr)
                        }

                        with(visibleList) {
                            this.add(binding.btnFittile)
                        }

                        GlideApp.with(this).load(project.rawImage).into(binding.imvImage).also {
                            binding.loadingPanel.visibility = View.GONE
                        }
                    }
                }
            }
        }

        goneList.forEach { view -> view.visibility = View.GONE }
        visibleList.forEach { view -> view.visibility = View.VISIBLE }
    }

    private fun createTileCell(tile: Tile): View {
        val tileCellView = layoutInflater.inflate(R.layout.tile_detail_cell, null)
        val tileType: TileType = if (tile.type == "Wall") TileType.WALL else TileType.FLOOR

        if (tileType == TileType.WALL) {
            tileCellView.findViewById<MaterialButton>(R.id.calculateButton).visibility = View.GONE
        }

        tileCellView.findViewById<AppCompatTextView>(R.id.createdDateTextView).visibility =
            View.INVISIBLE
        tileCellView.findViewById<AppCompatTextView>(R.id.createdDateColonTextView).visibility =
            View.INVISIBLE
        tileCellView.findViewById<AppCompatTextView>(R.id.createdDateDetailTextView).visibility =
            View.INVISIBLE
        tileCellView.findViewById<AppCompatTextView>(R.id.artNumberDetailTextView).text = tile.id
        tileCellView.findViewById<AppCompatTextView>(R.id.brandDetailTextView).text = tile.vendor
        tileCellView.findViewById<AppCompatTextView>(R.id.sizeDetailTextView).text =
            getString(R.string.size_format, tile.length.toString(), tile.width.toString())
        tileCellView.findViewById<AppCompatTextView>(R.id.featureDetailTextView).text = tile.feature
        tileCellView.findViewById<AppCompatTextView>(R.id.nameDetailTextView).text =
            tile.shortDescription

        tileCellView.findViewById<MaterialButton>(R.id.calculateButton).setOnClickListener {
            viewModel.onCalculateClicked(tile.id)
        }

        viewModel.navigateToCalculate.observe(viewLifecycleOwner, Observer { tileId ->
            tileId?.let {
                navController.navigate(
                    ProjectDetailFragmentDirections.actionProjectDetailFragmentToCalculateFragment(
                        tileId = tileId
                    )
                )
                viewModel.onNavigateToCalculate()
            }
        })

        tileCellView.findViewById<AppCompatButton>(R.id.buyNowButton).setOnClickListener {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://www.homepro.co.th/p/${tile.id}?utm_source=fittile&utm_medium=bn_project")
                )
            )
        }

        tileCellView.findViewById<AppCompatButton>(R.id.roomSceneButton).setOnClickListener {
            if (tile.qrURL.isNotEmpty() && tile.qrURL.isNotBlank()) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(tile.qrURL)))
            } else {
                val megaHomeURL = "https://www.homepro.co.th/"
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(megaHomeURL)))
            }

        }

        return tileCellView
    }

    private fun selectBeforeMenu(project: MyProject) {
        binding.iconBeforeImageView.setColorFilter(
            ContextCompat.getColor(
                requireContext(),
                R.color.colorAccent
            ), PorterDuff.Mode.SRC_IN
        )
        binding.beforeTextView.setTextColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.colorAccent
            )
        )

        binding.iconAfterImageView.setColorFilter(
            ContextCompat.getColor(
                requireContext(),
                R.color.colorTextOnWhite
            ), PorterDuff.Mode.SRC_IN
        )
        binding.afterTextView.setTextColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.colorTextOnWhite
            )
        )

        project.rawImage?.let { imagePath ->
            showBeforeImage(imagePath)
        }
    }

    private fun showBeforeImage(imagePath: String) {
        binding.loadingPanel.visibility = View.VISIBLE

        if (imagePath.startsWith("content")) {
            GlideApp.with(this).load(Uri.parse(imagePath)).skipMemoryCache(false).centerInside()
                .into(binding.imvImage).also {
                    binding.loadingPanel.visibility = View.GONE
                }
        } else if (imagePath.startsWith("http")) {
            GlideApp.with(this).load(imagePath).skipMemoryCache(false).centerInside()
                .into(binding.imvImage).also {
                    binding.loadingPanel.visibility = View.GONE
                }
        } else {
            GlideApp.with(this).load(File(imagePath)).skipMemoryCache(false).centerInside()
                .into(binding.imvImage).also {
                    binding.loadingPanel.visibility = View.GONE
                }
        }
    }

    private fun selectAfterMenu(project: MyProject) {
        binding.iconBeforeImageView.setColorFilter(
            ContextCompat.getColor(
                requireContext(),
                R.color.colorTextOnWhite
            ), PorterDuff.Mode.SRC_IN
        )
        binding.beforeTextView.setTextColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.colorTextOnWhite
            )
        )

        binding.iconAfterImageView.setColorFilter(
            ContextCompat.getColor(
                requireContext(),
                R.color.colorAccent
            ), PorterDuff.Mode.SRC_IN
        )
        binding.afterTextView.setTextColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.colorAccent
            )
        )

        project.imagePath?.let { imagePath ->
            showAfterImage(imagePath)
        }
    }

    private fun showAfterImage(imagePath: String) {
        binding.loadingPanel.visibility = View.VISIBLE

        GlideApp.with(this).load(File(imagePath)).skipMemoryCache(false).centerInside()
            .into(binding.imvImage).also {
            binding.loadingPanel.visibility = View.GONE
        }
    }

    private fun fitTileProject() {
        GlideApp.with(requireContext())
            .asBitmap()
            .load(imagePath)
            .into(object : CustomTarget<Bitmap>() {
                override fun onLoadCleared(placeholder: Drawable?) {

                }

                override fun onResourceReady(
                    resource: Bitmap,
                    transition: com.bumptech.glide.request.transition.Transition<in Bitmap>?
                ) {
                    startTileActivity(resource, imagePath.toUri(), imagePath)
                }

            })
    }

    private fun startTileActivity(bitmap: Bitmap, path: Uri, str: String) {
        val direction: NavDirections = if (tile != null) {
            FitTileFragment.captureBitmap = bitmap
            ProjectDetailFragmentDirections.actionProjectDetailFragmentToFitTileFragment(
                isShare = true,
                transactionId = transactionId,
                data = "tile",
                tileProject = tile!!,
                captureBitmapFilePath = str,
                tileIds = "",
                roomImagePath = str
            )
        } else {
            FitTileFragment.captureBitmap = bitmap
            ProjectDetailFragmentDirections.actionProjectDetailFragmentToFitTileFragment(
                isShare = true,
                transactionId = transactionId,
                captureBitmapFilePath = str,
                tileIds = "",
                roomImagePath = str
            )
        }

        findNavController().navigate(direction)
    }


    fun getListener(): NavBarListener {
        return this.listener!!
    }

    fun setListener(listener: NavBarListener) {
        this.listener = listener
    }

    private fun openBarcodeDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setView(R.layout.for_sale_barcode_dialog)
            .show()
    }

    private fun receiveStatusProject() {
        AppService().getProject(object : ServiceListener<ProjectShare> {

            override fun onFailure(call: Call<ProjectShare>, t: Throwable) {
            }

            override fun onResponse(call: Call<ProjectShare>, response: Response<ProjectShare>) {
                val share = response.body()!!
                val projectS = share.project
                if (projectS?.stage.equals("complete")) {
                    shouldStopLoop = true
                    GlideApp.with(requireContext())
                        .asBitmap()
                        .load(projectS?.fittile_image)
                        .into(object : CustomTarget<Bitmap>() {
                            override fun onResourceReady(
                                bitmap: Bitmap,
                                transition: com.bumptech.glide.request.transition.Transition<in Bitmap>?
                            ) {
                                val fileName = "${System.currentTimeMillis()}_HomePro_FitTile.jpg"
                                val imageFile = ExternalFileManager.saveBitmapToFile(
                                    context!!,
                                    bitmap,
                                    fileName
                                )

                                projectS?.fittile_image = imageFile.absolutePath
                                projectS?.id = transactionId

                                updateProject(projectS!!)
                                binding.layoutButton2.visibility = View.GONE
                                binding.layoutQr.visibility = View.GONE
                                binding.layoutButton.visibility = View.VISIBLE
                                binding.layoutDetail.visibility = View.VISIBLE
                                GlideApp.with(context!!).load(projectS.fittile_image)
                                    .skipMemoryCache(false).centerInside().into(binding.imvImage)

                                val goneList = mutableListOf<View>(binding.imvTryFit)
                                with(goneList) {
                                    this.add(binding.layoutButton)
                                    this.add(binding.layoutButton2)
                                    this.add(binding.layoutQr)
                                }

                                val tileIds = projectS.tile_id?.split(",")?.toSet()
                                tileIds?.let {
                                    binding.layout.removeAllViewsInLayout()
                                    it.forEach { tileId ->
                                        val contentLoadingProgressBar = layoutInflater.inflate(
                                            R.layout.content_loading_progress_bar,
                                            binding.layout,
                                            false
                                        ) as ContentLoadingProgressBar
                                        binding.layout.addView(contentLoadingProgressBar)
                                        contentLoadingProgressBar.show()

                                        lifecycleScope.launch(Dispatchers.Main) {
                                            val tile = viewModel.getTileById(tileId)
                                            tile?.let {
                                                val tileCell = createTileCell(tile)
                                                binding.layout.addView(tileCell)
                                            }
                                            contentLoadingProgressBar.hide()
                                            binding.layout.removeView(contentLoadingProgressBar)
                                        }
                                    }
                                }

                                goneList.forEach { view -> view.visibility = View.GONE }
                            }

                            override fun onLoadCleared(placeholder: Drawable?) {
                            }
                        })
                }
            }
        }, transactionId)
    }

    private fun updateProject(project: ProjectShare.Project) {
        AppService().updateProject(
            project.id!!,
            project.tile_id!!,
            project.fittile_image!!
        )
    }

    private fun openSeeMore() {
        if (tile != null) {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(tile!!.url)))
        }
    }

    private fun removeAll() {
        removeProject(projectId)
        findNavController().navigateUp()
    }


    private fun removeProject(projectId: String) {
        AppService().removeProject(projectId)
    }

    private fun openFitTile(tileId: String) {
        navController.navigate(
            ProjectDetailFragmentDirections.actionProjectDetailFragmentToCameraFragment(
                tileIdFromCatalog = tileId
            )
        )
    }

    private fun openBuyNow() {
        startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://www.homepro.co.th/p/$tileId?utm_source=fittile&utm_medium=bn_project")
            )
        )
    }
}