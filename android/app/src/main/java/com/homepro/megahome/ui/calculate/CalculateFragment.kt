package com.homepro.megahome.ui.calculate

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.homepro.megahome.R
import com.homepro.megahome.api.FitTileApi
import com.homepro.megahome.api.FitTileApiService
import com.homepro.megahome.components.OnTabListener
import com.homepro.megahome.database.FitTileDatabase
import com.homepro.megahome.database.TileLocalCache
import com.homepro.megahome.database.TileSizeLocalCache
import com.homepro.megahome.databinding.CalculateFragmentBinding
import com.homepro.megahome.model.Size
import com.homepro.megahome.model.Tile
import com.homepro.megahome.model.TileSize
import com.homepro.megahome.model.TileType
import kotlinx.coroutines.*
import java.text.NumberFormat

class CalculateFragment : Fragment() {

    private var extraTile: String? = null
    private var layout2Tab = 0
    private var layout3Tab = 0
    private var layout1Tab = 0
    private var tileId = ""
    private var adapter: ArrayAdapter<String>? = null

    private var tileSizes = ArrayList<TileSize>()

    private lateinit var binding: CalculateFragmentBinding
    private lateinit var viewModel: CalculateViewModel
    private val args by navArgs<CalculateFragmentArgs>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate<CalculateFragmentBinding>(inflater, R.layout.calculate_fragment, container, false)

        binding.tabViewTileSize.setTextLeft(getString(R.string.message_by_area_size))
        binding.tabViewTileSize.setTextRight(getString(R.string.message_by_lw))

        binding.tabViewTileAdd.setTextLeft(getString(R.string.text_add_extra))
        binding.tabViewTileAdd.setTextRight(getString(R.string.text_no_extra))

        binding.layoutBuy.visibility = View.GONE
        binding.btnCalculate.visibility = View.VISIBLE
        binding.myRadioGroup.check(R.id.radio2)

        showLayout1(0)

        requestTileSize { list ->

            val format = NumberFormat.getIntegerInstance()
            val sizes = list.map { getString(
                R.string.tile_size_cm,
                format.format(it.cmSize.width) , format.format(it.cmSize.length)
            ) }.distinct()

            val adapter = ArrayAdapter(
                requireContext(),
                R.layout.dropdown_menu_popup_item,
                sizes
            )

            binding.sizeOutlinedDropdown.setAdapter(adapter)
        }

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val application = requireActivity().application
        val dataSource = FitTileDatabase.getInstance(application).tileDatabaseDao
        val viewModelFactory = CalculateViewModelFactory(dataSource, application)
        viewModel = ViewModelProvider(this, viewModelFactory).get(CalculateViewModel::class.java)

        initSpinner()

        showLayout2(layout2Tab)

        showLayout3(layout3Tab)

        initEvent()

        readFromSafeArgs(args)
    }

    private fun showLayout1(tab: Int) {
        layout1Tab = tab
        showCalculate()
        if (tab == 0) {
            binding.layout1Left.visibility = View.VISIBLE
            binding.layout1Right.visibility = View.GONE
        } else {
            binding.layout1Left.visibility = View.GONE
            binding.layout1Right.visibility = View.VISIBLE
        }
    }

    private fun showCalculate() {
        binding.layoutBuy.visibility = View.GONE
        binding.btnCalculate.visibility = View.VISIBLE
    }

    private fun readFromSafeArgs(safeArgs: CalculateFragmentArgs) {
        safeArgs.tileId?.let { tileId ->
            lifecycleScope.launch {
                val tile: Tile? = viewModel.getTileById(tileId)
                tile?.let { showTile(it) }
            }
        }
    }

    private fun initEvent() {
        binding.imvClose.setOnClickListener { showLayout1(0) }

        binding.layoutCatelog.setOnClickListener {
            findNavController().navigate(CalculateFragmentDirections.actionCalculateFragmentToCatalogFragment(isFromCalculate = true))
        }

        binding.tabViewTileSize.setOnTabListener(object : OnTabListener {
            override fun onTabLeft() {
                layout2Tab = 0
                showLayout2(layout2Tab)
            }

            override fun onTabRight() {
                layout2Tab = 1
                showLayout2(layout2Tab)
            }
        })

        binding.tabViewTileAdd.setOnTabListener(object : OnTabListener {
            override fun onTabLeft() {
                extraTile = null
                layout3Tab = 0
                showLayout3(layout3Tab)

            }

            override fun onTabRight() {
                extraTile = "5"
                layout3Tab = 1
                showLayout3(layout3Tab)
            }
        })

        binding.btnCalculate.setOnClickListener { calculate() }

        binding.btnBuy.setOnClickListener { startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://www.homepro.co.th/p/$tileId?utm_source=fittile&utm_medium=bn_project"))) }


        binding.edtWidth.setOnEditorActionListener { _, _, _ ->
            showCalculate()
            false
        }

        binding.edtHeigth.setOnEditorActionListener { _, _, _ ->
            showCalculate()
            false
        }

        binding.edtTileSize.setOnEditorActionListener { _, _, _ ->
            showCalculate()
            false
        }

        binding.myRadioGroup.setOnCheckedChangeListener { _, _ -> showCalculate() }
    }

    private fun showTile(tile: Tile) {
        tileId = tile.id
        Glide.with(this).load(tile.imageURL).into(binding.imvTile)
        binding.tvSizeSet.text = tile.width.toString()
        binding.tvNameSet.text = tile.shortDescription
        binding.tvTileSet.text = tile.vendor

        showLayout1(1)
    }

    private fun calculate() = runBlocking {
        val id: String? = if (layout1Tab == 0) {
            val component = binding.sizeOutlinedDropdown.text.split(" ").first().split("x")
            val width = component.first().toDouble()
            val length = component.last().toDouble()
            viewModel.getTileBySize(Size(width, length))?.id
        } else {
            tileId
        }

        val extraTile = getExtraTile();

        try {
            if (id == null) {
                throw NullPointerException()
            }

            val calculateResponse = withContext(Dispatchers.IO) {
                if (layout2Tab == 0) {
                    FitTileApi.service(requireContext()).calculateArea(id, getSquareM().toInt(), extraTile)
                } else {
                    FitTileApi.service(requireContext()).calculate(id, getHeight().toInt(), getWidth().toInt(), extraTile)
                }
            }

            binding.btnCalculate.visibility = View.GONE
            binding.layoutBuy.visibility = View.VISIBLE
            binding.tvBox.text = calculateResponse.box
        } catch (e: Exception) {
            MaterialAlertDialogBuilder(requireContext())
                .setTitle(getString(R.string.error))
                .setMessage(getString(R.string.error_cannot_call_api))
                .setPositiveButton(getString(R.string.ok), null)
                .show()
        }
    }

    private fun getWidth(): String {
        return binding.edtWidth.text.toString()
    }

    private fun getHeight(): String {
        return binding.edtHeigth.text.toString()
    }


    private fun getSquareM(): String {
        return binding.edtTileSize.text.toString()
    }


    private fun getExtraTile(): String {
        if (layout3Tab == 1) return "0"
        return when (binding.myRadioGroup.checkedRadioButtonId) {
            R.id.radio1 -> {
                "5"
            }
            R.id.radio2 -> {
                "10"
            }
            R.id.radio3 -> {
                "15"
            }
            else -> "0"
        }
    }

    private fun showLayout3(tab: Int) {
        showCalculate()
        if (tab == 0) {
            binding.layout3Left.visibility = View.VISIBLE
            binding.layout3Right.visibility = View.GONE
        } else {
            binding.layout3Left.visibility = View.GONE
            binding.layout3Right.visibility = View.VISIBLE
        }
    }

    private fun showLayout2(tab: Int) {
        showCalculate()
        if (tab == 0) {
            binding.layout2Left.visibility = View.VISIBLE
            binding.layout2Right.visibility = View.GONE
        } else {
            binding.layout2Left.visibility = View.GONE
            binding.layout2Right.visibility = View.VISIBLE
        }
    }

    private fun initSpinner() {
        binding.sizeOutlinedDropdown.inputType = InputType.TYPE_NULL
        binding.sizeOutlinedDropdown.setOnItemClickListener { _, _, _, _ ->
            showCalculate()
        }
    }

    private fun requestTileSize(complete: (ArrayList<TileSize>) -> Unit) = runBlocking {

        val service = FitTileApi.service(requireContext())

        val application = requireActivity().application
        val sizeDataSource = FitTileDatabase.getInstance(application).sizeDatabaseDao
        val sizeCache =  TileSizeLocalCache(sizeDataSource)

        try {
            val tileSizeResponse = withContext(Dispatchers.IO) {
                service.tileSize()
            }
            tileSizes.addAll(tileSizeResponse.sizes)
            sizeCache.insert(tileSizeResponse.sizes) {
                    activity?.runOnUiThread {
                        complete(tileSizes)
                    }
            }

        } catch (e: Exception) {

            Log.v("FitTile service", "Tile size Error : " + e.localizedMessage)
            sizeCache.sizes(TileType.ALL).observe(viewLifecycleOwner, Observer {
                    tileSizes.addAll(it)
                    activity?.runOnUiThread {
                        if (tileSizes.isEmpty()) {
                            MaterialAlertDialogBuilder(requireContext())
                                .setTitle(getString(R.string.error))
                                .setMessage(getString(R.string.error_cannot_call_api))
                                .setPositiveButton(getString(R.string.ok), null)
                                .show()
                        } else {
                            complete(tileSizes)
                        }
                    }
                })
        }
    }


}