package com.homepro.megahome.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.homepro.megahome.model.Project
import com.homepro.megahome.model.Tile
import com.homepro.megahome.model.TileSize

@Database(
    entities = [
        Project::class,
        Tile::class,
        TileSize::class
    ], version = 3, exportSchema = false
)
abstract class FitTileDatabase : RoomDatabase() {
    abstract val projectDatabaseDao: ProjectDatabaseDao
    abstract val tileDatabaseDao: TileDatabaseDao
    abstract val sizeDatabaseDao: TileSizeDatabaseDao

    companion object {
        @Volatile
        private var INSTANCE: FitTileDatabase? = null

        fun getInstance(context: Context): FitTileDatabase {
            synchronized(this) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        FitTileDatabase::class.java,
                        "fit_tile_database"
                    )
                        .fallbackToDestructiveMigration()
                        .build()

                    INSTANCE = instance
                }

                return instance
            }
        }
    }
}