package com.homepro.megahome.ui.projectdetail

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.homepro.megahome.api.FitTileApiService
import com.homepro.megahome.database.TileDatabaseDao

class ProjectDetailViewModelFactory(
    private val service: FitTileApiService,
    private val dataSource: TileDatabaseDao,
    private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ProjectDetailViewModel::class.java)) {
            return ProjectDetailViewModel(service, dataSource, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}