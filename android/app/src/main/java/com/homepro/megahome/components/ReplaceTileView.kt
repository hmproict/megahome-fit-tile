package com.homepro.megahome.components

import android.app.Activity
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.homepro.megahome.model.TileEvent
import java.util.*
import kotlin.math.absoluteValue
import kotlin.math.sqrt

interface DrawViewListener {
    fun onSelectComplete()
}

interface OnMoveListener {
    fun onMove(posX: Int, posY: Int)
}

class ReplaceTileView : View, View.OnTouchListener {
    private val points: MutableList<PointF> = ArrayList()
    private val strokePaint = Paint()
    private val newLine: MutableList<Int> = ArrayList()
    private var drawAreaComplete = false
    private var pointIndexSelect = -1
    private var drawViewListener: DrawViewListener? = null
    private var mActivePointerId = MotionEvent.INVALID_POINTER_ID
    private var mLastTouchX = 0f
    private var mLastTouchY = 0f
    private var mPosX = 0
    private var mPosY = 0
    private var tileEventId = TileEvent.PIN_AREA
    private var screenSize: android.graphics.Point? = null
    private var screenWidth = 0
    private var screenHeight = 0
    private val screenLeft = 0
    private val screenTop = 0
    private var roomBitmap: Bitmap? = null
    private var rotateBitmap: Bitmap? = null
    var gap = 0
    private var desity = 1f
    private var onMoveListener: OnMoveListener? = null

    private val path: Path by lazy {
        Path()
    }

    private val pinArea: Rect by lazy {
        Rect(0, 0, this.width, this.height)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        isFocusable = true
        isFocusableInTouchMode = true
        isClickable = true
        init()
    }

    constructor(context: Context?) : super(context) {
        isFocusable = true
        isFocusableInTouchMode = true
        init()
    }

    val imageWidth: Int
        get() = roomBitmap!!.width

    val imageHeight: Int
        get() = roomBitmap!!.height

    private fun init() {
        setOnTouchListener(this)
        val display = (context as Activity).windowManager.defaultDisplay
        screenSize = android.graphics.Point()
        display.getSize(screenSize)
        screenWidth = screenSize!!.x + 3
        screenHeight = screenSize!!.y + 3

        strokePaint.apply {
            color = Color.RED
            isAntiAlias = true
            style = Paint.Style.STROKE
            strokeWidth = 4f
        }

        desity = resources.displayMetrics.density
    }

    fun setOnMoveListener(onMoveListener: OnMoveListener?) {
        this.onMoveListener = onMoveListener
    }

    fun setRoomBitmap(roomBitmap: Bitmap?) {
        this.roomBitmap = roomBitmap
        rotateBitmap = roomBitmap
    }

    fun setColor(color: Int) {
        strokePaint.color = color
    }

    fun setTileEventId(id: Int) {
        tileEventId = id
    }

    public override fun onDraw(canvas: Canvas) {
        path.reset()
        path.fillType = Path.FillType.EVEN_ODD

        points.indices.forEach { index ->
            val point = points[index]

            if (index != 0 && comparePoint(points[0], points[index])) {
                return@forEach
            }

            if (index == 0) {
                path.moveTo(point.x, point.y)
            } else {
                path.lineTo(point.x, point.y)
            }
        }

        if (drawAreaComplete) {
            path.close()
        }

        if (tileEventId == TileEvent.PIN_AREA) {
            val rect = pinArea
            canvas.drawBitmap(rotateBitmap!!, null, rect, strokePaint)

            points.indices.forEach { index ->
                val point = points[index]
                if (index != 0 && comparePoint(points[0], points[index])) {
                    return@forEach
                }

                drawCircleFill(canvas, point.x, point.y)
            }

            canvas.drawPath(path, strokePaint)
        } else {
            val cropBitmap = getCropImage(rotateBitmap, path)
            canvas.drawBitmap(cropBitmap, 0f, 0f, strokePaint)
        }
    }

    private fun calculateGap(): Double {
        return if (isScreenPortrait) {
            val yGap = screenHeight - roomBitmap!!.height.toDouble()
            yGap / 2
        } else {
            val xGap = screenWidth - roomBitmap!!.width.toDouble()
            xGap / 2
        }
    }

    private fun resizeWithOrientation(): Bitmap {
        return if (isScreenPortrait) {
            val h = screenWidth * roomBitmap!!.height / roomBitmap!!.width
            Bitmap.createScaledBitmap(roomBitmap!!, screenWidth, h, false)
        } else {
            val w = screenHeight * roomBitmap!!.width / roomBitmap!!.height
            Bitmap.createScaledBitmap(roomBitmap!!, w, screenHeight, false)
        }
    }

    private val isScreenPortrait: Boolean
        get() = resources.configuration.orientation == 1

    private val isPosChange: Boolean
        get() = mLastTouchX != mPosX.toFloat() && mLastTouchY != mPosY.toFloat()

    private fun getCropImage(src: Bitmap?, path: Path): Bitmap {
        val output = Bitmap.createBitmap(this.width, this.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(output)
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.color = -0x1000000

        // draw destination
        canvas.drawPath(path, paint)
        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_OUT)

//        val rect = Rect(0, 0, this.width * 3, this.height * 3)
        val rect = pinArea

        // draw source
        canvas.drawBitmap(src!!, null, rect, paint)
        return output
    }

    private fun comparePoint(p1: PointF, p2: PointF): Boolean {
        if (points.size < 3) return false
        return (p1.x - p2.x).absoluteValue < 20 && (p1.y - p2.y).absoluteValue < 20
    }

    private fun drawCircleFill(canvas: Canvas, x: Float, y: Float) {
        if (points.size > 1) {
            if (points[0].x == x && points[0].y == y) {
                val paint = Paint()
                paint.color = Color.RED
                paint.style = Paint.Style.FILL
                canvas.drawCircle(x, y, 9 * desity, paint)
                return
            }
        }
        val paint = Paint()
        paint.color = Color.RED
        paint.style = Paint.Style.FILL
        canvas.drawCircle(x, y, 6 * desity, paint)
    }

    fun dragPlanEvent(ev: MotionEvent?): Boolean {
        when (ev?.action) {
            MotionEvent.ACTION_DOWN -> {
                val x = ev.x
                val y = ev.y
                mLastTouchX = x
                mLastTouchY = y
                mActivePointerId = ev.getPointerId(0)
            }
            MotionEvent.ACTION_MOVE -> {
                val x = ev.x
                val y = ev.y
                // Calculate the distance moved
                val dx = x - mLastTouchX
                val dy = y - mLastTouchY
                mPosX += dx.toInt()
                mPosY += dy.toInt()
                //                bitmapRecycle = getCroppedBitmap(tiltTile);
                onMoveListener!!.onMove(mPosX, mPosY)
                //                invalidate();
                mLastTouchX = x
                mLastTouchY = y
            }
            MotionEvent.ACTION_UP -> {
                mActivePointerId = MotionEvent.INVALID_POINTER_ID
            }
            MotionEvent.ACTION_CANCEL -> {
                mActivePointerId = MotionEvent.INVALID_POINTER_ID
            }
            MotionEvent.ACTION_POINTER_UP -> {
                val pointerIndex = ev.actionIndex
                val pointerId = ev.getPointerId(pointerIndex)
                if (pointerId == mActivePointerId) {
                    val newPointerIndex = if (pointerIndex == 0) 1 else 0
                    mLastTouchX = ev.x
                    mLastTouchY = ev.y
                    mActivePointerId = ev.getPointerId(newPointerIndex)
                }
            }
        }
        return true
    }

    override fun onTouch(view: View, event: MotionEvent): Boolean {
        val point = PointF(event.x, event.y)
        if (tileEventId == TileEvent.PIN_AREA) {
            if (event.action == MotionEvent.ACTION_DOWN) {
                if (!drawAreaComplete) {
                    if (checkDuplicatePoint(point) != null) { //                        points.add(checkDupicatePoint(point));
                        drawAreaComplete = true
                        drawViewListener!!.onSelectComplete()
                    } else {
                        points.add(point)
                    }
                    invalidate()
                    newLine.add(points.size)
                } else {
                    if (checkDuplicatePointIndex(point) != -1) {
                        pointIndexSelect = checkDuplicatePointIndex(point)
                    }
                }
            } else if (event.action == MotionEvent.ACTION_UP) {
                pointIndexSelect = -1
            } else if (event.action == MotionEvent.ACTION_MOVE) {
                if (pointIndexSelect != -1) {
                    points[pointIndexSelect] = point
                    invalidate()
                }
            }
        }
        if (tileEventId == TileEvent.ACTION_TILE) dragPlanEvent(event)
        return true
    }

    fun setComplete(ttt: Boolean) {
        drawAreaComplete = ttt
    }

    private fun checkDuplicatePoint(point: PointF): PointF? {
        if (points.size <= 3) return null
        val i = 0
        val centerX = points[i].x + 40.toDouble()
        val centerY = points[i].y + 40.toDouble()

        val radCircle =
            sqrt((centerX - point.x) * (centerX - point.x) + (centerY - point.y) * (centerY - point.y))

        return if (radCircle <= 60) points[i] else null
    }

    private fun checkDuplicatePointIndex(point: PointF): Int {
        for (i in points.indices) {
            val centerX = points[i].x + 35.toDouble()
            val centerY = points[i].y + 35.toDouble()
            val radCircle =
                Math.sqrt((centerX - point.x) * (centerX - point.x) + (centerY - point.y) * (centerY - point.y))
            if (radCircle <= 40) {
                return i
            }
        }
        return -1
    }

    fun setDrawViewListener(drawViewListener: DrawViewListener) {
        this.drawViewListener = drawViewListener
    }

    companion object {
        private const val TAG = "DrawView"
    }
}

internal class Point {
    var x = 0f
    var y = 0f
    override fun toString(): String {
        return "$x, $y"
    }
}