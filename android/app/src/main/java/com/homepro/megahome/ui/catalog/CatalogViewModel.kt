package com.homepro.megahome.ui.catalog

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.PagedList
import com.homepro.megahome.api.FitTileApiService
import com.homepro.megahome.data.TileRepository
import com.homepro.megahome.database.TileDatabaseDao
import com.homepro.megahome.model.*
import com.homepro.megahome.realm.ProjectShare
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CatalogViewModel(
    private val service: FitTileApiService,
    private val tilesDao: TileDatabaseDao,
    repository: TileRepository,
    application: Application
) : AndroidViewModel(application) {
    private val tilesFilterLiveData = MutableLiveData<TilesFilter>()
    private val tilesResult: LiveData<TilesResult> = Transformations.map(tilesFilterLiveData) {
        repository.getTiles(it.tileType, it.size)
    }

    val tiles: LiveData<PagedList<Tile>> = Transformations.switchMap(tilesResult) { it.data }
    val networksError: LiveData<String> = Transformations.switchMap(tilesResult) { it.networksError }
    val tileSizes = ArrayList<TileSize>()

    var sizeChipId: Int? = null

    private val _navigateToScanner = MutableLiveData<Boolean>()
    val navigateToScanner: LiveData<Boolean>
        get() = _navigateToScanner

    private val _navigateToProjectDetailFromScanner = MutableLiveData<ProjectShare.Project>()
    val navigateToProjectDetailFromScanner: LiveData<ProjectShare.Project>
        get() = _navigateToProjectDetailFromScanner

    private val _navigateToProjectDetail = MutableLiveData<String>()
    val navigateToProjectDetail: LiveData<String>
        get() = _navigateToProjectDetail

    init {
        tilesFilterLiveData.value = TilesFilter(TileType.ALL)
    }

    fun onQRScanClicked() {
        _navigateToScanner.value = true
    }

    fun onScannerNavigate() {
        _navigateToScanner.value = false
    }

    fun onScanSuccess(tileId: String) {
        _navigateToProjectDetail.value = tileId
    }

    fun onProjectDetailFromScannerNavigate() {
        _navigateToProjectDetailFromScanner.value = null
    }

    fun onTileClicked(tileId: String) {
        _navigateToProjectDetail.value = tileId
    }

    fun onProjectDetailNavigate() {
        _navigateToProjectDetail.value = null
    }

    fun setTileSizes(sizes: List<TileSize>) {
        tileSizes.clear()
        tileSizes.addAll(sizes)
    }

    fun changeTilesByTypeAndSize(tileType: TileType, size: Size? = null) {
        tilesFilterLiveData.postValue(TilesFilter(tileType, size))
    }

    fun lastFilter(): TilesFilter? = tilesFilterLiveData.value

    suspend fun getTileById(id: String): Tile? {
        return withContext(Dispatchers.IO) {
            try {
                tilesDao.tileById(id) ?: service.tile(id)
            } catch(e: Exception) {
                null
            }
        }
    }
}