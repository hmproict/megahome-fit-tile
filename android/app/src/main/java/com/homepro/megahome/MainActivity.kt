package com.homepro.megahome

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.FragmentContainerView
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import com.homepro.megahome.components.CustomBottomNavigation
import com.homepro.megahome.components.NavBarListener
import com.homepro.megahome.flutter.DartMessenger
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.embedding.engine.FlutterEngineCache
import io.flutter.embedding.engine.dart.DartExecutor
import io.flutter.plugins.GeneratedPluginRegistrant

class MainActivity : AppCompatActivity(), NavBarListener {

    lateinit var flutterEngine: FlutterEngine

    private val navController by lazy {
        this.findNavController(R.id.navHostFragment)
    }

    private val bottomNavigation: CustomBottomNavigation by lazy {
        findViewById<CustomBottomNavigation>(R.id.bottomNavigation)
    }

    private val fragmentContainerView: FragmentContainerView by lazy {
        findViewById<FragmentContainerView>(R.id.navHostFragment)
    }

    private val density by lazy {
        applicationContext.resources.displayMetrics.density
    }

    private var isFirstTime = true

    private val onDestinationChangedListener = object : NavController.OnDestinationChangedListener {
        override fun onDestinationChanged(
            controller: NavController,
            destination: NavDestination,
            arguments: Bundle?
        ) {
            when (destination.id) {
                R.id.projectFragment -> {
                    addMarginBottom()
                    bottomNavigation.visibility = View.VISIBLE
                }
                R.id.catalogFragment -> {
                    bottomNavigation.visibility = View.VISIBLE
                    addMarginBottom()
                }
                R.id.calculateFragment -> {
                    bottomNavigation.visibility = View.VISIBLE
                    addMarginBottom()
                }
                R.id.roomfragment -> {
                    bottomNavigation.visibility = View.VISIBLE
                    addMarginBottom()
                }
                else -> {
                    bottomNavigation.visibility = View.GONE
                    removeMarginBottom()
                }
            }
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Instantiate a FlutterEngine.
        flutterEngine = FlutterEngine(this)

        // Start executing Dart code to pre-warm the FlutterEngine.
        flutterEngine.dartExecutor.executeDartEntrypoint(
            DartExecutor.DartEntrypoint.createDefault()
        )

        GeneratedPluginRegistrant.registerWith(flutterEngine);

        // Cache the FlutterEngine to be used by FlutterActivity.
        FlutterEngineCache
            .getInstance()
            .put("fittile_flutter_engine", flutterEngine)

        DartMessenger.instance.initialMessage(flutterEngine.dartExecutor.binaryMessenger)

        bottomNavigation.setNavBarListener(this)
        bottomNavigation.selectNav(1)
        onSelect(1)
    }

    override fun onPause() {
        super.onPause()
        navController.removeOnDestinationChangedListener(onDestinationChangedListener)
    }

    override fun onResume() {
        super.onResume()
        navController.addOnDestinationChangedListener(onDestinationChangedListener)
    }

    override fun onSelect(id: Int) {
        when(id) {
            0 -> navigateToCatalog()
            1 -> {
                if (isFirstTime) {
                    isFirstTime = false
                    return
                }
                navigateToMyProject()
            }
            2 -> navigateToRoom()
            3 -> navigateToCalculate()
        }
    }

    override fun onBackPressed() {
        Log.d("Flutter Add to app", "onBackPressed")
        if (navController.currentDestination?.id == R.id.roomfragment) {
            Log.d("Flutter Add to app", "in roomflutterfragment")
            val success = DartMessenger.instance.backPage()
            if (!success) {
                super.onBackPressed()
            }
        } else {
            super.onBackPressed()
        }
    }

    private fun navigateToCatalog() {
        navController.navigate(
            R.id.catalogFragment, null, NavOptions.Builder()
                .setPopUpTo(R.id.projectFragment, true).build()
        )
    }

    private fun navigateToMyProject() {
        navController.navigate(
            R.id.projectFragment, null, NavOptions.Builder()
                .setPopUpTo(R.id.projectFragment, true).build()
        )
    }

    private fun navigateToRoom() {
        if (navController.currentDestination?.id == R.id.roomfragment) {
            return
        }
        DartMessenger.instance.backToRoot()
        navController.navigate(
            R.id.roomfragment, null, NavOptions.Builder()
                .setPopUpTo(R.id.projectFragment, true).build()
        )
    }

    private fun navigateToCalculate() {
        navController.navigate(
            R.id.calculateFragment, null, NavOptions.Builder()
                .setPopUpTo(R.id.projectFragment, true).build()
        )
    }

    private fun addMarginBottom() {
        val params = fragmentContainerView.layoutParams as ConstraintLayout.LayoutParams
        params.bottomMargin = (63 * density).toInt()
        fragmentContainerView.requestLayout()
    }

    private fun removeMarginBottom() {
        val params = fragmentContainerView.layoutParams as ConstraintLayout.LayoutParams
        params.bottomMargin = 0
        fragmentContainerView.requestLayout()
    }
}
