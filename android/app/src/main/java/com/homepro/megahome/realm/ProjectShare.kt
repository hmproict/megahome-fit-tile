package com.homepro.megahome.realm

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class ProjectShare : Default() {
    @SerializedName("project")
    var project: Project? = null

    class Project() : Parcelable {

        @SerializedName("stage")
        var stage: String? = null

        @SerializedName("title")
        var title: String? = null

        @SerializedName("Id")
        var id: String? = null

        @SerializedName("date")
        var date: String? = null

        @SerializedName("tile_id")
        var tile_id: String? = null

        @SerializedName("raw_image")
        var raw_image: String? = null

        @SerializedName("fittile_image")
        var fittile_image: String? = null

        constructor(parcel: Parcel) : this() {
            stage = parcel.readString()
            title = parcel.readString()
            id = parcel.readString()
            date = parcel.readString()
            tile_id = parcel.readString()
            raw_image = parcel.readString()
            fittile_image = parcel.readString()
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeString(stage)
            parcel.writeString(title)
            parcel.writeString(id)
            parcel.writeString(date)
            parcel.writeString(tile_id)
            parcel.writeString(raw_image)
            parcel.writeString(fittile_image)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<Project> {
            override fun createFromParcel(parcel: Parcel): Project {
                return Project(parcel)
            }

            override fun newArray(size: Int): Array<Project?> {
                return arrayOfNulls(size)
            }
        }
    }
}