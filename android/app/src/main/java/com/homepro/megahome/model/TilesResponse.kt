package com.homepro.megahome.model

import com.google.gson.annotations.SerializedName

data class TilesResponse(
    @SerializedName("total") val total: Int,
    @SerializedName("list") val tiles: List<Tile>
)
