package com.homepro.megahome.flutter

import android.util.Log
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.EventChannel.EventSink

internal class DartMessenger {
    private var event: EventSink? = null

    private object HOLDER {
        val INSTANCE = DartMessenger()
    }

    companion object {
        val instance: DartMessenger by lazy { HOLDER.INSTANCE }
    }

    fun backPage(): Boolean  {
        if (event != null) {
            event?.success("back")
            return true
        }
        return false
    }

    fun backToRoot()  {
        event?.success("backToRoot")
    }

    fun initialMessage(messenger: BinaryMessenger?) {
        EventChannel(messenger, "com.homepro.megahome.flutter.event.default")
            .setStreamHandler(
                object : EventChannel.StreamHandler {
                    override fun onListen(
                        arguments: Any,
                        sink: EventSink
                    ) {
                        Log.d("Flutter Add to app", "Add event sink")
                        event = sink
                    }

                    override fun onCancel(arguments: Any) {
                        Log.d("Flutter Add to app", "Cancel event sink")
                        event = null
                    }
                })
    }
}
