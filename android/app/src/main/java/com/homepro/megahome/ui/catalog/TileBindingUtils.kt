package com.homepro.megahome.ui.catalog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.homepro.megahome.R
import com.homepro.megahome.glide.GlideApp
import com.homepro.megahome.model.Tile

@BindingAdapter("tileImage")
fun AppCompatImageView.setTileImage(tile: Tile?) {
    tile?.let {
        GlideApp.with(this.context)
            .load(it.imageURL)
            .centerCrop()
            .placeholder(ColorDrawable(Color.LTGRAY))
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(this)
    }
}

@BindingAdapter("tileTag")
fun AppCompatImageView.setShowOrHideTag(tile: Tile?) {
    tile?.let {
        when (tile.type) {
            "Wall" -> {
                this.visibility = View.VISIBLE
            }
            else -> {
                this.visibility = View.GONE
            }
        }
    }
}

@BindingAdapter("tileTag")
fun AppCompatTextView.setTileTag(tile: Tile?) {
    tile?.let {
        when (tile.type) {
            "Wall" -> {
                this.visibility = View.VISIBLE
                this.text = context.resources.getString(R.string.wall_uppercase)
            }
            else -> {
                this.visibility = View.GONE
            }
        }
    }
}