package com.homepro.megahome.manager

import android.app.Activity
import android.net.Uri
import android.os.AsyncTask
import android.util.Log
import com.homepro.megahome.constant.AppConfig
import io.tus.android.client.TusAndroidUpload
import io.tus.android.client.TusPreferencesURLStore
import io.tus.java.client.TusClient
import io.tus.java.client.TusUpload
import java.net.URL

class UploadImageManager {
    private var fileUri: Uri? = null
    private var client: TusClient? = null
    private val activity: Activity
    private var transactionId: String? = null
    private var stage: String? = null
    private var tileId: String? = null
    private var uploadTask: UploadTask? = null
    private var status: UploadListener? = null

    constructor(status: UploadListener?, context: Activity) {
        this.activity = context
        this.status = status!!
        try {
            val pref = context.getSharedPreferences("tus", 0)
            client = TusClient()
            client!!.uploadCreationURL = URL(AppConfig.FIT_TILE_SERVICE_URL + "/uploads")
            client!!.enableResuming(TusPreferencesURLStore(pref))
        } catch (e: Exception) {
            Log.e("showError", "client " + e.message)
        }
    }

    fun resumeUpload() {
        val upload = TusAndroidUpload(fileUri, activity)
        val headMap = upload.metadata
        headMap["transaction_id"] = transactionId
        headMap["stage"] = stage
        headMap["tile_id"] = tileId
        upload.metadata = headMap
        uploadTask = UploadTask(status!!, client!!, upload)

        uploadTask!!.execute()
        try {


        } catch (e: Exception) {
            Log.e("showError", "resumeUpload  ${e.localizedMessage}")
            status!!.showError(e)
        }

    }

    fun beginUpload(uri: Uri, transactionId: String, stage: String, tileId: String?) {
        fileUri = uri
        this.transactionId = transactionId
        this.stage = stage
        this.tileId = tileId
        resumeUpload()
    }

    class UploadTask(
        private val activity: UploadListener,
        val client: TusClient,
        private val upload: TusUpload
    ) : AsyncTask<Void, Long, URL>() {
        private var exception: Exception? = null

        private val transactionId: String?
        private var stage: String? = null
        private var tileId: String? = null
        private var obj: UploadImageManager.Status? = null

        init {
            val map = upload.metadata
            transactionId = map["transaction_id"]
            stage = map["stage"]
            tileId = map["tile_id"]
        }

        override fun onPreExecute() {
        }

        override fun onPostExecute(uploadURL: URL) {
            obj!!.status = "Finished"
        }

        override fun onCancelled() {

        }

        protected fun onProgressUpdate(vararg updates: Long) {
            val uploadedBytes = updates[0]
            val totalBytes = updates[1]
            obj!!.status = "Uploaded"
            obj!!.uploadSize = uploadedBytes.toInt()
            obj!!.totalSize = totalBytes.toInt()
        }

        override fun doInBackground(vararg params: Void): URL? {
            try {
                val uploader = client.resumeOrCreateUpload(upload)
                val totalBytes = upload.size

                var uploadedBytes: Long
                uploader.chunkSize = 512

                while (!isCancelled && uploader.uploadChunk() > 0) {
                    uploadedBytes = uploader.offset
                    publishProgress(uploadedBytes, totalBytes)
                }

                uploader.finish()
                return uploader.uploadURL

            } catch (e: Exception) {
                exception = e
                cancel(true)
            }

            return null
        }
    }

    interface UploadListener {

        fun setUploadProgress(progress: Int)

        fun setStatus(obj: Status)

        fun showError(ex: Exception)

    }

    class Status {
        var status: String? = null
        var uploadSize: Int = 0
        var totalSize: Int = 0
        var tileId: String? = null

        var transactionId: String? = null
        var name: String? = null
        var path: String? = null
        var qrUrl: String? = null
        var stage: String? = null
    }
}