package com.homepro.megahome.ui.scanner

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.zxing.Result
import com.homepro.megahome.ui.fittile.FitTileFragmentArgs
import me.dm7.barcodescanner.zxing.ZXingScannerView

private const val REQUEST_CODE_PERMISSIONS = 10
private val REQUIRED_PERMISSIONS = arrayOf(
    Manifest.permission.CAMERA,
    Manifest.permission.WRITE_EXTERNAL_STORAGE,
    Manifest.permission.READ_EXTERNAL_STORAGE
)

class ScannerFragment : Fragment(), ZXingScannerView.ResultHandler {

    companion object {
        const val SCANNER_RESULT_KEY = "scanResult"
    }

    private val args: ScannerFragmentArgs by navArgs()


    private val scannerView: ZXingScannerView by lazy {
        ZXingScannerView(requireActivity())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return scannerView
    }

    override fun onResume() {
        super.onResume()
        if (allPermissionGranted()) {
            scannerView.setResultHandler(this)
            scannerView.startCamera()
        } else {
            requestPermissions(
                REQUIRED_PERMISSIONS,
                REQUEST_CODE_PERMISSIONS
            )
        }
    }

    override fun onPause() {
        super.onPause()
        scannerView.stopCamera()
    }

    override fun handleResult(rawResult: Result?) {
        scannerView.stopCamera()
        rawResult?.let {
            setFragmentResult(args.requestKey, bundleOf(SCANNER_RESULT_KEY to it.text))
            findNavController().popBackStack()
        }
    }

    private fun allPermissionGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(requireContext(), it) == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionGranted()) {
                scannerView.setResultHandler(this)
                scannerView.startCamera()
            } else {
                Toast.makeText(requireContext(), "ไม่สามารถใช้ฟีเจอร์นี้ได้", Toast.LENGTH_LONG)
                    .show()
                findNavController().navigateUp()
            }
        }
    }
}