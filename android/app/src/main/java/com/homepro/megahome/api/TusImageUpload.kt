package com.homepro.megahome.api

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import android.os.AsyncTask
import com.homepro.megahome.manager.UploadImageManager
import com.homepro.megahome.manager.UploadImageManager.UploadListener
import io.tus.android.client.TusAndroidUpload
import io.tus.android.client.TusPreferencesURLStore
import io.tus.java.client.TusClient
import io.tus.java.client.TusExecutor
import io.tus.java.client.TusUpload
import java.io.File
import java.io.IOException
import java.net.ProtocolException
import java.net.URL


class TusImgUpload {
    var listener: UploadListener? = null

    @Throws(IOException::class, ProtocolException::class)
    fun test(
        context: Context,
        url: String,
        transactionId: String,
        stage: String,
        tileId: String
    ) {
        val client = TusClient()
        client.uploadCreationURL = URL("https://fittile.digitopolisstudio.com/uploads")
        val pref: SharedPreferences = context.getSharedPreferences("tus", 0)
        client.enableResuming(TusPreferencesURLStore(pref))
        val file = File(url)
        val upload = TusUpload(file)
        val headMap =
            upload.metadata
        headMap["transaction_id"] = transactionId
        headMap["stage"] = stage
        headMap["tile_id"] = tileId
        upload.metadata = headMap
        val executor: TusExecutor = object : TusExecutor() {
            @Throws(ProtocolException::class, IOException::class)
            override fun makeAttempt() {
                val uploader = client.resumeOrCreateUpload(upload)
                uploader.chunkSize = 512
                do {

                } while (uploader.uploadChunk() > -1)
                uploader.finish()
            }
        }
        executor.makeAttempts()
    }

    @Throws(IOException::class, ProtocolException::class)
    fun test(
        context: Context,
        file: File?,
        transactionId: String,
        stage: String,
        tileId: String
    ) {
        val client = TusClient()
        client.uploadCreationURL = URL("https://fittile.digitopolisstudio.com/uploads")
        val pref: SharedPreferences = context.getSharedPreferences("tus", 0)
        client.enableResuming(TusPreferencesURLStore(pref))
        val upload = TusUpload(file!!)
        val headMap =
            upload.metadata
        headMap["transaction_id"] = transactionId
        headMap["stage"] = stage
        headMap["tile_id"] = tileId
        upload.metadata = headMap
        val executor: TusExecutor = object : TusExecutor() {
            @Throws(ProtocolException::class, IOException::class)
            override fun makeAttempt() {
                val uploader = client.resumeOrCreateUpload(upload)
                uploader.chunkSize = 512
                do {

                } while (uploader.uploadChunk() > -1)
                uploader.finish()
            }
        }
        executor.makeAttempts()
    }

    @Throws(IOException::class, ProtocolException::class)
    fun test2(
        activity: Activity,
        fileUri: Uri?,
        transactionId: String?,
        stage: String?,
        tileId: String?,
        listener: UploadListener?
    ) {
        var client: TusClient? = null
        this.listener = listener
        val status =
            UploadImageManager.Status()
        status.transactionId = transactionId
        status.stage = stage
        status.tileId = tileId
        try {
            val pref = activity.getSharedPreferences("tus", 0)
            client = CustomSSLTusClient()
            client.uploadCreationURL = URL("https://fittile.digitopolisstudio.com/uploads")
            client.enableResuming(TusPreferencesURLStore(pref))
        } catch (e: Exception) {
        }
        val upload: TusUpload = TusAndroidUpload(fileUri, activity)
        val headMap =
            upload.metadata
        headMap["transaction_id"] = transactionId
        headMap["stage"] = stage
        if (tileId != null) headMap["tile_id"] = tileId
        upload.metadata = headMap
        val uploadTask = UploadTask(client!!, upload)
        uploadTask.execute()
    }

    private inner class UploadTask(
        private val client: TusClient,
        private val upload: TusUpload
    ) :
        AsyncTask<Void?, Long?, URL?>() {
        private var exception: Exception? = null
        override fun onPreExecute() {
        }

        override fun onPostExecute(uploadURL: URL?) {
            val status =
                UploadImageManager.Status()
            status.status = "finish"
            listener!!.setStatus(status)
        }

        override fun onCancelled() {
            if (exception != null) {
            }
        }

        override fun onProgressUpdate(vararg updates: Long?) {
            
        }


        override fun doInBackground(vararg p0: Void?): URL? {
            try {
                val uploader = client.resumeOrCreateUpload(upload)
                //                TusAndroidUpload uploader = client.resumeOrCreateUpload(upload);
                val totalBytes = upload.size
                var uploadedBytes: Long
                uploader.chunkSize = 1000000
                uploader.requestPayloadSize = 1000000
                while (!isCancelled && uploader.uploadChunk() > 0) {
                    uploadedBytes = uploader.offset
                    publishProgress(uploadedBytes, totalBytes)

//                    uploader.finish();
                }
                uploader.finish()
                return uploader.uploadURL
            } catch (e: Exception) {
                exception = e
                cancel(true)
            }
            return null
        }

    }
}