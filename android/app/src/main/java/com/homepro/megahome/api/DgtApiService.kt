package com.homepro.megahome.api

import com.homepro.megahome.realm.Default
import com.homepro.megahome.realm.ProjectShare
import com.homepro.megahome.realm.ProjectUrl
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*


interface DgtApiService {
    @POST("api/project")
    fun createProject(@Body body: ProjectBody): Call<ProjectUrl>

    @GET("api/project")
    fun getProject(@Query("transaction_id") transactionId: String): Call<ProjectShare>

    @POST("api/project/{transaction_id}")
    fun editProject(
        @Path("transaction_id") transactionId: String,
        @Body editProjectBody: EditProjectBody
    ): Call<Default>

    @Multipart
    @POST("api/upload")
    fun upload(@Part image: MultipartBody.Part): Call<Default>
    class ProjectBody : Default() {
        var transaction_id: String? = null
        val app: String = "megahome"
    }

    class EditProjectBody : Default() {
        var title: String? = null
        var date: String? = null
    }
}