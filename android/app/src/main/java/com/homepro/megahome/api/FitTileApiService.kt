package com.homepro.megahome.api

import android.content.Context
import android.util.Log
import com.google.gson.*
import com.homepro.megahome.model.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*
import java.lang.reflect.Type

// Development
//private const val BASE_URL = "https://qas-api.homepro.co.th/hpg/qas/"
//private const val CLIENT_ID = "c0a5635f2c3903ed5e89988ce441b164"
//private const val CLIENT_SECRET = "49acc8c80f6aeef5dea320a0d5c6e133"

// Production
private const val BASE_URL = "https://api.homepro.co.th/hpg/prd/"
private const val CLIENT_ID = "40bbb073d96f196d3cfbe7834ad2f3cf"
private const val CLIENT_SECRET = "37e041077817e509d28375d23ed6a123"

private const val AUTH_PATH = "homeprooaprovider/oauth2/token"
private const val MASTER_PATH = "Master/"
private const val APP_COMPANY = "mh"

interface FitTileApiService {
    @POST(AUTH_PATH)
    @FormUrlEncoded
    suspend fun auth(
        @Field("client_id") clientID: String,
        @Field("client_secret") clientSecret: String,
        @Field("scope") scope: String,
        @Field("grant_type") grantType: String
    ): Token

    @GET("$MASTER_PATH/GET_FITTILE_INFO")
    suspend fun tile(
        @Query("id") tileId: String,
        @Query("company") company: String = APP_COMPANY
    ): Tile?

    @GET("$MASTER_PATH/GET_FITTILE_DEFAULT2")
    suspend fun tiles(
        @Query("from") from: Int = 0,
        @Query("company") company: String = APP_COMPANY,
        @Query("type") type: String? = null // floor | wall
    ): TilesResponse

    @GET("$MASTER_PATH/GET_FITTILE_DEFAULT2")
    suspend fun tilesWithSize(
        @Query("size") size: Int,
        @Query("from") from: Int = 0,
        @Query("company") company: String = APP_COMPANY,
        @Query("type") type: String? = null // floor | wall
    ): TilesResponse

    @GET("$MASTER_PATH/GET_FITTILE_CALCULATE")
    suspend fun calculateArea(
        @Query("id") tileId: String,
        @Query("area") area: Int?,
        @Query("extra") extra: String?,
        @Query("company") company: String = APP_COMPANY
    ): CalculateResponse

    @GET("$MASTER_PATH/GET_FITTILE_CALCULATE")
    suspend fun calculate(
        @Query("id") tileId: String,
        @Query("width") width: Int?,
        @Query("length") length: Int?,
        @Query("extra") extra: String?,
        @Query("company") company: String = APP_COMPANY
    ): CalculateResponse

    @GET("$MASTER_PATH/GET_FITTILE_SIZE")
    suspend fun tileSize(
        @Query("company") company: String = APP_COMPANY
    ): TileSizeResponse
}

class FitTileApi {
    companion object {
        private lateinit var retrofitService: FitTileApiService
        private var isInitialized = false
        private var isNeedRefreshToken = false

        fun service(context: Context): FitTileApiService = runBlocking {
            val sessionManager = SessionManager(context)

            if (sessionManager.fetchClientId() == null) {
                sessionManager.setClientId(CLIENT_ID)
            }

            val expiredAt = sessionManager.fetchAuthExpiredAt()

            if (expiredAt == 0L || expiredAt <= System.currentTimeMillis() / 1000) {
                val token = withContext(Dispatchers.IO) {
                    FitTileAuthApi.retrofitAuthService.auth(
                        CLIENT_ID,
                        CLIENT_SECRET,
                        "Master",
                        "client_credentials"
                    )
                }
                sessionManager.setClientId(CLIENT_ID)
                sessionManager.saveAuthToken(token)
                isNeedRefreshToken = true
            }

            if (!isInitialized || isNeedRefreshToken) {
                retrofitService = createRetrofit(
                    context
                ).create(FitTileApiService::class.java)

                isInitialized = true
                isNeedRefreshToken = false
            }

            retrofitService
        }

        private fun createRetrofit(context: Context): Retrofit {

            val gson = GsonBuilder()
                .registerTypeAdapter(Double::class.java, DoubleDeserializerTypeAdapter())
                .registerTypeAdapter(Int::class.java, IntDeserializerTypeAdapter())
                .registerTypeAdapter(TileSize::class.java, TileSizeDeserializerTypeAdapter())
                .create()

            return Retrofit.Builder()
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(
                    okHttpClient(
                        context
                    )
                )
                .baseUrl(BASE_URL)
                .build()
        }

        private fun okHttpClient(context: Context): OkHttpClient {
            return OkHttpClient.Builder()
                .addInterceptor(AuthInterceptor(context))
                .build()
        }

    }
}

private val retrofit = Retrofit.Builder()
    .addConverterFactory(ScalarsConverterFactory.create())
    .addConverterFactory(GsonConverterFactory.create())
    .baseUrl(BASE_URL)
    .build()

private object FitTileAuthApi {
    val retrofitAuthService: FitTileApiService by lazy {
        retrofit.create(FitTileApiService::class.java)
    }
}

class DoubleDeserializerTypeAdapter : JsonDeserializer<Double?> {
    @Throws(JsonParseException::class)
    override fun deserialize(
        jsonElement: JsonElement,
        type: Type?,
        jsonDeserializationContext: JsonDeserializationContext?
    ): Double? {
        var result: Double? = null
        result = try {
            if (type == String::class.java) {
                if (jsonElement.asString.isNotEmpty()) {
                    jsonElement.asString.toDouble()
                } else {
                    0.0
                }
            } else {
                jsonElement.asDouble
            }
        } catch (e: NumberFormatException) {
            return result
        }
        return result
    }
}

class IntDeserializerTypeAdapter : JsonDeserializer<Int?> {
    @Throws(JsonParseException::class)
    override fun deserialize(
        jsonElement: JsonElement,
        type: Type?,
        jsonDeserializationContext: JsonDeserializationContext?
    ): Int? {
        var result: Int? = null
        result = try {

            if (type == String::class.java) {
                if (jsonElement.asString.isNotEmpty()) {
                    jsonElement.asString.toInt()
                } else {
                    0
                }
            } else {
                jsonElement.asInt
            }
        } catch (e: NumberFormatException) {
            return result
        }
        return result
    }
}