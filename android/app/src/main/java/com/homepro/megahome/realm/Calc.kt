package com.homepro.megahome.realm

import com.google.gson.annotations.SerializedName

class Calc {
    @SerializedName("id")
    var id: String? = null

    @SerializedName("box")
    var box: Int = 0
}
