package com.homepro.megahome.realm

import com.google.gson.annotations.SerializedName

class TileAll {
    @SerializedName("total")
    var total: Int = 0

    @SerializedName("list")
    var list: List<Tile>? = null
}