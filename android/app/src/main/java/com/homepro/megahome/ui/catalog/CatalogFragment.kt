package com.homepro.megahome.ui.catalog

import android.graphics.drawable.BitmapDrawable
import android.net.UrlQuerySanitizer
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowInsets
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.paging.PagedList
import androidx.recyclerview.widget.GridLayoutManager
import coil.Coil
import coil.request.LoadRequest
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.homepro.megahome.R
import com.homepro.megahome.api.AppService
import com.homepro.megahome.api.FitTileApi
import com.homepro.megahome.api.ServiceListener
import com.homepro.megahome.data.TileRepository
import com.homepro.megahome.database.FitTileDatabase
import com.homepro.megahome.database.TileLocalCache
import com.homepro.megahome.database.TileSizeLocalCache
import com.homepro.megahome.databinding.CatalogFragmentBinding
import com.homepro.megahome.manager.DateManager
import com.homepro.megahome.manager.ExternalFileManager
import com.homepro.megahome.model.Size
import com.homepro.megahome.model.Tile
import com.homepro.megahome.model.TileSize
import com.homepro.megahome.model.TileType
import com.homepro.megahome.realm.ProjectShare
import com.homepro.megahome.singleton.AppData
import com.homepro.megahome.ui.fittile.FitTileFragment
import com.homepro.megahome.ui.project.ProjectFragment
import com.homepro.megahome.ui.scanner.ScannerFragment
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Response
import java.text.NumberFormat


private const val TAG = "CatalogFragment"

class CatalogFragment : Fragment() {
    private lateinit var binding: CatalogFragmentBinding

    private val args by navArgs<CatalogFragmentArgs>()

    private var tileSizes = ArrayList<TileSize>()
    private var topInset: Double = 0.0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.catalog_fragment, container, false)

        val application = requireActivity().application
        val service = FitTileApi.service(requireContext())
        val dataSource = FitTileDatabase.getInstance(application).tileDatabaseDao
        val repository = TileRepository(FitTileApi.service(requireContext()), TileLocalCache(dataSource))
        val viewModelFactory = CatalogViewModelFactory(service, dataSource, repository, application)
        val viewModel = ViewModelProvider(this, viewModelFactory).get(CatalogViewModel::class.java)

        (requireActivity() as AppCompatActivity).setSupportActionBar(binding.appBar)
        binding.filterListMenu.setOnClickListener(
            FilterIconClickListener(
                requireActivity(),
                binding.tileList,
                AccelerateDecelerateInterpolator(),
                ContextCompat.getDrawable(requireContext(), R.drawable.ic_filter_list),
                ContextCompat.getDrawable(requireContext(), R.drawable.ic_close)
            )
        )

        viewModel.navigateToProjectDetail.observe(viewLifecycleOwner, Observer { tileId ->
            tileId?.let {
                if (args.isFromCalculate) {
                    findNavController().navigate(
                        CatalogFragmentDirections.actionCatalogFragmentToCalculateFragment(tileId = it)
                    )
                } else {
                    findNavController().navigate(
                        CatalogFragmentDirections.actionCatalogFragmentToProjectDetailFragment(
                            type = "tile",
                            tileId = it
                        )
                    )
                }

                viewModel.onProjectDetailNavigate()
            }
        })

        initQRScanner(viewModel)

        initAdapter(viewModel)
        return binding.root
    }

    private fun initQRScanner(viewModel: CatalogViewModel) {
        binding.qrScanImageView.setOnClickListener {
            viewModel.onQRScanClicked()
        }

        viewModel.navigateToScanner.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(CatalogFragmentDirections.actionCatalogFragmentToScannerFragment(requestKey = SCAN_GET_TILE_REQUEST_KEY))
                viewModel.onScannerNavigate()
            }
        })

        setFragmentResultListener(SCAN_GET_TILE_REQUEST_KEY) { _, bundle ->
            val sanitizer = UrlQuerySanitizer(bundle.getString(ScannerFragment.SCANNER_RESULT_KEY))
            val tileId = sanitizer.getValue("id_scan")

            if (tileId == null) {
                MaterialAlertDialogBuilder(requireContext())
                    .setTitle(getString(R.string.error))
                    .setMessage(getString(R.string.not_valid_qrcode))
                    .setPositiveButton(getString(R.string.ok), null)
                    .show()

                return@setFragmentResultListener
            }

            if (tileId.isNotBlank() || tileId.isNotEmpty()) {
                lifecycleScope.launch {
                    viewModel.getTileById(tileId)?.let {
                        viewModel.onScanSuccess(it.id)
                        return@launch
                    }

                    Toast.makeText(
                        requireContext(),
                        getString(R.string.cannot_find_tile_from_scan),
                        Toast.LENGTH_SHORT).show()
                }
            }
        }

        viewModel.navigateToProjectDetailFromScanner.observe(viewLifecycleOwner, Observer { shareProject ->
            shareProject?.id?.let { shareProjectId ->
                val myProject = AppService().retriveProject(shareProjectId) ?: AppService().saveProjectTypeShare(
                    id = shareProjectId,
                    name = shareProject.title.takeIf { it != null } ?: "",
                    tileId = "",
                    fitTileImage = shareProject.fittile_image ?: "",
                    rawImage = shareProject.raw_image ?: "",
                    date = DateManager.getInstance().currentDate(),
                    transactionId = shareProjectId,
                    qrUrl = "",
                    stage = "fittile"
                )
                findNavController().navigate(
                    CatalogFragmentDirections.actionCatalogFragmentToProjectDetailFragment(
                        type = "share",
                        project = myProject
                    )
                )

                viewModel.onProjectDetailFromScannerNavigate()
            }
        })
    }

    private fun initAdapter(viewModel: CatalogViewModel) {
        val manager = GridLayoutManager(requireActivity(), 3)
        binding.tileList.layoutManager = manager

        val adapter = TileAdapter(TileListener { tileId ->
            viewModel.onTileClicked(tileId)
        })
        binding.tileList.adapter = adapter

        viewModel.networksError.observe(viewLifecycleOwner, Observer {
            Log.d("Flutter to App", "service error : $it")
            Toast.makeText(requireContext(), "\uD83D\uDE28 Wooops $it", Toast.LENGTH_LONG).show()
        })

        viewModel.tiles.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })

        requestTileSize {  list ->
            initFilter(viewModel, list)
        }
    }

    private fun initFilter(viewModel: CatalogViewModel, tileSizes: List<TileSize>) {

        viewModel.setTileSizes(tileSizes)

        val format = NumberFormat.getIntegerInstance()
        val sizes = tileSizes.associateBy(
            { tile -> getString(R.string.tile_size_cm, format.format(tile.cmSize.width), format.format(tile.cmSize.length)) },
            { tile -> tile.cmSize }
        )
        createChipTileSizes(tileSizes, binding.tileSizeChipGroup)

        binding.floorChip.setOnCheckedChangeListener { _, isChecked ->
            val sizeChipId = viewModel.sizeChipId ?: R.id.allSizeChip
            val selectedSize: Size? = if (sizeChipId == R.id.allSizeChip) null else sizes.values.toList()[sizeChipId]

            val checkedChipIds = binding.tileTypeChipGroup.checkedChipIds
            when (checkedChipIds.count()) {
                2 -> viewModel.changeTilesByTypeAndSize(tileType = TileType.ALL, size = selectedSize)
                1 -> {
                    if (isChecked) {
                        viewModel.changeTilesByTypeAndSize(tileType = TileType.FLOOR, size = selectedSize)
                    } else {
                        viewModel.changeTilesByTypeAndSize(tileType = TileType.WALL, size = selectedSize)
                    }
                }
            }
        }

        binding.wallChip.setOnCheckedChangeListener { _, isChecked ->
            val sizeChipId = viewModel.sizeChipId ?: R.id.allSizeChip
            val selectedSize: Size? = if (sizeChipId == R.id.allSizeChip) null else sizes.values.toList()[sizeChipId]

            val checkedChipIds = binding.tileTypeChipGroup.checkedChipIds
            when (checkedChipIds.count()) {
                2 -> viewModel.changeTilesByTypeAndSize(tileType = TileType.ALL, size = selectedSize)
                1 -> {
                    if (isChecked) {
                        viewModel.changeTilesByTypeAndSize(tileType = TileType.WALL, size = selectedSize)
                    } else {
                        viewModel.changeTilesByTypeAndSize(tileType = TileType.FLOOR, size = selectedSize)
                    }
                }
            }
        }

        binding.tileSizeChipGroup.setOnCheckedChangeListener { _, checkedId ->
            viewModel.sizeChipId = checkedId
            val sizeChipId = viewModel.sizeChipId
            sizeChipId?.let { chipId ->
                val selectedSize: Size? = if (chipId == R.id.allSizeChip) null else sizes.values.toList()[chipId]
                viewModel.changeTilesByTypeAndSize(tileType = viewModel.lastFilter()?.tileType ?: TileType.ALL, size = selectedSize)
            }
        }
    }

    private fun createChipTileSizes(tileSizes: List<TileSize>, parent: ChipGroup) {
        parent.removeAllViews()
        val format = NumberFormat.getIntegerInstance()
        val texts = tileSizes.map { getString(R.string.tile_size_cm, format.format(it.cmSize.width), format.format(it.cmSize.length)) }.distinct()

        val chipAll = layoutInflater.inflate(R.layout.chip_choice, parent, false) as Chip
        chipAll.text = getString(R.string.all)
        chipAll.id = R.id.allSizeChip
        parent.addView(chipAll)

        for ((index, text) in texts.withIndex()) {
            val chip = layoutInflater.inflate(R.layout.chip_choice, parent, false) as Chip
            chip.text = text
            chip.id = index
            parent.addView(chip)
        }
    }

    companion object {
        const val SCAN_GET_TILE_REQUEST_KEY = "requestTile"
    }

    private fun requestTileSize(complete: (ArrayList<TileSize>) -> Unit) = runBlocking {

        val service = FitTileApi.service(requireContext())

        val application = requireActivity().application
        val sizeDataSource = FitTileDatabase.getInstance(application).sizeDatabaseDao
        val sizeCache =  TileSizeLocalCache(sizeDataSource)

        try {
            val tileSizeResponse = withContext(Dispatchers.IO) {
                service.tileSize()
            }
            tileSizes.addAll(tileSizeResponse.sizes)
            sizeCache.insert(tileSizeResponse.sizes) {
                activity?.runOnUiThread {
                    complete(tileSizes)
                }
            }

        } catch (e: Exception) {

            sizeCache.sizes(TileType.ALL).observe(viewLifecycleOwner, Observer {
                tileSizes.addAll(it)
                activity?.runOnUiThread {
                    if (tileSizes.isEmpty()) {
                        MaterialAlertDialogBuilder(requireContext())
                            .setTitle(getString(R.string.error))
                            .setMessage(getString(R.string.error_cannot_call_api))
                            .setPositiveButton(getString(R.string.ok), null)
                            .show()
                    } else {
                        complete(tileSizes)
                    }
                }
            })
        }
    }
}