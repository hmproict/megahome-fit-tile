package com.homepro.megahome.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.paging.LivePagedListBuilder
import com.homepro.megahome.api.FitTileApiService
import com.homepro.megahome.database.TileLocalCache
import com.homepro.megahome.model.Size
import com.homepro.megahome.model.Tile
import com.homepro.megahome.model.TileType
import com.homepro.megahome.model.TilesResult
import kotlinx.coroutines.Dispatchers

class TileRepository(
    private val service: FitTileApiService,
    private val cache: TileLocalCache
) {
    fun getTiles(tileType: TileType, size: Size? = null): TilesResult {
        val dataSourceFactory = cache.tiles(tileType, size)
        val boundaryCallback = TileBoundaryCallback(service, cache)
        val networksError = boundaryCallback.networkErrors

        val data = LivePagedListBuilder(dataSourceFactory, DATABASE_PAGE_SIZE)
            .setBoundaryCallback(boundaryCallback)
            .build()

        return TilesResult(data, networksError)
    }

    fun getTile(id: String): LiveData<Tile?> = liveData(Dispatchers.IO) {
        val tileCache = cache.tile(id)
        tileCache?.let {
            emit(it)
            return@liveData
        }

        val tileFromNetwork = service.tile(id)
        tileFromNetwork?.let {
            cache.insert(listOf(it)) {}
            emit(it)
            return@liveData
        }

        emit(null)
    }

    companion object {
        private const val DATABASE_PAGE_SIZE = 21
    }
}