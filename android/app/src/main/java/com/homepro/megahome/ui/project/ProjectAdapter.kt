package com.homepro.megahome.ui.project

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.homepro.megahome.databinding.ProjectCellBinding
import com.homepro.megahome.realm.MyProject

class ProjectAdapter(private val clickListener: ProjectListener) :
    ListAdapter<MyProject, ProjectAdapter.ViewHolder>(ProjectDiffCallback()){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, clickListener)
    }

    class ViewHolder private constructor(private val binding: ProjectCellBinding) :
        RecyclerView.ViewHolder(binding.root) {
        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ProjectCellBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }

        fun bind(item: MyProject, clickListener: ProjectListener) {
            binding.project = item
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }
    }
}

class ProjectDiffCallback : DiffUtil.ItemCallback<MyProject>() {
    override fun areItemsTheSame(oldItem: MyProject, newItem: MyProject): Boolean = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: MyProject, newItem: MyProject): Boolean = oldItem.tileId == newItem.tileId
}


class ProjectListener(val clickListener: (project: MyProject) -> Unit) {
    fun onClick(project: MyProject) = clickListener(project)
}