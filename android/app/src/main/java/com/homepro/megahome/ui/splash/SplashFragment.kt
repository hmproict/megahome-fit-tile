package com.homepro.megahome.ui.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.homepro.megahome.BuildConfig
import com.homepro.megahome.R
import com.homepro.megahome.api.FitTileApi
import com.homepro.megahome.database.FitTileDatabase
import com.homepro.megahome.model.Tile
import com.homepro.megahome.model.TilesResponse
import kotlinx.android.synthetic.main.splash_fragment.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class SplashFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.splash_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.appVersionTextView.text = getString(
            R.string.app_version_format,
            BuildConfig.VERSION_NAME, BuildConfig.
            VERSION_CODE.toString()
        )
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        cacheTiles()
    }

    private fun cacheTiles() {
        lifecycleScope.launch {
            try {
                val tiles = getDefaultTiles(21).tiles

                insertAll(tiles)

                delay(1000)
                findNavController().navigate(SplashFragmentDirections.actionSplashFragmentToProjectFragment())
            } catch (httpException: HttpException) {
                delay(1000)
                findNavController().navigate(SplashFragmentDirections.actionSplashFragmentToProjectFragment())
            } catch (e: Exception) {
                delay(1000)
                findNavController().navigate(SplashFragmentDirections.actionSplashFragmentToProjectFragment())
            }
        }
    }

    private suspend fun insertAll(tiles: List<Tile>) {
        return withContext(Dispatchers.IO) {
            FitTileDatabase.getInstance(requireContext()).tileDatabaseDao.insertAll(tiles)
        }
    }

    private suspend fun getDefaultTiles(size: Int = 0): TilesResponse {
        return withContext(Dispatchers.IO) {
            FitTileApi.service(requireContext()).tilesWithSize(size)
        }
    }
}
