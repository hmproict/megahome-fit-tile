package com.homepro.megahome.model

import com.google.gson.annotations.SerializedName

data class Token(
    @SerializedName("token_type")
    val tokenType: String,
    @SerializedName("access_token")
    val accessToken: String,
    @SerializedName("scope")
    val scope: String,
    @SerializedName("expires_in")
    val expiresIn: Int,
    @SerializedName("consented_on")
    val consentedOn: Long,

    var expiredAt: Long
)