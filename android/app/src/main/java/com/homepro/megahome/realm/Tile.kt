package com.homepro.megahome.realm

import com.google.gson.annotations.SerializedName

class Tile {
    @SerializedName("id")
    var id: String? = null

    @SerializedName("short_desc")
    var shortDesc: String? = null

    @SerializedName("long_desc")
    var longDesc: String? = null

    @SerializedName("width")
    var width: String? = null

    @SerializedName("length")
    var length: String? = null

    @SerializedName("ramdom")
    var ramdom: String? = null

    @SerializedName("box")
    var box: String? = null

    @SerializedName("feature")
    var feature: String? = null

    @SerializedName("vendor")
    var vendor: String? = null

    @SerializedName("pattern")
    var pattern: String? = null

    @SerializedName("image")
    var image: String? = null

    @SerializedName("url")
    var url: String? = null

    @SerializedName("appflag")
    var appflag: String? = null

    @SerializedName("image_pattern")
    var image_pattern: String? = null

    @SerializedName("floor")
    val floor: String? = null

    @SerializedName("wall")
    val wall: String? = null

    @SerializedName("left_wall")
    val left_wall: String? = null

    @SerializedName("right_wall")
    val right_wall: String? = null

    @SerializedName("inch_width")
    val inch_width: Int? = null

    @SerializedName("inch_length")
    val inch_length: String? = null

    @SerializedName("category")
    val category: String? = null

    @SerializedName("area")
    val area: String? = null

    data class Result(val total_count: Int, val incomplete_results: Boolean, val items: List<Tile>)
}