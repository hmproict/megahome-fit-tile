package com.homepro.megahome.components

import android.content.Context
import android.content.res.ColorStateList
import android.os.Build
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import com.homepro.megahome.R
import kotlinx.android.synthetic.main.bottom_navigation.view.*

interface NavBarListener {
    fun onSelect(id: Int)
}

class CustomBottomNavigation : FrameLayout {
    private lateinit var view: View
    private lateinit var navBarListener: NavBarListener

    private var camaraState = false
    private var isAct2 = false

    constructor(context: Context) : super(context) {
        initInflate(context)
        initInstance()
    }

    constructor(
        context: Context,
        attrs: AttributeSet?
    ) : super(context, attrs) {
        initInflate(context)
        initInstance()
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        initInflate(context)
        initInstance()
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        initInflate(context)
        initInstance()
    }

    private fun initInflate(context: Context) {
        view = View.inflate(context, R.layout.bottom_navigation, this)
    }

    fun selectNav(tab: Int) {
        ImageViewCompat.setImageTintList(
            view.imvCatalog,
            ColorStateList.valueOf(
                ContextCompat.getColor(
                    context,
                    R.color.colorNonSelectBottomNavigation
                )
            )
        )
        view.tvCatalog.setTextColor(
            ContextCompat.getColor(
                context,
                R.color.colorNonSelectBottomNavigation
            )
        )

        ImageViewCompat.setImageTintList(
            view.imvCalculate,
            ColorStateList.valueOf(
                ContextCompat.getColor(
                    context,
                    R.color.colorNonSelectBottomNavigation
                )
            )
        )
        view.tvCalculate.setTextColor(
            ContextCompat.getColor(
                context,
                R.color.colorNonSelectBottomNavigation
            )
        )

        ImageViewCompat.setImageTintList(
            view.imvCameraTmp,
            ColorStateList.valueOf(
                ContextCompat.getColor(
                    context,
                    R.color.colorNonSelectBottomNavigation
                )
            )
        )

        ImageViewCompat.setImageTintList(
            view.imvRoom,
            ColorStateList.valueOf(
                ContextCompat.getColor(
                    context,
                    R.color.colorNonSelectBottomNavigation
                )
            )
        )

        view.tvRoom.setTextColor(
            ContextCompat.getColor(
                context,
                R.color.colorNonSelectBottomNavigation
            )
        )

        view.tvCamera.setTextColor(
            ContextCompat.getColor(
                context,
                R.color.colorNonSelectBottomNavigation
            )
        )

        when (tab) {
            0 -> {
                ImageViewCompat.setImageTintList(
                    view.imvCatalog,
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorAccent))
                )
                view.tvCatalog.setTextColor(ContextCompat.getColor(context, R.color.colorAccent))
            }
            1 -> {
                ImageViewCompat.setImageTintList(
                    view.imvCameraTmp,
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorAccent))
                )
                view.tvCamera.setTextColor(ContextCompat.getColor(context, R.color.colorAccent))
            }
            2 -> {
                view.tvRoom.text = context.getString(R.string.room)
                ImageViewCompat.setImageTintList(
                    view.imvRoom,
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorAccent))
                )
                view.tvRoom.setTextColor(ContextCompat.getColor(context, R.color.colorAccent))
            }
            3 -> {
                view.tvCalculate.text = context.getString(R.string.calculate)
                ImageViewCompat.setImageTintList(
                    view.imvCalculate,
                    ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorAccent))
                )
                view.tvCalculate.setTextColor(ContextCompat.getColor(context, R.color.colorAccent))
            }
        }
    }

    private fun initInstance() {
        view.layoutCatalog.setOnClickListener {
            selectNav(0)
            navBarListener.onSelect(0)
        }
        view.layoutCalculate.setOnClickListener {
            selectNav(3)
            navBarListener.onSelect(3)
        }
        view.layoutCamera.setOnClickListener {
            selectNav(1)
            navBarListener.onSelect(1)
        }
        view.layoutRoom.setOnClickListener {
            selectNav(2)
            navBarListener.onSelect(2)
        }
    }

    fun setNavBarListener(navBarListener: NavBarListener) {
        this.navBarListener = navBarListener
    }
}
