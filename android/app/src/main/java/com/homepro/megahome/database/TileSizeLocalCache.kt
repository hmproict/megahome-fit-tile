package com.homepro.megahome.database

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import com.homepro.megahome.model.TileSize
import com.homepro.megahome.model.TileType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class TileSizeLocalCache(
    private val sizesDao: TileSizeDatabaseDao
) {
    fun insert(tiles: List<TileSize>, insertFinished: () -> Unit) {
        GlobalScope.launch(Dispatchers.IO) {
            sizesDao.insertAll(tiles)
            insertFinished()
        }
    }

    fun sizes(tileType: TileType): LiveData<List<TileSize>> {
        return when (tileType) {
            TileType.ALL -> sizesDao.sizesFactory()
            TileType.WALL -> sizesDao.sizesByType("Wall")
            TileType.FLOOR -> sizesDao.sizesByType("Floor")
        }
    }
}