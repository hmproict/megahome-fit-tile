package com.homepro.megahome.database

import androidx.paging.DataSource
import com.homepro.megahome.model.Size
import com.homepro.megahome.model.Tile
import com.homepro.megahome.model.TileType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class TileLocalCache(
    private val tilesDao: TileDatabaseDao
) {
    fun insert(tiles: List<Tile>, insertFinished: () -> Unit) {
        GlobalScope.launch(Dispatchers.IO) {
            tilesDao.insertAll(tiles)
            insertFinished()
        }
    }

    suspend fun tile(id: String): Tile? {
        return tilesDao.tileById(id)
    }

    fun tiles(tileType: TileType, size: Size? = null): DataSource.Factory<Int, Tile> {
        size?.let { size ->
            return when (tileType) {
                TileType.ALL -> tilesDao.tilesBySize(size.width.toDouble(), size.length.toDouble())
                TileType.WALL -> tilesDao.tilesByTypeAndSize("Wall", size.width.toDouble(), size.length.toDouble())
                TileType.FLOOR -> tilesDao.tilesByTypeAndSize("Floor", size.width.toDouble(), size.length.toDouble())
            }
        }

        return when (tileType) {
            TileType.ALL -> tilesDao.tilesFactory()
            TileType.WALL -> tilesDao.tilesByType("Wall")
            TileType.FLOOR -> tilesDao.tilesByType("Floor")
        }
    }
}