package com.homepro.megahome.api

import com.google.gson.annotations.SerializedName

data class CalculateResponse(
    @SerializedName("id")
    val id: String,
    @SerializedName("box")
    val box: String
)