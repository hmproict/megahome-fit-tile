package com.homepro.megahome

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class MapUnitTest {
    @Test
    fun mergeMap() {
        val map1 = mapOf<Int, String>(20 to "20x20", 40 to "40x40")
        val map2 = mapOf<Int, String>(20 to "20x20", 30 to "30x30")
        assertEquals(mapOf(30 to "30x30"), map2 - map1.keys)
    }
}
