//
//  DGTCalendarView.h
//  DGTEngine
//
//  Created by Aduldach Pradubyart on 31/12/2554.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import "DGTCalendarViewDelegate.h"

@interface DGTCalendarView : UIView

@property (nonatomic, unsafe_unretained) id<DGTCalendarViewDelegate> delegate;

@property (nonatomic, strong) NSDate *currentDate;
@property (nonatomic, assign) CGFloat buttonHeight;
@property (nonatomic, assign) IBInspectable BOOL showHeader;

- (void)registerCalendarDateButtonClass:(Class)buttonClass;
- (void)registerCalendarHeaderViewClass:(Class)headerClass;

- (void)selectButtonDate:(NSDate*)date;
- (void)fillCalendar;

@end
