//
//  DGTApplicationLanguage.h
//  DGTEngine
//
//  Created by Aduldach Pradubyart on 12/17/2556 BE.
//  Copyright (c) 2556 Digitopolisstudio.co.ltd. All rights reserved.
//

typedef NS_OPTIONS(NSUInteger, DGTLanguage) {
    DGTLanguageNone = 0,
    DGTLanguageEng,
    DGTLanguageTha,
};

// Notification when change default language
extern NSString * const kDGTAppLanguageChangeNotification;

@interface DGTAppLanguage : NSObject

@property (class, readonly) DGTLanguage base;
    
/**
 *  @Author Aduldach Pradubyart, 14-09-30 13:09:52
 *
 *  Get current application language
 *
 *  @return DGTDefaultLanguage for default language
 */
@property (class, readonly) DGTLanguage current;
    
@property (class, readonly) NSLocale *locale;
    
+ (BOOL)currentLanguageIsBaseLanguage;

+ (BOOL)setBaseLanguage:(DGTLanguage)langauage;
/**
 *  @Author Aduldach Pradubyart, 14-09-30 13:09:34
 *
 *  The default language method is prefer for localizetion application by recived DGTDefaultLanguage.
 *
 *  @param langauage DGTDefaultLanguage for set default
 *
 *  @return success to set it
 */
+ (BOOL)setLangauge:(DGTLanguage)langauage;

/**
 *  @Author Aduldach Pradubyart, 14-09-30 13:09:31
 *
 *  Localize string by recived key of text in .string
 *
 *  @warning please check .string file is added to project
 *
 *  @param key text of key in .string file
 *
 *  @return string of localize
 */
+ (NSString *)localizedString:(NSString *)key;

+ (UIImage*)localizedImageByName:(NSString*)imageName;

@end

//------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////
// NSlocale
////////////////////////////////////////////////////////////////////////////////

#pragma mark -
#pragma mark NSlocale
#pragma mark

//------------------------------------------------------------------------------
@interface NSLocale (DGTAppLanguage)

@property (class, readonly) NSLocale *appLocale;
    
@end
//------------------------------------------------------------------------------

UIKIT_STATIC_INLINE NSString * DGTLocalizedString(NSString *key) {
    return [DGTAppLanguage localizedString:key];
}

UIKIT_STATIC_INLINE id DGTLocalizedObject(id enObject, id otherObject) {
    if (DGTAppLanguage.current == DGTLanguageEng) {
        return enObject;
    }
    return otherObject;
}
