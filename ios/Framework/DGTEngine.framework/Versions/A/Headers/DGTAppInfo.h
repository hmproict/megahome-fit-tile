//
//  DGTApplicationInfo.h
//  DGTEngine
//
//  Created by Aduldach Pradubyart on 5/2/2557 BE.
//  Copyright (c) 2557 Digitopolisstudio.co.ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DGTAppInfo : NSObject

+ (id)objectForKey:(NSString*)key;
  
@end
