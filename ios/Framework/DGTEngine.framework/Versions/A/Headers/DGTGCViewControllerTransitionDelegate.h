//
//  DGTGCViewControllerTransitionDelegate.h
//  DGTEngine
//
//  Created by Aduldach Pradubyart on 6/4/2557 BE.
//  Copyright (c) 2557 Digitopolisstudio.co.ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DGTGCViewController;

@protocol DGTGCViewControllerTransitionDelegate <NSObject>

@optional
- (void)updateTransitionForCGViewController:(DGTGCViewController*)cgViewController withTranslationValue:(CGFloat)translation;
    
@end
