//
//  DGTCalendarHeaderView.h
//  DGTEngine
//
//  Created by Saeed Pradubyart on 2/11/2559 BE.
//  Copyright © 2559 Digitopolisstudio.co.ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DGTCalendarHeaderView : UIView

@property (nonatomic, strong) IBInspectable UIFont *font;
@property (nonatomic, strong) IBInspectable UIColor *textColor;
@property (nonatomic, strong, readonly) NSArray<UILabel *> *labels;

@end
