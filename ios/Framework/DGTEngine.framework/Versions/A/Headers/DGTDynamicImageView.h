//
//  DGTDynamicImageView.h
//  DGTEngine
//
//  Created by Aduldach Pradubyart on 12/19/2557 BE.
//  Copyright (c) 2557 Digitopolisstudio.co.ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef UIImage* (^DGTDynamicImageViewDrawingBlock)(CGRect frame);

@interface DGTDynamicImageView : UIView

@property (nonatomic, strong) UIImage *image;

- (void)drawImageByBlock:(UIImage* (^)(CGRect frame))drawBlock;
- (void)drawImageIdentifier:(NSString*)identifier byBlock:(DGTDynamicImageViewDrawingBlock)drawBlock;

@end
