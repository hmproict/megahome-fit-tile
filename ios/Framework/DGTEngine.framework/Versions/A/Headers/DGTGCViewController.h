//
//  DGTGCViewController.h
//  DGTEngine
//
//  Created by Aduldach Pradubyart on 2/22/2557 BE.
//  Copyright (c) 2557 Digitopolisstudio.co.ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DGTGCViewControllerTransitionDelegate.h"

@interface DGTGCViewController : UIViewController

@property (nonatomic, assign) BOOL enableGCControl;
@property (nonatomic, assign) CGSize contentSize;
@property (nonatomic, strong) UIColor *bgColor;
@property (nonatomic, weak) id<DGTGCViewControllerTransitionDelegate> transitionDelegate;

- (UIScrollView*)registerGCScrollView;
- (BOOL)isPresenting;

- (void)presentGCViewController:(UIViewController*)parentViewController completion:(void (^)(void))completion;
- (void)dismissGCViewControllerCompletion:(void (^)(void))completion;;

@end
