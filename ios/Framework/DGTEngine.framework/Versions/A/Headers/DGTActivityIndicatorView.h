//
//  DGTActivityIndicatorView.h
//  DGTEngine
//
//  Created by Aduldach Pradubyart on 7/11/56 BE.
//  Copyright (c) 2556 Digitopolisstudio.co.ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DGTActivityIndicatorView : UIActivityIndicatorView

@end
