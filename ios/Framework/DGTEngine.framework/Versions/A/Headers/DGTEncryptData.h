//
//  DGTEncryptData.h
//  DGTEngine
//
//  Created by Saeed Pradubyart on 1/28/2559 BE.
//  Copyright © 2559 Digitopolisstudio.co.ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DGTEncryptData : NSObject

+ (NSData *)AES256EncryptData:(NSData*)data withKey:(NSString *)key;
+ (NSData *)AES256DecryptData:(NSData*)data withKey:(NSString *)key;

+ (NSData *)PKCS7EncryptData:(NSData*)data withKey:(NSString *)key;

@end
