//
//  DGTWebViewController.h
//  DGTEngine
//
//  Created by Aduldach Pradubyart on 7/11/56 BE.
//  Copyright (c) 2556 Digitopolisstudio.co.ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>

#import "DGTWebViewControllerDelegate.h"

@interface DGTWebViewController : UIViewController

- (instancetype)initWithUrl:(NSString*)urlLink;
- (instancetype)initWithUrlRequest:(NSURLRequest*)urlRequest;

@property (nonatomic, strong) NSString *urlLink;
@property (nonatomic, strong, readonly) WKWebView *webView;
@property (nonatomic, assign) BOOL isOpenLinkInSafari;
@property (nonatomic, assign) BOOL isHideNavigationBar;
@property (nonatomic, unsafe_unretained) id<DGTWebViewControllerDelegate> delegate;

@end
