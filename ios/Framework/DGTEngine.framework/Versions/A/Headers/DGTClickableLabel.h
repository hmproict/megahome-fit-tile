//
//  DGTClickableLabel.h
//  DGTEngine
//
//  Created by Aduldach Pradubyart on 6/6/56 BE.
//  Copyright (c) 2556 Digitopolisstudio.co.ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DGTClickableLabel : UILabel

typedef void (^onClickableLabel)(void);

@property (nonatomic, copy) onClickableLabel onClickThisLink;

- (instancetype)initWithTitle:(NSString*)title forFont:(UIFont*)font NS_DESIGNATED_INITIALIZER;

- (void)addActionForTarget:(id)target selector:(SEL)selector;

@end
