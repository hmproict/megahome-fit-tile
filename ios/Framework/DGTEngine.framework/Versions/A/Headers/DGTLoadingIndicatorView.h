//
//  DGTLoadingIndicatorView.h
//  DGTEngine
//
//  Created by Aduldach Pradubyart on 7/11/56 BE.
//  Copyright (c) 2556 Digitopolisstudio.co.ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DGTLoadingIndicatorView : UIView

@property (nonatomic, assign) NSTimeInterval animationDuration;

- (instancetype)initWithSpriteImage:(UIImage*)image size:(CGSize)size;
- (void)startAnimating;
- (void)stopAnimating;
- (BOOL)isAnimating;

@end
