//
//  DGTWebViewControllerDelegate.h
//  DGTEngine
//
//  Created by Aduldach Pradubyart on 3/6/2557 BE.
//  Copyright (c) 2557 Digitopolisstudio.co.ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>

@class DGTWebViewController;

@protocol DGTWebViewControllerDelegate <NSObject>

@optional
- (void)webViewControllerDidFinishController:(DGTWebViewController *)viewController;
- (void)webViewControllerWebViewLoadFinishController:(DGTWebViewController *)viewController;
- (BOOL)webViewController:(DGTWebViewController *)viewController handleError:(NSError*)error;
- (BOOL)webViewController:(DGTWebViewController *)viewController handleLoadRequest:(NSURLRequest *)request navigationType:(WKNavigationType)navigationType;
- (void)webViewController:(DGTWebViewController *)viewController allowPolicyOfRequest:(NSURLRequest *)request decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler;

@end
