//
//  DGTEngine.h
//  DGTEngine
//
//  Created by Aduldach Pradubyart on 8/16/2557 BE.
//  Copyright (c) 2557 Digitopolisstudio.co.ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

//! Project version number for DGTEngine.
FOUNDATION_EXPORT double DGTEngineVersionNumber;

//! Project version string for DGTEngine.
FOUNDATION_EXPORT const unsigned char DGTEngineVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DGTEngine/PublicHeader.h>

#define APPDELEGATE ((AppDelegate*)[UIApplication sharedApplication].delegate)

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#define IS_IPHONE_5 (IS_IPHONE && IS_HEIGHT_568)
#define HEIGHT_I5 568.0f
#define IS_HEIGHT_568 (SCREENSIZE.height >= 568.0f)

#define IS_RETINA ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&([UIScreen mainScreen].scale == 2.0))

#define IS_OSUPPER(os) ([[[UIDevice currentDevice] systemVersion] floatValue] >= os)
#define IS_OSCURRENT(os) (floor([[[UIDevice currentDevice] systemVersion] floatValue]) == os)
#define IS_OSLOWER(os) ([[[UIDevice currentDevice] systemVersion] floatValue] < os)

#define IS_STATUSBAR_LANDSCAPE (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation))

#define kStatusBarHeight (IS_IPAD?20:MIN([UIApplication sharedApplication].statusBarFrame.size.width,[UIApplication sharedApplication].statusBarFrame.size.height))

#define SCREENSIZE [[UIScreen mainScreen] bounds].size

#define NIBNAME_BY_DEVICE(nibName) ((IS_IPAD)?[nibName stringByAppendingString:@"_iPad"]:nibName)

#define INFO_DICT_FOR_KEY(key) [[NSBundle mainBundle] objectForInfoDictionaryKey:key]

#import "DGTAppLanguage.h"
#import "DGTAppInfo.h"
