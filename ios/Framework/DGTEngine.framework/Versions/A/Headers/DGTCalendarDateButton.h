//
//  DGTCalendarDateButton.h
//  DGTEngine
//
//  Created by Aduldach Pradubyart on 24/12/2554.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_OPTIONS(NSUInteger, DGTCalendarDateStyle) {
    DGTCalendarDateStyleNormal = 1 << 0,
    DGTCalendarDateStyleToday = 1 << 1,
    DGTCalendarDateStyleNotInMonth = 1 << 2,
};

@interface DGTCalendarDateButton : UIButton

@property (nonatomic, strong, readonly, nonnull) NSDate *date;
@property (nonatomic, strong, nullable) NSArray *events;
@property (nonatomic, strong, readonly, nullable) UILabel *todayLabel;
@property (nonatomic, strong, readonly, nullable) UIImageView *eventImageView;
@property (nonatomic, readonly) DGTCalendarDateStyle calendarStyle;

- (void)prepareForReuse;
- (void)setButtonDate:(NSDate*_Nonnull)date forButtonStyle:(DGTCalendarDateStyle)style;

@end
