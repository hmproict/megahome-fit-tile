//
//  DGTViewCapture.h
//  DGTViewCapture
//
//  Created by Aduldach Pradubyart on 7/11/56 BE.
//  Copyright (c) 2556 Digitopolisstudio.co.ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

///
/// A DGTViewCapture class.
/// This class provide ability to capture screenshot and save
/// it to image gallery.
///
@interface DGTViewCapture : NSObject

///
/// Save the given view to screenshot.
/// @param view
/// the view for save to photo album.
/// @param onCompletionBlock
/// A block object to be executed when the save screenshot. This block has no return value and takes
/// a single Boolean argument that indicates whether or not screenshot is incomplate
///

+ (void)saveScreenshotToPhotosAlbumWithView:(UIView *)view completion:(void (^)(BOOL success, NSError *error))onCompletionBlock;

///
/// Save the given image to screenshot.
/// @param image
/// the image for save to photo album.
/// @param onCompletionBlock
/// A block object to be executed when the save screenshot. This block has no return value and takes
/// a single Boolean argument that indicates whether or not screenshot is incomplate
///

+ (void)saveScreenshotToPhotosAlbumWithImage:(UIImage *)image completion:(void (^)(BOOL success, NSError *error))onCompletionBlock;

///
/// A method for capture screenshot and return UIImage object.
/// @param view
/// the view for save to photo album.
/// @return UIImage
/// A image for capture screenshot.
///
+ (UIImage*)captureView:(UIView *)view;

@end
