//
//  DGTPushNotificationHendler.h
//  DGTEngine
//
//  Created by Aduldach Pradubyart on 2/27/2557 BE.
//  Copyright (c) 2557 Digitopolisstudio.co.ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^PushNotificationCompletion)(NSData *response, NSError *error);

@interface DGTPushNotificationHendler : NSObject

+ (void)addPushNotificationToken:(NSString*)token completion:(PushNotificationCompletion)completion;
+ (void)addPushNotificationToken:(NSString*)token serviceURL:(NSURL*)url serviceAuthen:(NSString*)authen completion:(PushNotificationCompletion)completion;

@end
