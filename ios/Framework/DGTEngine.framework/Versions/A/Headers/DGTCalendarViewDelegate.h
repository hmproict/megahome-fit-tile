//
//  DGTCalendarViewDelegate.h
//  DGTEngine
//
//  Created by Aduldach Pradubyart on 31/12/2554.
//  Copyright (c) 2554 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DGTCalendarView;
@class DGTCalendarDateButton;
@protocol DGTCalendarViewDelegate <NSObject>

@optional
- (void)calendarView:(DGTCalendarView *)calendarView didSelectedButton:(DGTCalendarDateButton *)button;
- (void)calendarView:(DGTCalendarView *)calendarView didSelectedRepeatButton:(DGTCalendarDateButton *)button;
- (void)calendarView:(DGTCalendarView *)calendarView didSelecteNextMonthButton:(DGTCalendarDateButton *)button;
- (void)calendarView:(DGTCalendarView *)calendarView didSelectePrevMonthButton:(DGTCalendarDateButton *)button;

- (void)calendarView:(DGTCalendarView *)calendarView customizeButton:(DGTCalendarDateButton *)button;

@end
