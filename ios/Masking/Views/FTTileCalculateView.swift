//
//  FTTileCalculateView.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 18/2/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import Foundation
import DGTEngineSwift

class FTTileSizeListButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let style = AppUIStyle(traitCollection: traitCollection)
        backgroudImage = UIImage(color: .white, size: style.cornerInset.resizableSize, borderColor: .appDarkGray, borderWidth: 1, cornerRadius: style.cornerRedius)?.resizableImage()
        
        image = #imageLiteral(resourceName: "icon_arrow_down")
        updateUI(for: traitCollection)
        updateFont(for: traitCollection)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updateUI(for: traitCollection)
        updateFont(for: traitCollection)
    }
    
    func updateUI(for traitCollection: UITraitCollection) {
        let style = AppUIStyle(traitCollection: traitCollection)
        
    }
    
    func updateFont(for traitCollection: UITraitCollection) {
        let style = AppTextStyle(traitCollection: traitCollection)
        self.titleLabel?.font = style.font(style: .H6)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let centerY = self.centerPointOfView().y
        self.titleLabel?.sizeToFit()
        self.titleLabel?.center.y = centerY
        
        let style = AppUIStyle(traitCollection: traitCollection)
        self.titleLabel?.frame.origin.x = style.space
        
        self.imageView?.sizeToFit()
        self.imageView?.center.y = centerY
        let imageWidth = self.image?.size.width ?? 20
        self.imageView?.frame.origin.x = bounds.width - (imageWidth + style.space)
    }
}

class FTTileSizeCatalogButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let style = AppUIStyle(traitCollection: traitCollection)
        backgroudImage = UIImage(color: .white, size: style.cornerInset.resizableSize, borderColor: .appDarkGreen, borderWidth: 1, cornerRadius: style.cornerRedius)?.resizableImage()
        
        titleLabel?.textAlignment = .center
        setTitleColor(.appDarkGray, for: .normal)
        
        image = #imageLiteral(resourceName: "tabbar_catalog").tinted(.appDarkGray)
        title = LocalizedString("CalculateTileCatalogTitle")
        imageView?.contentMode = .scaleAspectFit
        invalidateIntrinsicContentSize()
        updateUI(for: traitCollection)
        updateFont(for: traitCollection)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updateUI(for: traitCollection)
        updateFont(for: traitCollection)
    }
    
    func updateUI(for traitCollection: UITraitCollection) {
        let style = AppUIStyle(traitCollection: traitCollection)
        
    }
    
    func updateFont(for traitCollection: UITraitCollection) {
        let style = AppTextStyle(traitCollection: traitCollection)
        self.titleLabel?.font = style.font(style: .H9)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let centerX = self.centerPointOfView().x
        self.titleLabel?.sizeToFit()
        let height = self.titleLabel?.bounds.height ?? 20
        
        self.titleLabel?.frame.origin.x = 0
        self.titleLabel?.frame.origin.y = bounds.height - height
        self.titleLabel?.frame.size.width = bounds.width
        
        self.imageView?.sizeToFit()
        self.imageView?.frame.origin.y = 2
        self.imageView?.frame.size.width = bounds.width
        self.imageView?.center.x = centerX
    }
    
    override var intrinsicContentSize: CGSize {
        guard let size = image?.size, let text = title, let font = titleLabel?.font else {
            return super.intrinsicContentSize
        }
        
        let textSize = text.size(font: font)
        return CGSize(width: ceil(max(textSize.width, size.width)) + 10, height: ceil(size.height + textSize.height) + 2)
    }
}

class FTTileInfoTextField: UITextField {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let style = AppUIStyle(traitCollection: traitCollection)
        background = UIImage(color: .white, size: style.cornerInset.resizableSize, borderColor: .appDarkGray, borderWidth: 1, cornerRadius: style.cornerRedius)?.resizableImage()
        
        let textStyle = AppTextStyle(traitCollection: traitCollection)
        font = textStyle.font(style: .H4)
        textAlignment = .center
        textColor = .black
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        let style = AppUIStyle()
        return bounds.insetBy(dx: style.cornerRedius, dy: 0)
    }
}

class FTCalculateAppButton: AppButton {
    
    override var customFont: UIFont {
        return AppTextStyle(regular: isRegularHorizontalSizeClass).font(style: .H2)
    }
    
    override func redrawBackgroundImage() {
        super.redrawBackgroundImage()
        
        self.tintColor = .white
        let style = AppUIStyle(traitCollection: self.traitCollection)
        let size = CGSize(width: (style.cornerRedius*2)+1, height: (style.cornerRedius*2)+1)
        let image = UIImage(color: .appDarkGreen, size: size, cornerRadius: style.cornerRedius)?.resizableImage()
        self.backgroudImage = image
        self.setTitleColor(.white, for: .normal)
    }
    
    override var intrinsicContentSize : CGSize {
        var size = super.intrinsicContentSize
        size.height = ceil(customFont.lineHeight) + 15
        size.width += 20
        return size
    }
    
}

class FTCalculateExtraButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let size = CGSize(width: 18, height: 18)
        let normalBgImage = UIImage(color: .white, size: size, borderColor: .appDarkGray, borderWidth: 1, cornerRadius: size.width/2)
        
        let bgImage = UIImage(color: .white, size: size, borderColor: .appDarkGreen, borderWidth: 1, cornerRadius: size.width/2)
        let image = UIImage(color: .appDarkGreen, size: CGSize(width: 10, height: 10), cornerRadius: 5)
        let result = bgImage?.imageAddingImage(image, offset: CGPoint(x: 9 - 5, y: 9 - 5))
        setTitleColor(.appDarkGray, for: .normal)
        self.image = normalBgImage
        selectedImage = result
        titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        updateFont(for: traitCollection)
        updateUI(for: traitCollection)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updateFont(for: traitCollection)
        updateUI(for: traitCollection)
    }
    
    func updateUI(for traitCollection: UITraitCollection) {
        let style = AppUIStyle(traitCollection: traitCollection)
        
    }
    
    func updateFont(for traitCollection: UITraitCollection) {
        let style = AppTextStyle(traitCollection: traitCollection)
        titleLabel?.font = style.font(style: .H6)
    }
    
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + 10, height: size.height)
    }
}

class FTSegmentControl: UIControl {
    
    fileprivate let contentView: UIStackView = {
        let view = UIStackView()
        view.alignment = .fill
        view.distribution = .fillEqually
        view.axis = .horizontal
        view.prepareForAutoLayout()
        view.backgroundColor = .clear
        view.isUserInteractionEnabled = false
        return  view
    }()
    
    fileprivate let bgImageView: UIImageView = {
        let imageView = UIImageView.autoLayout()
        let style = AppUIStyle()
        imageView.image = UIImage(color: .white, size: style.cornerInset.resizableSize, borderColor: .appDarkGreen, borderWidth: 1, cornerRadius: style.cornerRedius)?.resizableImage()
        return  imageView
    }()
    
    var fontStyle = AppFontStyle.H6 {
        didSet {
            items.forEach {
                $0.fontStyle = fontStyle
            }
        }
    }
    
    var hilightFontStyle = AppFontStyle.H5 {
        didSet {
            items.forEach {
                $0.hilightFontStyle = hilightFontStyle
            }
        }
    }
    
    var items = [FTSegmentContentView]()
    var contentClass: FTSegmentContentView.Type?
    
    lazy var growImageView: UIImageView = {
        let style = AppUIStyle()
        let image = UIImage(color: .appDarkGreen, size: style.cornerInset.resizableSize, cornerRadius: style.cornerRedius)?.resizableImage()
        let imageView = UIImageView(image: image)
        return imageView
    }()
    
    var selectedSegmentIndex: Int {
        set {
            items.forEach {
                $0.isSelected = false
            }
            if let view = items[safe: newValue], !view.isSelected {
                view.isSelected = true
                startAnimation(for: view)
            }
        }
        get {
            return items.firstIndex(where: { $0.isSelected }) ?? 0
        }
    }
    
    convenience init(items: [String], contentClass: FTSegmentContentView.Type = FTSegmentContentView.self) {
        self.init(frame: CGRect.zero)
        
        self.contentClass = contentClass
        isUserInteractionEnabled = true
        addSubview(contentView)
        contentView.constraintsAroundView()?.activated()
        
        for (index, item) in items.enumerated() {
            setTitle(item, forSegmentAtIndex: index)
        }
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        
        isUserInteractionEnabled = true
        
        addSubview(bgImageView)
        bgImageView.constraintsAroundView()?.activated()
        
        addSubview(growImageView)
        
        addSubview(contentView)
        contentView.constraintsAroundView()?.activated()
    }
    
    func setImage(_ image: UIImage, hilightImage: UIImage? = nil, forSegmentAtIndex segment: Int) {
        
        if let view = items[safe: segment] {
            view.setImage(image, hilightImage: hilightImage)
        } else {
            setTitle("", forSegmentAtIndex: segment)
            let view = items[safe: segment]
            view?.setImage(image, hilightImage: hilightImage)
        }
        self.invalidateIntrinsicContentSize()
    }
    
    func setTitle(_ title: String, forSegmentAtIndex segment: Int) {
        
        if let view = items[safe: segment] {
            view.titleLabel.text = title
        } else {
            
            let view: FTSegmentContentView = {
                if let content = self.contentClass {
                    return content.init(title: title)
                }
                return FTSegmentContentView(title: title)
            }()
            view.fontStyle = fontStyle
            view.hilightFontStyle = hilightFontStyle
            contentView.addArrangedSubview(view)
            items.append(view)
        }
        self.invalidateIntrinsicContentSize()
    }
    
    override var intrinsicContentSize : CGSize {
        var size = super.intrinsicContentSize
        let font = AppTextStyle(traitCollection: traitCollection).font(style: fontStyle)
        size.height = ceil(font.lineHeight) + 10
        if items.isEmpty {
            return size
        }
        let width = items.reduce(CGFloat(), { max($1.intrinsicContentSize.width, $0) })
        size.width = CGFloat(items.count) * width
        return size
    }
    
    override func addSubview(_ view: UIView) {
        super.addSubview(view)
        sendSubviewToBack(bgImageView)
        bringSubviewToFront(contentView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let width = bounds.width / CGFloat(items.count)
        growImageView.frame = CGRect(x: width * CGFloat(selectedSegmentIndex), y: 0, width: width, height: bounds.height)
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        if let view = items[safe: selectedSegmentIndex] {
            growImageView.center.x = view.convert(view.bounds, to: self).midX
        }
    }
    
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        let point = touch.location(in: self)
        
        if let view = items.first(where: { $0.frame.offsetBy(dx: 5, dy: 5).contains(point) }) {
            view.isHighlighted = true
            startAnimation(for: view)
            return true
        }
        return false
    }
    
    override func continueTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        let point = touch.location(in: self)
        
        for view in items {
            let hit = view.frame.offsetBy(dx: 5, dy: 5).contains(point)
            view.isHighlighted = view.isSelected || hit
            
            if hit {
               startAnimation(for: view)
            }
        }
        return true
    }
    
    override func endTracking(_ touch: UITouch?, with event: UIEvent?) {
        guard let point = touch?.location(in: self) else {
            return
        }
        let hitView = items.first(where: { $0.frame.offsetBy(dx: 5, dy: 5).contains(point) })
        for view in items {
            view.isHighlighted = false
            if let hitView = hitView {
                view.isSelected = hitView == view
            }
        }
        
        let animationView: UIView?
        if let view = hitView {
            animationView = view
            sendActions(for: .valueChanged)
        } else {
            animationView = items.first(where: { $0.isSelected })
        }
        
        if let view = animationView {
            startAnimation(for: view)
        }
    }
    
    override func cancelTracking(with event: UIEvent?) {
        super.cancelTracking(with: event)
        
        items.forEach({
            if $0.isSelected {
                startAnimation(for: $0)
            } else {
                $0.isHighlighted = false
            }
        })
    }
    
    func startAnimation(for view: UIView) {
        
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut, .beginFromCurrentState], animations: {
            self.growImageView.center.x = view.convert(view.bounds, to: self).midX
        }, completion: nil)
    }
    
    override func sizeToFit() {
        super.sizeToFit()
        self.frame.size = self.intrinsicContentSize
    }
}

class FTSegmentContentView: UIView {
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textAlignment = .center
        label.prepareForAutoLayout()
        label.font = AppTextStyle().font(style: .H6)
        label.textColor = .appDarkGray
        label.highlightedTextColor = .white
        label.setContentHuggingPriority(UILayoutPriority.required, for: .vertical)
        label.setContentCompressionResistancePriority(UILayoutPriority.required, for: .vertical)
        return label
    }()
    
    var imageView: UIImageView?
    
    var isSelected: Bool = false {
        didSet {
            titleLabel.isHighlighted = isSelected
            imageView?.isHighlighted = isSelected
            if let hilightFontStyle = hilightFontStyle {
                titleLabel.font = AppTextStyle(traitCollection: traitCollection).font(style: isSelected ? hilightFontStyle : fontStyle)
            }
        }
    }
    
    var isHighlighted: Bool = false {
        didSet {
            titleLabel.isHighlighted = isHighlighted || isSelected
            imageView?.isHighlighted = isHighlighted || isSelected
            
            if let hilightFontStyle = hilightFontStyle {
                titleLabel.font = AppTextStyle(traitCollection: traitCollection).font(style: isHighlighted || isSelected ? hilightFontStyle : fontStyle)
            }
        }
    }
    
    var fontStyle = AppFontStyle.H6 {
        didSet {
            titleLabel.font = AppTextStyle(traitCollection: traitCollection).font(style: fontStyle)
        }
    }
    
    var hilightFontStyle: AppFontStyle?
    
    required init(title: String) {
        super.init(frame: CGRect.zero)
        
        backgroundColor = .clear
        titleLabel.text = title
        
        let style = AppUIStyle()
        layoutMargins = UIEdgeInsets(top: style.space/2, left: style.cornerRedius, bottom: style.space/2, right: style.cornerRedius)
        prepareForAutoLayout()
        addSubview(titleLabel)
        titleLabel.constraintsAroundView(margin: true)?.activated()
    }
    
    func setImage(_ image: UIImage, hilightImage: UIImage? = nil) {
        titleLabel.textAlignment = .left
        if let imageView = imageView {
            if let hilightImage = hilightImage {
                imageView.image = image
                imageView.highlightedImage = hilightImage
            } else {
                imageView.highlightedImage = image.tinted(.white)
                imageView.image = image.tinted(.appLightGray)
            }
        } else {
            let imageView: UIImageView = {
                if let hilightImage = hilightImage {
                    return UIImageView(image: image, highlightedImage: hilightImage)
                } else {
                    let hilightImage = image.tinted(.white)
                    let image = image.tinted(.appLightGray)
                    return UIImageView(image: image, highlightedImage: hilightImage)
                }
            }()
            
            imageView.prepareForAutoLayout()
            imageView.setContentHuggingPriority(UILayoutPriority.required, for: .horizontal)
            imageView.setContentCompressionResistancePriority(UILayoutPriority.required, for: .horizontal)
            addSubview(imageView)
            if let constraint = findConstraint(view: titleLabel, attribute: .left) {
                removeConstraint(constraint)
            }
            
            let margin = AppUIStyle().cornerRedius
            addConstraint(imageView.left == left + (margin/2))
            addConstraint(imageView.right == titleLabel.left - (margin/2))
            addConstraint(imageView.centerY == titleLabel.centerY)
            self.imageView = imageView
        }
    }
    
    @available(iOS 8.0, *)
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        updateViewFont(isRegularHorizontalSizeClass)
    }
    
    func updateViewFont(_ regular: Bool? = nil) {
        if let regular = regular {
            titleLabel.font = AppTextStyle(regular: regular).font(style: fontStyle)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var intrinsicContentSize : CGSize {
        var size = super.intrinsicContentSize
        let textStyle = AppTextStyle(traitCollection: traitCollection)
        let font = textStyle.font(style: fontStyle)
        size.height = ceil(font.lineHeight) + layoutMargins.top + layoutMargins.bottom
        
        if let text = titleLabel.text {
            let space = layoutMargins.left + layoutMargins.right
            if let hilightFontStyle = hilightFontStyle {
                let hilightFont = textStyle.font(style: hilightFontStyle)
                size.width = ceil(max(text.size(font: font).width, text.size(font: hilightFont).width) + space)
            } else {
                size.width = ceil(text.size(font: font).width + space)
            }
        }
        return size
    }
}

