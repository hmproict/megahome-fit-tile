//
//  FTProjectCollectionViewCell.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 5/2/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import Foundation
import DGTEngineSwift

class FTProjectCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet internal weak var titleLabel: UILabel!
    @IBOutlet internal weak var dateLabel: UILabel!
    
    @IBOutlet internal weak var titleView: UIView!
    @IBOutlet internal weak var dateBGView: UIImageView!
    
    @IBOutlet internal weak var imageView: MSCachedImageView!
    
    lazy var highlightView: UIView = {
        let view = UIView()
        view.prepareForAutoLayout()
        view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.399614726)
        view.isHidden = true
        contentView.insertSubview(view, aboveSubview: imageView)
        view.constraintsAroundView()?.activated()
        return view
    }()
    
    override var isHighlighted: Bool {
        didSet {
            highlightView.isHidden = !isHighlighted
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let style = AppUIStyle(traitCollection: traitCollection)
        
        contentView.layer.cornerRadius = style.cornerRedius
        contentView.layer.borderWidth = 1.0
        contentView.layer.borderColor = UIColor.clear.cgColor
        contentView.layer.masksToBounds = true
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 2.0)
        layer.shadowRadius = 2.0
        layer.shadowOpacity = 0.5
        layer.masksToBounds = false
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: contentView.layer.cornerRadius).cgPath
        
        updateUI(for: traitCollection)
        updateFont(for: traitCollection)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imageView.image = nil
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updateUI(for: traitCollection)
        updateFont(for: traitCollection)
    }
    
    func updateUI(for traitCollection: UITraitCollection) {
        let style = AppUIStyle(traitCollection: traitCollection)
        contentView.layer.cornerRadius = style.cornerRedius
        titleView.layoutMargins = UIEdgeInsets(top: 0, left: style.space, bottom: 0, right: style.space)
        dateBGView?.image = UIImage(color: .appGreen, size: CGSize(width: (style.cornerRedius*2)+1, height: (style.cornerRedius*2)+1), cornerInset: DGTCornerInset(topleft: 0, topright: 0, bottomleft: style.cornerRedius, bottomright: 0))?.resizableImage()
    }
    
    func updateFont(for traitCollection: UITraitCollection) {
        let style = AppTextStyle(traitCollection: traitCollection)
        titleLabel?.font = style.font(style: .H4)
        dateLabel?.font = style.font(style: .H3)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: contentView.layer.cornerRadius).cgPath
    }
    
    func updateElemetOfCell(_ project: TileProject, for traitCollection: UITraitCollection, contrainIn width: CGFloat) {
        
        let size = FTProjectCollectionViewCell.contentSize(for: traitCollection, contrainIn: width)
            
        if project.completed {
            
            let cacheKey = project.screenshotImageName + "Cover"
            if let image = UIImage(contentsOfFile: AppUtility.pathFile(project.screenshotImageName, in: .screenShot)) {
                let size = CGSize(width: size.width, height: ceil((image.size.height * size.width) / image.size.width))
                self.imageView.setImage(image, forCacheKey: cacheKey, size: size)
            } else if project.imageFittileURL != "" {
                NetworkService.requestImage(project.imageFittileURL) { (image, cache) in
                    if let image = image, let imageName = AppUtility.saveImageToDocument(image, folder: .screenShot) {
                        AppData.write {
                            project.screenshotImageName = imageName
                        }
                        let size = CGSize(width: size.width, height: ceil((image.size.height * size.width) / image.size.width))
                        self.imageView.setImage(image, forCacheKey: cacheKey, size: size)
                    }
                }
            }
        } else {
            
            let cacheKey = project.imageName + "Cover"
            if let image = UIImage(contentsOfFile: AppUtility.pathFile(project.imageName, in: .image)) {
                let size = CGSize(width: size.width, height: ceil((image.size.height * size.width) / image.size.width))
                self.imageView.setImage(image, forCacheKey: cacheKey, size: size)
            } else if project.imageURL != "" {
                NetworkService.requestImage(project.imageURL) { (image, cache) in
                    if let image = image, let imageName = AppUtility.saveImageToDocument(image, folder: .image) {
                        AppData.write {
                            project.imageName = imageName
                        }
                        let size = CGSize(width: size.width, height: ceil((image.size.height * size.width) / image.size.width))
                        self.imageView.setImage(image, forCacheKey: cacheKey, size: size)
                    }
                }
            }
        }
        
        titleLabel?.text = project.title
        
        if project.completed {
            let formatter = DateFormatter()
            formatter.dateStyle = .medium
            formatter.timeStyle = .none
            dateLabel?.text = formatter.string(from: project.date)
        } else {
            dateLabel?.text = project.shareProject ? LocalizedString("ProjectStatusFittile") : LocalizedString("ProjectStatusPending")
        }
    }
    
    class func contentSize(for traitCollection: UITraitCollection, contrainIn width: CGFloat) -> CGSize {
        let style = AppUIStyle(traitCollection: traitCollection)
        let contentWidth = width - style.space*2
        return CGSize(width: contentWidth, height: ceil((contentWidth/16)*7))
    }
}
