//
//  FTBGView.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 5/2/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import Foundation

class FTBGView: UIView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        
        let currentContext = UIGraphicsGetCurrentContext()
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let gradientOption = CGGradientDrawingOptions(rawValue: 0)
        
        let componentCount: Int = 2
        let components: [CGFloat] = [
            2/255, 138/255, 131/255, 1,
            0/255, 88/255, 84/255, 1,
            ]
        
        let locations: [CGFloat] = [0, 1]
        let gradient = CGGradient(colorSpace: colorSpace, colorComponents: components, locations: locations, count: componentCount)
        currentContext?.drawLinearGradient(gradient!, start: CGPoint(x: rect.maxX, y: rect.minY), end: CGPoint(x: rect.minX, y: rect.maxY), options: gradientOption)
        
        let image = #imageLiteral(resourceName: "bg_bottom").scaleImageToWidth(rect.width)!
        image.draw(in: CGRect(x: 0, y: rect.height-image.size.height, width: rect.width, height: image.size.height), blendMode: .colorBurn, alpha: 1)
    }
}
