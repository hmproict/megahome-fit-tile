//
//  TileView.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 10/1/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import UIKit
import Async
import DGTEngineSwift

class TileView: UIView {

    var scale: CGFloat {
        set {
            imageScale = min(10, max(0.5, newValue))
            self.imageWidth = defaultImageWidth * imageScale
        }
        get {
            return imageScale
        }
    }
    
    var location: CGPoint = .zero {
        didSet {
//            updateUI()
            self.setNeedsDisplay()
        }
    }
    
    private var imageWidth: CGFloat = 0 {
        didSet {
            if imageWidth != oldValue {
//                updateUI()
                self.setNeedsDisplay()
            }
        }
    }
    
    var tiltStyle: TileTiltStyle = .floor {
        didSet {
            switch tiltStyle {
            case .left:
                center = CGPoint(x: 0, y: imageRect.height/2)
                layer.anchorPoint = CGPoint(x: 0, y: 0.5)
            case .right:
                center = CGPoint(x: imageRect.width, y: imageRect.height/2)
                layer.anchorPoint = CGPoint(x: 1, y: 0.5)
            case .floor:
                center = CGPoint(x: imageRect.width/2, y: imageRect.height)
                layer.anchorPoint = CGPoint(x: 0.5, y: 1)
            }
        }
    }
    
    private var imageScale: CGFloat = 0
    private var imageRect: CGRect = .zero
    
    private var drawImage: UIImage?
    
    private let tileImage: UIImage
    private let defaultImageWidth: CGFloat
    
    private let queue: OperationQueue = {
        let queue = OperationQueue()
        queue.name = "RanderImage"
        queue.maxConcurrentOperationCount = 1
        queue.qualityOfService = .background
        return queue
    }()

    
    
    required init(frame: CGRect, tileImage: UIImage, lineColor: UIColor = .white) {
        
        self.tileImage = {
            
            let lineWidth = ceil((tileImage.size.width * 0.2) / 30)
            let drawSize = CGSize(width: tileImage.size.width + lineWidth, height: tileImage.size.height + lineWidth)
            UIGraphicsBeginImageContextWithOptions(drawSize, true, 0.0)
            
            let context = UIGraphicsGetCurrentContext()
            lineColor.setFill()
            context?.fill(CGRect(origin: .zero, size: drawSize))
            tileImage.draw(at: CGPoint(x: lineWidth/2, y: lineWidth/2))
            
            let resultImage = UIGraphicsGetImageFromCurrentImageContext() ?? tileImage
            UIGraphicsEndImageContext()
            return resultImage
        }()
        self.defaultImageWidth = ceil(tileImage.size.width/8)
        
        var viewFrame = frame.applying(CGAffineTransform(scaleX: 4, y: 4))
        viewFrame.origin.x = -(frame.width*2)
        viewFrame.origin.y = -(frame.height*2)
        super.init(frame: viewFrame)
        clipsToBounds = true
        layer.anchorPoint = CGPoint(x: 0.5, y: 1)
        scale = 1
//        updateUI()
        self.setNeedsDisplay()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func drawFrameChanged(to frame: CGRect) {
        imageRect = frame
        switch tiltStyle {
        case .left:
            self.center = CGPoint(x: 0, y: frame.height/2)
        case .right:
            self.center = CGPoint(x: frame.width, y: frame.height/2)
        case .floor:
            self.center = CGPoint(x: frame.width/2, y: frame.height)
        }
    }
    
    fileprivate func updateUI() {
        let rect = self.bounds
        let traitCollection = self.traitCollection
        
//        queue.cancelAllOperations()
//        queue.addOperation {
//            self.redrawImage(rect, for: traitCollection)
//            DispatchQueue.main.async {
//                self.image = self.drawImage
//            }
//        }
        Async.background { () -> Void in
            self.redrawImage(rect, for: traitCollection)
        }.main { () -> Void in
//            self.setNeedsDisplay()
//            self.image = self.drawImage
        }
    }

    fileprivate func redrawImage(_ rect: CGRect, for traitCollection: UITraitCollection) {

        let format: UIGraphicsImageRendererFormat
        if #available(iOS 11.0, *) {
            format = UIGraphicsImageRendererFormat(for: traitCollection)
        } else {
            format = UIGraphicsImageRendererFormat()
        }
        format.opaque = true
        format.prefersExtendedRange = false
        if #available(iOS 12.0, *) {
            format.preferredRange = .standard
        }
        format.scale = 1

        DLog("rect.size : \(rect.size)")
        let date = Date()
        let size = CGSize(width: imageWidth, height: (self.tileImage.size.height * imageWidth) / self.tileImage.size.width)
        drawImage = UIGraphicsImageRenderer(size: rect.size, format: format).image { ctx in
            ctx.cgContext.translateBy(x: self.location.x, y: location.y)
            ctx.cgContext.draw(self.tileImage.cgImage!, in: CGRect(origin: .zero, size: size), byTiling: true)
        }
        DLog("time : \(date.timeIntervalSinceNow)")
    }
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {

        let frame = CGRect(x: location.x, y: location.y, width: imageWidth, height: (tileImage.size.height * imageWidth) / tileImage.size.width)
        let context = UIGraphicsGetCurrentContext()
        context?.draw(tileImage.cgImage!, in: frame, byTiling: true)
//
//        let format: UIGraphicsImageRendererFormat
//        if #available(iOS 11.0, *) {
//            format = UIGraphicsImageRendererFormat(for: traitCollection)
//        } else {
//            format = UIGraphicsImageRendererFormat()
//        }
//        format.opaque = true
//        format.scale = 1
//
//        DLog("rect.size : \(rect.size)")
//        let size = CGSize(width: imageWidth, height: (self.tileImage.size.height * imageWidth) / self.tileImage.size.width)
//        let context = UIGraphicsGetCurrentContext()
//        context?.translateBy(x: self.location.x, y: location.y)
//        context?.draw(self.tileImage.cgImage!, in: CGRect(origin: .zero, size: size), byTiling: true)
    }

}
