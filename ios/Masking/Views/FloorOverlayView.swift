//
//  FloorOverlayView.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 8/1/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import UIKit
import DGTEngineSwift

class FloorOverlayView: UIView {

    func drawFrameChanged(to frame: CGRect) {
        self.frame = frame
        DLog("view : \(self.frame)")
        DLog("frame : \(frame) | center : \(center)")
    }
}
