//
//  FTTileDetailTableViewCell.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 11/2/2563 BE.
//  Copyright © 2563 Aduldach Pradubyart. All rights reserved.
//

import UIKit
import DGTEngineSwift
import Async

class AltButton: AppButton {
    
    override var customFont: UIFont {
        return AppTextStyle(traitCollection: traitCollection).font(style: .H4)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setTitleColor(.appGreen, for: .normal)
    }
    
    override func redrawBackgroundImage() {
        super.redrawBackgroundImage()
        
        let style = AppUIStyle(traitCollection: traitCollection)
        let image = UIImage(color: .white, size: CGSize(width: (style.cornerRedius*2) + 1, height: (style.cornerRedius*2) + 1), borderColor: .appGreen, borderWidth: 1, cornerRadius: style.cornerRedius)
        backgroudImage = image?.resizableImage().withRenderingMode(.alwaysOriginal)
    }
}

class ConfirmButton: AppButton {
    
    lazy var loadingIndicatorView: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView(style: .white)
        view.prepareForAutoLayout()
        view.hidesWhenStopped = true
        return view
    }()
    
    override var customFont: UIFont {
        return AppTextStyle(traitCollection: traitCollection).font(style: .H2)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        adjustsImageWhenDisabled = false
        setTitleColor(.white, for: .normal)
    }
    
    override func redrawBackgroundImage() {
        super.redrawBackgroundImage()
        
        let style = AppUIStyle(traitCollection: traitCollection)
        let image = UIImage(color: .appGreen, size: CGSize(width: (style.cornerRedius*2) + 1, height: (style.cornerRedius*2) + 1), cornerRadius: style.cornerRedius)
        backgroudImage = image?.resizableImage().withRenderingMode(.alwaysOriginal)
    }
    
    func startLoadingAnimation() {
        DispatchQueue.main.async {
            if self.loadingIndicatorView.superview == nil {
                self.addSubview(self.loadingIndicatorView)
                let space = AppUIStyle().space
                [self.loadingIndicatorView.centerY == self.centerY,
                 self.loadingIndicatorView.right == self.right - space].activated()
            }
            self.loadingIndicatorView.startAnimating()
        }
    }
    
    func stopLoadingAnimation() {
        DispatchQueue.main.async {
            self.loadingIndicatorView.stopAnimating()
        }
    }
}

class FTTileDetailTableViewCell: UITableViewCell {
    
    @IBOutlet internal weak var inContentView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.preservesSuperviewLayoutMargins = false
        updateUI(for: self.traitCollection)
        updateFont(for: self.traitCollection)
    }
    
    func updateUI(for traitCollection: UITraitCollection) {
        let style = AppUIStyle(traitCollection: traitCollection)
        self.contentView.layoutMargins = style.viewMargin
        self.inContentView?.layoutMargins = style.viewMargin
    }
    
    func updateFont(for traitCollection: UITraitCollection) {
        
    }
}

class FTTileDetailImageTableViewCell: FTTileDetailTableViewCell {
    
    @IBOutlet internal weak var coverImageView: MSCachedImageView!
    @IBOutlet internal weak var fitTileButton: UIButton!
    
    var _cacheImage: UIImage?
    var _beforeCacheImage: UIImage?
    
    var wallImageView: UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        contentView.clipsToBounds = true;
        coverImageView.contentMode = .scaleAspectFill
        coverImageView.clipsToBounds = true
        coverImageView.backgroundColor = .appLightGray
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        wallImageView?.removeFromSuperview()
    }
    
    override func updateUI(for traitCollection: UITraitCollection) {
        super.updateUI(for: traitCollection)
        let style = AppUIStyle(traitCollection: traitCollection)
        self.contentView.layoutMargins = UIEdgeInsets(top: style.viewMargin.top, left: style.viewMargin.left, bottom: 0, right: style.viewMargin.right)
    }
    
    override func updateFont(for traitCollection: UITraitCollection) {
        super.updateFont(for: traitCollection)
        let style = AppTextStyle(traitCollection: traitCollection)
    }
    
    
    func setupElement(tile: Tile, completion: @escaping (() -> ())) {
        
        if let image = _cacheImage {
            self.coverImageView?.image = image
        } else {
            NetworkService.requestImage(tile.image) { (image, cache) in
                if let image = image {
                    let size = CGSize(width: self.coverImageView.bounds.width, height: ceil((image.size.height * self.coverImageView.bounds.width) / image.size.width))
                    self.coverImageView?.setImage(image, forCacheKey: tile.image, size: size, animate: true, completion: { image in
                        self._cacheImage = image
                        completion()
                    })
                }
            }
        }
        
        if tile.planType == .wall {
              setupWallBadge()
          }
    }
    
    func setupElement(project: TileProject, isAfterImage: Bool, completion: @escaping (() -> ())) {
        
        if isAfterImage {
            if let image = _cacheImage {
                self.coverImageView?.image = image
            } else if let image = UIImage(contentsOfFile: AppUtility.pathFile(project.screenshotImageName, in: .screenShot)) {
                let size = CGSize(width: self.coverImageView.bounds.width, height: ceil((image.size.height * self.coverImageView.bounds.width) / image.size.width))
                self.coverImageView?.setImage(image, forCacheKey: project.screenshotImageName + "Detail", size: size, animate: true, completion: { image in
                    self._cacheImage = image
                    completion()
                })
            } else if project.imageFittileURL != "" {
                NetworkService.requestImage(project.imageFittileURL) { (image, cache) in
                    if let image = image, let imageName = AppUtility.saveImageToDocument(image, folder: .screenShot) {
                        AppData.write {
                            project.screenshotImageName = imageName
                        }
                        let size = CGSize(width: self.coverImageView.bounds.width, height: ceil((image.size.height * self.coverImageView.bounds.width) / image.size.width))
                        self.coverImageView?.setImage(image, forCacheKey: project.screenshotImageName + "Detail", size: size, animate: true, completion: { image in
                            self._cacheImage = image
                            completion()
                        })
                    }
                }
            }
        } else {
            if let image = _beforeCacheImage {
                self.coverImageView?.image = image
            } else if let image = UIImage(contentsOfFile: AppUtility.pathFile(project.imageName, in: .image)) {
                let size = CGSize(width: self.coverImageView.bounds.width, height: ceil((image.size.height * self.coverImageView.bounds.width) / image.size.width))
                self.coverImageView?.setImage(image, forCacheKey: project.imageName + "Raw", size: size, animate: true, completion: { image in
                    self._beforeCacheImage = image
                    completion()
                })
            } else if project.imageURL != "" {
                NetworkService.requestImage(project.imageURL) { (image, cache) in
                    if let image = image, let imageName = AppUtility.saveImageToDocument(image, folder: .image) {
                        AppData.write {
                            project.imageName = imageName
                        }
                        let size = CGSize(width: self.coverImageView.bounds.width, height: ceil((image.size.height * self.coverImageView.bounds.width) / image.size.width))
                        self.coverImageView?.setImage(image, forCacheKey: project.imageName + "Raw", size: size, animate: true, completion: { image in
                            self._beforeCacheImage = image
                            completion()
                        })
                    }
                }
            }
        }
    }
    
    func setupElement(shareProject: TileProject, completion: @escaping (() -> ())) {
        
        if let image = _cacheImage {
            self.coverImageView?.image = image
        } else if let image = UIImage(contentsOfFile: AppUtility.pathFile(shareProject.imageName, in: .image)) {
            let size = CGSize(width: self.coverImageView.bounds.width, height: ceil((image.size.height * self.coverImageView.bounds.width) / image.size.width))
            self.coverImageView?.setImage(image, forCacheKey: shareProject.imageName + "Detail", size: size, animate: true, completion: { image in
                self._cacheImage = image
                completion()
            })
        } else if shareProject.imageURL != "" {
            NetworkService.requestImage(shareProject.imageURL) { (image, cache) in
                if let image = image, let imageName = AppUtility.saveImageToDocument(image, folder: .image) {
                    AppData.write {
                        shareProject.imageName = imageName
                    }
                    let size = CGSize(width: self.coverImageView.bounds.width, height: ceil((image.size.height * self.coverImageView.bounds.width) / image.size.width))
                    self.coverImageView?.setImage(image, forCacheKey: shareProject.imageName + "Detail", size: size, animate: true, completion: { _image in
                        self._cacheImage = image
                        completion()
                    })
                }
            }
        }
    }
    
    func setupWallBadge() {
        
        if let wallImageView = wallImageView {
            self.coverImageView?.addSubview(wallImageView)
            [wallImageView.centerX == coverImageView.left, coverImageView.top == wallImageView.centerY].activated()
            return
        }
        
        let imageView = UIImageView(image: UIImage(color: .appGreen, size: CGSize(width: 10, height: 10))?.resizableImage())
        imageView.prepareForAutoLayout()
        
        let label = UILabel.autoLayout()
        label.font = AppTextStyle().font(style: .H4)
        label.textAlignment = .center
        label.text = LocalizedString("CatalogWallTitle")
        imageView.addSubview(label)
        coverImageView?.addSubview(imageView)
        
        [label.bottom == imageView.bottom,
         label.centerX == imageView.centerX,
        ].activated()
        
        [imageView.height == label.height * 4, imageView.width == label.width * 4].activated()
        [imageView.centerX == coverImageView.left, coverImageView.top == imageView.centerY].activated()
        
        imageView.transform = CGAffineTransform(rotationAngle: -(.pi * 0.25))
        
        self.wallImageView = imageView
    }
}

class FTTileDetailSegmentButton: UIButton {
    
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + 20, height: size.height + 12)
    }
}

class FTTileDetailSegmentTableViewCell: FTTileDetailTableViewCell {
    
    @IBOutlet internal weak var beforeButton: UIButton!
    @IBOutlet internal weak var afterButton: UIButton!
    
    private var action: ((_ index :Int) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        beforeButton.setTitle("  " + LocalizedString("ProjectBeforeButton"), for: .normal)
        beforeButton.tag = 0
        
        afterButton.setTitle("  " + LocalizedString("ProjectAfterButton"), for: .normal)
        afterButton.tag = 1
        afterButton.isSelected = true
        beforeButton.addTarget(self, action: #selector(self.onSegmentTouch(_:)), for: .touchDown)
        afterButton.addTarget(self, action: #selector(self.onSegmentTouch(_:)), for: .touchDown)
    }
    
    override func updateUI(for traitCollection: UITraitCollection) {
        super.updateUI(for: traitCollection)
        let style = AppUIStyle(traitCollection: traitCollection)
        self.contentView.layoutMargins = UIEdgeInsets(top: 0, left: style.viewMargin.left, bottom: 0, right: style.viewMargin.right)
    }
    
    override func updateFont(for traitCollection: UITraitCollection) {
        super.updateFont(for: traitCollection)
        let style = AppTextStyle(traitCollection: traitCollection)
        beforeButton.titleLabel?.font = style.font(style: .H6)
        afterButton.titleLabel?.font = style.font(style: .H6)
    }
    
    @objc internal func onSegmentTouch(_ button: UIButton) {
        beforeButton.isSelected = false
        afterButton.isSelected = false
        button.isSelected = true
        action?(button.tag)
    }
    
    func addSegmentChangedAction(_ action: @escaping (_ index :Int) -> ()) {
        self.action = action
    }
}

class FTTileDetailInfoTableViewCell: FTTileDetailTableViewCell {
    
    @IBOutlet internal weak var info1TitleLabel: UILabel!
    @IBOutlet internal weak var info2TitleLabel: UILabel!
    @IBOutlet internal weak var info3TitleLabel: UILabel!
    @IBOutlet internal weak var info4TitleLabel: UILabel!
    
    @IBOutlet internal weak var info1Label: UILabel!
    @IBOutlet internal weak var info2Label: UILabel!
    @IBOutlet internal weak var info3Label: UILabel!
    @IBOutlet internal weak var info4Label: UILabel!
    
    @IBOutlet internal weak var seemoreButton: UIButton!
    @IBOutlet internal weak var productTitleLabel: UILabel!
    @IBOutlet internal weak var productLabel: UILabel!
    
    @IBOutlet internal weak var altButton: UIButton!
    @IBOutlet internal weak var confirmButton: ConfirmButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        seemoreButton?.title = LocalizedString("CatalogButtonSeeMore") + " "
        seemoreButton?.setTitleColor(UIColor.appDarkGreen, for: .normal)
        seemoreButton?.setImage(#imageLiteral(resourceName: "icon_room_scene"), for: .normal)
        seemoreButton?.tintColor = UIColor.appDarkGreen
        
        updateUI(for: self.traitCollection)
        updateFont(for: self.traitCollection)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updateUI(for: self.traitCollection)
        updateFont(for: self.traitCollection)
    }
    
    override func updateUI(for traitCollection: UITraitCollection) {
        super.updateUI(for: traitCollection)
        let style = AppUIStyle(traitCollection: traitCollection)
        self.contentView.layoutMargins = UIEdgeInsets(top: 0, left: style.viewMargin.left, bottom: 0, right: style.viewMargin.right)
    }
    
    override func updateFont(for traitCollection: UITraitCollection) {
        super.updateFont(for: traitCollection)
        let style = AppTextStyle(traitCollection: traitCollection)
        let font = style.font(style: .H8)
        info1TitleLabel?.font = font
        info2TitleLabel?.font = font
        info3TitleLabel?.font = font
        info4TitleLabel?.font = font
        
        info1Label?.font = font
        info2Label?.font = font
        info3Label?.font = font
        info4Label?.font = font
        
        let productFont = style.font(style: .H6)
        productTitleLabel?.font = productFont
        productLabel?.font = productFont
        
        seemoreButton?.titleLabel?.font = style.font(style: .H7)
    }
    
    func setupElement(tile: Tile) {
        
        info1TitleLabel?.text = LocalizedString("CatalogInfoProductNo")
        info2TitleLabel?.text = LocalizedString("CatalogInfoBrand")
        info3TitleLabel?.text = LocalizedString("CatalogInfoSize")
        info4TitleLabel?.text = LocalizedString("CatalogInfoFeture")
        productTitleLabel?.text = LocalizedString("CatalogInfoName") + " :"
        
        info1Label?.text = ": " + tile.id
        info2Label?.text = ": " + tile.vendor
        info3Label?.text = ": " + String(format: LocalizedString("CatalogInfoSizeValue"), tile.width, tile.length)
        info4Label?.text = ": " + tile.feature
        productLabel?.text = tile.shortDesc
        
        confirmButton?.title = LocalizedString("CatalogButtonBuy")
        
        if tile.planType == .floor {
            altButton?.title = LocalizedString("CatalogButtonCalculate")
        } else {
            altButton?.isHidden = true
        }
    }
    
    func setupElement(project: TileProject, index: Int = 0) {
        
        info1TitleLabel?.text = LocalizedString("ProjectDetailCreateDate")
        info2TitleLabel?.text = LocalizedString("CatalogInfoProductNo")
        info3TitleLabel?.text = LocalizedString("CatalogInfoBrand")
        info4TitleLabel?.text = LocalizedString("CatalogInfoSize")
        productTitleLabel?.text = LocalizedString("CatalogInfoName") + " :"
        
        let formatter = DateFormatter()
        formatter.timeStyle = .none
        formatter.dateStyle = .medium
        info1Label?.text = ": " + formatter.string(from: project.date)
        
        confirmButton?.tag = index
        altButton?.tag = index
        seemoreButton?.tag = index
        
        if project.completed {
            confirmButton?.title = LocalizedString("CatalogButtonBuy")
            altButton?.title = LocalizedString("CatalogButtonCalculate")
        } else {
            if project.shareProject {
                confirmButton?.title = LocalizedString("ProjectButtonFittile")
            } else {
                confirmButton?.title = LocalizedString("ProjectButtonReceive")
            }
        }
        
        if let tile = project.artboards[safe: index]?.tile {
            info2Label?.text = ": " + tile.id
            info3Label?.text = ": " + tile.vendor
            info4Label?.text = ": " + String(format: LocalizedString("CatalogInfoSizeValue"), tile.width, tile.length)
            productLabel?.text = tile.shortDesc
            
            if tile.planType == .wall {
                altButton?.isHidden = true
            }
        } else {
            info2Label?.text = ": -"
            info3Label?.text = ": -"
            info4Label?.text = ": -"
            productLabel?.text = "-"
        }
    }
}

class FTTileDetailQRTableViewCell: FTTileDetailTableViewCell {
    
    @IBOutlet internal weak var qrcodeView: UIView!
    @IBOutlet internal weak var qrcodeImageView: UIImageView!
    @IBOutlet internal weak var qrcodeLabel: UILabel!
    @IBOutlet internal weak var confirmButton: ConfirmButton!
    
    var _cacheImage: UIImage?
    
    override func updateUI(for traitCollection: UITraitCollection) {
        super.updateUI(for: traitCollection)
        let style = AppUIStyle(traitCollection: traitCollection)
        self.contentView.layoutMargins = UIEdgeInsets(top: 0, left: style.viewMargin.left, bottom: 0, right: style.viewMargin.right)
    }
    
    override func updateFont(for traitCollection: UITraitCollection) {
        super.updateFont(for: traitCollection)
        let style = AppTextStyle(traitCollection: traitCollection)
        qrcodeLabel?.font = style.font(style: .H6)
    }
    
    func setupElement(project: TileProject, completion: @escaping (() -> ())) {
        
        if let image = _cacheImage {
            self.qrcodeImageView.image = image
            self.qrcodeLabel.text = LocalizedString("ProjectDetailScanText")
        } else {
            qrcodeLabel.text = nil
            if project.qrcodeURL.isEmpty {
                confirmButton?.title = LocalizedString("ProjectButtonFittile")
            } else {
                let size = qrcodeImageView.bounds.size
                let url = project.qrcodeURL;
                Async.userInitiated {
                    
                    UIGraphicsBeginImageContextWithOptions(size, false, 0)
                    
                    let qrCodeData = url.data(using: .isoLatin1, allowLossyConversion: false)!
                    
                    let filter = CIFilter(name: "CIQRCodeGenerator")!
                    filter.setValue(qrCodeData, forKey: "inputMessage")
                    filter.setValue("Q", forKey: "inputCorrectionLevel")
                    
                    if let qrcodeImage = filter.outputImage {
                        
                        let scaleX = size.width / qrcodeImage.extent.size.width
                        let scaleY = size.height / qrcodeImage.extent.size.height
                        
                        let transformedImage = qrcodeImage.transformed(by: CGAffineTransform(scaleX: scaleX, y: scaleY))
                        let image = UIImage(ciImage: transformedImage)
                        image.draw(at: .zero)
                    }
                    let image = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                    self._cacheImage = image
                    Async.main {
                        completion()
                        self.qrcodeImageView.image = image
                        self.qrcodeLabel.text = LocalizedString("ProjectDetailScanText")
                    }
                }
            }
        }
    }
}

class FTTileDetailConfirmTableViewCell: FTTileDetailTableViewCell {
    
    @IBOutlet internal weak var confirmButton: ConfirmButton!
    
    override func updateUI(for traitCollection: UITraitCollection) {
        super.updateUI(for: traitCollection)
        let style = AppUIStyle(traitCollection: traitCollection)
        self.contentView.layoutMargins = UIEdgeInsets(top: 0, left: style.viewMargin.left, bottom: 0, right: style.viewMargin.right)
    }
    
}

class FTTileDetailStaffTableViewCell: FTTileDetailTableViewCell {
    
    @IBOutlet internal weak var sellManLabel: DGTClickableLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        sellManLabel.attributedText = NSAttributedString(string: LocalizedString("CatalogForSaleMan"), attributes: [.font: AppTextStyle().font(style: .H6), .foregroundColor: UIColor.appGreen, .underlineStyle: NSNumber(integerLiteral: NSUnderlineStyle.single.rawValue)])
    }
    
    override func updateUI(for traitCollection: UITraitCollection) {
        super.updateUI(for: traitCollection)
        let style = AppUIStyle(traitCollection: traitCollection)
        self.contentView.layoutMargins = UIEdgeInsets(top: 0, left: style.viewMargin.left, bottom: style.viewMargin.bottom, right: style.viewMargin.right)
    }
    
    override func updateFont(for traitCollection: UITraitCollection) {
        super.updateFont(for: traitCollection)
        let style = AppTextStyle(traitCollection: traitCollection)
        sellManLabel?.font = style.font(style: .H7)
    }
    
}

