//
//  FTTileSizeTableViewCell.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 15/2/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import Foundation

class FTTileSizeTableViewCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        self.textLabel?.textAlignment = .left
        self.textLabel?.textColor = .appDarkGray
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updateUI(for: traitCollection)
        updateFont(for: traitCollection)
    }
    
    func updateUI(for traitCollection: UITraitCollection) {
        let style = AppUIStyle(traitCollection: traitCollection)
        
    }
    
    func updateFont(for traitCollection: UITraitCollection) {
        let style = AppTextStyle(traitCollection: traitCollection)
        self.textLabel?.font = style.font(style: .H6)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let space = AppUIStyle(traitCollection: traitCollection).space
        self.textLabel?.frame = contentView.bounds.insetBy(dx: space, dy: 0)
    }
}
