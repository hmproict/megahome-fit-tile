//
//  LineOverlayView.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 8/1/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import UIKit

class LineOverlayView: UIView {

    private var points = [CGPoint]()
    
    var completed: Bool = false {
        didSet {
            setNeedsDisplay()
        }
    }
    
    func addPoint(_ point: CGPoint) {
        points.append(point)
        setNeedsDisplay()
    }
    
    func changePoint(_ point: CGPoint, at index: Int) {
        points[index] = point
        setNeedsDisplay()
    }
    
    func removeAllPoints() {
        points.removeAll()
        self.completed = false
    }
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        
        // Drawing code
        UIColor.appRed.setStroke()
        UIColor.white.setFill()
        
        let path = UIBezierPath()
        path.lineWidth = 1
        for (index, point) in points.enumerated() {
            if index == 0 {
                path.move(to: point)
            } else {
                path.addLine(to: point)
            }
        }
        
        if completed {
            path.close()
        }
        path.stroke()
    }

}
