//
//  AppButton.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 28/1/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import UIKit
import DGTEngineSwift

class AppButton: UIButton {
    
    var customFont: UIFont {
        return AppTextStyle(regular: isRegularHorizontalSizeClass).font(style: .H5)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        prepareView()
    }
    
    class func button() -> Self {
        let button = self.init(type: .system)
        button.prepareView()
        return button
    }
    
    internal func prepareView() {
        
        titleLabel?.font = customFont
        redrawBackgroundImage()
    }
    
    internal func redrawBackgroundImage() {
        
    }
    
    override var intrinsicContentSize : CGSize {
        var size = super.intrinsicContentSize
        size.height = ceil(customFont.lineHeight) + 6
        size.width += 6
        return size
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        titleLabel?.font = customFont
        invalidateIntrinsicContentSize()
        redrawBackgroundImage()
    }
}

class CancelAppButton: AppButton {
    
    override var customFont: UIFont {
        return AppTextStyle(regular: isRegularHorizontalSizeClass).font(style: .H6)
    }
    
    override func redrawBackgroundImage() {
        super.redrawBackgroundImage()
        
        let height = self.intrinsicContentSize.height
        let size = CGSize(width: (height*2)+1, height: height)
        let image = UIImage(color: UIColor(grayscalCode: 0, alpha: 0.6), size: size, borderColor: .white, borderWidth: 1, cornerRadius: size.height/2)?.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: size.width/2, bottom: 0, right: size.width/2))
        self.backgroudImage = image
        self.setTitleColor(.white, for: .normal)
    }
}

class SaveAppButton: AppButton {
    
    override func redrawBackgroundImage() {
        super.redrawBackgroundImage()
        
        let height = self.intrinsicContentSize.height
        let size = CGSize(width: (height*2)+1, height: height)
        let image = UIImage(color: UIColor(grayscalCode: 0, alpha: 0.6), size: size, borderColor: .white, borderWidth: 1, cornerRadius: size.height/2)?.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: size.width/2, bottom: 0, right: size.width/2))
        self.backgroudImage = image
        
        let disableImage = UIImage(color: UIColor(grayscalCode: 0, alpha: 0.6), size: size, borderColor: .gray, borderWidth: 1, cornerRadius: size.height/2)?.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: size.width/2, bottom: 0, right: size.width/2))
        self.setBackgroundImage(disableImage, for: .disabled)
        self.setTitleColor(.orange, for: .normal)
        self.setTitleColor(.lightGray, for: .disabled)
    }
}

class FilterAppButton: AppButton {
    
    override var customFont: UIFont {
        return UIFont.customFont(20, weight: .dgtRegular)
    }
    
    override func redrawBackgroundImage() {
        super.redrawBackgroundImage()
        
        let height = self.intrinsicContentSize.height
        let size = CGSize(width: (height*2)+1, height: height)
        let image = UIImage(color: .clear, size: size, borderColor: self.tintColor, borderWidth: 1, cornerRadius: size.height/2)?.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: size.width/2, bottom: 0, right: size.width/2))
        self.backgroudImage = image
    }
}
