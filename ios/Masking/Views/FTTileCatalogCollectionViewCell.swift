//
//  FTTileCatalogCollectionViewCell.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 8/2/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import Foundation
import DGTEngineSwift

class FTTileBaseCollectionViewCell: UICollectionViewCell {
    
    lazy var highlightView: UIView = {
        let view = UIView()
        view.prepareForAutoLayout()
        view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.399614726)
        view.isHidden = true
        contentView.addSubview(view)
        view.constraintsAroundView()?.activated()
        return view
    }()
    
    override var isHighlighted: Bool {
        didSet {
            highlightView.isHidden = !isHighlighted
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.bringSubviewToFront(highlightView)
    }
}

class FTTileQRCodeCollectionViewCell: FTTileBaseCollectionViewCell {
    
    var titleLabel: UILabel!
    var imageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let style = AppUIStyle(traitCollection: traitCollection)
        let font = AppTextStyle(traitCollection: traitCollection).font(style: .H7)
        contentView.backgroundColor = .appGreen
        
        let view = UIView.autoLayout()
        contentView.addSubview(view)
        [view.left == contentView.left,
         view.right == contentView.right,
         view.centerY == contentView.centerY].activated()
        
        let imageView = UIImageView(image: #imageLiteral(resourceName: "icon_QR_scaner"))
        imageView.prepareForAutoLayout()
        imageView.contentMode = .scaleAspectFit
        view.addSubview(imageView)
        [imageView.width == imageView.height,
         imageView.centerX == view.centerX,
         imageView.top == view.top].activated()
        
        let constraint = (imageView.width == view.width / 2)
        constraint.identifier = "DefaultImageHalfWidth"
        constraint.isActive = true
        
        self.imageView = imageView
        
        let label = UILabel.autoLayout()
        label.setContentHuggingPriority(.required, for: .vertical)
        label.text = LocalizedString("QRCodeScanTitle")
        label.textColor = .white
        label.font = font
        label.textAlignment = .center
        view.addSubview(label)
        [label.left == view.left,
         label.right == view.right,
         label.bottom == view.bottom,
         label.top == imageView.bottom + (style.space/2)].activated()
         self.titleLabel = label
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updateUI(for: traitCollection)
        updateFont(for: traitCollection)
    }
    
    func updateUI(for traitCollection: UITraitCollection) {
        let style = AppUIStyle(traitCollection: traitCollection)
        
    }
    
    func updateFont(for traitCollection: UITraitCollection) {
        let style = AppTextStyle(traitCollection: traitCollection)
        self.titleLabel?.font = style.font(style: .H7)
    }
    
    func changeToMiniStyle() {
        self.titleLabel?.removeFromSuperview()
        
        if contentView.findConstraint(view: imageView, attribute: .centerY) == nil {
            contentView.findConstraint(view: imageView, attribute: .top)?.isActive = false
            contentView.findConstraint(view: imageView, attribute: .bottom)?.isActive = false
            contentView.findConstraint(identifier: "DefaultImageHalfWidth")?.isActive = false
            
            [imageView.centerY == contentView.centerY,
             imageView.width == imageView.superview!.width / 1.5].activated()
        }
    }
    
}

class FTTileCatalogCollectionViewCell: FTTileBaseCollectionViewCell {
    
    let imageView = MSCachedImageView.autoLayout()
    lazy var selectImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(color: .clear, size: CGSize(width: 10, height: 10), borderColor: UIColor(r: 100, g: 255, b: 55), borderWidth: 2)?.resizableImage())
        imageView.prepareForAutoLayout()
        contentView.addSubview(imageView)
        imageView.constraintsAroundView()?.activated()
        return imageView
    }()
    
    var isMiniStyle = false
    var wallImageView: UIImageView?
    
    var selectedTile: Bool = false {
        didSet {
            selectImageView.isHidden = !selectedTile
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.backgroundColor = .white
        contentView.addSubview(imageView)
        contentView.clipsToBounds = true;
        imageView.contentMode = .scaleAspectFill
        imageView.constraintsAroundView(spaceInset: UIEdgeInsets(redius: 2))?.activated()
        imageView.clipsToBounds = true;
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        selectedTile = false
        wallImageView?.removeFromSuperview()
    }
    
    func setElementsOfCell(_ tile: Tile, at indexPath: IndexPath, contentSize: CGFloat) {
        
        imageView.image = nil
        let url = tile.image
        NetworkService.requestImage(url) { (image, cache) in
            
            if let image = image {
                let collectionView = self.firstSuperView(class: UICollectionView.self)
                let newIndexPath = collectionView?.indexPath(for: self)
                if cache || self.superview == nil || newIndexPath == nil {
                    self.imageView.setImage(image, forCacheKey: url, size: CGSize(width: contentSize, height: contentSize), animate: false)
                } else if newIndexPath == indexPath {
                    self.imageView.setImage(image, forCacheKey: url, size: CGSize(width: contentSize, height: contentSize), animate: true)
                }
            }
        }
        
        if tile.planType == .wall {
            setupWallBadge()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.bringSubviewToFront(imageView)
    }
    
    func setupWallBadge() {
        
        if let wallImageView = wallImageView {
            self.imageView.addSubview(wallImageView)
            [wallImageView.centerX == self.imageView.left, self.imageView.top == wallImageView.centerY].activated()
            return
        }
        
        let imageView = UIImageView(image: UIImage(color: .appGreen, size: CGSize(width: 10, height: 10))?.resizableImage())
        imageView.prepareForAutoLayout()
        
        let label = UILabel.autoLayout()
        label.font = AppTextStyle().font(style: isMiniStyle ? .H9 : .H4)
        label.textAlignment = .center
        label.text = LocalizedString("CatalogWallTitle")
        imageView.addSubview(label)
        self.imageView.addSubview(imageView)
        
        [label.bottom == imageView.bottom,
         label.centerX == imageView.centerX,
        ].activated()
        
        [imageView.height == label.height * 3, imageView.width == label.width * 4].activated()
        [imageView.centerX == self.imageView.left, self.imageView.top == imageView.centerY].activated()
        
        imageView.transform = CGAffineTransform(rotationAngle: -(.pi * 0.25))
        
        self.wallImageView = imageView
    }
    
    class func contentSize(for traitCollection: UITraitCollection, containtIn width: CGFloat, catalogStyle: FTCatalogStyle) -> CGFloat {
        
        let style = AppUIStyle(traitCollection: traitCollection)
        DLog("style : \(style.regular)")
        let space: CGFloat
        var start: CGFloat
        let minWidth: CGFloat
        if catalogStyle == .full {
            space = ceil(style.space/5)
            minWidth = style.regular ? 150 : 100
            start = style.regular ? 15 : 5
        } else {
            space = style.space/2
            minWidth = style.regular ? 100 : 50
            start = style.space
        }
        
        let item: CGFloat = {
            var contentWidth: CGFloat = 0
            repeat {
                let maxWidth = width-(space*(start+1))
                contentWidth = ceil(maxWidth/start)
                start -= 1
            } while contentWidth < minWidth
            return start + 1
        }()
        let maxWidth = width-(space*(item+1))
        return floor(maxWidth/item)
    }
}
