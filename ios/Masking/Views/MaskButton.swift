//
//  MaskButton.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 7/1/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import UIKit
import DGTEngineSwift

class MaskButton: UIButton {
    
    let imageSize = CGSize(width: 44, height: 44)
    
    var isMarked: Bool = false {
        didSet {
            
            let image: UIImage = {
                if startButton {
                    return isMarked ? #imageLiteral(resourceName: "pin_act") : #imageLiteral(resourceName: "pin")
                } else {
                    return isMarked ? #imageLiteral(resourceName: "pin_act").scaleImageToWidth(20)! : #imageLiteral(resourceName: "pin").scaleImageToWidth(18)!
                }
            }()
            setImage(image, for: .normal)
        }
    }
    
    private(set) var startButton: Bool = false
    
    static func button(start: Bool = false, location: CGPoint) -> MaskButton {
        
        let button = MaskButton(type: .custom)
        button.startButton = start
        button.isMarked = false
        button.frame.size = button.imageSize
        button.isExclusiveTouch = true
        button.adjustsImageWhenHighlighted = false
        button.center = CGPoint(x: location.x, y: location.y - button.imageSize.height/2)
        return button
    }
    
    override var intrinsicContentSize: CGSize {
        return imageSize
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let image = image {
            imageView?.frame.origin.y = bounds.height - image.size.height
        }
    }
}
