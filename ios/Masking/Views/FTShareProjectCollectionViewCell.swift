//
//  FTShareProjectCollectionViewCell.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 7/6/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import UIKit
import DGTEngineSwift

class FTShareProjectAppButton: ConfirmButton {
    
    override var customFont: UIFont {
        return AppTextStyle(traitCollection: traitCollection).font(style: .H3)
    }
    
    override func prepareView() {
        super.prepareView()
        titleLabel?.adjustsFontSizeToFitWidth = true
        titleLabel?.minimumScaleFactor = 0.5
        prepareForAutoLayout()
        image = #imageLiteral(resourceName: "icon_share")
        title = LocalizedString("CreateShareProjectButton")
        tintColor = .white
    }
    
    override var intrinsicContentSize : CGSize {
        var size = super.intrinsicContentSize
        let style = AppUIStyle()
        size.height = ceil(#imageLiteral(resourceName: "icon_share").size.height) + 6
        size.width += style.space + style.cornerRedius
        return size
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let style = AppUIStyle()
        imageView?.frame.origin.x = style.cornerRedius
        
        if let size = image?.size {
            titleLabel?.frame.origin.x = size.width + (style.cornerRedius*2)
            titleLabel?.frame.size.width = bounds.width - (size.width + (style.cornerRedius*3))
        }
    }
    
}

class FTScanProjectAppButton: AltButton {
    
    override var customFont: UIFont {
        return AppTextStyle(traitCollection: traitCollection).font(style: .H4)
    }
    
    override func prepareView() {
        super.prepareView()
        prepareForAutoLayout()
        image = #imageLiteral(resourceName: "icon_QR_scaner").scaleImageToHeight(#imageLiteral(resourceName: "icon_share").size.height)
//        imageEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        title = LocalizedString("ScanShareProjectButton")
        titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        setContentHuggingPriority(.required, for: .horizontal)
        setContentCompressionResistancePriority(.required, for: .horizontal)
        tintColor = .orange
    }
    
    override var intrinsicContentSize : CGSize {
        var size = super.intrinsicContentSize
        let style = AppUIStyle()
        size.height = ceil(#imageLiteral(resourceName: "icon_share").size.height) + 6
        size.width += style.space + style.cornerRedius
        return size
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let style = AppUIStyle()
        imageView?.frame.origin.x = style.cornerRedius
        
        if let size = image?.size {
            titleLabel?.frame.origin.x = size.width + (style.cornerRedius*2)
            titleLabel?.frame.size.width = bounds.width - (size.width + (style.cornerRedius*3))
        }
    }
    
    override func redrawBackgroundImage() {
        
        let style = AppUIStyle(traitCollection: traitCollection)
        let image = UIImage(color: .white, size: CGSize(width: (style.cornerRedius*2) + 1, height: (style.cornerRedius*2) + 1), borderColor: .appGreen, borderWidth: 2, cornerRadius: style.cornerRedius)
        backgroudImage = image?.resizableImage().withRenderingMode(.alwaysOriginal)
    }
}

class FTShareProjectCollectionViewCell: UICollectionViewCell {
    
    let shareButton = FTShareProjectAppButton.button()
    let scanButton = FTScanProjectAppButton.button()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let style = AppUIStyle()
        
        contentView.preservesSuperviewLayoutMargins = false
        contentView.layoutMargins = style.cellMargin
        contentView.addSubview(shareButton)
        contentView.addSubview(scanButton)
        
        [shareButton.top == contentView.topMargin,
        shareButton.bottom == contentView.bottomMargin,
        shareButton.left == contentView.leftMargin,
        shareButton.right == scanButton.left - style.space,
        scanButton.top == contentView.topMargin,
        scanButton.bottom == contentView.bottomMargin,
        scanButton.right == contentView.rightMargin].activated()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override class func rowHeight() -> CGFloat {
        return 50
    }
}
