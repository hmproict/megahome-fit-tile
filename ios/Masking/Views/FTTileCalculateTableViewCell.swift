//
//  FTTileCalculateTableViewCell.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 15/2/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import Foundation
import DGTEngineSwift

class FTBaseTileCalculateTableViewCell: UITableViewCell {
    
    @IBOutlet internal weak var inContentView: UIView?
    @IBOutlet internal weak var lineView: UIView?
    @IBOutlet internal weak var titleLabel: UILabel?
    
    func addAction(_ action: Selector, for target: Any) {
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.preservesSuperviewLayoutMargins = false
        inContentView?.preservesSuperviewLayoutMargins = false
        updateUI(for: traitCollection)
        updateFont(for: traitCollection)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updateUI(for: traitCollection)
        updateFont(for: traitCollection)
    }
    
    func updateUI(for traitCollection: UITraitCollection) {
        let style = AppUIStyle(traitCollection: traitCollection)
        contentView.layoutMargins = style.margin
        inContentView?.layoutMargins = style.viewMargin
    }
    
    func updateFont(for traitCollection: UITraitCollection) {
        let style = AppTextStyle(traitCollection: traitCollection)
        
    }
    
}

class FTTileSizeCalculateTableViewCell: FTBaseTileCalculateTableViewCell {
    
    @IBOutlet internal weak var orLabel: UILabel!
    @IBOutlet internal weak var tileListButton: UIButton!
    @IBOutlet internal weak var tileCatalogButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        titleLabel?.text = LocalizedString("CalculateTileSizeTitle")
        orLabel.text = LocalizedString("CalculateTileOrTitle")
        tileListButton.title = LocalizedString("CalculateTileNoneSizeTitle")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        tileListButton.title = LocalizedString("CalculateTileNoneSizeTitle")
    }
    
    func setElementOfCell(_ calculate: TileCalculate) {
        if let size = calculate.size {
            switch calculate.sizeUnit {
            case .inch:
                tileListButton.title = String(format: LocalizedString("CalculateInchSizeTitle"), size.inch.width, size.inch.length)
            case .centimetre:
                tileListButton.title = String(format: LocalizedString("CalculateCmSizeTitle"), size.centimetre.width, size.centimetre.length)
            }
        }
    }
    
    override func addAction(_ action: Selector, for target: Any) {
        tileListButton.addTarget(target, action: action, for: .touchDown)
    }
    
    func addCatalogAction(_ action: Selector, for target: Any) {
        tileCatalogButton.addTarget(target, action: action, for: .touchDown)
    }
    
    override func updateFont(for traitCollection: UITraitCollection) {
        super.updateFont(for: traitCollection)
        let style = AppTextStyle(traitCollection: traitCollection)
        self.titleLabel?.font = style.font(style: .H3)
        self.orLabel?.font = style.font(style: .H6)
    }
}

class FTTileSizeInfoCalculateTableViewCell: FTBaseTileCalculateTableViewCell {
    
    @IBOutlet internal weak var tileView: UIView!
    
    @IBOutlet internal weak var productNameTitleLabel: UILabel!
    @IBOutlet internal weak var sizeTitleLabel: UILabel!
    @IBOutlet internal weak var packTitleLabel: UILabel!
    
    @IBOutlet internal weak var productNameLabel: UILabel!
    @IBOutlet internal weak var sizeLabel: UILabel!
    @IBOutlet internal weak var packLabel: UILabel!
    
    @IBOutlet internal weak var tileImageView: MSCachedImageView!
    @IBOutlet internal weak var cancalTileButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        titleLabel?.text = LocalizedString("CalculateTileSizeTitle")
        
        productNameTitleLabel?.text = LocalizedString("CalculateTileInfoProductTitle")
        sizeTitleLabel?.text = LocalizedString("CalculateTileInfoSizeTitle")
        packTitleLabel?.text = LocalizedString("CalculateTileInfoPackTitle")
        
        cancalTileButton.image = #imageLiteral(resourceName: "icon_cancel").withRenderingMode(.alwaysOriginal)
        
        tileImageView.layer.masksToBounds = false
        tileImageView.layer.shadowColor = UIColor.black.cgColor
        tileImageView.layer.shadowOffset = .zero
        tileImageView.layer.shadowOpacity = 0.6
        tileImageView.layer.shadowRadius = 2.0
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.tileImageView.image = nil
    }
    
    func setElementOfCell(_ tile: Tile) {
        
        let key = tile.image + "BorderMini"
        if let image = MSCachedAsyncViewDrawing.sharedInstance()?.cacheImage(forKey: key) {
            self.tileImageView.setImage(image, forCacheKey: key)
        } else {
            NetworkService.requestImage(tile.image) { (image, cache) in
                if let image = image {
                    let borderSize = ceil(image.size.height / 60)
                    let lineColor = UIColor.white
                    let borderImage = UIImage(color: lineColor, size: CGSize(width: image.size.width + (borderSize*2), height:  image.size.height + (borderSize*2)))
                    if let image = borderImage?.imageAddingImage(image, offset: CGPoint(x: borderSize, y: borderSize)) {
                        self.tileImageView.setImage(image, forCacheKey: key)
                    }
                }
            }
        }
        
        productNameLabel?.text = tile.shortDesc
        sizeLabel?.text = String(format: LocalizedString("CalculateInchSizeTitle"), String(tile.width), String(tile.length))
        packLabel?.text = tile.box
    }
    
    override func addAction(_ action: Selector, for target: Any) {
        cancalTileButton.addTarget(target, action: action, for: .touchDown)
    }
    
    override func updateUI(for traitCollection: UITraitCollection) {
        super.updateUI(for: traitCollection)
        let style = AppUIStyle(traitCollection: traitCollection)
        tileView?.layoutMargins = style.viewMargin
    }
    
    override func updateFont(for traitCollection: UITraitCollection) {
        super.updateFont(for: traitCollection)
        let style = AppTextStyle(traitCollection: traitCollection)
        self.titleLabel?.font = style.font(style: .H3)
        
        let font = style.font(style: .H6)
        productNameTitleLabel.font = font
        sizeTitleLabel.font = font
        packTitleLabel.font = font
        productNameLabel.font = font
        sizeLabel.font = font
        packLabel.font = font
    }
    
}

class FTTileAreaCalculateTableViewCell: FTBaseTileCalculateTableViewCell {
    
    @IBOutlet internal weak var segmentView: FTSegmentControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel?.text = LocalizedString("CalculateTileAreaTitle")
        
        segmentView.setTitle(LocalizedString("CalculateTileAreaOption1Title"), forSegmentAtIndex: 0)
        segmentView.setTitle(LocalizedString("CalculateTileAreaOption2Title"), forSegmentAtIndex: 1)
    }
    
    override func addAction(_ action: Selector, for target: Any) {
        segmentView.addTarget(target, action: action, for: .valueChanged)
    }
    
    override func updateFont(for traitCollection: UITraitCollection) {
        super.updateFont(for: traitCollection)
        let style = AppTextStyle(traitCollection: traitCollection)
        let font = style.font(style: .H3)
        self.titleLabel?.font = font
    }
    
}

class FTTileAreaInfoCalculateTableViewCell: FTBaseTileCalculateTableViewCell {
    
    @IBOutlet internal weak var unitLabel: UILabel!
    @IBOutlet internal weak var textField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        titleLabel?.text = LocalizedString("CalculateTileAreaSizeTitle")
        unitLabel?.text = LocalizedString("CalculateTileAreaUnitTitle")
        textField.tag = 0
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        textField.tag = 0
        lineView?.isHidden = false
    }
    
    override func addAction(_ action: Selector, for target: Any) {
        textField.addTarget(target, action: action, for: .editingChanged)
    }
    
    override func updateFont(for traitCollection: UITraitCollection) {
        super.updateFont(for: traitCollection)
        let style = AppTextStyle(traitCollection: traitCollection)
        let font = style.font(style: .H6)
        self.titleLabel?.font = font
        self.unitLabel?.font = font
    }
    
    func setTitleLabelWidth(_ width: CGFloat) {
        guard let label = titleLabel else {
            return
        }
        
        let identifier = "TitleLabelWidthConstraint"
        if let constraint = label.findConstraint(identifier: identifier) {
            constraint.constant = width
        } else {
            let constraint = label.width == width
            constraint.identifier = identifier
            constraint.isActive = true
        }
    }
    
    class func maxWidth(of titles: [String], for traitCollection: UITraitCollection) -> CGFloat {
        let style = AppTextStyle(traitCollection: traitCollection)
        let font = style.font(style: .H6)
        return titles.reduce(CGFloat(0)) { max($0, ceil($1.size(font: font).width)) }
    }
}

class FTTileExtraCalculateTableViewCell: FTBaseTileCalculateTableViewCell {
    
    @IBOutlet internal weak var segmentView: FTSegmentControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel?.text = LocalizedString("CalculateTileExtraTitle")
        
        segmentView.setTitle(LocalizedString("CalculateTileExtraOption1Title"), forSegmentAtIndex: 0)
        segmentView.setTitle(LocalizedString("CalculateTileExtraOption2Title"), forSegmentAtIndex: 1)
    }
    
    override func addAction(_ action: Selector, for target: Any) {
        segmentView.addTarget(target, action: action, for: .valueChanged)
    }
    
    override func updateFont(for traitCollection: UITraitCollection) {
        super.updateFont(for: traitCollection)
        let style = AppTextStyle(traitCollection: traitCollection)
        let font = style.font(style: .H3)
        self.titleLabel?.font = font
    }
    
}

class FTTileExtraInfoCalculateTableViewCell: FTBaseTileCalculateTableViewCell {
    
    @IBOutlet internal var buttons: [UIButton]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        for (index, button) in buttons.enumerated() {
            button.title = LocalizedString("CalculateTileExtraOptionUnit\(index)Title")
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        unselectedButtons()
    }
    
    override func updateUI(for traitCollection: UITraitCollection) {
        super.updateUI(for: traitCollection)
        let style = AppUIStyle(traitCollection: traitCollection)
        
    }
    
    override func updateFont(for traitCollection: UITraitCollection) {
        super.updateFont(for: traitCollection)
        let style = AppTextStyle(traitCollection: traitCollection)
        let font = style.font(style: .H6)
        self.titleLabel?.font = font
    }
    
    func unselectedButtons() {
        buttons.forEach({ $0.isSelected = false })
    }
    
    override func addAction(_ action: Selector, for target: Any) {
        buttons.forEach({ $0.addTarget(target, action: action, for: .touchDown) })
    }
}

class FTTileRemarkCalculateTableViewCell: FTBaseTileCalculateTableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func updateUI(for traitCollection: UITraitCollection) {
        super.updateUI(for: traitCollection)
        let style = AppUIStyle(traitCollection: traitCollection)
        
    }
    
    override func updateFont(for traitCollection: UITraitCollection) {
        super.updateFont(for: traitCollection)
        let style = AppTextStyle(traitCollection: traitCollection)
        self.titleLabel?.font = style.font(style: .H7)
    }
}

class FTTileConfirmCalculateTableViewCell: FTBaseTileCalculateTableViewCell {
    
    @IBOutlet internal weak var confirmButton: UIButton!
    @IBOutlet internal var resultView: UIView!
    @IBOutlet internal var actionView: UIView!
    
    @IBOutlet internal weak var resultTitleLabel: UILabel!
    @IBOutlet internal weak var resultBoxTitleLabel: UILabel!
    @IBOutlet internal weak var resultBoxLabel: UILabel!
    @IBOutlet internal weak var resultImageView: UIImageView!
    @IBOutlet internal weak var remarkLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let style = AppUIStyle(traitCollection: traitCollection)
        resultImageView?.image = UIImage(color: .white, size: style.cornerInset.resizableSize, borderColor: .appDarkGray, borderWidth: 1, cornerRadius: style.cornerRedius)?.resizableImage()
        
        confirmButton.title = LocalizedString("CalculateTileResultConfirmTitle")
        resultTitleLabel?.text = LocalizedString("CalculateTileResultTitle")
        resultBoxTitleLabel?.text = LocalizedString("CalculateTileResultTitleUnit")
        remarkLabel?.text = nil
        
        resultView.isHidden = true
        actionView.isHidden = false
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        confirmButton.title = LocalizedString("CalculateTileResultConfirmTitle")
        resultView.isHidden = true
        actionView.isHidden = false
        confirmButton.removeTarget(nil, action: nil, for: .allEvents)
        remarkLabel?.text = nil
    }
    
    override func updateFont(for traitCollection: UITraitCollection) {
        let style = AppTextStyle(traitCollection: traitCollection)
        self.titleLabel?.font = style.font(style: .H6)
        self.resultTitleLabel?.font = style.font(style: .H4)
        self.resultBoxLabel?.font = UIFont.systemFont(ofSize: 48, weight: .semibold)
        self.resultBoxTitleLabel?.font = style.font(style: .H6)
        self.remarkLabel?.font = style.font(style: .H8)
    }
    
    override func addAction(_ action: Selector, for target: Any) {
        confirmButton?.addTarget(target, action: action, for: .touchUpInside)
    }
}

