//
//  FTFittileOptionViewController.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 7/2/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import Foundation
import DGTEngineSwift
import Async

class FTFittileOptionViewController: UIViewController {
    
    lazy var gallaryButton: UIButton = {
        let button = UIButton(type: .system)
        button.prepareForAutoLayout()
        button.image = #imageLiteral(resourceName: "icon_camera_gallary").withRenderingMode(.alwaysOriginal)
        button.addTarget(self, action: #selector(self.onOpenGallary), for: .touchUpInside)
        return button
    }()
    
    @available(iOS 11.0, *)
    lazy var cameraButton: UIButton = {
        let button = UIButton(type: .system)
        button.prepareForAutoLayout()
        button.image = #imageLiteral(resourceName: "icon_camera_cam").withRenderingMode(.alwaysOriginal)
        button.addTarget(self, action: #selector(self.onOpenChangeToCamera), for: .touchUpInside)
        return button
    }()
    
    @available(iOS 11.0, *)
    lazy var arButton: UIButton = {
        let button = UIButton(type: .system)
        button.prepareForAutoLayout()
        button.image = #imageLiteral(resourceName: "icon_camera_AR").withRenderingMode(.alwaysOriginal)
        button.addTarget(self, action: #selector(self.onOpenChangeToARCamera), for: .touchUpInside)
        return button
    }()
    
    lazy var cameraViewController: FTCameraViewController = {
        let controller = FTCameraViewController()
        controller.view.prepareForAutoLayout()
        return controller
    }()
    
    @available(iOS 11.0, *)
    lazy var arViewController: FTARViewController = {
        let controller = FTARViewController()
        controller.view.prepareForAutoLayout()
        return controller
    }()
    
    lazy var cancelButton: UIButton = {
        let button = CancelAppButton.button()
        button.prepareForAutoLayout()
        button.title = LocalizedString("cancel")
        button.addTarget(self, action: #selector(self.onCancelCamera), for: .touchUpInside)
        return button
    }()
    
    private var activeViewController: UIViewController {
        return children.first!
    }
    
    private let defaultSpace: CGFloat = 80
    
    enum Mode {
        case camera
        case ar
    }
    
    private var mode: Mode = .camera
    var tile: Tile?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AppDelegateObject.sendAppAnalytics("Fittile Camera")
        view.addSubview(cameraViewController.view)
        if #available(iOS 11.0, *) {
            cameraViewController.view.constraintsLayoutGuide(view.safeAreaLayoutGuide)?.activated()
        } else {
            // Fallback on earlier versions
            cameraViewController.view.constraintsAroundView()?.activated()
        }
        addChild(cameraViewController)
        
        view.addSubview(cancelButton)
        let space = AppUIStyle(traitCollection: traitCollection).space
        [cancelButton.left == view.leftMargin,
         cancelButton.top == view.topMargin + space].activated()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let tabbarController = self.navigationController as? FTTabbarViewController {
            
            let button = tabbarController.actionButton
            if gallaryButton.superview == nil {
                
                tabbarController.addCustomViewToTabbar(gallaryButton)
                [gallaryButton.centerY == button.centerY,
                 gallaryButton.centerX == button.centerX - defaultSpace].activated()
            }
            
            if #available(iOS 11.0, *) {
                if arButton.superview == nil && FTARViewController.isSupportAR {
                    
                    tabbarController.addCustomViewToTabbar(arButton)
                    [arButton.centerY == button.centerY,
                     arButton.centerX == button.centerX + defaultSpace].activated()
                }
                if cameraButton.superview == nil {
                    
                    cameraButton.isHidden = true
                    tabbarController.addCustomViewToTabbar(cameraButton)
                    [cameraButton.centerY == button.centerY,
                     cameraButton.centerX == button.centerX].activated()
                }
            }
            
            tabbarController.addMainTarget(self, action: #selector(self.onCaptureButtonAction(_:)))
        }
        
        AppData.prepareTileStarterPack { (success) in
            if !success {
                let controller = UIAlertController(title: LocalizedString("notice"), message: LocalizedString("LoadTileError"), preferredStyle: .alert)
                controller.addAction(UIAlertAction(title: LocalizedString("ok"), style: .default, handler: nil))
                self.present(controller, animated: true, completion: nil)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
        
        let tabbarController = self.navigationController as? FTTabbarViewController
        switch mode {
        case .ar:
            tabbarController?.actionTitleLabel.text = LocalizedString("ARActionTitle")
            tabbarController?.actionButton.image = #imageLiteral(resourceName: "icon_AR_act")
            tabbarController?.actionButton.selectedImage = #imageLiteral(resourceName: "icon_AR_act")
        default:
            tabbarController?.actionTitleLabel.text = LocalizedString("CameraActionTitle")
            tabbarController?.actionButton.image = #imageLiteral(resourceName: "icon_camera_capture_act")
            tabbarController?.actionButton.selectedImage = #imageLiteral(resourceName: "icon_camera_capture_act")
        }
        if #available(iOS 11.0, *) {
            tabbarController?.additionalSafeAreaInsets = .zero
        }
        
        tabbarController?.showTabber = false
        tabbarController?.showFitTileButton = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let tabbarController = self.navigationController as? FTTabbarViewController
        tabbarController?.removeAllCustomView()
        
        if isMovingFromParent {
            if #available(iOS 11.0, *) {
                tabbarController?.additionalSafeAreaInsets = UIEdgeInsets(top: 0, left: 0, bottom: tabbarController?.defaultLength ?? 0, right: 0)
            }
        }
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @objc internal func onCancelCamera() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc internal func onSaveARProject() {
        
    }
    
    @objc internal func onCaptureButtonAction(_ button: UIButton) {
        
        let tabbarController = self.navigationController as? FTTabbarViewController
        if let viewcontroller = activeViewController as? FTCameraViewController {
            if tile == nil {
                self.tile = AppData.realm.objects(Tile.self).first?.copy()
            }
            viewcontroller.capturePhoto(animation: true) { (url) in
                
                tabbarController?.showTabber = false
                tabbarController?.showFitTileButton = false
                if let drawController = tabbarController?.storyboard?.instantiateViewController(class: FTDrawViewController.self), let imageName = AppUtility.moveImageToDocument(url: url) {
                    let project = TileProject()
                    project.artboard.tile = self.tile
                    project.imageName = imageName
                    drawController.project = project
                    tabbarController?.pushViewController(drawController, animated: true)
                }
            }
        }
    }
    
    @available(iOS 11.0, *)
    @objc internal func onStartARButtonAction(_ button: UIButton) {
        
        AppDelegateObject.displayLoadingView()
        self.cameraButton.isHidden = true
        
        if tile == nil {
            self.tile = AppData.realm.objects(Tile.self).first?.copy()
        }
        
        let project = TileProject()
        project.artboard.tile = tile
        if let url = project.artboard.tile?.drawImage {
            NetworkService.requestImage(url, completion: { (image, cache) in
                
                if let _ = image {
                    let tabbarController = self.navigationController as? FTTabbarViewController
                    tabbarController?.showTabber = false
                    tabbarController?.showFitTileButton = false
                    
                    self.cameraViewController.view.removeFromSuperview()
                    self.cameraViewController.removeFromParent()
                    
                    self.arViewController.project = project
                    self.view.addSubview(self.arViewController.view)
                    self.arViewController.view.constraintsLayoutGuide(self.view.safeAreaLayoutGuide)?.activated()
                    self.addChild(self.arViewController)
                    self.view.bringSubviewToFront(self.cancelButton)
                }
            })
        } else {
            AppDelegateObject.hideLoadingView();
        }
    }
    
    @available(iOS 11.0, *)
    @objc internal func onOpenChangeToCamera() {
        
        AppDelegateObject.sendAppAnalytics("Fittile Camera")
        mode = .camera
        let contentView = arButton.superview
        arButton.isHidden = false
        arButton.alpha = 0
        let constraintAR = contentView?.findConstraint(view: arButton, attribute: .centerX)
        constraintAR?.constant = defaultSpace
        
        gallaryButton.isHidden = false
        gallaryButton.alpha = 0
        let constraintGallary = contentView?.findConstraint(view: gallaryButton, attribute: .centerX)
        constraintGallary?.constant = -defaultSpace
        
        let constraintCamera = contentView?.findConstraint(view: cameraButton, attribute: .centerX)
        constraintCamera?.constant = 0
        
        let tabbarController = self.navigationController as? FTTabbarViewController
        UIView.animate(withDuration: 0.4, animations: {
            contentView?.layoutIfNeeded()
            self.arButton.alpha = 1
            self.gallaryButton.alpha = 1
            self.cameraButton.alpha = 0
            tabbarController?.actionButton.image = #imageLiteral(resourceName: "icon_camera_capture_act")
            tabbarController?.actionButton.selectedImage = #imageLiteral(resourceName: "icon_camera_capture_act")
            tabbarController?.actionTitleLabel.text = LocalizedString("CameraActionTitle")
            tabbarController?.addMainTarget(self, action: #selector(self.onCaptureButtonAction(_:)))
        }) { (success) in
            self.cameraButton.isHidden = true
        }
        
        if cameraViewController.parent == nil {
            let transition = CATransition()
            transition.duration = 0.4
            transition.type = .push
            transition.subtype = .fromLeft
            transition.isRemovedOnCompletion = true
            transition.fillMode = .both
            transition.timingFunction = CAMediaTimingFunction(name: .easeOut)
            self.view.layer.add(transition, forKey: kCATransition)
            
            arViewController.view.removeFromSuperview()
            arViewController.removeFromParent()
            view.addSubview(cameraViewController.view)
            self.cameraViewController.view.constraintsLayoutGuide(self.view.safeAreaLayoutGuide)?.activated()
            addChild(cameraViewController)
            
            view.bringSubviewToFront(cancelButton)
        }
    }
    
    @objc internal func onOpenGallary() {
        
        AppDelegateObject.sendAppAnalytics("Fittile Gallary")
        let photoLibrary = UIImagePickerController.isSourceTypeAvailable(.photoLibrary)
        if photoLibrary {
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = false
            FTNavigationBar.setupDefaultStyle(to: imagePicker.navigationBar)
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            
        }
    }
    
    @available(iOS 11.0, *)
    @objc internal func onOpenChangeToARCamera() {
        
        AppDelegateObject.sendAppAnalytics("Fittile AR")
        mode = .ar
        let contentView = arButton.superview
        let constraintAR = contentView?.findConstraint(view: arButton, attribute: .centerX)
        constraintAR?.constant = 0
        
        let constraintGallary = contentView?.findConstraint(view: gallaryButton, attribute: .centerX)
        constraintGallary?.constant = -(defaultSpace*2)
        
        cameraButton.isHidden = false
        cameraButton.alpha = 0
        let constraintCamera = contentView?.findConstraint(view: cameraButton, attribute: .centerX)
        constraintCamera?.constant = -defaultSpace
        
        let tabbarController = self.navigationController as? FTTabbarViewController
        UIView.animate(withDuration: 0.4, animations: {
            contentView?.layoutIfNeeded()
            self.arButton.alpha = 0
            self.gallaryButton.alpha = 0
            self.cameraButton.alpha = 1
            tabbarController?.actionButton.selectedImage = #imageLiteral(resourceName: "icon_AR_act")
            tabbarController?.actionButton.image = #imageLiteral(resourceName: "icon_AR_act")
            tabbarController?.actionTitleLabel.text = LocalizedString("ARActionTitle")
            tabbarController?.addMainTarget(self, action: #selector(self.onStartARButtonAction(_:)))
        }) { (success) in
            self.arButton.isHidden = true
            self.gallaryButton.isHidden = true
        }
    }
}

extension FTFittileOptionViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if tile == nil {
            self.tile = AppData.realm.objects(Tile.self).first?.copy()
        }
        
        if #available(iOS 11.0, *) {
            if let url = info[.imageURL] as? URL {
                
                let tabbarController = self.navigationController as? FTTabbarViewController
                tabbarController?.showTabber = false
                if let drawController = tabbarController?.storyboard?.instantiateViewController(class: FTDrawViewController.self),
                    let imageName = AppUtility.moveImageToDocument(url: url) {
                    
                    let project = TileProject()
                    project.artboard.tile = self.tile
                    project.imageName = imageName
                    drawController.project = project
                    tabbarController?.pushViewController(drawController, animated: true)
                }
                picker.dismiss(animated: true, completion: nil)
                return
            }
        }
        
        if let image = info[.originalImage] as? UIImage {
            
            let tabbarController = self.navigationController as? FTTabbarViewController
            tabbarController?.showTabber = false
            if let drawController = tabbarController?.storyboard?.instantiateViewController(class: FTDrawViewController.self),
                let imageName = AppUtility.saveImageToDocument(image) {
                
                let project = TileProject()
                project.artboard.tile = self.tile
                project.imageName = imageName
                drawController.project = project
                tabbarController?.pushViewController(drawController, animated: true)
            }
            picker.dismiss(animated: true, completion: nil)
        }
    }
}

extension UIImagePickerController {
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
