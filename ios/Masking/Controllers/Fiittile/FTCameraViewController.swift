//
//  FTCameraViewController.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 7/2/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import Foundation
import AVFoundation
import DGTEngineSwift
import Photos
import CoreLocation
import Async

class FTPreviewView: UIView {
    
    override var backgroundColor: UIColor? {
        get {
            return .black
        }
        set {
            
        }
    }
    
    var videoPreviewLayer: AVCaptureVideoPreviewLayer {
        guard let layer = layer as? AVCaptureVideoPreviewLayer else {
            fatalError("Expected `AVCaptureVideoPreviewLayer` type for layer. Check PreviewView.layerClass implementation.")
        }
        
        return layer
    }
    
    var session: AVCaptureSession? {
        get {
            return videoPreviewLayer.session
        }
        set {
            videoPreviewLayer.session = newValue
            videoPreviewLayer.videoGravity = .resizeAspectFill
        }
    }
    
    // MARK: UIView
    
    override class var layerClass: AnyClass {
        return AVCaptureVideoPreviewLayer.self
    }
}

class FTCameraViewController: FTBaseViewController, AVCaptureFileOutputRecordingDelegate {
    
    // MARK: Session Management
    
    private enum SessionSetupResult {
        case success
        case notAuthorized
        case configurationFailed
    }
    
    private let session = AVCaptureSession()
    
    private var isSessionRunning = false
    
    private let sessionQueue = DispatchQueue(label: "camera session queue") // Communicate with the session and other session objects on this queue.
    
    private var setupResult: SessionSetupResult = .success
    
    private var orientation = UIDeviceOrientation.portrait
    
    var videoDeviceInput: AVCaptureDeviceInput!
    
    lazy var previewView: FTPreviewView = {
        let view = FTPreviewView.autoLayout()
        self.view.addSubview(view)
        view.constraintsAroundView()?.activated()
        return view
    }()
    
    lazy var flashButton: UIButton = {
        let button = UIButton(type: .system)
        button.prepareForAutoLayout()
        button.image = #imageLiteral(resourceName: "icon_camera_flash_autp").withRenderingMode(.alwaysOriginal)
        button.addTarget(self, action: #selector(self.onChageFlashMode(_:)), for: .touchUpInside)
        return button
    }()
    var flashMode: AVCaptureDevice.FlashMode = .auto
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupNotifications()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.focusAndExposeTap(_:)))
        tapGesture.delegate = self
        view.addGestureRecognizer(tapGesture)
        
        // Set up the video preview view.
        previewView.session = session
        
        /*
         Check video authorization status. Video access is required and audio
         access is optional. If audio access is denied, audio is not recorded
         during movie recording.
         */
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            // The user has previously granted access to the camera.
            break
            
        case .notDetermined:
            /*
             The user has not yet been presented with the option to grant
             video access. We suspend the session queue to delay session
             setup until the access request has completed.
             
             Note that audio access will be implicitly requested when we
             create an AVCaptureDeviceInput for audio during session setup.
             */
            sessionQueue.suspend()
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { granted in
                if !granted {
                    self.setupResult = .notAuthorized
                }
                self.sessionQueue.resume()
            })
            
        default:
            // The user has previously denied access.
            setupResult = .notAuthorized
        }
        
        /*
         Setup the capture session.
         In general it is not safe to mutate an AVCaptureSession or any of its
         inputs, outputs, or connections from multiple threads at the same time.
         
         Why not do all of this on the main queue?
         Because AVCaptureSession.startRunning() is a blocking call which can
         take a long time. We dispatch session setup to the sessionQueue so
         that the main queue isn't blocked, which keeps the UI responsive.
         */
        sessionQueue.async {
            self.configureSession()
            Async.main {
                self.setupFlashButton()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        NotificationCenter.default.addObserver(self, selector: #selector(self.onOrientationChanged), name: UIDevice.orientationDidChangeNotification, object: nil)
        
        sessionQueue.async {
            switch self.setupResult {
            case .success:
                // Only setup observers and start the session running if setup succeeded.
                self.addObservers()
                self.session.startRunning()
                self.isSessionRunning = self.session.isRunning
            case .notAuthorized:
                DispatchQueue.main.async {
                    let message = LocalizedString("AppCameraPermisstionError")
                    let alertController = UIAlertController(title: LocalizedString("error"), message: message, preferredStyle: .alert)
                    
                    alertController.addAction(UIAlertAction(title: LocalizedString("ok"),
                                                            style: .cancel,
                                                            handler: nil))
                    
                    alertController.addAction(UIAlertAction(title: LocalizedString("settings"),
                                                            style: .`default`,
                                                            handler: { _ in
                                                                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
                    }))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
            case .configurationFailed:
                DispatchQueue.main.async {
                    let message = LocalizedString("AppCameraNotOpenError")
                    let alertController = UIAlertController(title: LocalizedString("error"), message: message, preferredStyle: .alert)
                    
                    alertController.addAction(UIAlertAction(title: LocalizedString("ok"),
                                                            style: .cancel,
                                                            handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        sessionQueue.async {
            if self.setupResult == .success {
                self.session.stopRunning()
                self.isSessionRunning = self.session.isRunning
                self.removeObservers()
            }
        }
        endUpdateLocation()
        super.viewWillDisappear(animated)
        UIDevice.current.endGeneratingDeviceOrientationNotifications()
        NotificationCenter.default.removeObserver(self, name: UIDevice.orientationDidChangeNotification, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startUpdateLocation()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        if let videoPreviewLayerConnection = previewView.videoPreviewLayer.connection {
            let deviceOrientation = UIDevice.current.orientation
            guard let newVideoOrientation = AVCaptureVideoOrientation(deviceOrientation: deviceOrientation),
                deviceOrientation.isPortrait || deviceOrientation.isLandscape else {
                    return
            }
            videoPreviewLayerConnection.videoOrientation = newVideoOrientation
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func removeFromParent() {
        super.removeFromParent()
        sessionQueue.async {
            if self.setupResult == .success {
                self.session.stopRunning()
                self.isSessionRunning = self.session.isRunning
                self.removeObservers()
            }
        }
    }

    @objc internal func focusAndExposeTap(_ gestureRecognizer: UITapGestureRecognizer) {
        DLog("focusAndExposeTap")
        let devicePoint = previewView.videoPreviewLayer.captureDevicePointConverted(fromLayerPoint: gestureRecognizer.location(in: gestureRecognizer.view))
        focus(with: .autoFocus, exposureMode: .autoExpose, at: devicePoint, monitorSubjectAreaChange: true)
    }
    
    @objc internal func onChageFlashMode(_ button: UIButton) {
        
        switch flashMode {
        case .auto:
            flashMode = .on
            button.image = #imageLiteral(resourceName: "icon_camera_flash_act").withRenderingMode(.alwaysOriginal)
        case .on:
            flashMode = .off
            button.image = #imageLiteral(resourceName: "icon_camera_flash").withRenderingMode(.alwaysOriginal)
        case .off:
            flashMode = .auto
            button.image = #imageLiteral(resourceName: "icon_camera_flash_autp").withRenderingMode(.alwaysOriginal)
        default: break
        }
    }
    
    // MARK: Location Management
    
    internal func showAuthenUserLocationDeniedAlert() {
//        let alert = UIAlertController(title: "Location Service is off", message: "For your convenience please open setting and allow location service.", preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//        alert.addAction(UIAlertAction(title: "Setting", style: .destructive, handler: { (alert) in
//            let url = URL(string: UIApplication.openSettingsURLString)!
//            UIApplication.shared.open(url, options: [:], completionHandler: nil)
//        }))
//        alert.show(self, sender: nil)
    }
    
    internal func startUpdateLocation() {
//        DNLocationManager.default.updateLocation()
//
//        if let currentPlacemark = currentPlacemark {
//            self.setLocationName(for: currentPlacemark)
//            locationIconImageView.isHidden = false
//            locationLoadIndicatorView.stopAnimating()
//        } else {
//            locationLabel.text = DGTLocalizedString("locating")
//            locationIconImageView.isHidden = true
//            locationLoadIndicatorView.startAnimating()
//        }
    }
    
    internal func endUpdateLocation() {
//        DNLocationManager.default.stopUpdateLocation()
    }
    
    internal func changeToLocation(_ location: CLLocation) {
//        if #available(iOS 11.0, *) {
//            CLGeocoder().reverseGeocodeLocation(location, preferredLocale: DGTApplicationLanguage.locale) { (marks, error) in
//                if let mark = marks?.first {
//                    self.currentPlacemark = mark
//                    self.setLocationName(for: mark)
//                    self.locationIconImageView.isHidden = false
//                    self.locationLoadIndicatorView.stopAnimating()
//                }
//            }
//        } else {
//            // Fallback on earlier versions
//            CLGeocoder().reverseGeocodeLocation(location) { (marks, error) in
//                if let mark = marks?.first {
//                    self.currentPlacemark = mark
//                    self.setLocationName(for: mark)
//                    self.locationIconImageView.isHidden = false
//                    self.locationLoadIndicatorView.stopAnimating()
//                }
//            }
//        }
    }
    
    internal func sendLocationToServer(_ location: CLLocation) {
        
//        Async.background {
//            let location = "\(location.coordinate.latitude),\(location.coordinate.longitude)"
//            do {
//                try DNAppService.call.sendLocation(location, for: DNAppData.token)
//            } catch {
//                DLog("error : \(error)")
//            }
//        }
    }
    
    internal func setLocationName(for placemark: CLPlacemark) {
//        let date = DateFormatter.string(format: "dd/MM/yyyy", date: Date(), locale: DGTApplicationLanguage.locale)
//        let location: String = {
//            let name = locationName(for: placemark)
//            if let subLocality = name.subVanue {
//                return name.vanue + ", " + subLocality
//            }
//            return name.vanue
//        }()
//        self.locationLabel.text = date + "\n" + location
    }
    
//    internal func locationName(for placemark: CLPlacemark) -> (vanue: String, subVanue: String?) {
//
//        var title, subTitle: String?
//
//        if let subLocality = placemark.subLocality {
//            title = subLocality
//        }
//        if let locality = placemark.locality {
//            if let _ = title {
//                subTitle = locality
//            } else {
//                title = locality
//            }
//        }
//        if let title = title, let subTitle = subTitle {
//            return (vanue: title, subVanue: subTitle)
//        }
//
//        if let name = placemark.name {
//            if let _ = title {
//                subTitle = name
//            } else {
//                title = name
//            }
//        }
//        if let country = placemark.country {
//            if let _ = title {
//                subTitle = country
//            } else {
//                title = country
//            }
//        }
//        if let title = title {
//            if let subTitle = subTitle {
//                return (vanue: title, subVanue: subTitle)
//            }
//            return (vanue: title, subVanue: nil)
//        }
//        let location = placemark.locality ?? placemark.country ?? placemark.name ?? ""
//        return (vanue: location, subVanue: nil)
//    }
    
    private func setupNotifications() {
        
        let notification = NotificationCenter.default
//        notification.addObserver(self, selector: #selector(self.onUserLocationChange(_:)), name: .userLocationChange, object: nil)
//        notification.addObserver(self, selector: #selector(self.onLocationAuthenChange(_:)), name: .locationAuthenChange, object: nil)
    }
    
    
    // Call this on the session queue.
    private func configureSession() {
        if setupResult != .success {
            return
        }
        
        session.beginConfiguration()
        
        /*
         We do not create an AVCaptureMovieFileOutput when setting up the session because the
         AVCaptureMovieFileOutput does not support movie recording with AVCaptureSession.Preset.Photo.
         */
        session.sessionPreset = .high
        
        // Add video input.
        do {
            var defaultVideoDevice: AVCaptureDevice?
            
            // Choose the back dual camera if available, otherwise default to a wide angle camera.
            if #available(iOS 10.2, *) {
                if let dualCameraDevice = AVCaptureDevice.default(.builtInDualCamera, for: .video, position: .back) {
                    defaultVideoDevice = dualCameraDevice
                }
            }
            
            if let backCameraDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back), defaultVideoDevice == nil {
                // If the back dual camera is not available, default to the back wide angle camera.
                defaultVideoDevice = backCameraDevice
            }
            
            if let backCameraDevice = AVCaptureDevice.default(.builtInTelephotoCamera, for: .video, position: .back), defaultVideoDevice == nil {
                // If the back dual camera is not available, default to the back wide angle camera.
                defaultVideoDevice = backCameraDevice
            }
            
            if let device = defaultVideoDevice {
                let videoDeviceInput = try AVCaptureDeviceInput(device: device)
                
                if session.canAddInput(videoDeviceInput) {
                    session.addInput(videoDeviceInput)
                    self.videoDeviceInput = videoDeviceInput
                }
                
                DispatchQueue.main.async {
                    /*
                     Why are we dispatching this to the main queue?
                     Because AVCaptureVideoPreviewLayer is the backing layer for PreviewView and UIView
                     can only be manipulated on the main thread.
                     Note: As an exception to the above rule, it is not necessary to serialize video orientation changes
                     on the AVCaptureVideoPreviewLayer’s connection with other session manipulation.
                     
                     Use the status bar orientation as the initial video orientation. Subsequent orientation changes are
                     handled by CameraViewController.viewWillTransition(to:with:).
                     */
                    let statusBarOrientation = UIApplication.shared.statusBarOrientation
                    var initialVideoOrientation: AVCaptureVideoOrientation = .portrait
                    if statusBarOrientation != .unknown {
                        if let videoOrientation = AVCaptureVideoOrientation(interfaceOrientation: statusBarOrientation) {
                            initialVideoOrientation = videoOrientation
                        }
                    }
                    
                    self.previewView.videoPreviewLayer.connection?.videoOrientation = initialVideoOrientation
                }
            } else {
                DLog("Could not add video device input to the session")
                setupResult = .configurationFailed
                session.commitConfiguration()
                return
            }
        } catch {
            DLog("Could not create video device input: \(error)")
            setupResult = .configurationFailed
            session.commitConfiguration()
            return
        }
        
        if session.canAddOutput(photoOutput) {
            session.addOutput(photoOutput)
            
            photoOutput.isHighResolutionCaptureEnabled = true
            photoOutput.isLivePhotoCaptureEnabled = photoOutput.isLivePhotoCaptureSupported
            if #available(iOS 11.0, *) {
                photoOutput.isDepthDataDeliveryEnabled = photoOutput.isDepthDataDeliverySupported
            }
        } else {
            DLog("Could not add photo output to the session")
            setupResult = .configurationFailed
        }
        session.commitConfiguration()
    }
    
    private func setupFlashButton() {
        if self.videoDeviceInput?.device.isFlashAvailable == true {
            view.addSubview(flashButton)
            let space = AppUIStyle(traitCollection: traitCollection).space
            [flashButton.right == view.rightMargin,
             flashButton.top == view.topMargin + space].activated()
        }
    }
    
    // MARK: Device Configuration

    private let videoDeviceDiscoverySession: AVCaptureDevice.DiscoverySession = {
        if #available(iOS 10.2, *) {
            return AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera, .builtInDualCamera],
                                                    mediaType: .video, position: .unspecified)
        } else {
            // Fallback on earlier versions
            return AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera, .builtInTelephotoCamera],
                                                    mediaType: .video, position: .unspecified)
        }
    }()
    
    private func focus(with focusMode: AVCaptureDevice.FocusMode, exposureMode: AVCaptureDevice.ExposureMode, at devicePoint: CGPoint, monitorSubjectAreaChange: Bool) {
        
        guard let input = videoDeviceInput else {
            return
        }
        sessionQueue.async {
            let device = input.device
            do {
                try device.lockForConfiguration()
                
                /*
                 Setting (focus/exposure)PointOfInterest alone does not initiate a (focus/exposure) operation.
                 Call set(Focus/Exposure)Mode() to apply the new point of interest.
                 */
                if device.isFocusPointOfInterestSupported && device.isFocusModeSupported(focusMode) {
                    device.focusPointOfInterest = devicePoint
                    device.focusMode = focusMode
                }
                
                if device.isExposurePointOfInterestSupported && device.isExposureModeSupported(exposureMode) {
                    device.exposurePointOfInterest = devicePoint
                    device.exposureMode = exposureMode
                }
                
                device.isSubjectAreaChangeMonitoringEnabled = monitorSubjectAreaChange
                device.unlockForConfiguration()
            } catch {
                DLog("Could not lock device for configuration: \(error)")
            }
        }
    }
    
    // MARK: Capturing Photos
    
    private let photoOutput = AVCapturePhotoOutput()
    
    private var inProgressPhotoCaptureDelegates = [Int64: FTPhotoCaptureProcessor]()
    
    func capturePhoto(animation: Bool = true, completion: @escaping ((_ imageURL: URL)->Void)) {
        /*
         Retrieve the video preview layer's video orientation on the main queue before
         entering the session queue. We do this to ensure UI elements are accessed on
         the main thread and session configuration is done on the session queue.
         */
        
        var deviceOrientation = orientation
        Async.main {
            if UIDevice.current.orientation != .unknown {
                deviceOrientation = UIDevice.current.orientation
            }
        }.custom(queue: sessionQueue) {
            if let photoOutputConnection = self.photoOutput.connection(with: .video) {
                
                guard let newVideoOrientation = AVCaptureVideoOrientation(deviceOrientation: deviceOrientation),
                    deviceOrientation.isPortrait || deviceOrientation.isLandscape else {
                        return
                }
                photoOutputConnection.videoOrientation = newVideoOrientation
            }
            
            let photoSettings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecJPEG])
            // Capture HEIF photo when supported, with flash set to auto and high resolution photo enabled.
            
            if self.videoDeviceInput?.device.isFlashAvailable == true {
                photoSettings.flashMode = self.flashMode
            }
            
            photoSettings.isHighResolutionPhotoEnabled = true
            if !photoSettings.__availablePreviewPhotoPixelFormatTypes.isEmpty {
                photoSettings.previewPhotoFormat = [kCVPixelBufferPixelFormatTypeKey as String: photoSettings.__availablePreviewPhotoPixelFormatTypes.first!]
            }
            
            if #available(iOS 11.0, *) {
                if self.photoOutput.isDepthDataDeliverySupported {
                    photoSettings.isDepthDataDeliveryEnabled = true
                } else {
                    photoSettings.isDepthDataDeliveryEnabled = false
                }
            }
            
            // Use a separate object for the photo capture delegate to isolate each capture life cycle.
            let photoCaptureProcessor = FTPhotoCaptureProcessor(with: photoSettings, willCapturePhotoAnimation: {
                
                if animation {
                    DispatchQueue.main.async {
                        self.previewView.videoPreviewLayer.opacity = 0
                        UIView.animate(withDuration: 0.25) {
                            self.previewView.videoPreviewLayer.opacity = 1
                        }
                    }
                }
            }, livePhotoCaptureHandler: { capturing in
                /*
                 Because Live Photo captures can overlap, we need to keep track of the
                 number of in progress Live Photo captures to ensure that the
                 Live Photo label stays visible during these captures.
                 */
                
            }, completionHandler: { photoCaptureProcessor, url in
                // When the capture is complete, remove a reference to the photo capture delegate so it can be deallocated.
                if let url = url {
//                    self.uploadNews?.addImage(url: url)
                    DLog("url : \(url)")
                    completion(url)
                } else {
                    let message = LocalizedString("AppCantSaveImageError")
                    let alertController = UIAlertController(title: LocalizedString("error"), message: message, preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: LocalizedString("ok"), style: .cancel, handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                }
                self.sessionQueue.async {
                    self.inProgressPhotoCaptureDelegates[photoCaptureProcessor.requestedPhotoSettings.uniqueID] = nil
                }
            }
            )
            
            /*
             The Photo Output keeps a weak reference to the photo capture delegate so
             we store it in an array to maintain a strong reference to this object
             until the capture is completed.
             */
            self.inProgressPhotoCaptureDelegates[photoCaptureProcessor.requestedPhotoSettings.uniqueID] = photoCaptureProcessor
            self.photoOutput.capturePhoto(with: photoSettings, delegate: photoCaptureProcessor)
        }
    }
    
    // MARK: KVO and Notifications
    
    private var keyValueObservations = [NSKeyValueObservation]()
    
    private func addObservers() {
        let keyValueObservation = session.observe(\.isRunning, options: .new) { _, change in
            guard let isSessionRunning = change.newValue else { return }
            
            DispatchQueue.main.async {
                // Only enable the ability to change camera if the device has more than one camera.
//                self.cameraButton.isEnabled = isSessionRunning && self.videoDeviceDiscoverySession.uniqueDevicePositionsCount > 1
//                self.recordButton.isEnabled = isSessionRunning && self.movieFileOutput != nil
            }
        }
        keyValueObservations.append(keyValueObservation)
        
        let notification = NotificationCenter.default
        notification.addObserver(self, selector: #selector(subjectAreaDidChange), name: .AVCaptureDeviceSubjectAreaDidChange, object: videoDeviceInput.device)
        notification.addObserver(self, selector: #selector(sessionRuntimeError), name: .AVCaptureSessionRuntimeError, object: session)
        
        /*
         A session can only run when the app is full screen. It will be interrupted
         in a multi-app layout, introduced in iOS 9, see also the documentation of
         AVCaptureSessionInterruptionReason. Add observers to handle these session
         interruptions and show a preview is paused message. See the documentation
         of AVCaptureSessionWasInterruptedNotification for other interruption reasons.
         */
        notification.addObserver(self, selector: #selector(sessionWasInterrupted), name: .AVCaptureSessionWasInterrupted, object: session)
        notification.addObserver(self, selector: #selector(sessionInterruptionEnded), name: .AVCaptureSessionInterruptionEnded, object: session)
    }
    
    private func removeObservers() {
        
        let notification = NotificationCenter.default
        notification.removeObserver(self, name: .AVCaptureDeviceSubjectAreaDidChange, object: videoDeviceInput.device)
        notification.removeObserver(self, name: .AVCaptureSessionRuntimeError, object: session)
        notification.removeObserver(self, name: .AVCaptureSessionWasInterrupted, object: session)
        notification.removeObserver(self, name: .AVCaptureSessionInterruptionEnded, object: session)
        
        for keyValueObservation in keyValueObservations {
            keyValueObservation.invalidate()
        }
        keyValueObservations.removeAll()
    }
    
    @objc
    func subjectAreaDidChange(notification: NSNotification) {
        let devicePoint = CGPoint(x: 0.5, y: 0.5)
        focus(with: .continuousAutoFocus, exposureMode: .continuousAutoExposure, at: devicePoint, monitorSubjectAreaChange: false)
    }
    
    @objc
    func sessionRuntimeError(notification: NSNotification) {
        guard let error = notification.userInfo?[AVCaptureSessionErrorKey] as? AVError else { return }
        
        DLog("Capture session runtime error: \(error)")
        
        /*
         Automatically try to restart the session running if media services were
         reset and the last start running succeeded. Otherwise, enable the user
         to try to resume the session running.
         */
        if error.code == .mediaServicesWereReset {
            sessionQueue.async {
                if self.isSessionRunning {
                    self.session.startRunning()
                    self.isSessionRunning = self.session.isRunning
                } else {
                }
            }
        }
    }
    
    @objc
    func sessionWasInterrupted(notification: NSNotification) {
        /*
         In some scenarios we want to enable the user to resume the session running.
         For example, if music playback is initiated via control center while
         using AVCam, then the user can let AVCam resume
         the session running, which will stop music playback. Note that stopping
         music playback in control center will not automatically resume the session
         running. Also note that it is not always possible to resume, see `resumeInterruptedSession(_:)`.
         */
        if let userInfoValue = notification.userInfo?[AVCaptureSessionInterruptionReasonKey] as AnyObject?,
            let reasonIntegerValue = userInfoValue.integerValue,
            let reason = AVCaptureSession.InterruptionReason(rawValue: reasonIntegerValue) {
            DLog("Capture session was interrupted with reason \(reason)")
            
            var showResumeButton = false
            
            if reason == .audioDeviceInUseByAnotherClient || reason == .videoDeviceInUseByAnotherClient {
                showResumeButton = true
            } else if reason == .videoDeviceNotAvailableWithMultipleForegroundApps {
                // Simply fade-in a label to inform the user that the camera is unavailable.
                //                cameraUnavailableLabel.alpha = 0
                //                cameraUnavailableLabel.isHidden = false
                //                UIView.animate(withDuration: 0.25) {
                //                    self.cameraUnavailableLabel.alpha = 1
                //                }
            }
            
            if showResumeButton {
                // Simply fade-in a button to enable the user to try to resume the session running.
                //                resumeButton.alpha = 0
                //                resumeButton.isHidden = false
                //                UIView.animate(withDuration: 0.25) {
                //                    self.resumeButton.alpha = 1
                //                }
            }
        }
    }
    
    @objc
    func sessionInterruptionEnded(notification: NSNotification) {
        print("Capture session interruption ended")
        
        //        if !resumeButton.isHidden {
        //            UIView.animate(withDuration: 0.25,
        //                           animations: {
        //                            self.resumeButton.alpha = 0
        //            }, completion: { _ in
        //                self.resumeButton.isHidden = true
        //            }
        //            )
        //        }
        //        if !cameraUnavailableLabel.isHidden {
        //            UIView.animate(withDuration: 0.25,
        //                           animations: {
        //                            self.cameraUnavailableLabel.alpha = 0
        //            }, completion: { _ in
        //                self.cameraUnavailableLabel.isHidden = true
        //            }
        //            )
        //        }
    }
    
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        
    }
    
    @objc internal func onUserLocationChange(_ notification: Notification) {
        
        guard let location = notification.userInfo?["location"] as? CLLocation else {
            return
        }
        self.changeToLocation(location)
        self.sendLocationToServer(location)
    }
    
    @objc internal func onLocationAuthenChange(_ notification: Notification) {
        
        let authen = notification.userInfo?["status"] as? Bool
        if authen == true {
//            DNLocationManager.default.updateLocation()
        } else {
            self.showAuthenUserLocationDeniedAlert()
        }
    }
    
    @objc internal func onOrientationChanged(_ notification: Notification) {
        self.orientation = UIDevice.current.orientation
    }
}

extension FTCameraViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if gestureRecognizer is UITapGestureRecognizer, touch.view?.isKind(of: UIControl.self) == true {
            return false
        }
        return true
    }
}

extension AVCaptureVideoOrientation {
    init?(deviceOrientation: UIDeviceOrientation) {
        switch deviceOrientation {
        case .portrait: self = .portrait
        case .portraitUpsideDown: self = .portraitUpsideDown
        case .landscapeLeft: self = .landscapeRight
        case .landscapeRight: self = .landscapeLeft
        default: return nil
        }
    }
    
    init?(interfaceOrientation: UIInterfaceOrientation) {
        switch interfaceOrientation {
        case .portrait: self = .portrait
        case .portraitUpsideDown: self = .portraitUpsideDown
        case .landscapeLeft: self = .landscapeLeft
        case .landscapeRight: self = .landscapeRight
        default: return nil
        }
    }
}

@available(iOS 10.0, *)
extension AVCaptureDevice.DiscoverySession {
    var uniqueDevicePositionsCount: Int {
        var uniqueDevicePositions: [AVCaptureDevice.Position] = []
        
        for device in devices {
            if !uniqueDevicePositions.contains(device.position) {
                uniqueDevicePositions.append(device.position)
            }
        }
        
        return uniqueDevicePositions.count
    }
}

class FTPhotoCaptureProcessor: NSObject {
    
    private(set) var requestedPhotoSettings: AVCapturePhotoSettings
    
    private let willCapturePhotoAnimation: () -> Void
    
    private let livePhotoCaptureHandler: (Bool) -> Void
    
    private let completionHandler: (_ processor: FTPhotoCaptureProcessor, _ imageURL: URL?) -> Void
    
    private var photoData: Data?
    
    private var livePhotoCompanionMovieURL: URL?
    private var imageURL: URL?
    
    init(with requestedPhotoSettings: AVCapturePhotoSettings,
         willCapturePhotoAnimation: @escaping () -> Void,
         livePhotoCaptureHandler: @escaping (Bool) -> Void,
         completionHandler: @escaping (FTPhotoCaptureProcessor, URL?) -> Void) {
        self.requestedPhotoSettings = requestedPhotoSettings
        self.willCapturePhotoAnimation = willCapturePhotoAnimation
        self.livePhotoCaptureHandler = livePhotoCaptureHandler
        self.completionHandler = completionHandler
    }
    
    private func didFinish() {
        let fileManager = FileManager.default
        if let livePhotoCompanionMoviePath = livePhotoCompanionMovieURL?.path {
            if fileManager.fileExists(atPath: livePhotoCompanionMoviePath) {
                do {
                    try fileManager.removeItem(atPath: livePhotoCompanionMoviePath)
                } catch {
                    print("Could not remove file at url: \(livePhotoCompanionMoviePath)")
                }
            }
        }
        completionHandler(self, imageURL)
        
        if let url = imageURL, fileManager.fileExists(atPath: url.absoluteString) {
            do {
                try fileManager.removeItem(at: url)
            } catch {
                print("Could not remove file at url: \(url)")
            }
        }
    }
    
}

extension FTPhotoCaptureProcessor: AVCapturePhotoCaptureDelegate {
    /*
     This extension includes all the delegate callbacks for AVCapturePhotoCaptureDelegate protocol
     */
    
    func photoOutput(_ output: AVCapturePhotoOutput, willBeginCaptureFor resolvedSettings: AVCaptureResolvedPhotoSettings) {
        if resolvedSettings.livePhotoMovieDimensions.width > 0 && resolvedSettings.livePhotoMovieDimensions.height > 0 {
            livePhotoCaptureHandler(true)
        }
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, willCapturePhotoFor resolvedSettings: AVCaptureResolvedPhotoSettings) {
        willCapturePhotoAnimation()
    }
    
    @available(iOS 11.0, *)
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        if let error = error {
            DLog("Error capturing photo: \(error)")
        } else {
            photoData = photo.fileDataRepresentation()
        }
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishRecordingLivePhotoMovieForEventualFileAt outputFileURL: URL, resolvedSettings: AVCaptureResolvedPhotoSettings) {
        livePhotoCaptureHandler(false)
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingLivePhotoToMovieFileAt outputFileURL: URL, duration: CMTime, photoDisplayTime: CMTime, resolvedSettings: AVCaptureResolvedPhotoSettings, error: Error?) {
        if error != nil {
            DLog("Error processing live photo companion movie: \(String(describing: error))")
            return
        }
        livePhotoCompanionMovieURL = outputFileURL
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        if let error = error {
            print(error.localizedDescription);
        }
        
        if let sampleBuffer = photoSampleBuffer, let previewBuffer = previewPhotoSampleBuffer, let dataImage = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: sampleBuffer, previewPhotoSampleBuffer: previewBuffer) {
            
            do {
                let uuids = UUID().uuidString.components(separatedBy: "-")
                let name = uuids.first! + uuids.last!
                let fileURL = URL(fileURLWithPath: NSTemporaryDirectory() + name + ".jpg")
                try dataImage.write(to: fileURL)
                self.imageURL = fileURL
            } catch {
                DLog("error : \(error)")
            }
        }
        self.didFinish()
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishCaptureFor resolvedSettings: AVCaptureResolvedPhotoSettings, error: Error?) {
        if let error = error {
            DLog("Error capturing photo: \(error)")
            didFinish()
            return
        }
        
        guard let photoData = photoData else {
            DLog("No photo data resource")
            didFinish()
            return
        }
        
        do {
            let uuids = UUID().uuidString.components(separatedBy: "-")
            let name = uuids.first! + uuids.last!
            let fileURL = URL(fileURLWithPath: NSTemporaryDirectory() + name + ".jpg")
            try photoData.write(to: fileURL)
            self.imageURL = fileURL
        } catch {
            DLog("error : \(error)")
        }
        self.didFinish()
    }
}
