//
//  ViewController.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 7/1/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import UIKit
import DGTEngineSwift
import Async

enum TileStage: Int {
    case plot
    case scale
    case tilt
    case location
}

class FTDrawViewController: UIViewController {
    
    @IBOutlet weak var menuStackView: UIStackView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var artboardView: UIView!
    @IBOutlet weak var overlayView: LineOverlayView!
    
    @IBOutlet weak var tapGesture: UITapGestureRecognizer!
    @IBOutlet weak var panGesture: UIPanGestureRecognizer!
    
    @IBOutlet weak var catalogPanGasture: UIPanGestureRecognizer!
    @IBOutlet weak var slideView: UIView!
    @IBOutlet weak var slideActionView: UIView!
    @IBOutlet weak var slideIndicatorView: UIImageView!
    @IBOutlet weak var filterButton: UIButton!
    
    var catalogListController: FTCatalogListViewController {
        return children.first(where: { $0 is FTCatalogListViewController }) as! FTCatalogListViewController
    }
    
    weak var activeButton: MaskButton?
    weak var floorOverlayView: FloorOverlayView?
    
    var project: TileProject! {
        didSet {
            canvasImage = UIImage(contentsOfFile: AppUtility.pathFile(project.imageName))
            self.imageView?.image = canvasImage
            if project.shareProject == false {
                saveButton?.title = LocalizedString("share")
                saveButton?.isEnabled = true
            }
        }
    }
    
    private var canvasImage: UIImage!
    private var buttons = [MaskButton]()
    private var tileView: TileView?
    private var artboard: TileArtboard? {
        return project?.artboard
    }
    private var currentWidth: CGFloat = 0
    
    private var pinchGesture: UIPanGestureRecognizer?
    private var didShowWallAlert = false
    
    private var stage: TileStage = .plot {
        didSet {
            if oldValue == .plot && stage != .plot {
                buttons.forEach({ $0.isHidden = true })
                overlayView.isHidden = true
            } else if stage == .plot {
                buttons.forEach({ $0.isHidden = false })
                overlayView.isHidden = false
            }
            
            if oldValue == .location && stage != .location {
                moveIndicatorImageView.removeFromSuperview()
            }
            
            if stage == .scale || stage == .tilt {
                sliderControlView?.isHidden = false
                let infoLabel = sliderControlView?.firstSubview(class: UILabel.self)
                switch stage {
                case .scale: infoLabel?.text = LocalizedString("Scale")
                case .tilt: infoLabel?.text = LocalizedString("Tilt")
                default: break
                }
            } else {
                sliderControlView?.isHidden = true
            }
            
            pinButton.isSelected = false
            scaleButton.isSelected = false
            tiltButton.isSelected = false
            locationButton.isSelected = false
            switch stage {
            case .plot:
                pinButton.isSelected = true
            case .scale:
                scaleButton.isSelected = true
            case .tilt:
                tiltButton.isSelected = true
            case .location:
                locationButton.isSelected = true
            }
        }
    }
    
    @IBOutlet weak var pinButton: UIButton!
    @IBOutlet weak var scaleButton: UIButton!
    @IBOutlet weak var tiltButton: UIButton!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    private var sliderControlView: UIView?
    private var sliderView: UISlider?
    
    private var completePoint: Bool = false
    
    lazy var moveIndicatorImageView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "icon_tile_move_indicator"))
        imageView.sizeToFit()
        return imageView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        imageView.backgroundColor = .black
        pinButton?.isSelected = true
        
        catalogListController.style = .mini
        catalogListController.delegate = self
        
        if canvasImage == nil {
            canvasImage = UIImage(contentsOfFile: AppUtility.pathFile(project.imageName))
        }
        imageView?.image = canvasImage?.scaleImageToWidth(view.bounds.width)
        slideView?.isHidden = true
        
        slideIndicatorView.image = UIImage(color: .appLightGray, size: CGSize(width: 70, height: 6), cornerRadius: 3)
        slideIndicatorView.contentMode = .center
        filterButton.tintColor = .appGreen
        filterButton.titleFont = UIFont.customFont(20, weight: .dgtRegular)
        filterButton.title = " " + LocalizedString("CatalogFilterTitle")
        filterButton.image = #imageLiteral(resourceName: "icon_filter").scaleImageToHeight(#imageLiteral(resourceName: "icon_filter").size.height/1.5)
        cancelButton.title = LocalizedString("cancel")
        
        saveButton?.isEnabled = false
        saveButton?.setTitleColor(UIColor.appGreen, for: .normal)
        if let project = project {
            if project.shareProject == false && project.completed == false {
                saveButton?.title = LocalizedString("share")
                saveButton?.isEnabled = true
            }
        }
        
        AppDelegateObject.sendAppAnalytics("Fittile Try")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        let tabbarController = self.navigationController as? FTTabbarViewController
        tabbarController?.showTabber = false
        tabbarController?.showFitTileButton = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
  
        if AppData.appInfos.tutorialCompleted == false {
            modalPresentationStyle = .custom
            let plotLocation = pinButton.convert(pinButton.bounds, to: self.view)
            let scaleLocation = scaleButton.convert(scaleButton.bounds, to: self.view)
            let tiltLocation = tiltButton.convert(tiltButton.bounds, to: self.view)
            let moveLocation = locationButton.convert(locationButton.bounds, to: self.view)
            let controller = FTTutorialViewController(step: [.plot: plotLocation, .scale: scaleLocation, .tilt: tiltLocation, .move: moveLocation])
            self.present(controller, animated: true, completion: nil)
            AppData.appInfos.tutorialCompleted = true
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if currentWidth != view.bounds.width {
            DLog("Resize")
            imageView?.image = canvasImage?.scaleImageToWidth(view.bounds.width)
            currentWidth = view.bounds.width
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        let tabbarController = self.navigationController as? FTTabbarViewController
        if isMovingFromParent {
            tabbarController?.showTabber = true
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: nil) { (context) in
            if let points = self.artboard?.points, self.completePoint {
                let frame = self.drawFrame(of: points)
                self.floorOverlayView?.drawFrameChanged(to: frame)
                self.tileView?.drawFrameChanged(to: frame)
                self.maskImage(to: points)
                self.artboard?.frame = frame
            }
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updateFont(for: traitCollection)
        updateUI(for: traitCollection)
    }
    
    func updateUI(for traitCollection: UITraitCollection) {
        let style = AppUIStyle(traitCollection: traitCollection)
        slideActionView.layoutMargins = UIEdgeInsets(top: style.space/4, left: style.space, bottom: style.space/4, right: style.space)
    }
    
    func updateFont(for traitCollection: UITraitCollection) {
        
    }
    
    func addFloor(to points: [CGPoint]) {
        let floorView = FloorOverlayView()
        floorView.clipsToBounds = true
        floorView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        artboardView.insertSubview(floorView, belowSubview: imageView)
        
        guard let imageURL = self.artboard?.tile?.drawImage, let image = NetworkService.cacheImage(imageURL) else {
            return
        }
        
        let tileView = TileView(frame: view.bounds, tileImage: image)
        let drawFrame = self.drawFrame(of: points)
        floorView.drawFrameChanged(to: drawFrame)
        tileView.drawFrameChanged(to: drawFrame)
        floorView.addSubview(tileView)
        self.floorOverlayView = floorView
        self.tileView = tileView
        
        if let perspective = self.artboard?.perspective, perspective != 0 {
            var transform3D = CATransform3DIdentity
            switch self.artboard?.tiltType {
            case .floor?:
                transform3D.m24 = CGFloat(-perspective) / 1000
            case .left?:
                transform3D.m14 = CGFloat(perspective) / 1000
            case .right?:
                transform3D.m14 = CGFloat(-perspective) / 1000
            default:break
            }
            transform3D = CATransform3DRotate(transform3D, CGFloat.pi / 2, 0, 0, 0)
            tileView.layer.transform = transform3D
        }
        
        if let frame = self.artboard?.frame, frame != drawFrame {
            self.artboard?.frame = drawFrame
        }
    }
    
    func maskImage(to points: [CGPoint]) {
        
        let path = CGMutablePath()
        path.addRect(imageView.bounds)
        for (index, point) in points.enumerated() {
            
            let p = CGPoint(x: point.x-imageView.frame.origin.x, y: point.y-imageView.frame.origin.y)
            if index == 0 {
                path.move(to: p)
            } else {
                path.addLine(to: p)
            }
        }
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = path
        maskLayer.fillRule = .evenOdd
        
        imageView.layer.mask = maskLayer
    }
    
    func drawFrame(of points: [CGPoint]) -> CGRect {
        
        guard let firstPoint = points.first else {
            return .zero
        }
        
        let rect = points.reduce(CGRect(x: firstPoint.x, y: firstPoint.y, width: firstPoint.x, height: firstPoint.y), { CGRect(x: min($0.origin.x, $1.x), y: min($0.origin.y, $1.y), width: max($0.width, $1.x), height: max($0.height, $1.y)) })
        let frame = CGRect(x: rect.origin.x, y: rect.origin.y, width: rect.width-rect.origin.x, height: rect.height-rect.origin.y)
        return frame
    }
    
    func addPinchGesture() {
        
        let gesture = UIPinchGestureRecognizer(target: self, action: #selector(self.onPinchGesture(_:)))
        gesture.delegate = self
        self.view.addGestureRecognizer(gesture)
    }
    
    func addSliderView() {
        
        self.sliderView?.removeFromSuperview()
        self.sliderControlView?.removeFromSuperview()
        self.sliderView = nil
        self.sliderControlView = nil
        
        let stye = AppUIStyle(traitCollection: traitCollection)
        
        let sliderView: UISlider = {
            let slider = UISlider()
            slider.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi/2))
            slider.setThumbImage(UIImage(color: .appGreen, size: CGSize(width: stye.space*2, height: stye.space*2), borderColor: .white, borderWidth: 1, cornerRadius: stye.space), for: .normal)
            slider.addTarget(self, action: #selector(self.onSliderChaged(_:)), for: .valueChanged)
            let maxImage = UIImage(color: .appLightGray, size: CGSize(width: (stye.space/2)+1, height: stye.space/2), cornerRadius: stye.space/4)?.resizableImage()
            slider.setMaximumTrackImage(maxImage, for: .normal)
            let minImage = UIImage(color: .appGreen, size: CGSize(width: (stye.space/2)+1, height: stye.space/2), cornerRadius: stye.space/4)?.resizableImage()
            slider.setMinimumTrackImage(minImage, for: .normal)
            return slider
        }()
        
        let width = stye.space*3
        let controlView: UIView = {
            let view = UIView.autoLayout()
            view.layer.cornerRadius = width/2
            view.backgroundColor = UIColor(grayscalCode: 255, alpha: 0.6)
            view.addSubview(sliderView)
            
            let infoLabel = UILabel.autoLayout()
            infoLabel.textAlignment = .center
            infoLabel.minimumScaleFactor = 0.5
            infoLabel.adjustsFontSizeToFitWidth = true
            switch stage {
            case .scale: infoLabel.text = LocalizedString("Scale")
            case .tilt: infoLabel.text = LocalizedString("Tilt")
            default: break
            }
            infoLabel.font = AppTextStyle(traitCollection: traitCollection).font(style: .H8)
            infoLabel.textColor = .black
            infoLabel.setContentHuggingPriority(.required, for: .vertical)
            view.addSubview(infoLabel)
            
            [infoLabel.width == view.width - stye.space/2,
             infoLabel.centerX == view.centerX,
             infoLabel.bottom == view.bottom - stye.space,
             view.width == width
                ].activated()
            
            let contentWidth = ceil(self.view.bounds.height/2)
            sliderView.frame = CGRect(x: 0, y: stye.space, width: width, height: contentWidth-(ceil(infoLabel.font.lineHeight)+stye.space*2))
            
            return view
        }()
        
        self.view.addSubview(controlView)
        [controlView.height == self.view.height / 2,
         controlView.centerY == self.view.centerY,
         controlView.right == self.view.right - stye.space].activated()
        
        self.sliderView = sliderView
        self.sliderControlView = controlView
    }
    
    func plotComplete() {
        
        guard let imageURL = self.artboard?.tile?.drawImage else {
            return
        }
        
        AppDelegateObject.displayLoadingView()
        NetworkService.requestImage(imageURL) { (image, cache) in
            
            AppDelegateObject.hideLoadingView()
            if let _ = image {
                
                self.project.completed = true
                self.completePoint = true
                self.overlayView.completed = true
                if let points = self.artboard?.points {
                    self.addFloor(to: points)
                    self.maskImage(to: points)
                }
                self.addPinchGesture()
                self.tapGesture.isEnabled = false
                self.activeButton = nil
                self.saveButton?.isEnabled = true
                self.saveButton?.title = LocalizedString("save")
                
                if let navigationController = self.navigationController as? FTTabbarViewController {
                    self.slideView.isHidden = false
                    let constraint = self.slideView.findConstraint(view: self.slideView, attribute: .height)
                    self.startHeight = navigationController.bottomLayoutGuide.length + 100 + self.slideActionView.bounds.height
                    constraint?.constant = self.startHeight
                    
                    UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 4.0, options: [], animations: {
                        self.view.layoutIfNeeded()
                    }, completion: nil)
                }
            } else {
                
            }
        }
    }
    
    private func taggleShowTileCatalog() {
        let constraint = self.slideView.findConstraint(view: self.slideView, attribute: .height)
        if self.slideView.isHidden == false {
            constraint?.constant = 0
            UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 4.0, options: [], animations: {
                self.view.layoutIfNeeded()
                self.slideView.alpha = 0
            }, completion: { _ in
                self.slideView.isHidden = true
                self.slideView.alpha = 1
            })
        } else {
            self.slideView.isHidden = false
            constraint?.constant = self.startHeight
            UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 4.0, options: [], animations: {
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    private func showTiltSlider(style: TileTiltStyle) {
        self.tileView?.tiltStyle = style;
        artboard?.tiltType = style
        stage = .tilt
        addSliderView()
        
        switch style {
        case .floor:
            sliderView?.minimumValue = 0
            sliderView?.maximumValue = 5
            sliderView?.value = min(artboard?.perspective ?? 0, 5)
            onChangeTilt(value: min(artboard?.perspective ?? 0, 5))
        default:
            sliderView?.minimumValue = 0
            sliderView?.maximumValue = 8
            sliderView?.value = artboard?.perspective ?? 0
            onChangeTilt(value: artboard?.perspective ?? 0)
        }
    }
    
    private func onChangeTilt(value: Float) {
        var transform3D = CATransform3DIdentity
        switch self.artboard?.tiltType {
        case .floor?:
            transform3D.m24 = CGFloat(-value) / 1000
        case .left?:
            transform3D.m14 = CGFloat(value) / 1000
        case .right?:
            transform3D.m14 = CGFloat(-value) / 1000
        default:break
        }
        
        artboard?.perspective = value
        transform3D = CATransform3DRotate(transform3D, CGFloat.pi / 2, 0, 0, 0)
        tileView?.layer.transform = transform3D
    }
    
    private func saveProject() {
        
        if let image = captureImage(), let imageName = AppUtility.saveImageToDocument(image, folder: .screenShot) {
            project.screenshotImageName = imageName
        }
        
        if let controller = self.storyboard?.instantiateViewController(class: FTCatalogDetailViewController.self) {
            controller.style = .saveproject
            controller.data = project
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    private func captureImage() -> UIImage? {
        switch stage {
        case .plot: overlayView.isHidden = true
        default:break
        }
        
        let image = UIGraphicsImageRenderer(size: artboardView.bounds.size, format: UIGraphicsImageRendererFormat()).image {
            // Draw view in that context
            let captureComplete = artboardView.drawHierarchy(in: artboardView.bounds, afterScreenUpdates: true)
            if !captureComplete {
                if let snapView = artboardView.snapshotView(afterScreenUpdates: true) {
                    let complete = snapView.drawHierarchy(in: artboardView.bounds, afterScreenUpdates: true)
                    if !complete {
                        artboardView.layer.render(in: $0.cgContext)
                    }
                }
            }
        }
        
        switch stage {
        case .plot: overlayView.isHidden = false
        default:break
        }
        
        let cropFrame = CGRect(x: 0, y: image.size.height-(floor(imageView.bounds.height)+20), width: floor(imageView.bounds.width), height: floor(imageView.bounds.height))
        DLog("cropFrame : \(cropFrame)")
        
        return image.cropImageInRect(cropFrame)
    }
    
    private func addArtboard() {
        canvasImage = captureImage()
        imageView.image = canvasImage?.scaleImageToWidth(view.bounds.width)
        imageView.layer.mask = nil
        completePoint = false
        overlayView.removeAllPoints()
        tapGesture.isEnabled = true
        self.activeButton = nil
        stage = .plot
        
        buttons.forEach({ $0.removeFromSuperview() })
        buttons.removeAll()
        
        let tile = artboard?.tile
        project.addArtboard();
        artboard?.tile = tile
        floorOverlayView?.removeFromSuperview()
        floorOverlayView = nil
        
        let constraint = self.slideView.findConstraint(view: self.slideView, attribute: .height)
        constraint?.constant = 0
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 4.0, options: [], animations: {
            self.view.layoutIfNeeded()
            self.slideView.alpha = 0
        }, completion: { _ in
            self.slideView.isHidden = true
            self.slideView.alpha = 1
        })
    }
    
    var previousLocation = CGPoint.zero
    var panCountDown = 3
    
    var isPerceptionPan = false
    var perceptionPanCountDown = 3
    var perceptionStartValue: CGFloat = 0
    
    var isLocationPan = false
    var locationStartValue: CGPoint = .zero
    var locationIconStartValue: CGPoint = .zero
    
    @IBAction internal func onPanGesture(_ gesture: UIPanGestureRecognizer) {
        let location = gesture.location(in: view)
        
        if let button = activeButton {
            
            if let index = buttons.firstIndex(where: { $0 == button }) {
                overlayView.changePoint(location, at: index)
                artboard?.changeLocation(location, at: index)
                
                if let points = artboard?.points, completePoint {
                    let frame = drawFrame(of: points)
                    floorOverlayView?.drawFrameChanged(to: frame)
                    tileView?.drawFrameChanged(to: frame)
                    maskImage(to: points)
                    artboard?.frame = frame
                }
            }
            
            let buttonCenter = CGPoint(x: location.x, y: location.y - button.imageSize.height/2)
            button.center = buttonCenter
            switch gesture.state {
            case .began:
                button.isMarked = true
            case .ended,.cancelled:
                button.isMarked = false
                activeButton = nil
            default:break
            }
        } else if let frame = self.artboard?.frame {
            
            let containLocation = frame.contains(location)
            switch gesture.state {
            case .began where containLocation:
                previousLocation = location
                perceptionStartValue = CGFloat(self.artboard?.perspective ?? 0)
                locationStartValue = tileView?.location ?? .zero
                locationIconStartValue = .zero
                panCountDown = 3
                perceptionPanCountDown = 3
                if stage == .location {
                    moveIndicatorImageView.center = CGPoint(x: frame.midX, y: frame.midY)
                }
            case .changed where containLocation:
                let perception = abs(location.y-previousLocation.y) > abs(location.x-previousLocation.x)
                if stage == .location {
                    panCountDown = 0
                    isPerceptionPan = false
                    isLocationPan = true
                }
                if panCountDown > 0 {
                    panCountDown -= 1
                    if perception {
                        perceptionPanCountDown -= 1
                    }
                    if panCountDown == 0 {
                        if perceptionPanCountDown < 1 {
                            isPerceptionPan = true
                            isLocationPan = false
                            DLog("Perception")
                        } else {
                            isPerceptionPan = false
                            isLocationPan = true
                            DLog("LocationPan")
                        }
                        previousLocation = location
                    }
                } else if isPerceptionPan {
                    let changePerceptionValue = perceptionStartValue - ((previousLocation.y - location.y)/10)
                    let value = min(0, max(-5, changePerceptionValue))
                    
                    //                    DLog("Perception Value : \(value)")
                    var transform3D = CATransform3DIdentity
                    transform3D.m24 = value / 1000
                    transform3D = CATransform3DRotate(transform3D, CGFloat.pi / 2, 0, 0, 0)
                    tileView?.layer.transform = transform3D
                    self.artboard?.perspective = Float(value)
                } else if isLocationPan {
                    let x = locationStartValue.x - (previousLocation.x - location.x)
                    let y = locationStartValue.y - (previousLocation.y - location.y)
                    let changeLocationValue = CGPoint(x: x, y: y)
                    //                    DLog("changeLocationValue : \(changeLocationValue)")
                    tileView?.location = changeLocationValue
                    self.artboard?.location = changeLocationValue
                    
                    let iconX = locationIconStartValue.x - (previousLocation.x - location.x)
                    let iconY = locationIconStartValue.y - (previousLocation.y - location.y)
                    moveIndicatorImageView.center = CGPoint(x: frame.midX + iconX, y: frame.midY + iconY)
                }
            case .ended, .cancelled, .failed:
                if isLocationPan {
                    moveIndicatorImageView.center = CGPoint(x: frame.midX, y: frame.midY)
                }
            default:break
            }
        }
    }
    
    @IBAction internal func onTapGesture(_ gesture: UITapGestureRecognizer) {
        let location = gesture.location(in: view)
        
        DLog("Add button")
        let startButton = buttons.isEmpty
        let button = MaskButton.button(start: startButton, location: location)
        view.insertSubview(button, belowSubview: slideView)
        
        buttons.append(button)
        overlayView.addPoint(location)
        
        if let _ = self.project {
            artboard?.addPoint(location)
        } else {
            let project = TileProject()
            project.artboard.addPoint(location)
            self.project = project
        }
    }
    
    @IBAction internal func onSliderChaged(_ slider: UISlider) {
        
        switch stage {
        case .scale:
            tileView?.scale = CGFloat(slider.value)
            self.artboard?.scale = slider.value
        case .tilt:
            onChangeTilt(value: slider.value)
        default:break
        }
    }
    
    @IBAction internal func onSaveProject(_ button: UIButton) {
        
        if saveButton?.title == LocalizedString("share") {
            
            if let controller = self.storyboard?.instantiateViewController(class: FTCatalogDetailViewController.self) {
                controller.style = .saveproject
                controller.data = project
                self.navigationController?.pushViewController(controller, animated: true)
            }
            return
        }
        
        let pickerController = FTTileActionViewController(items: [FTTileAction(title: LocalizedString("SaveProjectOptionAddArtboard"), handler: { _ in
            self.addArtboard()
        }),FTTileAction(title: LocalizedString("SaveProjectOptionSave"), handler: { _ in
            self.saveProject()
        }),FTTileAction(title: LocalizedString("cancel"))])
        pickerController.title = LocalizedString("SaveProjectAlartTitle")
        pickerController.popoverPresentationController?.sourceView = button
        pickerController.popoverPresentationController?.sourceRect = button.bounds
        pickerController.popoverPresentationController?.permittedArrowDirections = [.up, .down]
        self.present(pickerController, animated: true, completion: nil)
    }
    
    @IBAction internal func onPlot(_ button: UIButton) {
        
        if stage == .plot && completePoint {
            taggleShowTileCatalog()
        }
        
        stage = .plot
    }
    
    @IBAction internal func onScale(_ button: UIButton) {
        
        guard completePoint else {
            return
        }
        
        if stage == .scale {
            taggleShowTileCatalog()
            return
        }
        
        stage = .scale
        addSliderView()
        sliderView?.minimumValue = 0.5
        sliderView?.maximumValue = 10
        sliderView?.value = artboard?.scale ?? 1
    }
    
    @IBAction internal func onTilt(_ button: UIButton) {
        
        guard completePoint else {
            return
        }
        
        let pickerController = FTTileActionViewController(items: [FTTileAction(title: LocalizedString("TiltLeft"), image: #imageLiteral(resourceName: "icon_tilt_left"), handler: { _ in
            self.showTiltSlider(style: .left)
            self.catalogListController.planType = nil
        }),FTTileAction(title: LocalizedString("TiltRight"), image: #imageLiteral(resourceName: "icon_tilt_right"), handler: { _ in
            self.showTiltSlider(style: .right)
            self.catalogListController.planType = nil
        }),FTTileAction(title: LocalizedString("TiltFloor"), image: #imageLiteral(resourceName: "icon_tilt_floor"), handler: { _ in
            self.showTiltSlider(style: .floor)
            self.catalogListController.planType = .floor
            if let tile = self.catalogListController.selectedTile, tile.planType == .wall {
                self.catalogListController.changeToFirstTile()
                let alert = UIAlertController(title: nil, message: LocalizedString("AlartWallPlanFloorMessage"), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: LocalizedString("ok"), style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else if self.catalogListController.selectedTile == nil {
                self.catalogListController.changeToFirstTile()
            }
        })])
        pickerController.title = LocalizedString("AlartTiltMessage")
        pickerController.popoverPresentationController?.sourceView = button
        pickerController.popoverPresentationController?.sourceRect = button.bounds
        pickerController.popoverPresentationController?.permittedArrowDirections = [.up, .down]
        self.present(pickerController, animated: true, completion: nil)
    }
    
    @IBAction internal func onLocation(_ button: UIButton) {
        
        guard completePoint else {
            return
        }
        
        if stage == .location {
            taggleShowTileCatalog()
            return
        }
        
        stage = .location
        
        view.insertSubview(moveIndicatorImageView, belowSubview: slideView)
        if let frame = self.artboard?.frame {
            moveIndicatorImageView.center = CGPoint(x: frame.midX, y: frame.midY)
        }
    }
    
    @IBAction internal func onCancelProject(_ button: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    var previousScale: CGFloat = 1.0
    @objc internal func onPinchGesture(_ gesture: UIPinchGestureRecognizer) {
        
        guard let tileView = tileView else {
            return
        }
        
        switch gesture.state {
        case .began:
            previousScale = 1
        case .changed:
            let scale = tileView.scale
            let changeScale = scale - (previousScale - gesture.scale)
            tileView.scale = changeScale
            previousScale = gesture.scale
            self.artboard?.scale = Float(changeScale)
        default:break
        }
    }
    
    enum SlideStyle: Int {
        case full
        case haft
        case less
    }
    
    var startHeight: CGFloat = 0
    var stageHeight: CGFloat = 0
    var slideStyle: SlideStyle = .haft
    
    @IBAction func onPanSlideView(_ gesture: UIPanGestureRecognizer) {
        let gestureView = gesture.view!
        let progess = gesture.translation(in: gestureView).y
        
        switch gesture.state {
        case .began:
            let constraint = slideView.findConstraint(view: slideView, attribute: .height)
            self.stageHeight = constraint?.constant ?? 0
        case .changed:
            let constraint = slideView.findConstraint(view: slideView, attribute: .height)
            constraint?.constant = stageHeight - progess
            self.view.layoutIfNeeded()
        case .ended, .cancelled:
            
            gesture.isEnabled = false
            let velocity = gesture.velocity(in: gestureView).y
            let height = stageHeight - progess
            let fullHeight = self.view.bounds.height - startHeight
            let nextStyle: SlideStyle = {
                
                switch slideStyle {
                case .full:
                    if height >= fullHeight {
                        return .full
                    } else if height <= startHeight || velocity > 500 {
                        return .less
                    } else {
                        return .haft
                    }
                case .haft:
                    if (height >= fullHeight/2 && progess < 0) || velocity < -100 {
                        return .full
                    } else if (height <= startHeight && progess > 0) || velocity > 100 {
                        return .less
                    } else {
                        return .haft
                    }
                default:
                    if height >= fullHeight/2 || velocity < -500 {
                        return .full
                    } else if progess > 0 || velocity > 100 {
                        return .less
                    } else {
                        return .haft
                    }
                }
            }()
            
            let distanceToAnimate: CGFloat = {
                switch nextStyle {
                case .full: return abs(height - fullHeight)
                default: return abs(stageHeight - progess)
                }
            }()
            let constraint = slideView.findConstraint(view: slideView, attribute: .height)
            switch nextStyle {
            case .full: constraint?.constant = fullHeight
            case .haft: constraint?.constant = startHeight
            default: constraint?.constant = startHeight - 75
            }
            
            let springVelocity = abs(velocity / distanceToAnimate)
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: springVelocity, options: [.beginFromCurrentState, .curveLinear], animations: {
                self.view.layoutIfNeeded()
            }, completion: { _ in
                self.slideStyle = nextStyle
                gesture.isEnabled = true
            })
            
        default:break
        }
    }
    
    @IBAction internal func onFilterTile(_ button: UIButton) {
        
        self.catalogListController.showFilterTile(in: self, sourceView: button, sourceRect: button.convert(button.bounds, to: self.view))
    }
}

extension FTDrawViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        DLog("gestureRecognizerShouldBegin : \(gestureRecognizer.className())")
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        DLog("shouldReceive : \(gestureRecognizer.className()) in \(touch.view?.className() ?? "")")
        
        let point = touch.location(in: self.view)
        if let _ = touch.view as? LineOverlayView, gestureRecognizer is UITapGestureRecognizer {
            if touch.view?.isKind(of: UIControl.self) == true {
                return false
            }
            return true
        }
        
        if catalogPanGasture == gestureRecognizer {
            return true
        }
        
        let frame = self.artboard?.frame ?? .zero
        if gestureRecognizer is UIPanGestureRecognizer, let button = touch.view as? MaskButton {
            button.isMarked = true
            activeButton = button
            Async.main(after: 1) {
                if gestureRecognizer.state == .possible {
                    button.isMarked = false
                    if button.startButton && self.floorOverlayView == nil && (self.artboard?.points.count ?? 0) > 3 {
                        self.plotComplete()
                    }
                }
            }
            return true
        }
        if gestureRecognizer is UIPanGestureRecognizer, frame.contains(point), stage == .location {
            return true
        }
        return false
    }
}

extension FTDrawViewController: FTCatalogListViewControllerDelegate {
    
    func catalogList(_ catalogList: FTCatalogListViewController, didSelectedTile tile: Tile) {
        
        guard let floorView = floorOverlayView, let project = self.project, let artboard = self.artboard else {
            return
        }
        
        if slideStyle == .full {
            let nextStyle: SlideStyle = .less
            let constraint = slideView.findConstraint(view: slideView, attribute: .height)
            constraint?.constant = startHeight - 75
            
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.2, options: [.beginFromCurrentState, .curveLinear], animations: {
                self.view.layoutIfNeeded()
            }, completion: { _ in
                self.slideStyle = nextStyle
            })
        }
        
        func changeTileImage(_ image: UIImage) {
            let tileView = TileView(frame: view.bounds, tileImage: image)
            tileView.scale = CGFloat(artboard.scale)
            tileView.location = artboard.location
            tileView.tiltStyle = artboard.tiltType
            
            var transform3D = CATransform3DIdentity
            
            switch artboard.tiltType {
            case .floor:
                transform3D.m24 = CGFloat(-artboard.perspective) / 1000
            case .left:
                transform3D.m14 = CGFloat(artboard.perspective) / 1000
            case .right:
                transform3D.m14 = CGFloat(-artboard.perspective) / 1000
            }
            transform3D = CATransform3DRotate(transform3D, .pi / 2, 0, 0, 0)
            tileView.layer.transform = transform3D
            
            tileView.drawFrameChanged(to: artboard.frame)
            
            floorView.subviews.first?.removeFromSuperview()
            floorView.addSubview(tileView)
            self.tileView = tileView
            self.artboard?.tile = tile
        }
        
        if let image = NetworkService.cacheImage(tile.drawImage) {
            changeTileImage(image)
        } else {
            AppDelegateObject.displayLoadingView()
            NetworkService.requestImage(tile.drawImage) { (image, cache) in
                AppDelegateObject.hideLoadingView()
                if let image = image {
                    changeTileImage(image)
                }
            }
        }
        
        if tile.planType == .wall && !didShowWallAlert {
            didShowWallAlert = true
            let alert = UIAlertController(title: nil, message: LocalizedString("AlartWallMessage"), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: LocalizedString("ok"), style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
