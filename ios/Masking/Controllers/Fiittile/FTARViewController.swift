//
//  FTARViewController.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 12/2/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import Foundation
import ARKit
import SceneKit
import DGTEngineSwift

@available(iOS 11.0, *)
class Plane: SCNNode {
    
    let anchor: ARPlaneAnchor
    var planeGeometry: SCNPlane?
    
    required init(tile: Tile, anchor: ARPlaneAnchor) {
        self.anchor = anchor
        super.init()
        
        let width = CGFloat(anchor.extent.x)
        let height = CGFloat(anchor.extent.z)

        self.planeGeometry = SCNPlane(width: width, height: height)
        
        let material = SCNMaterial()
        material.diffuse.contents = Plane.processTile(tile)
        material.diffuse.wrapS = .repeat
        material.diffuse.wrapT = .repeat
        self.planeGeometry?.materials = [material]
        
        let planeNode = SCNNode(geometry: planeGeometry)
        planeNode.position = SCNVector3(anchor.center.x, -0.005, anchor.center.z)
        planeNode.transform = SCNMatrix4MakeRotation(-.pi/2, 1, 0, 0)
        
        self.setTextureScale()
        self.addChildNode(planeNode)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(anchor: ARPlaneAnchor) {
        self.planeGeometry?.width = CGFloat(anchor.extent.x)
        self.planeGeometry?.height = CGFloat(anchor.extent.z)
        
        self.position = SCNVector3(anchor.center.x, -0.005, anchor.center.z)
        self.setTextureScale()
    }
    
    func setTextureScale() {
        
        guard let width = self.planeGeometry?.width, let height = self.planeGeometry?.height else {
            return
        }
        
        let material = self.planeGeometry?.materials.first
        material?.diffuse.contentsTransform = SCNMatrix4MakeScale(Float(width), Float(height), 1)
    }
    
    class func processTile(_ tile: Tile) -> UIImage? {
        guard let drawImage = NetworkService.cacheImage(tile.drawImage) else {
                return nil
        }
        return processTileImage(drawImage)
    }
    
    class func processTileImage(_ image: UIImage, lineColor: UIColor = .white) -> UIImage? {
        
        guard let tileImage = image.scaleImageToWidth(image.size.width/4) else {
                return nil
        }
        
        let lineWidth = ceil((tileImage.size.width * 0.2) / 30)
        let drawSize = CGSize(width: tileImage.size.width + lineWidth, height: tileImage.size.height + lineWidth)
        
        let borderImage: UIImage = {
            
            UIGraphicsBeginImageContextWithOptions(drawSize, true, 0.0)
            
            let context = UIGraphicsGetCurrentContext()
            lineColor.setFill()
            context?.fill(CGRect(origin: .zero, size: drawSize))
            tileImage.draw(at: CGPoint(x: lineWidth/2, y: lineWidth/2))
            
            let resultImage = UIGraphicsGetImageFromCurrentImageContext() ?? tileImage
            UIGraphicsEndImageContext()
            return resultImage
        }()
        
        return {
            let size = CGSize(width: drawSize.width*5, height: drawSize.width*5)
            UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
            
            let context = UIGraphicsGetCurrentContext()
            UIColor(patternImage: borderImage).setFill()
            context?.fill(CGRect(origin: .zero, size: size))
            
            let resultImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return resultImage
        }()
    }
}

@available(iOS 11.0, *)
class FTARViewController: UIViewController {
    
    class var isSupportAR: Bool {
        return ARWorldTrackingConfiguration.isSupported
    }
    
    lazy var sceneView: ARSCNView = {
        let view = ARSCNView.autoLayout()
        view.delegate = self
        view.autoenablesDefaultLighting = true
        view.scene = SCNScene()
        view.session.delegate = self
        view.debugOptions = []
        view.antialiasingMode = .multisampling4X
        return view
    }()
    
    lazy var saveButton: UIButton = {
        let button = SaveAppButton.button()
        button.prepareForAutoLayout()
        button.title = LocalizedString("save")
        button.addTarget(self, action: #selector(self.onSaveProject), for: .touchUpInside)
        return button
    }()
    
    lazy var slideView: UIView = {
        let view = UIView.autoLayout()
        view.frame = CGRect(x: 0, y: self.view.bounds.height, width: self.view.bounds.width, height: 100)
        view.backgroundColor = UIColor(grayscalCode: 0, alpha: 0.7)
        [view.height == 0].activated()
        
        let indicatorImageView = UIImageView(image: UIImage(color: .appLightGray, size: CGSize(width: 70, height: 6), cornerRadius: 3))
        indicatorImageView.contentMode = .center
        indicatorImageView.prepareForAutoLayout()
        indicatorImageView.isUserInteractionEnabled = true
        
        let space = AppUIStyle().space
        indicatorImageView.layoutMargins = UIEdgeInsets(top: space/4, left: space, bottom: space/4, right: space)
        view.addSubview(indicatorImageView)
        [view.top == indicatorImageView.top,
         view.left == indicatorImageView.left,
         view.right == indicatorImageView.right].activated()
        
        let filterButton = UIButton(type: .system)
        filterButton.prepareForAutoLayout()
        filterButton.tintColor = .appGreen
        filterButton.titleFont = UIFont.customFont(20, weight: .dgtRegular)
        filterButton.title = LocalizedString("CatalogFilterTitle")
        filterButton.addTarget(self, action: #selector(self.onFilterTile(_:)), for: .touchUpInside)
        filterButton.setContentHuggingPriority(.required, for: .vertical)
        indicatorImageView.addSubview(filterButton)
        [indicatorImageView.rightMargin == filterButton.right,
        indicatorImageView.topMargin == filterButton.top,
        indicatorImageView.bottomMargin == filterButton.bottom].activated()
        
        let controller = FTCatalogListViewController(style: .mini)
        controller.planType = .floor
        controller.showQRCode = false
        controller.delegate = self
        controller.view.prepareForAutoLayout()
        view.addSubview(controller.view)
        [controller.view.left == view.left,
         controller.view.right == view.right,
         controller.view.bottom == view.bottom,
         controller.view.top == indicatorImageView.bottom].activated()
        
        addChild(controller)
        
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(self.onPanSlideView(_:)))
        indicatorImageView.addGestureRecognizer(gesture)
        
        return view
    }()
    
    var project: TileProject!
    var planes = [String: Plane]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(sceneView)
        sceneView.constraintsAroundView()?.activated()
        
        view.addSubview(saveButton)
        let space = AppUIStyle(traitCollection: traitCollection).space
        [saveButton.right == view.rightMargin,
         saveButton.top == view.topMargin + space].activated()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let configulation = ARWorldTrackingConfiguration()
        configulation.planeDetection = .horizontal
        self.sceneView.session.run(configulation)
        AppDelegateObject.hideLoadingView()
        
        if let navigationController = self.navigationController as? FTTabbarViewController {
            
            view.insertSubview(slideView, belowSubview: saveButton)
            [slideView.left == view.left,
             slideView.right == view.right,
             slideView.bottom == view.bottom].activated()
            
            let constraint = slideView.findConstraint(view: slideView, attribute: .height)
            self.startHeight = navigationController.bottomLayoutGuide.length + 120
            constraint?.constant = self.startHeight - 75
            
            UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 4.0, options: [], animations: {
                self.view.layoutIfNeeded()
            }, completion: nil)
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
    }

    override func removeFromParent() {
        super.removeFromParent()
        sceneView.session.pause()
        sceneView.removeFromSuperview()
    }
    
    deinit {
        sceneView.session.pause()
        sceneView.removeFromSuperview()
    }
    
    private func restartSession() {
        
    }
    
    @objc internal func onSaveProject() {
        let image = self.sceneView.snapshot()
        if let file = AppUtility.saveImageToDocument(image, folder: .screenShot) {
            project?.screenshotImageName = file
            project?.completed = true
        }
        
        if let controller = self.navigationController?.storyboard?.instantiateViewController(class: FTCatalogDetailViewController.self) {
            controller.style = .saveproject
            controller.data = project
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    enum SlideStyle: Int {
        case full
        case haft
        case less
    }
    
    var startHeight: CGFloat = 0
    var stageHeight: CGFloat = 0
    var slideStyle: SlideStyle = .less
    
    @objc func onPanSlideView(_ gesture: UIPanGestureRecognizer) {
        
        let gestureView = gesture.view!
        let progess = gesture.translation(in: gestureView).y
        
        switch gesture.state {
        case .began:
            let constraint = slideView.findConstraint(view: slideView, attribute: .height)
            self.stageHeight = constraint?.constant ?? 0
        case .changed:
            let constraint = slideView.findConstraint(view: slideView, attribute: .height)
            constraint?.constant = stageHeight - progess
            self.view.layoutIfNeeded()
        case .ended, .cancelled:
            
            gesture.isEnabled = false
            let velocity = gesture.velocity(in: gestureView).y
            let height = stageHeight - progess
            let fullHeight = self.view.bounds.height - startHeight
            let nextStyle: SlideStyle = {
                
                switch slideStyle {
                case .full:
                    if height >= fullHeight {
                        return .full
                    } else if height <= startHeight || velocity > 500 {
                        return .less
                    } else {
                        return .haft
                    }
                case .haft:
                    if (height >= fullHeight/2 && progess < 0) || velocity < -100 {
                        return .full
                    } else if (height <= startHeight && progess > 0) || velocity > 100 {
                        return .less
                    } else {
                        return .haft
                    }
                default:
                    if height >= fullHeight/2 || velocity < -500 {
                        return .full
                    } else if progess > 0 || velocity > 100 {
                        return .less
                    } else {
                        return .haft
                    }
                }
            }()
            
            let distanceToAnimate: CGFloat = {
                switch nextStyle {
                case .full: return abs(height - fullHeight)
                default: return abs(stageHeight - progess)
                }
            }()
            let constraint = slideView.findConstraint(view: slideView, attribute: .height)
            switch nextStyle {
            case .full: constraint?.constant = fullHeight
            case .haft: constraint?.constant = startHeight
            default: constraint?.constant = startHeight - 75
            }
            
            let springVelocity = abs(velocity / distanceToAnimate)
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: springVelocity, options: [.beginFromCurrentState, .curveLinear], animations: {
                self.view.layoutIfNeeded()
            }, completion: { _ in
                self.slideStyle = nextStyle
                gesture.isEnabled = true
            })
            
        default:break
        }
    }
    
    @objc internal func onFilterTile(_ button: UIButton) {
        
        let catalogListController = children.first(where: { $0 is FTCatalogListViewController }) as? FTCatalogListViewController
        catalogListController?.showFilterTile(in: self, sourceView: button, sourceRect: button.convert(button.bounds, to: self.view))
    }
}

@available(iOS 11.0, *)
extension FTARViewController: ARSCNViewDelegate {
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        
        guard let planeAnchor = anchor as? ARPlaneAnchor, let tile = project?.artboard.tile else {
            return
        }
        
        let plane = Plane(tile: tile, anchor: planeAnchor)
        planes[planeAnchor.identifier.uuidString] = plane
        node.addChildNode(plane)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        
        guard let planeAnchor = anchor as? ARPlaneAnchor, let plane = planes[anchor.identifier.uuidString] else {
            return
        }
        
        plane.update(anchor: planeAnchor)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        planes.removeValue(forKey: anchor.identifier.uuidString)
    }
}

@available(iOS 11.0, *)
extension FTARViewController: FTCatalogListViewControllerDelegate {
    
    func catalogList(_ catalogList: FTCatalogListViewController, didSelectedTile tile: Tile) {
        
        if slideStyle == .full {
            let nextStyle: SlideStyle = .less
            let constraint = slideView.findConstraint(view: slideView, attribute: .height)
            constraint?.constant = startHeight - 75
            
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.2, options: [.beginFromCurrentState, .curveLinear], animations: {
                self.view.layoutIfNeeded()
            }, completion: { _ in
                self.slideStyle = nextStyle
            })
        }
        
        func changePlaneTileToImage(_ image: UIImage, lineColor: UIColor = .white) {
            
            self.project?.artboard.tile = tile.copy()
            
            let material = SCNMaterial()
            material.diffuse.contents = Plane.processTileImage(image, lineColor: lineColor)
            material.diffuse.wrapS = .repeat
            material.diffuse.wrapT = .repeat
            self.planes.forEach({
                if let oldMaterial = $0.value.planeGeometry?.materials.first {
                    material.diffuse.contentsTransform = oldMaterial.diffuse.contentsTransform
                }
                $0.value.planeGeometry?.materials = [material]
            })
        }
        
        if let drawImage = NetworkService.cacheImage(tile.drawImage) {
            changePlaneTileToImage(drawImage)
        } else {
            AppDelegateObject.displayLoadingView()
            NetworkService.requestImage(tile.drawImage, completion: { (image, cache) in
                AppDelegateObject.hideLoadingView()
                if let image = image {
                    changePlaneTileToImage(image)
                }
            })
        }
    }
}

@available(iOS 11.0, *)
extension FTARViewController: ARSessionDelegate {
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        
        let configulation = ARWorldTrackingConfiguration()
        configulation.planeDetection = .horizontal
        if let error = error as? NSError {
            switch error.code {
            case 102:
                configulation.worldAlignment = .gravity
            default:
                configulation.worldAlignment = .gravityAndHeading
            }
        }
        self.sceneView.session.pause()
        self.sceneView.session.run(configulation, options: [.resetTracking, .removeExistingAnchors])
    }
}
