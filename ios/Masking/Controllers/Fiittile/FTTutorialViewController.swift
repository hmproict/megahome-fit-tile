//
//  FTTutorialViewController.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 19/3/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import Foundation
import DGTEngineSwift

class FTTutorialViewController: FTBaseViewController {
    
    private var tutorialView: UIView?
    private var arrowView: UIView?
    private var currentStep: Step = .plot
    private let stepLocation: [Step: CGRect]
    
    enum Step: Int {
        case plot
        case tilt
        case scale
        case move
        
        var title: String {
            switch self {
            case .plot: return LocalizedString("TutorialPlottTitle")
            case .scale: return LocalizedString("TutorialScaleTitle")
            case .tilt: return LocalizedString("TutorialTiltTitle")
            case .move: return LocalizedString("TutorialLocationTitle")
            }
        }
        
        var detail: String {
            switch self {
            case .plot: return LocalizedString("TutorialPlottDetail")
            case .scale: return LocalizedString("TutorialScaleDetail")
            case .tilt: return LocalizedString("TutorialTiltDetail")
            case .move: return LocalizedString("TutorialLocationDetail")
            }
        }
        
        var image: UIImage? {
            switch self {
            case .plot: return UIImage.gifImageWithName("Tool-01")
            case .scale: return UIImage.gifImageWithName("Tool-02")
            case .tilt: return UIImage.gifImageWithName("Tool-03")
            case .move: return UIImage.gifImageWithName("Tool-04")
            }
        }
        
        mutating func next() {
            switch self {
            case .plot: self = .tilt
            case .scale: self = .move
            case .tilt: self = .scale
            case .move: self = .move
            }
        }
        
        mutating func prev() {
            switch self {
            case .plot: self = .plot
            case .scale: self = .tilt
            case .tilt: self = .plot
            case .move: self = .scale
            }
        }
    }
    
    class NextButton: UIButton {
        
        class func button() -> Self {
            let button = self.init(type: .system)
            button.tintColor = .appLightGray
            button.setTitleColor(.appLightGray, for: .normal)
            button.titleLabel?.font = AppTextStyle().font(style: .H4)
            button.image = #imageLiteral(resourceName: "icon_next")
            button.title = LocalizedString("TutorialNextTitle")
            return button
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            titleLabel?.frame.origin.x = 0
            
            let width = image?.size.width ?? 0
            imageView?.frame.origin.x = bounds.width - width
        }
        
        override var intrinsicContentSize: CGSize {
            let size = super.intrinsicContentSize
            return CGSize(width: size.width+5, height: size.height)
        }
    }
    
    class PrevButton: UIButton {
        
        class func button() -> Self {
            let button = self.init(type: .system)
            button.tintColor = .appLightGray
            button.setTitleColor(.appLightGray, for: .normal)
            button.titleLabel?.font = AppTextStyle().font(style: .H4)
            button.image = UIImage(cgImage: #imageLiteral(resourceName: "icon_next").cgImage!, scale: #imageLiteral(resourceName: "icon_next").scale, orientation: .down)
            button.title = LocalizedString("TutorialPreviousTitle")
            return button
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            titleLabel?.frame.origin.x += 5
        }
        
        override var intrinsicContentSize: CGSize {
            let size = super.intrinsicContentSize
            return CGSize(width: size.width+5, height: size.height)
        }
    }

    
    let nextButton: UIButton = {
        let button = NextButton.button()
        button.prepareForAutoLayout()
        button.setContentHuggingPriority(.required, for: .vertical)
        button.setContentCompressionResistancePriority(.required, for: .vertical)
        return button
    }()
    
    let prevButton: UIButton = {
        let button = PrevButton.button()
        button.prepareForAutoLayout()
        button.setContentHuggingPriority(.required, for: .vertical)
        button.setContentCompressionResistancePriority(.required, for: .vertical)
        return button
    }()
    
    required init(step: [Step: CGRect]) {
        self.stepLocation = step
        super.init(nibName: nil, bundle: nil)
        modalTransitionStyle = .crossDissolve
        modalPresentationStyle = .custom
        self.transitioningDelegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        
        nextButton.addTarget(self, action: #selector(self.onNextStep), for: .touchDown)
        prevButton.addTarget(self, action: #selector(self.onPrevStep), for: .touchDown)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        showTutorialView(step: currentStep)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        

    }
    
    override func updateUI(for traitCollection: UITraitCollection) {
        let style = AppUIStyle(traitCollection: traitCollection)
        tutorialView?.layoutMargins = style.margin
    }
    
    override func updateFont(for traitCollection: UITraitCollection) {
        let style = AppTextStyle(traitCollection: traitCollection)
        nextButton.titleLabel?.font = style.font(style: .H4)
        prevButton.titleLabel?.font = style.font(style: .H4)
    }
    
    func showTutorialView(step: Step) {
        
        let frame = view.bounds
        
        let arrowImage: UIImage? = {
            UIGraphicsBeginImageContextWithOptions(CGSize(width: 40, height: 20), false, 0.0)
            let context = UIGraphicsGetCurrentContext()
            UIColor(r: 2, g: 138, b: 131).setFill()
            context?.move(to: CGPoint(x: 0, y: 20))
            context?.addLine(to: CGPoint(x: 20, y: 0))
            context?.addLine(to: CGPoint(x: 40, y: 20))
            context?.closePath()
            context?.fillPath()
            
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return image
        }()
        let arrowImageView = UIImageView(image: arrowImage)
        arrowImageView.setContentHuggingPriority(.required, for: .vertical)
        arrowImageView.setContentCompressionResistancePriority(.required, for: .vertical)
        arrowImageView.prepareForAutoLayout()
        
        let style = AppUIStyle(traitCollection: traitCollection)
        let bgImage = UIImage(color: .white, size: frame.insetBy(dx: style.space, dy: style.space).size, cornerRadius: style.cornerRedius)?.tinted([UIColor(r: 2, g: 138, b: 131), UIColor(r: 0, g: 88, b: 84)])
        let bgView = UIImageView(image: bgImage)
        bgView.prepareForAutoLayout()
        bgView.isUserInteractionEnabled = true
        bgView.layoutMargins = style.viewMargin
        
        let titleLabel: UILabel = {
            let label = UILabel.autoLayout()
            label.text = step.title
            label.textColor = .white
            label.font = AppTextStyle(traitCollection: traitCollection).font(style: .H0)
            label.textAlignment = .center
            label.setContentHuggingPriority(.required, for: .vertical)
            label.setContentCompressionResistancePriority(.required, for: .vertical)
            return label
        }()
        bgView.addSubview(titleLabel)
        
        let detailLabel: UILabel = {
            let label = UILabel.autoLayout()
            label.text = step.detail
            label.font = AppTextStyle(traitCollection: traitCollection).font(style: .H4)
            label.textAlignment = .center
            label.textColor = .white
            label.numberOfLines = 0
            label.setContentHuggingPriority(.required, for: .vertical)
            label.setContentCompressionResistancePriority(.required, for: .vertical)
            return label
        }()
        bgView.addSubview(detailLabel)
        
        let imageView = UIImageView(image: step.image)
        imageView.prepareForAutoLayout()
        imageView.layer.cornerRadius = style.cornerRedius
        imageView.layer.masksToBounds = true
        imageView.setContentHuggingPriority(.defaultLow, for: .vertical)
        
        let shadowImageView = UIImageView(image: {
            
            let rect = CGRect(x: 0, y: 0, width: 100, height: 100)
            let shadowView = UIView(frame: rect)
            shadowView.layer.shadowColor = UIColor.black.cgColor
            shadowView.layer.shadowOffset = .zero
            shadowView.layer.shadowRadius = 4.0
            shadowView.layer.shadowOpacity = 0.5
            shadowView.layer.masksToBounds = false
            
            let style = AppUIStyle(traitCollection: traitCollection)
            let cornerRadius = style.cornerRedius
            shadowView.layer.shadowPath = UIBezierPath(roundedRect: rect.insetBy(dx: 5, dy: 3), cornerRadius: cornerRadius).cgPath
            shadowView.setNeedsDisplay()
            
            let image = shadowView.takeSnapshot()?.resizableImage(withCapInsets: UIEdgeInsets(redius: 30))
            return image
        }())
        shadowImageView.prepareForAutoLayout()
        shadowImageView.clipsToBounds = false
        
        let contentView = UIView.autoLayout()
        contentView.backgroundColor = .clear
        contentView.clipsToBounds = false
        contentView.setContentHuggingPriority(.defaultLow, for: .vertical)
        
        contentView.addSubview(shadowImageView)
        contentView.addSubview(imageView)
        bgView.addSubview(contentView)
        
        nextButton.title = LocalizedString("TutorialNextTitle")
        nextButton.removeTarget(nil, action: nil, for: .touchDown)
        nextButton.addTarget(self, action: #selector(self.onNextStep), for: .touchDown)
        nextButton.removeFromSuperview()
        prevButton.removeFromSuperview()
        
        switch step {
        case .plot:
            bgView.addSubview(nextButton)
        case .move:
            nextButton.title = LocalizedString("TutorialDoneTitle")
            nextButton.removeTarget(nil, action: nil, for: .touchDown)
            nextButton.addTarget(self, action: #selector(self.onDone), for: .touchDown)
            fallthrough
        default:
            bgView.addSubview(nextButton)
            bgView.addSubview(prevButton)
        }
        
        self.view.addSubview(bgView)
        self.view.addSubview(arrowImageView)
        
        let margin = style.margin
        let location = stepLocation[step]!
        [arrowImageView.top == view.top + (location.maxY + margin.top),
         arrowImageView.centerX == view.left + location.midX,
         arrowImageView.bottom == bgView.top].activated()
        
        [bgView.left == view.left + margin.left,
         bgView.right == view.right - margin.right,
         bgView.bottom <= self.bottomLayoutGuideTop - style.space].activated()
        
        [titleLabel.top == bgView.topMargin,
         titleLabel.centerX == bgView.centerX,
         titleLabel.bottom == detailLabel.top].activated()
        
        imageView.constraintsAroundView(spaceInset: UIEdgeInsets(top: 0, left: 5, bottom: 5, right: 5))?.activated()
        shadowImageView.constraintsAroundView()?.activated()
        
        [detailLabel.left == bgView.leftMargin,
         detailLabel.right == bgView.rightMargin,
         detailLabel.bottom == contentView.top - style.space,
         contentView.left == bgView.leftMargin ~ LayoutPriority.defaultHigh,
         contentView.centerX == bgView.centerX,
         contentView.right <= bgView.rightMargin,
         contentView.height == contentView.width].activated()
        
        if let _ = nextButton.superview {
            [nextButton.right == bgView.rightMargin,
             nextButton.top == contentView.bottom + style.space,
             nextButton.bottom == bgView.bottomMargin].activated()
        }
        
        if let _ = prevButton.superview {
            [prevButton.left == bgView.leftMargin,
             prevButton.top == contentView.bottom + style.space,
             prevButton.bottom == bgView.bottomMargin].activated()
        }
        
        self.tutorialView = bgView
        self.arrowView = arrowImageView
    }
    
    @objc internal func onNextStep() {
        
        currentStep.next()
        let oldTutorialView = self.tutorialView
        let oldArrowView = self.arrowView
        showTutorialView(step: currentStep)
        
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = .push
        transition.subtype = .fromLeft
        transition.isRemovedOnCompletion = true
        transition.fillMode = .both
        transition.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        self.view.layer.add(transition, forKey: kCATransition)
        
        oldTutorialView?.removeFromSuperview()
        oldArrowView?.removeFromSuperview()
    }
    
    @objc internal func onPrevStep() {
        
        currentStep.prev()
        let oldTutorialView = self.tutorialView
        let oldArrowView = self.arrowView
        showTutorialView(step: currentStep)
        
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = .push
        transition.subtype = .fromRight
        transition.isRemovedOnCompletion = true
        transition.fillMode = .both
        transition.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        self.view.layer.add(transition, forKey: kCATransition)
        
        oldTutorialView?.removeFromSuperview()
        oldArrowView?.removeFromSuperview()
    }
    
    @objc internal func onDone() {
        AppData.appInfos.tutorialCompleted = true
        self.dismiss(animated: true, completion: nil)
    }
}


extension FTTutorialViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
}

extension FTTutorialViewController: UIViewControllerAnimatedTransitioning {
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let fromController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        
        let toController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        
        let isPresent = toController.isBeingPresented
        let actionViewController = (isPresent ? toController : fromController) as! FTTutorialViewController
        let actionView = actionViewController.view!
        let containerView = transitionContext.containerView
        
        actionView.frame = containerView.frame
        actionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        containerView.addSubview(actionView)
        
        let dulation = self.transitionDuration(using: transitionContext)
        let center = containerView.centerPointOfView()
        
        if (isPresent)
        {
            actionView.center = CGPoint(x: center.x, y: center.y+50)
            actionView.alpha = 0
            UIView.animate(withDuration: dulation, delay: 0, options: .curveEaseInOut, animations: {
                actionView.alpha = 1
                actionView.center = center
            }) { (finish) in
                transitionContext.completeTransition(finish)
            }
        } else {
            
            actionView.alpha = 1
            UIView.animate(withDuration: dulation, delay: 0, options: .curveEaseInOut, animations: {
                actionView.alpha = 0
                actionView.center = CGPoint(x: center.x, y: center.y+50)
            }) { (finish) in
                transitionContext.completeTransition(finish)
            }
        }
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
}
