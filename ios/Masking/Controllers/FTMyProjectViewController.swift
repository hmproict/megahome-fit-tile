//
//  FTMyProjectViewController.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 5/2/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import Foundation
import Async
import DGTEngineSwift
import QRCodeReader

class FTNoProjectContentView: UIView {
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updateUI(for: traitCollection)
        updateFont(for: traitCollection)
    }
    
    func updateUI(for traitCollection: UITraitCollection) {
        let style = AppUIStyle(traitCollection: traitCollection)
        self.preservesSuperviewLayoutMargins = false
        self.layoutMargins = style.viewMargin
        setNeedsDisplay()
    }
    
    func updateFont(for traitCollection: UITraitCollection) {
        let style = AppTextStyle(traitCollection: traitCollection)
        
    }
    
    override func draw(_ rect: CGRect) {
        let radius = AppUIStyle(traitCollection: traitCollection).cornerRedius
        let path = UIBezierPath(roundedRect: rect, cornerRadius: radius)
        let dashes: [CGFloat] = [5.0, 5.0]
        path.setLineDash(dashes, count: dashes.count, phase: 0)
        path.lineWidth = 2
        path.lineCapStyle = .round
        path.lineJoinStyle = .round
        UIColor.appGreen.set()
        path.stroke()
    }
}

class FTMyProjectViewController: FTBaseViewController {
    
    @IBOutlet internal weak var collectionView: UICollectionView!
    
    @IBOutlet internal weak var noProjectView: UIView!
    @IBOutlet internal weak var noProjectTitleLabel: UILabel!
    @IBOutlet internal weak var noProjectDetailLabel: UILabel!
    @IBOutlet internal weak var noProjectDescriptionLabel: UILabel!
    @IBOutlet internal weak var noProjectRemarkLabel: UILabel!
    
    fileprivate var projects = [TileProject]()
    fileprivate var transitionAnimator: CameraTransitionAnimator?
    var requiredReloadData: Bool = true
    fileprivate var backFromFullScreen: Bool = false
    
    fileprivate var mainButton: UIButton!
    fileprivate var mainTitleLabel: UILabel!
    
    var preferredProjectId: String?
    
    lazy fileprivate var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
            
            // Configure the view controller (optional)
            $0.showTorchButton = true
            $0.showSwitchCameraButton = false
            $0.showCancelButton = true
            $0.showOverlayView = true
            $0.rectOfInterest = CGRect(x: 0.2, y: 0.2, width: 0.6, height: 0.6)
        }
        
        let controller = QRCodeReaderViewController(builder: builder)
        controller.delegate = self
        return controller
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = FTMenuItem.camera.pageTitle
        
        noProjectTitleLabel?.textColor = .white
        noProjectTitleLabel?.text = LocalizedString("GeetingTitle")
        noProjectDetailLabel?.text = LocalizedString("GeetingDetail")
        noProjectRemarkLabel?.text = LocalizedString("StartProjectRemark")
        
        if #available(iOS 11.0, *) {
            self.additionalSafeAreaInsets = UIEdgeInsets(top: 0, left: 0, bottom: 60, right: 0)
            collectionView.contentInsetAdjustmentBehavior = .always
        }
        
        setupBottomLayoutGuide(in: noProjectView)
        setupBottomLayoutGuide(in: collectionView)
        
        NotificationCenter.default.addObserver(forName: .MyProjectsSaveChanged, object: nil, queue: .main) { (notification) in
            if self.isVisible() {
                self.projects = AppData.loadAllProjects()
                self.noProjectView.isHidden = self.projects.isEmpty == false
                self.collectionView.reloadData()
            } else {
                self.requiredReloadData = true
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: LocalizedString("back"), style: .plain, target: nil, action: nil)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_scan_qr"), style: .plain, target: self, action: #selector(self.onScanQRCode))
        
        navigationController?.delegate = self
        let tabbarController = self.navigationController as? FTTabbarViewController
        if tabbarController?.showTabber == false {
            tabbarController?.showTabber = true
        }
        if tabbarController?.activeMainMenu == true {
            tabbarController?.activeMainMenu = false
            backFromFullScreen = true
        }
        tabbarController?.showFitTileButton = true
        
        if requiredReloadData {
            requiredReloadData = false
            
            self.projects = AppData.loadAllProjects()
            self.noProjectView.isHidden = self.projects.isEmpty == false
            self.collectionView.reloadData()
        }
        
        if let projectId = preferredProjectId {
            prepareShareProjectData(with: projectId)
            preferredProjectId = nil
        }
        
        if #available(iOS 11.0, *) {
            let imageHeight = tabbarController?.actionButton.image?.size.height ?? 0
            let titleHeight = tabbarController?.actionTitleLabel.font.lineHeight ?? 0
            self.additionalSafeAreaInsets = UIEdgeInsets(top: 0, left: 0, bottom: imageHeight + titleHeight, right: 0)
            collectionView.contentInsetAdjustmentBehavior = .always
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        let tabbarController = self.navigationController as? FTTabbarViewController
        if tabbarController?.selectedMenuIndex == 1 {
            
        } else {
            tabbarController?.showFitTileButton = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let tabbarController = self.navigationController as? FTTabbarViewController
        tabbarController?.addMainTarget(self, action: #selector(self.onMainButtonAction(_:)))
        
        if isMovingToParent == false && backFromFullScreen {
            backFromFullScreen = false
            let tabbarController = self.navigationController as? FTTabbarViewController
            tabbarController?.isNavigationBarHidden = true
            tabbarController?.isNavigationBarHidden = false
        }
        
        if #available(iOS 11.0, *) {
            let imageHeight = tabbarController?.actionButton.image?.size.height ?? 0
            let titleHeight = tabbarController?.actionTitleLabel.font.lineHeight ?? 0
            self.additionalSafeAreaInsets = UIEdgeInsets(top: 0, left: 0, bottom: imageHeight + titleHeight, right: 0)
            collectionView.contentInsetAdjustmentBehavior = .always
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        collectionView.collectionViewLayout.invalidateLayout()
        collectionView.reloadData()
    }
    
    override func updateUI(for traitCollection: UITraitCollection) {
        let style = AppUIStyle(traitCollection: traitCollection)
        noProjectView.layoutMargins = style.margin
        
        noProjectView?.findConstraint(view: noProjectDetailLabel, attribute: .bottom)?.constant = traitCollection.verticalSizeClass == .compact ? 0 : 30
    }
    
    override func updateFont(for traitCollection: UITraitCollection) {
        let style = AppTextStyle(traitCollection: traitCollection)
        noProjectDetailLabel?.font = style.font(style: .H0)
        noProjectRemarkLabel?.font = style.font(style: .H8)
        
        let font = style.font(style: .H3)
        let text = LocalizedString("StartProjectTitle")
        let hilightText = LocalizedString("StartProjectTitleHilight")
        let hilightRange = NSRange(text.range(of: hilightText)!, in: text)
        let imageRange = NSRange(text.range(of: "|")!, in: text)
        
        let attibute = NSMutableAttributedString(string: text.replacingOccurrences(of: "|", with: " "), attributes: [.font: font, .foregroundColor: UIColor.white])
        attibute.setAttributes([.font: font, .foregroundColor: UIColor.appLightGreen], range: hilightRange)
        
        let attachment = NSTextAttachment()
        attachment.image = #imageLiteral(resourceName: "tabbar_fittile_act").scaleImageToHeight(ceil(font.lineHeight))
        let imageAttibute = NSAttributedString(attachment: attachment)
        attibute.replaceCharacters(in: imageRange, with: imageAttibute)
        noProjectDescriptionLabel?.attributedText = attibute
    }
    
    @objc internal func onMainButtonAction(_ button: UIButton) {
        
        guard let tabbarController = self.navigationController as? FTTabbarViewController else {
            return
        }
        tabbarController.pushViewController(FTFittileOptionViewController(), animated: true, completion: {
            tabbarController.setViewControllers([self, tabbarController.viewControllers.last!], animated: false)
        })
    }
    
    @objc internal func onCreateShareProject() {
        
        
    }
    
    @objc internal func onScanShareProject() {
        
        
    }
    
    @objc internal func onScanQRCode() {
        
        readerVC.delegate = self
        
        // Presents the readerVC as modal form sheet
        readerVC.modalPresentationStyle = .formSheet
        present(readerVC, animated: true, completion: nil)
    }
    
    fileprivate func prepareShareProjectData(with id: String) {
        
        if let project = AppData.realm.objects(TileProject.self).first(where: { $0.id == id }) {
            gotoProjectDetailPage(by: project)
        } else {
            AppDelegateObject.displayLoadingView()
            AppService.call({ try $0.tileProject(transactionId: id) }) { (project, error) in
                AppDelegateObject.hideLoadingView()
                if let project = project {
                    self.gotoProjectDetailPage(by: project)
                    AppData.write {
                        AppData.realm.add(project, update: .modified)
                        self.collectionView.reloadData()
                    }
                } else {
                    DLog("error : \(error?.localizedDescription ?? "No Error")")
                    let alert = UIAlertController(title: LocalizedString("error"), message: LocalizedString("LoadTileError"), preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: LocalizedString("ok"), style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    fileprivate func gotoProjectDetailPage(by project: TileProject) {
        let tabbarController = self.navigationController as? FTTabbarViewController
        tabbarController?.showFitTileButton = false
            
        guard let controller = self.storyboard?.instantiateViewController(class: FTCatalogDetailViewController.self) else {
            return
        }
        controller.style = .project
        controller.data = project
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

extension FTMyProjectViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return projects.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(class: FTProjectCollectionViewCell.self, for: indexPath)
        
        if let project = projects[safe: indexPath.item] {
            let contentWidth = min(collectionView.bounds.width, collectionView.bounds.height)
            cell.updateElemetOfCell(project, for: traitCollection, contrainIn: contentWidth)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let project = projects[safe: indexPath.item] else {
            return
        }
        gotoProjectDetailPage(by: project)
    }
}

extension FTMyProjectViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let style = AppUIStyle(traitCollection: traitCollection)
        return UIEdgeInsets(redius: style.space)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        let style = AppUIStyle(traitCollection: traitCollection)
        return style.space
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        let style = AppUIStyle(traitCollection: traitCollection)
        return style.space
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let contentWidth = min(collectionView.bounds.width, collectionView.bounds.height)
        return FTProjectCollectionViewCell.contentSize(for: traitCollection, contrainIn: contentWidth)
    }
}

extension FTMyProjectViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return nil
    }
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        if (toVC is FTFittileOptionViewController && operation == .push) ||
            (fromVC is FTFittileOptionViewController && operation == .pop) {
            // Pass the thumbnail frame to the transition animator.
            if operation == .push {
                transitionAnimator = CameraTransitionAnimator()
            }
            transitionAnimator?.operation = operation
            return transitionAnimator
        }
        return nil
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if viewController == self {
            let tabbarController = navigationController as? FTTabbarViewController
            tabbarController?.activeMainMenu = false
        }
    }
}

class CameraTransitionAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    private let duration: TimeInterval = 0.5
    var operation: UINavigationController.Operation = .push
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let presenting = operation == .push
        
        // Determine which is the master view and which is the detail view that we're navigating to and from. The container view will house the views for transition animation.
        let containerView = transitionContext.containerView
        guard let toView = transitionContext.view(forKey: .to) else { return }
        guard let fromView = transitionContext.view(forKey: .from) else { return }
        
        let bounds = containerView.bounds
        
        let minSize: CGFloat = presenting ? 10 : 1
        let minFrame = CGRect(x: bounds.midX-(minSize/2), y: bounds.midY-(minSize/2), width: minSize, height: minSize)
        let maxSize: CGFloat = bounds.height + bounds.width/2
        let maxFrame = CGRect(x: bounds.midX-(maxSize/2), y: bounds.midY-(maxSize/2), width: maxSize, height: maxSize)
        let frame = presenting ? minFrame : maxFrame
        
        let maskLayer: CAShapeLayer
        if presenting {
            
            toView.frame = bounds
            containerView.addSubview(toView)
            
            fromView.frame = bounds
            containerView.addSubview(fromView)
            
            let fullPath = CGMutablePath()
            fullPath.addRect(bounds)
            fullPath.addEllipse(in: frame)
            
            maskLayer = CAShapeLayer()
            maskLayer.fillRule = .evenOdd
            maskLayer.path = fullPath
            
            fromView.layer.mask = maskLayer
            
        } else {
            
            toView.frame = bounds
            containerView.addSubview(toView)
            
            fromView.frame = bounds
            containerView.addSubview(fromView)
            
            let fullPath = CGMutablePath()
            fullPath.addEllipse(in: frame)
            
            maskLayer = CAShapeLayer()
            maskLayer.path = fullPath
            
            fromView.layer.mask = maskLayer
        }
        
        // define your new path to animate the mask layer to
        let finishFrame = presenting ? maxFrame : minFrame
        let finishPath = CGMutablePath()
        if presenting {
            finishPath.addRect(bounds)
            finishPath.addEllipse(in: finishFrame)
        } else {
            finishPath.addEllipse(in: finishFrame)
        }

        // create new animation
        let anim = CABasicAnimation(keyPath: "path")

        // from value is the current mask path
        anim.fromValue = maskLayer.path

        // to value is the new path
        anim.toValue = finishPath

        // duration of your animation
        anim.duration = duration

        // custom timing function to make it look smooth
        anim.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)

        // update the path property on the mask layer, using a CATransaction to prevent an implicit animation
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        CATransaction.setCompletionBlock {
            fromView.layer.mask = nil
            toView.layer.mask = nil
            transitionContext.completeTransition(true)
        }
        // add animation
        maskLayer.add(anim, forKey: "PathAnimationKey")
        maskLayer.path = finishPath
        CATransaction.commit()
    }
}

extension FTMyProjectViewController: QRCodeReaderViewControllerDelegate {
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        
        guard let url = URL(string: result.value) else {
            return
        }
        
        let parameters = AppUtility.parameters(for: url)
        if let transactionId = parameters["qrcode"] {
            
            reader.dismiss(animated: true, completion: nil)
            if let project = self.projects.first(where: { $0.id == transactionId }) {
                gotoProjectDetailPage(by: project)
                return
            }
            
            DLog("transectionId : \(transactionId)")
            AppDelegateObject.displayLoadingView()
            
            AppService.call({ try $0.tileProject(transactionId: transactionId) }) { [weak self] (project, error) in
                AppDelegateObject.hideLoadingView()
                if let project = project {
                    self?.requiredReloadData = true
                    AppData.write {
                        AppData.realm.add(project, update: .modified)
                    }
                    self?.gotoProjectDetailPage(by: project)
                } else {
                    DLog("error : \(error?.localizedDescription ?? "No Error")")
                    let alert = UIAlertController(title: LocalizedString("error"), message: LocalizedString("LoadProjectError"), preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: LocalizedString("ok"), style: .default, handler: nil))
                    self?.present(alert, animated: true, completion: nil)
                }
            }
        } else if let tileId = parameters["id_scan"] {
            
            DLog("tileId : \(tileId)")
            AppDelegateObject.displayLoadingView()
            
            AppService.call({ try $0.tile(id: tileId) }) { [weak self] (tile, error) in
                AppDelegateObject.hideLoadingView()
                if let tile = tile {
                    
                    AppData.write {
                        AppData.realm.add(tile, update: .modified)
                    }
                    AppDelegateObject.changeToMenuItem(.catalog, animatd: false, completion: {
                        if let controller = self?.storyboard?.instantiateViewController(class: FTCatalogDetailViewController.self) {
                            controller.style = .tile
                            controller.data = tile
                            AppDelegateObject.navigationController.pushViewController(controller, animated: true)
                        }
                    })
                } else {
                    DLog("error : \(error?.localizedDescription ?? "No Error")")
                    let alert = UIAlertController(title: LocalizedString("error"), message: LocalizedString("LoadTileError"), preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: LocalizedString("ok"), style: .default, handler: nil))
                    self?.present(alert, animated: true, completion: nil)
                }
            }
        } else {
            let alert = UIAlertController(title: LocalizedString("error"), message: LocalizedString("QRCodeIncorrectFormatError"), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: LocalizedString("ok"), style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.dismiss(animated: true, completion: nil)
    }
}
