//
//  BCPopoverViewController.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 4/2/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import UIKit
import DGTEngineSwift

@objc protocol FTPopoverViewControllerDelegate {
    @objc optional func didDismissBCPopoverViewController(_ controller: FTPopoverViewController)
}

class FTPopoverViewController: UIViewController {
    
    fileprivate(set) var contentViewController: UIViewController!
    
    let contentView: UIView = UIView()
    var tapgesture: UITapGestureRecognizer?
    
    weak var delegate: FTPopoverViewControllerDelegate?
    var spaceWhenKeyboardShowing: CGFloat = 0
    var dismissWhenTouchBackground: Bool = false {
        didSet {
            tapgesture?.isEnabled = dismissWhenTouchBackground
        }
    }
    
    override var preferredContentSize: CGSize {
        get {
            return contentViewController.preferredContentSize
        }
        set {
            
        }
    }
    
    required convenience init(contentViewController: UIViewController) {
        self.init()
        self.contentViewController = contentViewController
        modalPresentationStyle = .custom
        self.transitioningDelegate = self
        
        let size = contentViewController.preferredContentSize
        contentView.frame = self.view.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        contentViewController.view.bounds = CGRect(origin: CGPoint.zero, size: CGSize(width: size.width, height: size.height))
        contentView.addSubview(contentViewController.view)
        addChild(contentViewController)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentView.clipsToBounds = true
        view.addSubview(contentView)
        
        tapgesture = UITapGestureRecognizer(target: self, action: #selector(self.onTapBackground(_:)))
        tapgesture?.isEnabled = dismissWhenTouchBackground
        tapgesture?.delegate = self
        view.addGestureRecognizer(tapgesture!)
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(self.onKeyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(self.onKeyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        contentViewController.view.center = view.centerPointOfView() 
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.superview?.backgroundColor = UIColor.clear
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        delegate?.didDismissBCPopoverViewController?(self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    deinit {
        let notificationCenter = NotificationCenter.default
        notificationCenter.removeObserver(self)
        DLog("deinit")
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        if let controller = contentViewController as? UINavigationController, let firstController = controller.topViewController {
            return firstController.preferredStatusBarStyle
        }
        return contentViewController.preferredStatusBarStyle
    }
    
    @objc internal func onTapBackground(_ gesture: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: -------------------------------------------------------
    // MARK: NSNotification For keyboard
    // MARK: -------------------------------------------------------
    
    @objc internal func onKeyboardWillShow(_ notification: Notification) {
        moveKeyboard(true, forNotification: notification)
    }
    
    @objc internal func onKeyboardWillHide(_ notification: Notification) {
        moveKeyboard(false, forNotification: notification)
    }
    
    internal func moveKeyboard(_ up: Bool, forNotification notification: Notification) {
        
        let userInfo = notification.userInfo!
        
        // Get animation info from userInfo
        let animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! TimeInterval
        let keyboardEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let curve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as! Int
        let options = UIView.AnimationOptions(rawValue: UInt(curve) << 16)
        
        DLog("options : \(options) - \(curve)")
        
        if (contentView.frame.minY + contentView.bounds.height) > view.frame.height - keyboardEndFrame.height {
            UIView.animate(withDuration: animationDuration, delay: 0, options: options, animations: {
                self.contentView.center.y = up ? self.view.center.y - self.spaceWhenKeyboardShowing : self.view.center.y
            }, completion: nil)
        }
    }
    
    // MARK: -------------------------------------------------------
    // MARK: InterfaceOrientations
    // MARK: -------------------------------------------------------
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }
}

extension FTPopoverViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
}

extension FTPopoverViewController: UIViewControllerAnimatedTransitioning {
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let fromController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        
        let toController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        
        let isPresent = toController.isBeingPresented
        let actionViewController = (isPresent ? toController : fromController) as! FTPopoverViewController
        let actionView = actionViewController.view
        let contentView = actionViewController.contentView
        
        let containerView = transitionContext.containerView
        actionView?.frame = containerView.frame
        actionView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        containerView.addSubview(actionView!)
        
        let dulation = self.transitionDuration(using: transitionContext)
        
        if isPresent {
            
            contentView.transform = CGAffineTransform.identity.scaledBy(x: 0.4, y: 0.4)
            UIView.animate(withDuration: dulation, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 8.0, options: [], animations: { () -> Void in
                
                actionView?.layer.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.595890411).cgColor
                contentView.transform = CGAffineTransform.identity
                
            }) { (success) -> Void in
                transitionContext.completeTransition(true)
            }
        } else {
            
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                contentView.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
            }, completion: { (success) -> Void in
                
                UIView.animate(withDuration: 0.4, animations: { () -> Void in
                    
                    actionView?.layer.backgroundColor = UIColor.clear.cgColor
                    contentView.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
                    contentView.alpha = 0
                }, completion: { (success) -> Void in
                    transitionContext.completeTransition(true)
                })
            })
        }
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
}

extension FTPopoverViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if let view = touch.view, view != contentView && view.isDescendant(of: contentView) {
            return false
        }
        return true
    }
}
