//
//  FTTilePickerViewController.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 19/2/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import Foundation
import UIKit
import DGTEngineSwift

enum FTMenuPickerViewStyle {
    case date
    case custom
}

protocol FTMenuPickerViewDelegate {
    func menuPicker(_ picker: FTMenuPickerViewController, textForData data: Any) -> String
    func menuPicker(_ picker: FTMenuPickerViewController, valueChanged data: Any)
    func menuPickerDidFinish()
}

class FTMenuPickerViewController: UIViewController {
    
    var delegate: FTMenuPickerViewDelegate?
    fileprivate lazy var customPicker: UIPickerView = {
        let picker = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 0))
        picker.dataSource = self
        picker.delegate = self
        return picker
    }()
    fileprivate lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.minimumDate = Date().add([.year], value: -107)
        picker.maximumDate = Date().add([.year], value: -7)
        let locale = Locale.current
        picker.locale = locale
        picker.calendar = locale.calendar
        picker.addTarget(self, action: #selector(self.onDatePickerChange(_:)), for: .valueChanged)
        return picker
    }()
    fileprivate let contentView: UIView = UIView()
    
    fileprivate var picker: UIView {
        return pickerStyle == .custom ? customPicker : datePicker
    }
    
    var menus: Array<Any>? {
        didSet {
            if isViewLoaded {
                customPicker.reloadAllComponents()
            }
        }
    }
    var preferSelectIndexs: [Int]?
    var topView: UIView? {
        didSet {
            if let topView = topView {
                topView.sizeToFit()
                contentView.addSubview(topView)
            } else {
                oldValue?.removeFromSuperview()
            }
        }
    }
    let pickerStyle: FTMenuPickerViewStyle
    
    init(menus: Array<Any>) {
        self.menus = menus
        self.pickerStyle = .custom
        super.init(nibName: nil, bundle: nil)
        
        if DeviceType.IS_IPAD {
            modalPresentationStyle = .popover
            popoverPresentationController?.permittedArrowDirections = .up
        } else {
            modalPresentationStyle = .custom
            self.transitioningDelegate = self
        }
    }
    
    init(date: Date) {
        self.pickerStyle = .date
        super.init(nibName: nil, bundle: nil)
        
        datePicker.date = date
        if DeviceType.IS_IPAD {
            modalPresentationStyle = .popover
            popoverPresentationController?.permittedArrowDirections = .up
        } else {
            modalPresentationStyle = .custom
            self.transitioningDelegate = self
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var preferredContentSize: CGSize {
        get {
            let height = topView?.bounds.height ?? 0
            if DeviceType.IS_IPAD {
                return CGSize(width: 320, height: picker.bounds.height + height)
            }
            return CGSize(width: ScreenSize.width, height: picker.bounds.height + height)
        }
        set {
            
        }
    }
    
    var currentValue: Any? {
        if pickerStyle == .custom {
            let index = customPicker.selectedRow(inComponent: 0)
            return menus?[safe: index]
        } else if pickerStyle == .date {
            return datePicker.date
        }
        return nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.clear;
        view.frame.size = preferredContentSize
        
        let style = AppUIStyle()
        let image = UIImage(color: .white, size: style.cornerInset.resizableSize, cornerInset: DGTCornerInset(topleft: style.cornerRedius, topright: style.cornerRedius, bottomleft: 0, bottomright: 0))?.resizableImage()
        let bgView = UIImageView(image: image)
        bgView.isUserInteractionEnabled = false
        bgView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        bgView.frame = contentView.bounds
        contentView.insertSubview(bgView, at: 0)
        
        picker.sizeToFit()
        picker.frame.size.width = contentView.bounds.width
        picker.autoresizingMask = .flexibleWidth
        
        contentView.addSubview(picker)
        contentView.frame.size = preferredContentSize
        view.addSubview(contentView)
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.onTouchForDismiss(_:)))
        gesture.delegate = self
        view.addGestureRecognizer(gesture)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if modalPresentationStyle == .custom {
            var frame = self.contentView.frame
            frame.origin.y = self.view.bounds.height
            let userInfo: [AnyHashable: Any] = [UIResponder.keyboardAnimationCurveUserInfoKey: 7, UIResponder.keyboardAnimationDurationUserInfoKey: 0.4, UIResponder.keyboardFrameEndUserInfoKey: frame]
            NotificationCenter.default.post(name: UIResponder.keyboardWillShowNotification, object: nil, userInfo: userInfo)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if modalPresentationStyle == .custom {
            let userInfo: [AnyHashable: Any] = [UIResponder.keyboardAnimationCurveUserInfoKey: 7, UIResponder.keyboardAnimationDurationUserInfoKey: 0.4, UIResponder.keyboardFrameEndUserInfoKey: self.contentView.frame]
            NotificationCenter.default.post(name: UIResponder.keyboardDidShowNotification, object: nil, userInfo: userInfo)
        }
        if let indexs = preferSelectIndexs {
            for (component, index) in indexs.enumerated() {
                customPicker.selectRow(index, inComponent: component, animated: false)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if let picker = picker as? UIPickerView {
            let row = picker.selectedRow(inComponent: 0)
            
            let data: Any = {
                if let menus = menus as? Array<Array<String>> {
                    
                    var datas = [Any]()
                    for (index, menu) in menus.enumerated() {
                        datas.append(menu[picker.selectedRow(inComponent: index)])
                    }
                    return datas
                }
                return menus?[row] ?? ""
            }()
            delegate?.menuPicker(self, valueChanged: data)
        }
        delegate?.menuPickerDidFinish()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if let segmetView = topView {
            let style = AppUIStyle()
            let inset = view.bounds.inset(by: style.viewMargin)
            let y: CGFloat
            if #available(iOS 11.0, *) {
                if (isModalInPopover) {
                    y = inset.origin.y + self.view.safeAreaInsets.top
                } else {
                    y = inset.origin.y
                }
            } else {
                y = inset.origin.y
            }
            segmetView.frame = CGRect(x: inset.origin.x, y: y, width: inset.width, height: segmetView.frame.height)
            picker.frame = CGRect(x: 0, y: segmetView.bounds.height, width: view.bounds.width, height: contentView.bounds.height-segmetView.bounds.height)
        } else {
            contentView.frame.size.width = view.bounds.width
        }
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        if let controller = presentedViewController {
            return controller.preferredStatusBarStyle
        }
        return .lightContent
    }
    
    @objc internal func onTouchForDismiss(_ gesture: UIGestureRecognizer) {
        
        if modalPresentationStyle == .custom {
            let userInfo: [AnyHashable: Any] = [UIResponder.keyboardAnimationCurveUserInfoKey: 7, UIResponder.keyboardAnimationDurationUserInfoKey: 0.4, UIResponder.keyboardFrameEndUserInfoKey: contentView.frame]
            NotificationCenter.default.post(name: UIResponder.keyboardWillHideNotification, object: nil, userInfo: userInfo)
        }
        self.dismiss(animated: true, completion: {
            if self.modalPresentationStyle == .custom {
                var frame = self.contentView.frame
                frame.origin.y = self.view.bounds.height
                let userInfo: [AnyHashable: Any] = [UIResponder.keyboardAnimationCurveUserInfoKey: 7, UIResponder.keyboardAnimationDurationUserInfoKey: 0.4, UIResponder.keyboardFrameEndUserInfoKey: frame]
                NotificationCenter.default.post(name: UIResponder.keyboardDidHideNotification, object: nil, userInfo: userInfo)
            }
        })
    }
    
    @objc internal func onDatePickerChange(_ picker: UIDatePicker) {
        delegate?.menuPicker(self, valueChanged: picker.date)
    }

    func reloadPicker() {
        customPicker.reloadAllComponents()
    }
}

extension FTMenuPickerViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if let _ = menus?.first as? Array<String> {
            return menus?.count ?? 0
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if let array = menus?[safe: component] as? Array<String> {
            return array.count
        }
        return menus?.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let data: Any = {
            if let array = menus?[safe: component] as? Array<String> {
                return array[row]
            }
            return menus?[row] ?? ""
        }()
        
        if let text = delegate?.menuPicker(self, textForData: data) {
            return NSAttributedString(string: text, attributes: [.font: AppTextStyle().font(style: .H6), .foregroundColor: UIColor.appDarkGray])
        } else {
            let text = data as? String ?? ""
            return NSAttributedString(string: text, attributes: [.font: AppTextStyle().font(style: .H6), .foregroundColor: UIColor.appDarkGray])
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let data: Any = {
            if let menus = menus as? Array<Array<String>> {
                
                var datas = [Any]()
                for (index, menu) in menus.enumerated() {
                    datas.append(menu[pickerView.selectedRow(inComponent: index)])
                }
                return datas
            }
            return menus?[row] ?? ""
        }()
        delegate?.menuPicker(self, valueChanged: data)
    }
}

extension FTMenuPickerViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
}

extension FTMenuPickerViewController: UIViewControllerAnimatedTransitioning {
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let fromController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let toController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        
        let isPresent = toController.isBeingPresented
        let actionViewController = (isPresent ? toController : fromController) as! FTMenuPickerViewController
        let actionView = actionViewController.view!
        let contentView = actionViewController.contentView
        
        let containerView = transitionContext.containerView
        actionView.frame = containerView.frame
        actionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        containerView.addSubview(actionView)
        
        let dulation = self.transitionDuration(using: transitionContext)
        
        if isPresent {
            
            contentView.frame = CGRect(x: 0, y: actionView.bounds.height, width: actionView.bounds.width, height: contentView.bounds.height)
            UIView.animate(withDuration: dulation, delay: 0, options: .curveEaseInOut, animations: { () -> Void in
                
                contentView.frame.origin.y = actionView.bounds.height - contentView.bounds.height
                
            }) { (success) -> Void in
                transitionContext.completeTransition(true)
            }
        } else {
            
            UIView.animate(withDuration: dulation, delay: 0, options: .curveEaseInOut, animations: { () -> Void in
                
                contentView.frame.origin.y = actionView.bounds.height
                
            }) { (success) -> Void in
                transitionContext.completeTransition(true)
            }
        }
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.4
    }
}

extension FTMenuPickerViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if let view = touch.view, let _ = gestureRecognizer as? UITapGestureRecognizer, view.isKind(of: UIControl.self) {
            return false
        }
        return true
    }
}
