//
//  FTTileActionViewController.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 29/2/2563 BE.
//  Copyright © 2563 Aduldach Pradubyart. All rights reserved.
//

import UIKit

import Foundation
import UIKit
import DGTEngineSwift

class FTTileAction: NSObject {
    
    let title: String
    let image: UIImage?
    let handler: ((FTTileAction) -> Void)?
    
    required init(title: String, image: UIImage? = nil, handler: ((FTTileAction) -> Void)? = nil) {
        self.title = title
        self.handler = handler
        self.image = image
        super.init()
    }
}

class FTTileActionViewController: UIViewController {
    
    let contentView: UIView = UIView()
    let items: [FTTileAction]
    
    fileprivate lazy var titleLabel: UILabel = {
        let style = AppTextStyle()
        let titleLabel = UILabel.autoLayout()
        titleLabel.textAlignment = .center
        titleLabel.font = style.font(style: .H5)
        titleLabel.textColor = .init(grayscalCode: 74)
        [titleLabel.height == 50].activated()
        contentView.addSubview(titleLabel)
        return titleLabel
    }()
    
    fileprivate lazy var bgView: UIImageView = {
        let image = UIImage(color: .white, size: CGSize(width: 50, height: 50), cornerInset: DGTCornerInset(topleft: 20, topright: 20, bottomleft: 0, bottomright: 0))?.resizableImage()
        let bgView = UIImageView(image: image)
        bgView.isUserInteractionEnabled = false
        bgView.prepareForAutoLayout()
        return bgView
    }()
    
    required init(items: [FTTileAction]) {
        self.items = items
        super.init(nibName: nil, bundle: nil)
        
        if DeviceType.IS_IPAD {
            modalPresentationStyle = .popover
            popoverPresentationController?.permittedArrowDirections = .up
        } else {
            modalPresentationStyle = .custom
            self.transitioningDelegate = self
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var preferredContentSize: CGSize {
        get {
            let titleHeight: CGFloat = 51
            let itemHeight: CGFloat = 60 * CGFloat(items.count)
            if #available(iOS 11.0, *) {
                return CGSize(width: min(ScreenSize.width, 320), height: titleHeight + itemHeight + view.safeAreaInsets.bottom)
            }
            return CGSize(width: min(ScreenSize.width, 320), height: titleHeight + itemHeight)
        }
        set {
            
        }
    }
    
    override var title: String? {
        get {
            return super.title
        }
        set {
            super.title = newValue
            titleLabel.text = newValue
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.clear;
        view.frame.size = preferredContentSize
        
        contentView.clipsToBounds = true
        contentView.insertSubview(bgView, at: 0)
        bgView.constraintsAroundView(contentView)?.activated()
        
        let lineTitleView = UIView.autoLayout()
        lineTitleView.backgroundColor = .init(grayscalCode: 216)
        [lineTitleView.height == 1].activated()
        contentView.addSubview(lineTitleView)
        
        func makeButton(title: String, image: UIImage? = nil) -> UIView {
            
            class ActionButton: UIButton {
                
                override func layoutSubviews() {
                    super.layoutSubviews()
                    if let _ = self.image {
                        imageView?.frame.origin.x = self.center.x - 50
                        titleLabel?.frame.origin.x = self.center.x
                    }
                }
            }

            let style = AppTextStyle()
            let button = ActionButton(type: .custom)
            button.prepareForAutoLayout()
            button.title = title
            button.titleLabel?.font = style.font(style: .H6)
            button.image = image
            button.textColor = .init(grayscalCode: 74)
            button.setBackgroundImage(UIImage(color: .white, size: CGSize(width: 5, height: 5))?.resizableImage(), for: .normal)
            button.setBackgroundImage(UIImage(color: .lightGray, size: CGSize(width: 5, height: 5))?.resizableImage(), for: .highlighted)
            button.addTarget(self, action: #selector(self.onSelectedAction(_:)), for: .touchUpInside)
            
            let lineView = UIView.autoLayout()
            [lineView.height == 1].activated()
            lineView.backgroundColor = .init(grayscalCode: 216)
            
            let view = UIView.autoLayout()
            view.addSubview(button)
            view.addSubview(lineView)
            
            button.constraintsAroundView()?.activated()
            [lineView.left == view.left + 30,
             lineView.right == view.right - 30,
             lineView.bottom == view.bottom].activated()
            
            return view
        }
        
        let views = self.items.map { makeButton(title: $0.title, image: $0.image) }
        let stackView = UIStackView(arrangedSubviews: views)
        stackView.prepareForAutoLayout()
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        stackView.frame = CGRect(x: 0, y: 51, width: contentView.bounds.width, height: 0)
        contentView.addSubview(stackView)
        
        [titleLabel.left == contentView.leftMargin,
        titleLabel.right == contentView.rightMargin,
        titleLabel.top == contentView.topMargin,
        lineTitleView.top == titleLabel.bottom,
        lineTitleView.right == contentView.rightMargin,
        lineTitleView.left == contentView.leftMargin,
        stackView.top == lineTitleView.bottom,
        stackView.right == contentView.rightMargin,
        stackView.left == contentView.leftMargin,
            stackView.bottom == contentView.bottomMargin].activated()
        
        contentView.frame.size = preferredContentSize
        view.addSubview(contentView)
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.onTouchForDismiss(_:)))
        gesture.delegate = self
        view.addGestureRecognizer(gesture)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if modalPresentationStyle == .custom {
            var frame = self.contentView.frame
            frame.origin.y = self.view.bounds.height
            let userInfo: [AnyHashable: Any] = [UIResponder.keyboardAnimationCurveUserInfoKey: 7, UIResponder.keyboardAnimationDurationUserInfoKey: 0.4, UIResponder.keyboardFrameEndUserInfoKey: frame]
            NotificationCenter.default.post(name: UIResponder.keyboardWillShowNotification, object: nil, userInfo: userInfo)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if modalPresentationStyle == .custom {
            let userInfo: [AnyHashable: Any] = [UIResponder.keyboardAnimationCurveUserInfoKey: 7, UIResponder.keyboardAnimationDurationUserInfoKey: 0.4, UIResponder.keyboardFrameEndUserInfoKey: self.contentView.frame]
            NotificationCenter.default.post(name: UIResponder.keyboardDidShowNotification, object: nil, userInfo: userInfo)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        contentView.frame.size = CGSize(width: view.bounds.width, height: preferredContentSize.height)
    }
    
    @available(iOS 11.0, *)
    override func viewSafeAreaInsetsDidChange() {
        super.viewSafeAreaInsetsDidChange()
        if (self.isModalInPopover) {
            contentView.layoutMargins = view.safeAreaInsets
        } else {
            contentView.layoutMargins = UIEdgeInsets(top: 0, left: view.safeAreaInsets.left, bottom: view.safeAreaInsets.bottom, right: view.safeAreaInsets.right)
        }
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        if let controller = presentedViewController {
            return controller.preferredStatusBarStyle
        }
        return .lightContent
    }
    
    @objc internal func onTouchForDismiss(_ gesture: UIGestureRecognizer) {
        
        if modalPresentationStyle == .custom {
            let userInfo: [AnyHashable: Any] = [UIResponder.keyboardAnimationCurveUserInfoKey: 7, UIResponder.keyboardAnimationDurationUserInfoKey: 0.4, UIResponder.keyboardFrameEndUserInfoKey: contentView.frame]
            NotificationCenter.default.post(name: UIResponder.keyboardWillHideNotification, object: nil, userInfo: userInfo)
        }
        self.dismiss(animated: true, completion: {
            if self.modalPresentationStyle == .custom {
                var frame = self.contentView.frame
                frame.origin.y = self.view.bounds.height
                let userInfo: [AnyHashable: Any] = [UIResponder.keyboardAnimationCurveUserInfoKey: 7, UIResponder.keyboardAnimationDurationUserInfoKey: 0.4, UIResponder.keyboardFrameEndUserInfoKey: frame]
                NotificationCenter.default.post(name: UIResponder.keyboardDidHideNotification, object: nil, userInfo: userInfo)
            }
        })
    }
    
    @objc internal func onSelectedAction(_ button: UIButton) {
        guard let action = self.items.first(where: { $0.title == button.title }) else {
            return
        }
        self.dismiss(animated: true, completion: {
            action.handler?(action)
        })
    }
}

extension FTTileActionViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
}

extension FTTileActionViewController: UIViewControllerAnimatedTransitioning {
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let fromController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let toController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        
        let isPresent = toController.isBeingPresented
        let actionViewController = (isPresent ? toController : fromController) as! FTTileActionViewController
        let actionView = actionViewController.view!
        let contentView = actionViewController.contentView
        
        let containerView = transitionContext.containerView
        actionView.frame = containerView.frame
        actionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        containerView.addSubview(actionView)
        
        let dulation = self.transitionDuration(using: transitionContext)
        
        if isPresent {
            
            contentView.frame = CGRect(x: 0, y: actionView.bounds.height, width: actionView.bounds.width, height: contentView.bounds.height)
            UIView.animate(withDuration: dulation, delay: 0, options: .curveEaseInOut, animations: { () -> Void in
                
                contentView.frame.origin.y = actionView.bounds.height - contentView.bounds.height
                
            }) { (success) -> Void in
                transitionContext.completeTransition(true)
            }
        } else {
            
            UIView.animate(withDuration: dulation, delay: 0, options: .curveEaseInOut, animations: { () -> Void in
                
                contentView.frame.origin.y = actionView.bounds.height
                
            }) { (success) -> Void in
                transitionContext.completeTransition(true)
            }
        }
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.4
    }
}

extension FTTileActionViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if let view = touch.view, let _ = gestureRecognizer as? UITapGestureRecognizer, view.isKind(of: UIControl.self) {
            return false
        }
        if touch.view == contentView {
            return false
        }
        return true
    }
}
