//
//  FTTabberViewController.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 4/2/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import Foundation
import DGTEngineSwift

class FTNavigationController: UINavigationController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return self.topViewController?.preferredStatusBarStyle ?? .lightContent
    }
}

class FTTabbarViewController: FTNavigationController {
    
    private lazy var tabberView = FTTabbarView()
    
    var selectedMenuIndex: Int {
        set {
            tabberView.selectedIndex = newValue
        }
        get {
            return tabberView.selectedIndex
        }
    }
    
    var showTabber: Bool = true {
        didSet {
            
            let bottomSpace: CGFloat
            if #available(iOS 11.0, *) {
                bottomSpace = self.view.safeAreaInsets.bottom
            } else {
                bottomSpace = self.bottomLayoutGuide.length > 0 ? self.bottomLayoutGuide.length : 40.0
            }
            
            if let constraint = view.findConstraint(view: tabberView, attribute: .bottom) {
                constraint.constant = showTabber ? 0 : self.tabberView.length + bottomSpace
            }
            
            if let constraint = view.findConstraint(view: actionTitleLabel, attribute: .bottom) {
                constraint.constant = showTabber ? 0 : self.tabberView.length + bottomSpace
            }
            if #available(iOS 11.0, *) {
                additionalSafeAreaInsets = UIEdgeInsets(top: 0, left: 0, bottom: showTabber ? self.tabberView.length : 0, right: 0)
            }
            UIView.animate(withDuration: TimeInterval(UINavigationController.hideShowBarDuration)) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    var showFitTileButton: Bool = true {
        didSet {
            
            self.actionButton.isHidden = false
            self.actionButton.alpha = 0
            UIView.animate(withDuration: TimeInterval(UINavigationController.hideShowBarDuration)) {
                self.actionButton.alpha = self.showFitTileButton ? 1 : 0
            } completion: { (success) in
                self.actionButton.isHidden = !self.showFitTileButton
                if self.showFitTileButton == false {
                    self.actionTitleLabel.isHidden = true
                } else if self.showFitTileButton && self.activeMainMenu {
                    self.actionTitleLabel.isHidden = false
                }
            }
        }
    }
    
    var activeMainMenu: Bool = false {
        didSet {
            tabberView.showMainMenu = activeMainMenu
            actionTitleLabel.isHidden = !activeMainMenu
            showTabber = !activeMainMenu
            actionButton.image = activeMainMenu ? #imageLiteral(resourceName: "icon_camera_capture_act") : #imageLiteral(resourceName: "tabbar_fittile_act")
            self.setNavigationBarHidden(activeMainMenu, animated: true)
            UIView.animate(withDuration: TimeInterval(UINavigationController.hideShowBarDuration)) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    var bottomLayoutHeight: CGFloat {
        return showTabber ? tabberView.length + self.bottomLayoutGuide.length : 0
    }
    
    var defaultLength: CGFloat {
        return tabberView.length
    }
    
    let actionButton: UIButton = {
        let button = UIButton(type: .custom)
        button.prepareForAutoLayout()
        button.image = #imageLiteral(resourceName: "tabbar_fittile_act").withRenderingMode(.alwaysOriginal)
        button.tag = 1
        button.setContentHuggingPriority(.required, for: .horizontal)
        button.setContentHuggingPriority(.required, for: .vertical)
        return button
    }()

    let actionTitleLabel: UILabel = {
        let label = UILabel.autoLayout()
        let font = AppTextStyle().font(style: AppFontStyle.H8)
        label.font = font
        label.text = LocalizedString("TabbarTitleFittile")
        label.textColor = .appDarkGray
        label.highlightedTextColor = .appGreen
        label.isHighlighted = true
        label.textAlignment = .center
        label.isHidden = true
        let constraint = label.height == ceil(font.lineHeight)
        constraint.identifier = "TitleLabelHeight"
        constraint.isActive = true
        return label
    }()
    
    private var mainAction: Selector?
    private var mainTarget: Any?
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        tabberView.adjustHeight = max(self.tabberView.length, self.bottomLayoutGuide.length)
    }
    
    deinit {
        let notification = NotificationCenter.default
        notification.removeObserver(self)
    }
    
    init(contentViewController: UIViewController) {
        super.init(navigationBarClass: FTNavigationBar.self, toolbarClass: nil)
        self.viewControllers = [contentViewController]
        contentViewController.edgesForExtendedLayout = .bottom
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    internal func setupView() {
        self.automaticallyAdjustsScrollViewInsets = false
        self.edgesForExtendedLayout = .bottom
        
        tabberView.prepareForAutoLayout()
        view.addSubview(tabberView)
        [tabberView.left == view.left,
         tabberView.right == view.right,
         tabberView.bottom == view.bottom].activated()
        tabberView.addTarget(self, action: #selector(self.onUserChangeTabbar(_:)), for: .touchDown)
        actionButton.addTarget(self, action: #selector(self.onUserMainTabbar(_:)), for: .touchUpInside)
        
        view.addSubview(actionButton)
        view.addSubview(actionTitleLabel)
        
        [tabberView.top == actionTitleLabel.bottom,
         actionTitleLabel.left == actionButton.left,
         actionTitleLabel.right == actionButton.right,
         actionTitleLabel.top == actionButton.bottom + 5,
         actionButton.centerX == tabberView.centerX].activated()
        
        if #available(iOS 11.0, *) {
            additionalSafeAreaInsets = UIEdgeInsets(top: 0, left: 0, bottom: self.tabberView.length, right: 0)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if #available(iOS 11.0, *) {
            print("bottom : \(self.view.safeAreaInsets.bottom)")
        }
    }
    
    @objc internal func onUserChangeTabbar(_ button: UIButton) {
        if let item = FTMenuItem(rawValue: button.tag) {
            tabberView.selectedIndex = button.tag
            AppDelegateObject.changeToMenuItem(item)
            self.showFitTileButton = item == .camera
        }
    }
    
    @objc internal func onUserMainTabbar(_ button: UIButton) {
        
        if activeMainMenu == false {
            activeMainMenu = true
        }
        if let target = mainTarget as? NSObject, let action = mainAction {
            target.perform(action, with: button)
        }
    }
    
    @objc internal func notificationChangeAppLanguage(_ notification: NSNotification) {
        
    }
    
    override func setViewControllers(_ viewControllers: [UIViewController], animated: Bool) {
        super.setViewControllers(viewControllers, animated: true)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if #available(iOS 11.0, *) {
            self.tabberView.adjustHeight = self.view.safeAreaInsets.bottom
        } else {
            self.tabberView.adjustHeight = max(self.tabberView.length, self.bottomLayoutGuide.length)
        }
    }
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        
        let button = UIBarButtonItem(title: LocalizedString("back"), style: .done, target: nil, action: nil)
        navigationItem.backBarButtonItem = button
        viewController.navigationItem.backBarButtonItem = button
        
        guard showTabber else {
            return
        }
        
        func setScrollViewInset(_ scrollView: UIScrollView?) {
            scrollView?.contentInset.bottom = tabberView.length
            scrollView?.scrollIndicatorInsets.bottom = tabberView.length
        }
        
        let scrollView: UIScrollView? = {
            if viewController.view.isKind(of: UIScrollView.self) {
                return viewController.view as? UIScrollView
            }
            return viewController.view.subviews.first(where: { $0.isKind(of: UIScrollView.self) }) as? UIScrollView
        }()
        setScrollViewInset(scrollView)
    }
    
    private var customViews = [UIView]()
    func addCustomViewToTabbar(_ view: UIView) {
        self.view.addSubview(view)
        customViews.append(view)
    }
    
    func removeCustomView(_ view: UIView) {
        view.removeFromSuperview()
        customViews.remove(where: { $0 == view })
    }
    
    func removeAllCustomView() {
        customViews.forEach({ $0.removeFromSuperview() })
        customViews.removeAll()
    }
    
    func addMainTarget(_ target: Any, action: Selector) {
        self.mainTarget = target
        self.mainAction = action
    }
}

class FTTabbarView: UIControl {
    
    var adjustHeight: CGFloat = 0 {
        didSet {
            if oldValue != adjustHeight {
                self.setNeedsLayout()
                self.layoutIfNeeded()
                self.invalidateIntrinsicContentSize()
            }
        }
    }
    
    var selectedIndex: Int = FTMenuItem.camera.rawValue {
        didSet {
            let buttons = self.buttons
            buttons?.forEach{ $0.isSelected = false }
            let button = buttons?.first(where: { $0.tag == selectedIndex })
            button?.isSelected = true
        }
    }
    
    var showMainMenu: Bool = true {
        didSet {
            
            let height = self.adjustHeight - 40
            
//            findConstraint(firstItem: mainTitleLabel, secondItem: menuView, firstAttribute: .bottom, secondAttribute: .top)?.constant = showMainMenu ? -height : 0
//
//            let font = AppTextStyle().font(style: showMainMenu ? .H6 : defaultFontStyle)
//            mainTitleLabel?.findConstraint(identifier: "TitleLabelHeight")?.constant = showMainMenu ? 20 + ceil(font.lineHeight) : ceil(font.lineHeight)
//            mainTitleLabel?.font = font
//            mainTitleLabel?.isHighlighted = !showMainMenu
//            mainTitleLabel?.textColor = showMainMenu ? .white : .appDarkGray
            
            let imageHeight: CGFloat = showMainMenu ? height : 5
            shadowImageView?.image = AppUIStyle.shadowImage(height: imageHeight)
            
//            mainButton?.isSelected = showMainMenu
//            mainButton?.image = showMainMenu ? #imageLiteral(resourceName: "icon_camera_capture_act") : #imageLiteral(resourceName: "tabbar_fittile_act")
//            mainButton?.selectedImage = showMainMenu ? #imageLiteral(resourceName: "icon_camera_capture_act") : #imageLiteral(resourceName: "tabbar_fittile_act")
//            mainTitleLabel?.isHighlighted = showMainMenu
//            mainTitleLabel?.text = showMainMenu ? LocalizedString("TabbarTitleFittile") : FTMenuItem.camera.tabberTitle
//
//            if !showMainMenu {
//                mainTitleLabel?.text = LocalizedString("TabbarTitleFittile")
//                mainButton?.image = #imageLiteral(resourceName: "tabbar_fittile_act")
//                mainButton?.selectedImage = #imageLiteral(resourceName: "tabbar_fittile_act")
//            }
        }
    }
    
    let defaultFontStyle = AppFontStyle.H8
//    let mainMenuHeight: CGFloat = 64
    
    fileprivate var menuView: UIView!
//    fileprivate var bgView: UIView!
    fileprivate var buttons: [UIButton]? {
        return self.menuView?.findSubviews(class: UIButton.self)
    }
    
    fileprivate var shadowImageView: UIImageView!
    
    init() {
        super.init(frame: CGRect.zero)
        autoresizesSubviews = true
        UIFont.registerFontFamily("DB Helvethaica X")
        
        let bgView = UIView.autoLayout()
        bgView.backgroundColor = .white
        addSubview(bgView)
        bgView.constraintsAroundView()?.activated()
        
        let itemView = UIStackView()
        itemView.prepareForAutoLayout()
        itemView.alignment = .center
        itemView.distribution = .fillEqually
        itemView.axis = .horizontal
        itemView.backgroundColor = .clear
        bgView.addSubview(itemView)
     
        [itemView.left == bgView.left,
         itemView.right == bgView.right,
         itemView.top == bgView.top].activated()
        
        let shadowImage = AppUIStyle.shadowImage(height: 5)
        
        let imageView = UIImageView(image: shadowImage)
        imageView.prepareForAutoLayout()
        addSubview(imageView)
        [imageView.left == left,
         imageView.right == right,
         imageView.bottom == bgView.top].activated()
        
//        let button = UIButton(type: .custom)
//        button.prepareForAutoLayout()
//        button.image = #imageLiteral(resourceName: "tabbar_fittile_act").withRenderingMode(.alwaysOriginal)
//        button.highlightedImage = #imageLiteral(resourceName: "tabbar_fittile_act").withRenderingMode(.alwaysOriginal)
//        button.selectedImage = #imageLiteral(resourceName: "tabbar_fittile_act").withRenderingMode(.alwaysOriginal)
//        button.isSelected = true
//        button.tag = 1
//        button.setContentHuggingPriority(.required, for: .horizontal)
//        button.setContentHuggingPriority(.required, for: .vertical)
//
//        let label = UILabel.autoLayout()
//        let font = AppTextStyle().font(style: defaultFontStyle)
//        label.font = font
//        label.text = LocalizedString("TabbarTitleFittile")
//        label.textColor = .appDarkGray
//        label.highlightedTextColor = .appOrange
//        label.isHighlighted = true
//        label.textAlignment = .center
//        label.isHidden = true
//        let constraint = label.height == ceil(font.lineHeight)
//        constraint.identifier = "TitleLabelHeight"
//        constraint.isActive = true
//
        for item in [FTMenuItem.catalog, FTMenuItem.camera, FTMenuItem.room, FTMenuItem.calculate] {
            let button = FTTabberButton(item: item)
            button.prepareForAutoLayout()
            button.tag = item.rawValue
            button.isSelected = item == .camera
            itemView.addArrangedSubview(button)
        }

//        addSubview(button)
//        addSubview(label)
//
//        [label.left == button.left,
//         label.right == button.right,
//         label.bottom == itemView.top,
//         label.top == button.bottom,
//         button.top == top,
//         button.centerX == itemView.centerX].activated()
//
        self.menuView = itemView
//        self.mainButton = button
//        self.mainTitleLabel = label
        self.shadowImageView = imageView
//        self.bgView = bgView
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: super.intrinsicContentSize.width, height: self.adjustHeight)
    }
    
    override func addTarget(_ target: Any?, action: Selector, for controlEvents: UIControl.Event) {
        buttons?.forEach{ $0.addTarget(target, action: action, for: controlEvents) }
    }
    
    func changeUIForChangeLanguage() {
//        guard let buttons = buttons else {
//            return
//        }
//        
//        let texts = ["TabberMovies", "TabberLocation", "TabberMyMovies", "TabberPromotion", "TabberProfile"]
//        for button in buttons {
//            button.setTitle(DGTLocalizedString(texts[safe:button.tag]), for: .normal)
//        }
    }
    
    class FTTabberButton: UIButton {
        
        convenience init(item: FTMenuItem) {
            self.init(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
            setTitle(item.tabberTitle, for: .normal)
            
            setImage(item.image.tinted(.appDarkGray), for: .normal)
            setImage(item.image.tinted(.appDarkGray), for: .normal)
            setImage(item.image.tinted(.appGreen), for: .selected)
        }
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            titleLabel?.font = AppTextStyle().font(style: .H8)
            titleLabel?.textAlignment = .center
            
            setTitleColor(.appDarkGray, for: .normal)
            setTitleColor(.appGreen, for: .highlighted)
            setTitleColor(.appGreen, for: .selected)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            let centerX = bounds.midX
            imageView?.center = CGPoint(x: centerX, y: bounds.midY-7)
            titleLabel?.frame.size.width = bounds.width
            
            if let height = titleLabel?.font.lineHeight {
                titleLabel?.frame = CGRect(x: 0, y: bounds.height-ceil(height), width: bounds.width, height: height)
            }
            titleLabel?.center.x = centerX
        }
        
        override var intrinsicContentSize: CGSize {
            let size = super.intrinsicContentSize
            return CGSize(width: size.width, height: 50)
        }
        
        override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
            super.traitCollectionDidChange(previousTraitCollection)
            titleLabel?.font = AppTextStyle().font(style: .H8)
        }
    }
}

extension FTTabbarView: UILayoutSupport {
    
    var length: CGFloat {
        return 50
    }
}

class FTNavigationBar: UINavigationBar {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        FTNavigationBar.setupDefaultStyle(to: self)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        FTNavigationBar.setupDefaultStyle(to: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        FTNavigationBar.setupDefaultStyle(to: self)
    }
    
    class func setupDefaultStyle(to navigationBar: UINavigationBar?) {
        
        navigationBar?.tintColor = .white
        navigationBar?.isTranslucent = false
        let image = UIImage(color: .appDarkGreen, size: CGSize(width: 3, height: 64))
        navigationBar?.setBackgroundImage(image?.resizableImage(1, verticalSpace: 0), for: .default)
        
        let size = CGSize(width: 3, height: 5)
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        let currentContext = UIGraphicsGetCurrentContext()
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let gradientOption = CGGradientDrawingOptions(rawValue: 0)
        
        let startComponent = UIColor.black.components
        
        let componentCount: Int = 2
        let components: [CGFloat] = [
            startComponent.red, startComponent.green,   startComponent.blue,    0.5,
            startComponent.red, startComponent.green,   startComponent.blue,    0,
            ]
        
        let locations: [CGFloat] = [0, 1]
        let gradient = CGGradient(colorSpace: colorSpace, colorComponents: components, locations: locations, count: componentCount)
        
        currentContext?.drawLinearGradient(gradient!, start: CGPoint(x: 2, y: 0), end: CGPoint(x: 2, y: size.height), options: gradientOption)
        let gradientImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        navigationBar?.shadowImage = gradientImage?.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 1, bottom: 0, right: 1))
    }
}
