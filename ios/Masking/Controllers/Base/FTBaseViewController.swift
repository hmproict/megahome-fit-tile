//
//  FTBaseViewController.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 5/2/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import Foundation
import DGTEngineSwift
import Async

class FTLayoutGuide: UILayoutGuide, UILayoutSupport {
    
    var length: CGFloat = 50
}

class FTBaseViewController: UIViewController {
    
    let bottomGuide = FTLayoutGuide()
    @IBOutlet weak var contentView: UIView?
    
    var setupBottomLayout: Bool = true {
        didSet {
            if setupBottomLayout {
                if #available(iOS 11.0, *) {
                    
                } else if view.layoutGuides.contains(bottomGuide) == false {
                    view.addLayoutGuide(bottomGuide)
                    [bottomGuide.height == 50,
                     bottomGuide.bottom == view.bottom,
                     bottomGuide.right == view.right,
                     bottomGuide.left == view.left,].activated()
                }
            } else {
                view.removeLayoutGuide(bottomGuide)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateFont(for: traitCollection)
        updateUI(for: traitCollection)
    }
    
    override var bottomLayoutGuide: UILayoutSupport {
        if setupBottomLayout {
            if #available(iOS 11.0, *) {
                
            } else if let _ = self.navigationController as? FTTabbarViewController {
                return bottomGuide
            }
        }
        return super.bottomLayoutGuide
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updateFont(for: traitCollection)
        updateUI(for: traitCollection)
    }
    
    func updateUI(for traitCollection: UITraitCollection) {
        
    }
    
    func updateFont(for traitCollection: UITraitCollection) {
        
    }
    
    func setLayoutMargin(_ margin: UIEdgeInsets, in view: UIView) {
        
        if #available(iOS 11.0, *) {
            additionalSafeAreaInsets = UIEdgeInsets(top: 0, left: margin.left, bottom: 0, right: margin.right)
        } else {
            // Fallback on earlier versions
            let leftConstraint = self.view.findConstraint(view: view, attribute: .leading)
            if let leftView = leftConstraint?.firstItem as? UIView, leftView == view {
                leftConstraint?.constant = margin.left
            } else {
                leftConstraint?.constant = -margin.left
            }
            let rightConstraint = self.view.findConstraint(view: view, attribute: .trailing)
            if let rightView = rightConstraint?.firstItem as? UIView, rightView == view {
                rightConstraint?.constant = -margin.right
            } else {
                rightConstraint?.constant = margin.right
            }
        }
    }
    
    func setupBottomLayoutGuide(in view: UIView) {
        if #available(iOS 11.0, *) {
        } else {
            setupBottomLayout = true
            let constraint = self.view.findConstraint(view: view, attribute: .bottom)
            constraint?.isActive = false
            [view.bottom == bottomGuide.top].activated()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
