//
//  FTRoomViewController.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 11/3/2564 BE.
//  Copyright © 2564 BE Aduldach Pradubyart. All rights reserved.
//

import UIKit
import DGTEngineSwift
import Flutter
import FlutterPluginRegistrant
import QRCodeReader
import AVFoundation
import PhotosUI

enum AppTileError: Error {
    case unknow
    case emptyTile
}

class FTRoomViewController: FlutterViewController {

    var flutterResult: FlutterResult?
    var isDissmiss = false
    let bottomGuide = FTLayoutGuide()
    
    fileprivate var tileSizes: [TileSize]?
    
    var statusBarStyle: UIStatusBarStyle = .lightContent {
        didSet {
            UIApplication.shared.statusBarStyle = statusBarStyle
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    var tiles = [Tile]()
    
    lazy fileprivate var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
            
            // Configure the view controller (optional)
            $0.showTorchButton = true
            $0.showSwitchCameraButton = false
            $0.showCancelButton = true
            $0.showOverlayView = true
            $0.rectOfInterest = CGRect(x: 0.2, y: 0.2, width: 0.6, height: 0.6)
        }
        
        let controller = QRCodeReaderViewController(builder: builder)
        controller.modalPresentationStyle = .formSheet
        controller.delegate = self
        return controller
    }()
    
    fileprivate let manager = FTTilePaging(serviceObject: ServiceObject(servicePath: "GET_FITTILE_DEFAULT2", maxPerPage: 20))
    
    required init() {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let engine = delegate.flutterEngine
        
        super.init(engine: engine, nibName: nil, bundle: nil)
        modalPresentationStyle = .fullScreen
        
        let channel = FlutterMethodChannel(name: "com.homepro.megahome.flutter",
                                           binaryMessenger: self.binaryMessenger)
        channel.setMethodCallHandler { (method, result) in
            self.invokeMethod(method.method, arguments: method.arguments, result: result)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AppData.prepareTileSize { [weak self] (sized) in
            self?.tileSizes = sized
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func popRoute() {
        super.popRoute()
    }
    
    override func pushRoute(_ route: String) {
        super.pushRoute(route)
    }
    
    func invokeMethod(_ method: String, arguments: Any?, result callback: FlutterResult? = nil) {
        
        print("method : \(method)")
        switch method {
        case "pop":
            if let navigationController = self.navigationController {
                navigationController.popViewController(animated: true)
                callback?(true)
            } else {
                self.dismiss(animated: true, completion: {
                    callback?(true)
                })
            }
        case "changeToWhiteStatusBarStyle":
            statusBarStyle = .lightContent
            callback?(true)
        case "changeToBlackStatusBarStyle":
            if #available(iOS 13.0, *) {
                statusBarStyle = .darkContent
            } else {
                statusBarStyle = .default
            }
            callback?(true)
        case "tileList":
            if manager.isShouldDownload {
                if manager.isLoading {
                    callback?(FlutterError(code: "02", message: "Is Loading", details: nil))
                    return;
                }
                manager.callService { [weak self] (tiles) in
                    if let tiles = tiles {
                        DLog("loaded tile : \(tiles.count)")
                        self?.tiles.append(contentsOf: tiles)
                        
                        do {
                            guard let tiles = self?.tiles else {
                                throw AppTileError.emptyTile
                            }
                            
                            let matchTiles: [Tile]
                            if let roomId = (arguments as? [String: Any])?["room_id"] as? String {
                                matchTiles = tiles.filter({ $0.area.contains(roomId) })
                            } else {
                                matchTiles = tiles
                            }
                            
                            let data = try JSONEncoder().encode(matchTiles)
                            let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                            callback?(json)
                        } catch {
                            callback?(FlutterError(code: "99", message: "Unknow Error", details: nil))
                        }
                    } else {
                        if let error = self?.manager.error {
                            callback?(FlutterError(code: "01", message: error.localizedDescription, details: nil))
                        } else {
                            callback?(FlutterError(code: "99", message: "Unknow Error", details: nil))
                        }
                    }
                }
            } else if manager.stage == .downloaded {
                
                do {
                    let matchTiles: [Tile]
                    if let roomId = (arguments as? [String: Any])?["room_id"] as? String {
                        matchTiles = tiles.filter({ $0.area.isEmpty || $0.area.contains(roomId) })
                    } else {
                        matchTiles = tiles
                    }
                    
                    let data = try JSONEncoder().encode(matchTiles)
                    let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    callback?(FlutterError(code: "00", message: "Downloaded", details: json))
                } catch {
                    callback?(FlutterError(code: "99", message: "Unknow Error", details: nil))
                }
            } else {
                callback?(FlutterError(code: "99", message: "Unknow Error", details: nil))
            }
        case "resetTileList":
            manager.reloadPageService()
            callback?(true)
        case "tileSizes":
            if let tileSizes = self.tileSizes {
                do {
                    let matchSizes: [TileSize]
                    if let type = (arguments as? [String: Any])?["type"] as? String {
                        matchSizes = tileSizes.filter({ $0.planType == type })
                    } else {
                        matchSizes = tileSizes
                    }
                    
                    let data = try JSONEncoder().encode(matchSizes.filterDuplicates(includeElement: { $0.inch == $1.inch }))
                    let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    callback?(json)
                } catch {
                    callback?(FlutterError(code: "99", message: "Parse JSON Error", details: nil))
                }
            } else {
                callback?(FlutterError(code: "12", message: "No Tile Sizes", details: nil))
            }
        case "qrCodeScanner":
            flutterResult = callback
            present(readerVC, animated: true, completion: nil)
        case "defaultRoomNo":
            callback?(NumberFormatter.decimal(AppData.appInfos.unknowRoomNameIndex))
        case "saveProject":
            if let imagePath = (arguments as? [String: Any])?["image"] as? String, let image = UIImage(contentsOfFile: imagePath) {
                print("imagePath : \(imagePath)")
                
                let name = (arguments as? [String: Any])?["name"] as? String
                let text = LocalizedString("Room") + " " + NumberFormatter.decimal(AppData.appInfos.unknowRoomNameIndex)
                if name == text {
                    AppData.appInfos.unknowRoomNameIndex += 1
                }
                
                let project = TileProject()
                project.title = name ?? text
                project.completed = true
                if let imageName = AppUtility.saveImageToDocument(image, folder: .screenShot) {
                    project.screenshotImageName = imageName
                }
                
                do {
                    if let textTiles = (arguments as? [String: Any])?["tiles"] as? String,
                       let rawTiles = textTiles.data(using: .utf8) {
                        let tiles = try JSONDecoder().decode([Tile].self, from: rawTiles)
                        for tile in tiles {
                            project.addArtboard()
                            project.artboard.tile = tile
                        }
                    }
                } catch {
                    callback?(FlutterError(code: "09", message: "No Tiles", details: nil))
                    return;
                }
                
                AppData.save(project)
                if AppUtility.fileExists(project.screenshotImageName, in: .screenShot) {
                    let url = AppUtility.pathFile(project.screenshotImageName, in: .screenShot)
                    AppUtility.authenPhotoLibraryAlbum { status in
                        switch status {
                        case .allow:
                            AppUtility.createPhotoLibraryAlbum(name: "Fittile by HomePro") { (album) in
                                if let album = album {
                                    let urls: [URL]
                                    if let topImagePath = (arguments as? [String: Any])?["top_image"] as? String,
                                       let topImageURL = URL(string: topImagePath) {
                                        urls = [URL(string: url)!, topImageURL]
                                    } else {
                                        urls = [URL(string: url)!]
                                    }
                                    
                                    AppUtility.saveImage(urls: urls, to: album, completion: { (success) in
                                        if success {
                                            DLog("Save image to album fittile complete.")
                                        } else {
                                            DLog("Can't save image to album fittile.")
                                        }
                                        callback?(true)
                                    })
                                } else {
                                    callback?(true)
                                    DLog("Can't create album fittile.")
                                }
                            }
                        case .limit:
                            let urls: [String]
                            if let topImagePath = (arguments as? [String: Any])?["top_image"] as? String {
                                urls = [url, topImagePath]
                            } else {
                                urls = [url]
                            }
                            for url in urls {
                                if let image = UIImage(contentsOfFile: url) {
                                    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                                }
                            }
                            callback?(true)
                        case .none:
                            callback?(true)
                        }
                    }
                } else {
                    callback?(true)
                }
            } else {
                callback?(FlutterError(code: "07", message: "No Image File", details: nil))
            }
        default:
            callback?(FlutterMethodNotImplemented)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.navigationItem.hidesBackButton = true
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if isDissmiss {
//            let delegate = UIApplication.shared.delegate as? AppDelegate
//            delegate?.flutterEvent.clearPage()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.hidesBackButton = false
        setNeedsStatusBarAppearanceUpdate()
        isDissmiss = isBeingDismissed
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return statusBarStyle
    }
    
    override var bottomLayoutGuide: UILayoutSupport {
        if #available(iOS 11.0, *) {
            
        } else if let _ = self.navigationController as? FTTabbarViewController {
            return bottomGuide
        }
        return super.bottomLayoutGuide
    }
}

extension FTRoomViewController: QRCodeReaderViewControllerDelegate {
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.dismiss(animated: true, completion: nil)
        if let url = URL(string: result.value), let tileId = AppUtility.parameters(for: url)["id_scan"] {
            
            DLog("tileId : \(tileId)")
            AppDelegateObject.displayLoadingView()
            
            AppService.call({ try $0.tile(id: tileId) }) { [weak self] (tile, error) in
                AppDelegateObject.hideLoadingView()
                if let tile = tile {
                    
                    if let result = self?.flutterResult, let data = try? JSONEncoder().encode(tile), let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) {
                        result(json)
                    }
                    AppData.write {
                        AppData.realm.add(tile, update: .modified)
                    }
                } else {
                    DLog("error : \(error?.localizedDescription ?? "No Error")")
                    let alert = UIAlertController(title: LocalizedString("error"), message: LocalizedString("LoadTileError"), preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: LocalizedString("ok"), style: .default, handler: nil))
                    self?.present(alert, animated: true, completion: nil)
                    
                    if let result = self?.flutterResult {
                        result(FlutterError(code: "04", message: LocalizedString("LoadTileError"), details: nil))
                    }
                }
                self?.flutterResult = nil
            }
        } else {
            let alert = UIAlertController(title: LocalizedString("error"), message: LocalizedString("QRCodeIncorrectFormatError"), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: LocalizedString("ok"), style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            if let result = self.flutterResult {
                result(FlutterError(code: "05", message: LocalizedString("QRCodeIncorrectFormatError"), details: nil))
            }
            self.flutterResult = nil
        }
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.dismiss(animated: true, completion: nil)
    }
}
