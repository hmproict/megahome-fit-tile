//
//  FTCatalogDetailViewController.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 14/2/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import Foundation
import DGTEngineSwift
import SafariServices
import TUSKit
import Async

class FTCatalogDetailViewController: FTBaseViewController {
    
    enum Style {
        case tile
        case project
        case saveproject
    }
    
    enum CellStyle {
        case cover
        case segment
        case info
        case qrcode
        case confirm
        case staff
    }
    
    @IBOutlet internal weak var tableView: UITableView!
    
    var data: Any? {
        didSet {
            reloadCellPattarn()
            if isViewLoaded {
                self.tableView?.reloadData()
            }
        }
    }
    
    var style: Style = .tile {
        didSet {
            switch style {
            case .project:
                AppDelegateObject.sendAppAnalytics("Project Detail")
            case .tile:
                AppDelegateObject.sendAppAnalytics("Tile Info")
            default:
                AppDelegateObject.sendAppAnalytics("Project Save")
            }
        }
    }
    
    var currentTile: Tile? {
        switch style {
        case .tile: return data as? Tile
        case .project: return (data as? TileProject)?.artboard.tile
        case .saveproject: return (data as? TileProject)?.artboard.tile
        }
    }
    
    var tileProject: TileProject? {
        switch style {
        case .project: return data as? TileProject
        case .saveproject: return data as? TileProject
        default: return nil
        }
    }
    
    fileprivate var cellPattarn = [CellStyle]()
    
    fileprivate var transitionAnimator: CameraTransitionAnimator?
    fileprivate var backFromFullScreen: Bool = false
    fileprivate var isProcessing: Bool = false {
        didSet {
            self.confirmButton?.isEnabled = !isProcessing
            if isProcessing {
                confirmButton?.startLoadingAnimation()
            } else {
                confirmButton?.stopLoadingAnimation()
            }
        }
    }
    fileprivate var uploadId: String?
    fileprivate var confirmButton: ConfirmButton? {
        
        if let index = cellPattarn.firstIndex(of: .info) {
            let cell = self.tableView?.cellForRow(at: IndexPath(row: index, section: 0)) as? FTTileDetailInfoTableViewCell
            return cell?.confirmButton
        } else if let index = cellPattarn.firstIndex(of: .qrcode) {
            let cell = self.tableView?.cellForRow(at: IndexPath(row: index, section: 0)) as? FTTileDetailQRTableViewCell
            return cell?.confirmButton
        }
        return nil
    }
    fileprivate var isAfterImage: Bool = true
    
    // MARK: View controller life circle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = LocalizedString("CatalogDetailTitle")
        
        if #available(iOS 11.0, *) {
            tableView?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)
        }
        setupBottomLayoutGuide(in: tableView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.delegate = self
        navigationItem.backBarButtonItem = UIBarButtonItem(title: LocalizedString("back"), style: .plain, target: nil, action: nil)
        
        if isMovingToParent == false {
            let tabbarController = self.navigationController as? FTTabbarViewController
            if tabbarController?.showTabber == false {
                tabbarController?.showTabber = true
            }
            if tabbarController?.activeMainMenu == true {
                tabbarController?.activeMainMenu = false
                tabbarController?.selectedMenuIndex = FTMenuItem.catalog.rawValue
                backFromFullScreen = true
            }
        }
        
        switch style {
        case .saveproject:
            self.title = nil
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            if tileProject?.completed == false {
                self.navigationItem.hidesBackButton = true
                uploadProject()
            }
            if tileProject?.shareProject == true {
                self.title = tileProject?.title
            }
        case .project where tileProject?.shareProject == false && tileProject?.completed == false && isMovingToParent:
            reloadShareProject(showError: false)
            fallthrough
        case .project:
            self.title = tileProject?.title
        default: break
        }
        self.tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isMovingToParent == false {
            
            if backFromFullScreen {
                backFromFullScreen = false
                let tabbarController = self.navigationController as? FTTabbarViewController
                tabbarController?.isNavigationBarHidden = true
                tabbarController?.isNavigationBarHidden = false
            }
        }
        
        if style == .saveproject && isMovingToParent && tileProject?.shareProject == false {
            
            let textField: UITextField = {
                let frame = CGRect(x: 0, y: 0, width: view.bounds.width/2, height: 21)
                let textField = UITextField(frame: frame)
                textField.textAlignment = .center
                textField.borderStyle = .none
                textField.font = UIFont.customFont(24)
                textField.textColor = .white
                textField.attributedPlaceholder = NSAttributedString(string: LocalizedString("CatalogTitlePlaceholder"), attributes: [.font: UIFont.customFont(24), .foregroundColor: UIColor.appLightGray])
                textField.returnKeyType = .done
                textField.background = {
                    UIGraphicsBeginImageContextWithOptions(frame.size, false, 0.0)
                    
                    let context = UIGraphicsGetCurrentContext()
                    UIColor.white.setStroke()
                    context?.setLineWidth(1)
                    context?.move(to: CGPoint(x: 0, y: frame.height-0.5))
                    context?.addLine(to: CGPoint(x: frame.width, y: frame.height-0.5))
                    context?.strokePath()
                    
                    let destImage = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                    return destImage
                }()
                return textField
            }()
            textField.delegate = self
            self.navigationItem.titleView = textField
            textField.becomeFirstResponder()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: User Action Methods
    
    @IBAction internal func onUserTryTile(_ button: UIButton) {
        
        let tabberViewController = self.navigationController as? FTTabbarViewController
        tabberViewController?.selectedMenuIndex = FTMenuItem.camera.rawValue
        tabberViewController?.activeMainMenu = true
        let controller = FTFittileOptionViewController()
        controller.tile = self.currentTile?.copy()
        tabberViewController?.pushViewController(controller, animated: true)
    }
    
    @IBAction internal func onUserOption(_ button: UIButton) {
        
        switch style {
        case .saveproject where tileProject?.completed == false:
            if let project = tileProject {
                AppService.call({ try $0.deleteShareProject(project: project) }) { _,_  in }
            }
            fallthrough
        case .saveproject:
            self.navigationController?.popToRootViewController(animated: true)
        case .project where tileProject?.completed == false:
            if let project = tileProject {
                AppData.write {
                    AppData.realm.delete(project)
                    if let controller = self.navigationController?.viewControllers.first as? FTMyProjectViewController {
                        controller.requiredReloadData = true
                    }
                    self.navigationController?.popViewController(animated: true)
                }
            }
        default:
            let tabberViewController = self.navigationController as? FTTabbarViewController
            let controller = tabberViewController?.storyboard?.instantiateViewController(class: FTCalculateViewController.self)
            let tile = self.tileProject?.artboards[safe: button.tag]?.tile ?? self.currentTile
            controller?.preferTile = tile?.copy()
            if let controller = controller {
                tabberViewController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    @IBAction internal func onUserConfirm(_ button: UIButton) {
        
        switch style {
        case .saveproject where tileProject?.shareProject == true && tileProject?.completed == true:
            if isProcessing {
                
            } else {
                uploadCompleteProject()
            }
        case .saveproject where tileProject?.completed == false:
            if isProcessing {
                
            } else {
                fallthrough
            }
        case .saveproject:
            if let textField = self.navigationItem.titleView as? UITextField {
                let project = self.data as! TileProject
                project.title = {
                    if let text = textField.text, text.isEmpty == false {
                        return text
                    }
                    let text = LocalizedString("Room") + " " + NumberFormatter.decimal(AppData.appInfos.unknowRoomNameIndex)
                    AppData.appInfos.unknowRoomNameIndex += 1
                    return text
                }()
            }
            
            guard let project = self.tileProject else {
                self.navigationController?.popToRootViewController(animated: true)
                return
            }
            
            if project.completed == false {
                let copyProject: TileProject = project.copy()
                AppService.call({ try $0.editShareProject(project: copyProject) }) { (success, error) in
                    if success == true {
                        
                    }
                }
            }
            
            AppData.save(project)
            if AppUtility.fileExists(project.screenshotImageName, in: .screenShot) {
                let url = AppUtility.pathFile(project.screenshotImageName, in: .screenShot)
                AppUtility.authenPhotoLibraryAlbum { status in
                    
                    switch status {
                    case .allow:
                        AppUtility.createPhotoLibraryAlbum(name: "Fittile by HomePro") { (album) in
                            if let album = album {
                                AppUtility.saveImage(urls: [URL(string: url)!], to: album, completion: { (success) in
                                    if success {
                                        DLog("Save image to album fittile complete.")
                                    } else {
                                        DLog("Can't save image to album fittile.")
                                    }
                                })
                            } else {
                                DLog("Can't create album fittile.")
                            }
                        }
                    case .limit:
                        if let image = UIImage(contentsOfFile: url) {
                            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                        }
                    case .none:
                        break;
                    }
                }
            }
            self.navigationController?.popToRootViewController(animated: true)
        case .project where tileProject?.shareProject == true && tileProject?.completed == false:
            
            if let drawController = self.storyboard?.instantiateViewController(class: FTDrawViewController.self),
                let project = tileProject, project.imageName != "" {
                
                let project: TileProject = project.copy()
                project.artboard.tile = AppData.realm.objects(Tile.self).first?.copy()
                drawController.project = project
                
                self.backFromFullScreen = true
                let tabberViewController = self.navigationController as? FTTabbarViewController
                tabberViewController?.setNavigationBarHidden(true, animated: true)
                tabberViewController?.showTabber = false
                tabberViewController?.pushViewController(drawController, animated: true)
            }
        case .project where tileProject?.completed == false:
            reloadShareProject(showError: true)
        default:
            if let tile = self.tileProject?.artboards[safe: button.tag]?.tile {
                let page = style == .project ? "bn_project" : "bn_product"
                let url = "https://www.homepro.co.th/p/\(tile.id)?utm_source=fittile&utm_medium=\(page)"
                let controller = SFSafariViewController(url: URL(string: url)!)
                controller.preferredBarTintColor = .appGreen
                controller.preferredControlTintColor = .white
                self.present(controller, animated: true, completion: nil)
            } else {
                let url = "https://www.homepro.co.th"
                let controller = SFSafariViewController(url: URL(string: url)!)
                controller.preferredBarTintColor = .appGreen
                controller.preferredControlTintColor = .white
                self.present(controller, animated: true, completion: nil)
            }
        }
    }
    
    @objc internal func onSeeMore(_ button: UIButton) {
        let webViewURL: URL
        if let url = self.tileProject?.artboards[safe: button.tag]?.tile?.url, let webURL = URL(string: url) {
            webViewURL = webURL
        } else if let url = self.currentTile?.url, let webURL = URL(string: url) {
            webViewURL = webURL
        } else {
            webViewURL = URL(string: "https://www.homepro.co.th")!
        }
        let controller = SFSafariViewController(url: webViewURL)
        controller.preferredBarTintColor = .appGreen
        controller.preferredControlTintColor = .white
        self.present(controller, animated: true, completion: nil)
    }
    
    @objc internal func onDeleteProject() {
        
        guard let project = data as? TileProject else {
            return
        }
        
        if !project.shareProject && !project.completed {
            let copyProject: TileProject = project.copy()
            AppService.call({ try $0.deleteShareProject(project: copyProject) }) { (success, error) in
                if success == true {
                    
                }
            }
        }
        
        AppData.write {
            AppData.realm.delete(project)
            if let controller = self.navigationController?.viewControllers.first as? FTMyProjectViewController {
                controller.requiredReloadData = true
            }
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc internal func onShowStaffOnly() {
        
        let style = AppUIStyle(traitCollection: traitCollection)
        let view = UIView.autoLayout()
        view.backgroundColor = .white
        view.layoutMargins = style.margin
        view.layer.cornerRadius = style.cornerRedius
        view.layer.masksToBounds = true
        
        let titleLabel: UILabel = {
            let label = UILabel.autoLayout()
            label.text = LocalizedString("CatalogForSaleManTitle")
            label.textColor = .appGreen
            label.font = AppTextStyle(traitCollection: traitCollection).font(style: .H7)
            label.sizeToFit()
            label.textAlignment = .center
            label.setContentHuggingPriority(.required, for: .vertical)
            label.setContentCompressionResistancePriority(.required, for: .vertical)
            label.setContentHuggingPriority(.required, for: .horizontal)
            label.setContentCompressionResistancePriority(.required, for: .horizontal)
            return label
        }()
        
        let imageView = UIImageView(image: #imageLiteral(resourceName: "barcode"))
        imageView.prepareForAutoLayout()
        imageView.setContentHuggingPriority(.required, for: .vertical)
        imageView.setContentCompressionResistancePriority(.required, for: .vertical)
        
        view.addSubview(titleLabel)
        view.addSubview(imageView)
        
        [titleLabel.left == view.leftMargin,
         titleLabel.right == view.rightMargin,
         titleLabel.top == view.topMargin + style.space].activated()
        [imageView.top == titleLabel.bottom + style.space*2,
         imageView.centerX == view.centerX,
         imageView.bottom == view.bottomMargin].activated()
        
        let controller = UIViewController()
        let size = view.systemLayoutSizeFitting(self.view.bounds.size)
        controller.preferredContentSize = size
        controller.view.addSubview(view)
        
        let popoverController = FTPopoverViewController(contentViewController: controller)
        popoverController.dismissWhenTouchBackground = true
        self.present(popoverController, animated: true, completion: nil)
    }
    
    // MARK: Private Methods
    
    private func setupNavigationItem() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_delete"), style: .plain, target: self, action: #selector(self.onDeleteProject))
    }
    
    private func showProgressBar(percent: CGFloat) {
        
        guard let navigationBar = self.navigationController?.navigationBar else {
            return
        }
        
        let size = CGSize(width: navigationBar.bounds.width, height: 2)
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        let context = UIGraphicsGetCurrentContext()
        
        UIColor(grayscalCode: 222).setFill()
        context?.fill(CGRect(origin: .zero, size: size))
        
        context?.saveGState()
        UIColor.white.setFill()
        let progressWidth = percent * size.width / 100
        DLog("progressWidth : \(progressWidth) : size")
        context?.fill(CGRect(origin: .zero, size: CGSize(width: progressWidth, height: size.height)))
        context?.restoreGState()
        
        let resultImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        navigationBar.shadowImage = resultImage
    }
    
    private func uploadProject() {
        
        guard let project = tileProject else {
            return
        }
        
        self.isProcessing = true
        AppService.call({try $0.createShareProject(project: project) }) { (qrcodeURL, error) in
            if let qrcodeURL = qrcodeURL {
                project.qrcodeURL = qrcodeURL
                self.tableView?.reloadData()
            } else {
                let alert = UIAlertController(title: LocalizedString("error"), message: LocalizedString("UploadTileProjectError"), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: LocalizedString("ok"), style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        let session = AppService.call.uploadSession
        let resumeUpload: TUSResumableUpload? = {
            if let uploadId = self.uploadId, let upload = session.restoreUpload(uploadId) {
                return upload
            }
            let url = AppUtility.pathFile(project.imageName, in: .image)
            let upload = session.createUpload(fromFile: URL(fileURLWithPath: url), retry: 0, headers: nil, metadata: ["transaction_id": project.id, "stage": "initial"])
            upload?.setChunkSize(1000000)
            self.uploadId = upload?.uploadId
            return upload
        }()
        
        guard let upload = resumeUpload else {
            let alert = UIAlertController(title: LocalizedString("error"), message: LocalizedString("UploadTileProjectError"), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: LocalizedString("ok"), style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        upload.resultBlock = { url in
            self.isProcessing = false
            FTNavigationBar.setupDefaultStyle(to: self.navigationController?.navigationBar)
            self.tableView?.reloadData()
        }
        upload.failureBlock = { error in
            self.isProcessing = false
            FTNavigationBar.setupDefaultStyle(to: self.navigationController?.navigationBar)
            self.confirmButton?.title = LocalizedString("ProjectButtonReupload")
            DLog("error : \(error.localizedDescription)")
            let alert = UIAlertController(title: LocalizedString("error"), message: LocalizedString("UploadTileProjectError"), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: LocalizedString("ok"), style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            self.tableView?.reloadData()
        }
        upload.progressBlock = { progess, total in
            DLog("progess : \(progess) | \(total)")
            let percent: CGFloat = (CGFloat(progess) * 100) / CGFloat(total)
            DLog("percent : \(percent)")
            self.showProgressBar(percent: percent)
        }
        upload.resume()
    }
    
    private func uploadCompleteProject() {
        
        guard let project = tileProject else {
            return
        }
        
        self.isProcessing = true
        self.confirmButton?.title = LocalizedString("ProjectButtonUploading")
        
        let session = AppService.call.uploadSession
        let resumeUpload: TUSResumableUpload? = {
            if let uploadId = self.uploadId, let upload = session.restoreUpload(uploadId) {
                return upload
            }
            
            let tileIds = project.artboards.compactMap({ $0.tile?.id }).joined(separator: ",")
            let url = AppUtility.pathFile(project.screenshotImageName, in: .screenShot)
            let upload = session.createUpload(fromFile: URL(fileURLWithPath: url), retry: 0, headers: nil, metadata: ["transaction_id": project.id, "stage": "complete", "tile_id": tileIds])
            upload?.setChunkSize(1000000)
            self.uploadId = upload?.uploadId
            return upload
        }()
        
        guard let upload = resumeUpload else {
            let alert = UIAlertController(title: LocalizedString("error"), message: LocalizedString("UploadTileProjectError"), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: LocalizedString("ok"), style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        upload.resultBlock = { url in
            self.isProcessing = false
            FTNavigationBar.setupDefaultStyle(to: self.navigationController?.navigationBar)
            AppData.write {
                if let project = AppData.realm.objects(TileProject.self).first(where: { $0.id == project.id }) {
                    AppData.realm.delete(project)
                }
                if let controller = self.navigationController?.viewControllers.first as? FTMyProjectViewController {
                    controller.requiredReloadData = true
                }
                self.navigationController?.popToRootViewController(animated: true)
            }
            self.tableView?.reloadData()
        }
        upload.failureBlock = { error in
            self.isProcessing = false
            FTNavigationBar.setupDefaultStyle(to: self.navigationController?.navigationBar)
            self.confirmButton?.title = LocalizedString("ProjectButtonReupload")
            self.tableView?.reloadData()
            DLog("error : \(error.localizedDescription)")
            let alert = UIAlertController(title: LocalizedString("error"), message: LocalizedString("UploadTileProjectError"), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: LocalizedString("ok"), style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        upload.progressBlock = { progess, total in
            DLog("progess : \(progess) | \(total)")
            let percent: CGFloat = (CGFloat(progess) * 100) / CGFloat(total)
            DLog("percent : \(percent)")
            self.showProgressBar(percent: percent)
        }
        upload.resume()
    }
    
    private func reloadShareProject(showError: Bool = true) {
        
        guard let project = tileProject else {
            return
        }
        
        isProcessing = true
        self.confirmButton?.title = LocalizedString("ProjectButtonPreparing")
        
        let tiles = AppData.realm.objects(Tile.self)
        let tileList = Array(tiles).map({ $0.copy() as Tile })
        let transactionId = project.id
        AppService.async({ [weak self] in
            
            let project = try AppService.call.tileProject(transactionId: transactionId)
            if project.completed {
                for artboard in project.artboards where artboard.tile?.id != nil {
                    if let matchTile = tileList.first(where: {$0.id == artboard.tile?.id}) {
                        artboard.tile = matchTile
                    } else {
                        artboard.tile = try AppService.call.tile(id: artboard.tile!.id)
                    }
                }
                Async.main {
                    self?.isProcessing = false
                    self?.data = project
                    let controller = self?.navigationController?.viewControllers.first as? FTMyProjectViewController
                    controller?.requiredReloadData = true
                    AppData.write {
                        AppData.realm.add(project, update: .modified)
                    }
                    self?.tableView?.reloadData()
                }
            } else {
                Async.main {
                    self?.isProcessing = false
                    self?.confirmButton?.title = LocalizedString("ProjectButtonReceive")
                    self?.tableView?.reloadData()
                }
            }
        }) { [weak self] (error) in
            DLog("error : \(error.localizedDescription)")
            self?.isProcessing = false
            self?.confirmButton?.title = LocalizedString("ProjectButtonReceive")
            if showError {
                let alert = UIAlertController(title: LocalizedString("error"), message: LocalizedString("LoadProjectError"), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: LocalizedString("ok"), style: .default, handler: nil))
                self?.present(alert, animated: true, completion: nil)
            }
            self?.tableView?.reloadData()
        }
    }
    
    private func reloadCellPattarn() {
        switch style {
        case .tile:
            self.cellPattarn = [.cover, .info, .staff]
        case .project where tileProject?.qrcodeURL.isEmpty == false:
            self.cellPattarn = [.cover, .qrcode]
        case .project where tileProject?.completed == false:
            self.cellPattarn = [.cover, .confirm]
        case .project where tileProject?.noOriginImage == true:
            self.cellPattarn = [.cover]
            let artboards = tileProject?.artboards.count ?? 0;
            for _ in 0..<artboards {
                cellPattarn.append(.info)
            }
            cellPattarn.append(.staff)
        case .project:
            self.cellPattarn = [.cover, .segment]
            let artboards = tileProject?.artboards.count ?? 0;
            for _ in 0..<artboards {
                cellPattarn.append(.info)
            }
            cellPattarn.append(.staff)
        case .saveproject where tileProject?.shareProject == true && tileProject?.completed == false:
            self.cellPattarn = [.cover, .qrcode]
        case .saveproject where tileProject?.completed == false:
            self.cellPattarn = [.cover, .qrcode]
        case .saveproject where tileProject?.imageName.isEmpty == true || tileProject?.screenshotImageName.isEmpty == true:
            self.cellPattarn = [.cover, .info, .staff]
        case .saveproject:
            self.cellPattarn = [.cover, .segment, .info, .staff]
        }
    }
}

extension FTCatalogDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellPattarn.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let style = cellPattarn[indexPath.row]
        switch style {
        case .cover:
            let cell = tableView.dequeueReusableCell(class: FTTileDetailImageTableViewCell.self, for: indexPath)
            switch self.style {
            case .tile where currentTile != nil:
                cell.setupElement(tile: currentTile!, completion: {
                    tableView.reloadData()
                })
            case .project where tileProject != nil:
                cell.fitTileButton.isHidden = true
                
                let afterImage = tileProject!.completed && (isAfterImage || tileProject!.noOriginImage);
                cell.setupElement(project: tileProject!, isAfterImage: afterImage, completion: {
                    tableView.reloadData()
                })
            case .saveproject where tileProject != nil && tileProject!.shareProject && tileProject!.completed == false:
                cell.fitTileButton.isHidden = true
                cell.setupElement(shareProject: tileProject!) {
                    tableView.reloadData()
                }
            case .saveproject where tileProject != nil:
                cell.fitTileButton.isHidden = true
                cell.setupElement(project: tileProject!, isAfterImage: tileProject!.completed && isAfterImage, completion: {
                    tableView.reloadData()
                })
            default:
                break
            }
            cell.fitTileButton.addTarget(self, action: #selector(self.onUserTryTile(_:)), for: .touchUpInside)
            return cell
        case .info:
            let cell = tableView.dequeueReusableCell(class: FTTileDetailInfoTableViewCell.self, for: indexPath)
            switch self.style {
            case .tile where currentTile != nil:
                cell.setupElement(tile: currentTile!)
            case .saveproject where tileProject != nil:
                let index = cellPattarn.firstIndex(of: .info) ?? 0
                cell.setupElement(project: tileProject!, index: indexPath.row - index)
                
                if tileProject!.shareProjectComplete {
                    cell.confirmButton?.title = LocalizedString("ProjectButtonComplete")
                    cell.altButton?.isHidden = true
                } else {
                    cell.confirmButton?.title = LocalizedString("ProjectButtonSave")
                    cell.altButton?.title = LocalizedString("ProjectButtonDelete")
                }
            default:
                let index = cellPattarn.firstIndex(of: .info) ?? 0
                if let project = tileProject {
                    cell.setupElement(project: project, index: indexPath.row - index)
                    
                    if project.shareProject {
                        if !project.completed {
                            cell.confirmButton?.title = LocalizedString("ProjectButtonFittile")
                        } else if !project.qrcodeURL.isEmpty {
                            cell.confirmButton?.title = LocalizedString("ProjectButtonPreparing")
                        }
                    }
                }
            }
            cell.seemoreButton.addTarget(self, action: #selector(self.onSeeMore(_:)), for: .touchUpInside)
            cell.confirmButton.addTarget(self, action: #selector(self.onUserConfirm(_:)), for: .touchUpInside)
            cell.altButton.addTarget(self, action: #selector(self.onUserOption(_:)), for: .touchUpInside)
            return cell
        case .qrcode:
            let cell = tableView.dequeueReusableCell(class: FTTileDetailQRTableViewCell.self, for: indexPath)
            if let project = tileProject {
                cell.setupElement(project: project, completion: {
                    tableView.reloadData()
                })
                
                switch self.style {
                case .project where isProcessing:
                    cell.confirmButton?.title = LocalizedString("ProjectButtonPreparing")
                case .project:break
                case .saveproject where isProcessing:
                    cell.confirmButton.title = LocalizedString("ProjectButtonUploading")
                case .saveproject where project.qrcodeURL.isEmpty:
                    cell.confirmButton.title = LocalizedString("ProjectButtonReupload")
                case .saveproject:
                    cell.confirmButton.title = LocalizedString("ProjectButtonSave")
                default:break
                }
            }
            return cell
        case .segment:
            let cell = tableView.dequeueReusableCell(class: FTTileDetailSegmentTableViewCell.self, for: indexPath)
            cell.addSegmentChangedAction { (index) in
                self.isAfterImage = index != 0
                tableView.startBatchUpdates({
                    tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
                })
            }
            return cell
        case .confirm:
            let cell = tableView.dequeueReusableCell(class: FTTileDetailConfirmTableViewCell.self, for: indexPath)
            switch self.style {
            case .project where tileProject?.qrcodeURL.isEmpty == false:
                cell.confirmButton?.title = LocalizedString("ProjectButtonReceive")
            default:
                cell.confirmButton?.title = LocalizedString("ProjectButtonFittile")
            }
            cell.confirmButton.addTarget(self, action: #selector(self.onUserConfirm(_:)), for: .touchUpInside)
            return cell
        case .staff:
            let cell = tableView.dequeueReusableCell(class: FTTileDetailStaffTableViewCell.self, for: indexPath)
            cell.sellManLabel.addAction(forTarget: self, selector: #selector(self.onShowStaffOnly))
            return cell
        }
    }
}

extension FTCatalogDetailViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return nil
    }
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        if (toVC is FTFittileOptionViewController && operation == .push) ||
            (fromVC is FTFittileOptionViewController && operation == .pop) {
            // Pass the thumbnail frame to the transition animator.
            if operation == .push {
                transitionAnimator = CameraTransitionAnimator()
            }
            transitionAnimator?.operation = operation
            return transitionAnimator
        }
        return nil
    }
}

extension FTCatalogDetailViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
