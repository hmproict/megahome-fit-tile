//
//  FTCatalogListViewController.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 7/2/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import Foundation
import DGTEngineSwift
import Async
import QRCodeReader
import AVFoundation

protocol FTCatalogListViewControllerDelegate: class {
    func catalogList(_ catalogList: FTCatalogListViewController, didSelectedTile tile: Tile)
}

enum FTCatalogStyle: Int {
    case full
    case mini
}

class FTCatalogListViewController: FTBaseViewController {
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.prepareForAutoLayout()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .clear
        collectionView.register(FTTileCatalogCollectionViewCell.self)
        collectionView.register(FTTileQRCodeCollectionViewCell.self)
        collectionView.register(DownloadCollectionViewCell.self)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.indicatorStyle = style == .full ? .black : .white
        collectionView.alwaysBounceVertical = true
        collectionView.alwaysBounceHorizontal = false
        return collectionView
    }()
    
    var style: FTCatalogStyle = .full {
        didSet {
            self.contentWidth = FTTileCatalogCollectionViewCell.contentSize(for: self.traitCollection, containtIn: view.bounds.width, catalogStyle: style)
            collectionView.indicatorStyle = style == .full ? .black : .white
            collectionView.collectionViewLayout.invalidateLayout()
            DispatchQueue.main.async(execute: collectionView.reloadData)
        }
    }
    
    var planType: Tile.PlanType? {
        didSet {
            if isViewLoaded {
                reloadFilterTile()
                DispatchQueue.main.async(execute: collectionView.reloadData)
            }
        }
    }
    var showQRCode: Bool = true
    
    var filterTiles: [Tile]?
    var selectedTile: Tile?
    var contentWidth: CGFloat = 0
    var requiredReloadTile: Bool = true
    var preferredTileId: String?
    
    weak var delegate: FTCatalogListViewControllerDelegate?
    
    fileprivate var tileSizes: [TileSize]?
    fileprivate var filterSize: TileSize?
    fileprivate var filterUnit: TileSize.Unit = .inch
    fileprivate let manager: FTTilePaging
    fileprivate var selectedTileIndex: Int?
    fileprivate var showQRInCollection: Bool {
        return style == .mini && showQRCode
    }
    
    lazy fileprivate var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
            
            // Configure the view controller (optional)
            $0.showTorchButton = true
            $0.showSwitchCameraButton = false
            $0.showCancelButton = true
            $0.showOverlayView = true
            $0.rectOfInterest = CGRect(x: 0.2, y: 0.2, width: 0.6, height: 0.6)
        }
        
        let controller = QRCodeReaderViewController(builder: builder)
        controller.delegate = self
        return controller
    }()
    fileprivate var backFromFullScreen: Bool = false
    fileprivate var willRotateOraintation: Bool = false
    fileprivate var filterButton: UIButton?
    
    fileprivate var contentMaxWidth: CGFloat {
        if #available(iOS 11.0, *) {
            return view.bounds.width - (self.view.safeAreaInsets.left + self.view.safeAreaInsets.right)
        }
        return view.bounds.width
    }
    
    required init(style: FTCatalogStyle, planType: Tile.PlanType? = nil) {
        self.manager = FTTilePaging(serviceObject: ServiceObject(servicePath: "GET_FITTILE_DEFAULT2", maxPerPage: 10))
        if let planType = planType {
            manager.serviceObject.param = ["type": planType == .wall ? "wall" : "floor"]
        }
        manager.page = AppData.tilePageIndex
        
        super.init(nibName: nil, bundle: nil)
        self.style = style
        if style == .full {
            let bgView = FTBGView.autoLayout()
            view.insertSubview(bgView, at: 0)
            bgView.constraintsAroundView()?.activated()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.manager = FTTilePaging(serviceObject: ServiceObject(servicePath: "GET_FITTILE_DEFAULT2", maxPerPage: 10))
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = FTMenuItem.catalog.pageTitle
        
        view.addSubview(collectionView)
        collectionView.constraintsAroundView()?.activated()
        if #available(iOS 11.0, *) {
            collectionView.contentInsetAdjustmentBehavior = .always
        }
        
        self.contentWidth = FTTileCatalogCollectionViewCell.contentSize(for: self.traitCollection, containtIn: view.bounds.width, catalogStyle: style)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: LocalizedString("back"), style: .plain, target: nil, action: nil)
        setupNavigationItem()
        
        if style == .full {
            self.navigationController?.isNavigationBarHidden = false
        }
        
        if requiredReloadTile || isMovingToParent {
            requiredReloadTile = false
            
            AppData.prepareTileSize { [weak self] (sized) in
                self?.tileSizes = sized
            }
        }
        
        if style == .full && isMovingToParent == false {
            let tabbarController = self.navigationController as? FTTabbarViewController
            if tabbarController?.showTabber == false {
                tabbarController?.showTabber = true
            }
            if tabbarController?.activeMainMenu == true {
                tabbarController?.activeMainMenu = false
                tabbarController?.selectedMenuIndex = FTMenuItem.catalog.rawValue
                backFromFullScreen = true
            }
        }
    
        if let tileId = preferredTileId {
            prepareTileData(with: tileId)
            preferredTileId = nil
        }
        
        let page = manager.page
        manager.page = max(manager.page, AppData.tilePageIndex)
        if (page != manager.page) {
            DispatchQueue.main.async(execute: collectionView.reloadData)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if style == .full && isMovingToParent == false {
            if backFromFullScreen {
                backFromFullScreen = false
                let tabbarController = self.navigationController as? FTTabbarViewController
                tabbarController?.isNavigationBarHidden = true
                tabbarController?.isNavigationBarHidden = false
            }
        }
        
        if style == .full {
            self.navigationController?.isNavigationBarHidden = false
        }
        
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        guard willRotateOraintation == false else {
            return
        }
        
        self.contentWidth = FTTileCatalogCollectionViewCell.contentSize(for: self.traitCollection, containtIn: self.contentMaxWidth, catalogStyle: style)
        collectionView.collectionViewLayout.invalidateLayout()
        DispatchQueue.main.async(execute: collectionView.reloadData)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        willRotateOraintation = true
        self.contentWidth = FTTileCatalogCollectionViewCell.contentSize(for: self.traitCollection, containtIn: size.width, catalogStyle: self.style)
        self.collectionView.collectionViewLayout.invalidateLayout()
        DispatchQueue.main.async(execute: collectionView.reloadData)
        coordinator.animate(alongsideTransition: nil) { (context) in
            self.willRotateOraintation = false
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return style == .full ? false : true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @available(iOS 11.0, *)
    override func viewSafeAreaInsetsDidChange() {
        super.viewSafeAreaInsetsDidChange()
        
        DLog("safeAreaInsets: \(self.view.safeAreaInsets)")
        let maxWidth = view.bounds.width - (self.view.safeAreaInsets.left + self.view.safeAreaInsets.right)
        self.contentWidth = FTTileCatalogCollectionViewCell.contentSize(for: self.traitCollection, containtIn: maxWidth, catalogStyle: self.style)
        self.collectionView.collectionViewLayout.invalidateLayout()
        DispatchQueue.main.async(execute: collectionView.reloadData)
    }
    
    private func reloadTiles() {
        if let _ = planType {
            reloadFilterTile()
        }
    }
    
    private func setupNavigationItem() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_filter"), style: .plain, target: self, action: #selector(self.onFilterCatalog))
        if showQRCode {
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_scan_qr"), style: .plain, target: self, action: #selector(self.onScanQRCode))
        } else {
            navigationItem.leftBarButtonItem = nil
        }
    }
    
    @objc func onFilterCatalog(_ sender: Any) {
        
        guard let tileSizes = self.tileSizes?.filterDuplicates(includeElement: { $0.inch == $1.inch }) else {
            return
        }
        
        print("tileSizes : \(tileSizes)")
        let isInchUnit = filterUnit == .inch
        let segmentView = FTSegmentControl(items: [LocalizedString("CalculateTileUnitInchTitle"), LocalizedString("CalculateTileUnitCentimaterTitle")])
        segmentView.sizeToFit()
        segmentView.autoresizingMask = .flexibleWidth
        segmentView.selectedSegmentIndex = isInchUnit ? 0 : 1
        segmentView.addTarget(self, action: #selector(self.onUnitSegmentValueChanged(_:)), for: .valueChanged)
        
        var index = 0
        if let type = planType {
            index = type == .floor ? 1 : 2
        }
        let tileSegmentView = FTSegmentControl(items: [LocalizedString("SegmentTypeAllTitle"), LocalizedString("SegmentTypeFloorTitle"), LocalizedString("SegmentTypeWallTitle")])
        tileSegmentView.sizeToFit()
        tileSegmentView.autoresizingMask = .flexibleWidth
        tileSegmentView.selectedSegmentIndex = index
        tileSegmentView.addTarget(self, action: #selector(self.onTileTypeSegmentValueChanged(_:)), for: .valueChanged)
        
        let items: [Any] = [LocalizedString("CatalogFilterAllTitle")] + tileSizes
        let controller = FTMenuPickerViewController(menus: items)
        if let item = sender as? UIBarButtonItem {
            controller.popoverPresentationController?.barButtonItem = item
        } else if let button = sender as? UIButton {
            self.filterButton = button
            controller.popoverPresentationController?.sourceView = button
            controller.popoverPresentationController?.sourceRect = button.convert(button.bounds, to: self.view)
            controller.popoverPresentationController?.permittedArrowDirections = .down
        }
        if let index = tileSizes.firstIndex(where: { $0 == filterSize }) {
            controller.preferSelectIndexs = [index+1]
        }
        controller.delegate = self
        
        let topView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: (tileSegmentView.bounds.height * 2) + 10))
        topView.addSubview(segmentView)
        topView.addSubview(tileSegmentView)
        
        tileSegmentView.frame = CGRect(x: 0, y: 0, width: topView.bounds.width, height: tileSegmentView.bounds.height)
        segmentView.frame = CGRect(x: 0, y: tileSegmentView.bounds.height + 10, width: topView.bounds.width, height: segmentView.bounds.height)
        
        controller.topView = topView
        self.present(controller, animated: true, completion: nil)
        
    }
    
    @objc internal func onUnitSegmentValueChanged(_ segment: FTSegmentControl) {
        filterUnit = segment.selectedSegmentIndex == 0 ? .inch : .centimetre
        
        if let pickerController = self.presentedViewController as? FTMenuPickerViewController {
            pickerController.reloadPicker()
        }
    }
    
    @objc internal func onTileTypeSegmentValueChanged(_ segment: FTSegmentControl) {
        
        let tileSizes: [TileSize]
        switch segment.selectedSegmentIndex {
        case 1:
            planType = .floor
            tileSizes = self.tileSizes?.filter({ $0.type == .floor }) ?? []
        case 2:
            planType = .wall
            tileSizes = self.tileSizes?.filter({ $0.type == .wall }) ?? []
        default:
            planType = nil
            tileSizes = self.tileSizes ?? []
        }
        if let pickerController = self.presentedViewController as? FTMenuPickerViewController {
            let items: [Any] = [LocalizedString("CatalogFilterAllTitle")] + tileSizes.filterDuplicates(includeElement: { $0.centimetre == $1.centimetre })
            pickerController.menus = items
        }
        reloadFilterTile()
        DispatchQueue.main.async(execute: collectionView.reloadData)
    }
    
    @objc func showFilterTile(in controller: UIViewController, sourceView: UIView, sourceRect: CGRect) {
        
        guard let tileSizes = self.tileSizes else {
            return
        }
        
        if let button = sourceView as? UIButton {
            self.filterButton = button
        }
        let isInchUnit = filterUnit == .inch
        let segmentView = FTSegmentControl(items: [LocalizedString("CalculateTileUnitInchTitle"), LocalizedString("CalculateTileUnitCentimaterTitle")])
        segmentView.sizeToFit()
        segmentView.selectedSegmentIndex = isInchUnit ? 0 : 1
        segmentView.addTarget(self, action: #selector(self.onUnitSegmentValueChanged(_:)), for: .valueChanged)
        
        let items: [Any] = [LocalizedString("CatalogFilterAllTitle")] + tileSizes
        let pickerController = FTMenuPickerViewController(menus: items)
        pickerController.popoverPresentationController?.sourceView = sourceView
        pickerController.popoverPresentationController?.sourceRect = sourceView.bounds
        pickerController.popoverPresentationController?.permittedArrowDirections = .down
        
        if let index = tileSizes.firstIndex(where: { $0 == filterSize }) {
            pickerController.preferSelectIndexs = [index+1]
        }
        pickerController.delegate = self
        pickerController.topView = segmentView
        controller.present(pickerController, animated: true, completion: nil)
    }
    
    @objc func onScanQRCode() {
        readerVC.delegate = self
        
        // Presents the readerVC as modal form sheet
        readerVC.modalPresentationStyle = .formSheet
        present(readerVC, animated: true, completion: nil)
    }
    
    func changeToFirstTile() {
        
        selectedTile = filterTiles?.first ?? AppData.tiles.first
        selectedTileIndex = 0
        if let tile = selectedTile {
            delegate?.catalogList(self, didSelectedTile: tile)
        }
        DispatchQueue.main.async(execute: collectionView.reloadData)
    }
    
    fileprivate func gotoTileDetailPage(by tile: Tile) {
        if let controller = self.storyboard?.instantiateViewController(class: FTCatalogDetailViewController.self) {
            controller.style = .tile
            controller.data = tile
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    fileprivate func prepareTileData(with tileId: String) {
        
        if let tile = AppData.realm.objects(Tile.self).first(where: { $0.id == tileId }) {
            gotoTileDetailPage(by: tile)
        } else {
            AppDelegateObject.displayLoadingView()
            AppService.call({ try $0.tile(id: tileId) }) { (tile, error) in
                AppDelegateObject.hideLoadingView()
                if let tile = tile {
                    self.gotoTileDetailPage(by: tile)
                    AppData.write {
                        AppData.realm.add(tile, update: .modified)
                        self.reloadTiles()
                        DispatchQueue.main.async(execute: self.collectionView.reloadData)
                    }
                } else {
                    DLog("error : \(error?.localizedDescription ?? "No Error")")
                    let alert = UIAlertController(title: LocalizedString("error"), message: LocalizedString("LoadTileError"), preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: LocalizedString("ok"), style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    fileprivate func reloadFilterTile() {
        
        if let planType = planType {
            filterTiles = AppData.tiles.filter({ $0.planType == planType })
            if let size = filterSize {
                filterTiles = filterTiles?.filter({ $0.length == Int(size.centimetre.length) && $0.width == Int(size.centimetre.width) })
            }
            filterButton?.image = #imageLiteral(resourceName: "icon_filtered").scaleImageToHeight(#imageLiteral(resourceName: "icon_filtered").size.height/1.5)
            navigationItem.rightBarButtonItem?.image = #imageLiteral(resourceName: "icon_filtered")
        } else if let size = filterSize {
            filterTiles = AppData.tiles.filter({ $0.length == Int(size.centimetre.length) && $0.width == Int(size.centimetre.width) })
            filterButton?.image = #imageLiteral(resourceName: "icon_filtered").scaleImageToHeight(#imageLiteral(resourceName: "icon_filtered").size.height/1.5)
            navigationItem.rightBarButtonItem?.image = #imageLiteral(resourceName: "icon_filtered")
        } else {
            filterTiles?.removeAll()
            filterTiles = nil
            filterButton?.image = #imageLiteral(resourceName: "icon_filter").scaleImageToHeight(#imageLiteral(resourceName: "icon_filter").size.height/1.5)
            navigationItem.rightBarButtonItem?.image = #imageLiteral(resourceName: "icon_filter")
        }
    }
}

extension FTCatalogListViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        DLog("manager.isShowSection : \(manager.isShowSection)")
        return manager.isShowSection ? 2 : 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let items = filterTiles {
            if section == 0 {
                return items.count + (showQRInCollection ? 1 : 0)
            }
            return 0
        }
        if section == 1 {
            return 1
        }
        return AppData.tiles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 1 {
            let cell = collectionView.dequeueReusableCell(class: DownloadCollectionViewCell.self, for: indexPath)
            cell.downloadView.downloadIndicator.color = .white
            cell.addDelegateFromDownloadManager(manager)
            
            if manager.isShouldDownload {
                manager.callService { [weak self] (tiles) in
                    if let tiles = tiles {
                        DLog("loaded tile : \(tiles.count)")
                        AppData.addTiles(tiles, pageIndex: self?.manager.page ?? 0)
                        AppData.write {
                            AppData.realm.add(tiles, update: .modified)
                            self?.reloadTiles()
                            DispatchQueue.main.async(execute: collectionView.reloadData)
                        }
                    } else if let _ = self?.manager.error, AppData.tiles.count == 0 {
                        self?.manager.stage = DownloadStage.downloaded
                        AppData.loadDefaultTiles()
                        self?.reloadTiles()
                        DispatchQueue.main.async(execute: collectionView.reloadData)
                    }
                }
            }
            return cell
        }
        
        if showQRInCollection && indexPath.section == 0 && indexPath.item == 0 {
            let cell = collectionView.dequeueReusableCell(class: FTTileQRCodeCollectionViewCell.self, for: indexPath)
            if style == .mini {
                cell.changeToMiniStyle()
            }
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(class: FTTileCatalogCollectionViewCell.self, for: indexPath)
        cell.isMiniStyle = style == .mini
        let tileIndex: Int = {
            let index: Int = showQRInCollection ? indexPath.item-1 : indexPath.item
            if let selectedTileIndex = selectedTileIndex, index <= selectedTileIndex {
                return index-1
            }
            return index
        }()
        if let tile = selectedTile, indexPath.item == (showQRInCollection ? 1 : 0) {
            cell.selectedTile = true
            cell.setElementsOfCell(tile, at: indexPath, contentSize: contentWidth)
        } else if let tile = (filterTiles ?? AppData.tiles)[safe: tileIndex] {
            cell.setElementsOfCell(tile, at: indexPath, contentSize: contentWidth)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        if let _ = collectionView.cellForItem(at: indexPath) as? DownloadCollectionViewCell {
            return false
        }
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let tileIndex: Int = {
            let index: Int = showQRInCollection ? indexPath.item-1 : indexPath.item
            if let selectedTileIndex = selectedTileIndex, index <= selectedTileIndex {
                return index-1
            }
            return index
        }()
        
        DLog("selected Index : \(tileIndex)")
        
        if showQRInCollection && indexPath.section == 0 && indexPath.item == 0 {

            readerVC.delegate = self
            
            // Presents the readerVC as modal form sheet
            readerVC.modalPresentationStyle = .formSheet
            present(readerVC, animated: true, completion: nil)
            
        } else if let tile = (filterTiles ?? AppData.tiles)?[safe: tileIndex] {
            if let delegate = delegate {
                selectedTile = tile
                selectedTileIndex = tileIndex
                delegate.catalogList(self, didSelectedTile: tile)
                DispatchQueue.main.async(execute: collectionView.reloadData)
            } else {
                if let controller = self.storyboard?.instantiateViewController(class: FTCatalogDetailViewController.self) {
                    controller.style = .tile
                    controller.data = tile
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            }
        }
    }
}

extension FTCatalogListViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 1 {
            return CGSize(width: collectionView.bounds.width, height: DownloadCollectionViewCell.rowHeight())
        }
        return CGSize(width: contentWidth, height: contentWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 1 {
            return .zero
        }
        let space = AppUIStyle(traitCollection: traitCollection).space
        if style == .full {
            return UIEdgeInsets(redius: floor(space/5))
        }
        return UIEdgeInsets(top: 0, left: space, bottom: space, right: space)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if section == 1 {
            return 0
        }
        let space = AppUIStyle(traitCollection: traitCollection).space
        return style == .full ? floor(space/5) : space/2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if section == 1 {
            return 0
        }
        let space = AppUIStyle(traitCollection: traitCollection).space
        return style == .full ? floor(space/5) : space/2
    }
}

extension FTCatalogListViewController: FTMenuPickerViewDelegate {
    
    func menuPicker(_ picker: FTMenuPickerViewController, textForData data: Any) -> String {
        if let tileSize = data as? TileSize {
            switch filterUnit {
            case .inch:
                return String(format: LocalizedString("CalculateInchSizeTitle"), tileSize.inch.width, tileSize.inch.length)
            case .centimetre:
                return String(format: LocalizedString("CalculateCmSizeTitle"), tileSize.centimetre.width, tileSize.centimetre.length)
            }
        } else if let title = data as? String {
            return title
        }
        return ""
    }
    
    func menuPicker(_ picker: FTMenuPickerViewController, valueChanged data: Any) {
        if let tileSize = data as? TileSize {
            filterSize = tileSize
        } else if let _ = data as? String {
            filterSize = nil
        }
        reloadFilterTile()
        DispatchQueue.main.async(execute: collectionView.reloadData)
    }
    
    func menuPickerDidFinish() {
        
    }
}

extension FTCatalogListViewController: QRCodeReaderViewControllerDelegate {
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.dismiss(animated: true, completion: nil)
        if let url = URL(string: result.value), let tileId = AppUtility.parameters(for: url)["id_scan"] {
            
            DLog("tileId : \(tileId)")
            AppDelegateObject.displayLoadingView()
            
            AppService.call({ try $0.tile(id: tileId) }) { (tile, error) in
                AppDelegateObject.hideLoadingView()
                if let tile = tile {
                    if let delegate = self.delegate {
                        self.selectedTile = tile
                        self.selectedTileIndex = {
                            let last = AppData.tiles.count
                            return self.showQRInCollection ? last + 1 : last
                        }()
                        delegate.catalogList(self, didSelectedTile: tile)
                    } else {
                        if let controller = self.storyboard?.instantiateViewController(class: FTCatalogDetailViewController.self) {
                            controller.style = .tile
                            controller.data = tile
                            self.navigationController?.pushViewController(controller, animated: true)
                        }
                    }
                    AppData.write {
                        AppData.realm.add(tile, update: .modified)
                        self.reloadTiles()
                        DispatchQueue.main.async(execute: self.collectionView.reloadData)
                    }
                } else {
                    DLog("error : \(error?.localizedDescription ?? "No Error")")
                    let alert = UIAlertController(title: LocalizedString("error"), message: LocalizedString("LoadTileError"), preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: LocalizedString("ok"), style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        } else {
            let alert = UIAlertController(title: LocalizedString("error"), message: LocalizedString("QRCodeIncorrectFormatError"), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: LocalizedString("ok"), style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.dismiss(animated: true, completion: nil)
    }
}

extension Array {

    func filterDuplicates(includeElement: (_ lhs:Element, _ rhs:Element) -> Bool) -> [Element]{
        var results = [Element]()

        forEach { (element) in
            let existingElements = results.filter {
                return includeElement(element, $0)
            }
            if existingElements.count == 0 {
                results.append(element)
            }
        }

        return results
    }
}
