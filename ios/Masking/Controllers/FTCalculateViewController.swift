//
//  FTCalculateViewController.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 7/2/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import Foundation
import DGTEngineSwift
import Async
import SafariServices

class FTCalculateViewController: FTBaseViewController {
    
    @IBOutlet internal weak var tableView: UITableView!
    
    fileprivate var pattarns = [UITableViewCell.Type]()
    fileprivate var calculate = TileCalculate()
    fileprivate var tileSizes: [TileSize]?
    fileprivate var unitSegmentView: FTSegmentControl?
    
    fileprivate var activeTextField: UITextField?
    
    fileprivate var areaLabelMaxWidth: CGFloat = 0
    
    var preferTile: Tile? {
        didSet {
            calculate.tile = preferTile
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = FTMenuItem.calculate.pageTitle
        
        if #available(iOS 11.0, *) {
            self.additionalSafeAreaInsets = UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)
        }
        
        AppData.prepareTileSize { [weak self] (sized) in
            self?.tileSizes = sized
        }
        
        let titles = [LocalizedString("CalculateTileAreaSizeTitle"), LocalizedString("CalculateTileAreaWidthTitle"), LocalizedString("CalculateTileAreaLengthTitle")]
        areaLabelMaxWidth = FTTileAreaInfoCalculateTableViewCell.maxWidth(of: titles, for: self.traitCollection)
        
        setupBottomLayoutGuide(in: tableView)
        prepareCellPattarn()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavigationItem()
        registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        deregisterForKeyboardNotifications()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        let titles = [LocalizedString("CalculateTileAreaSizeTitle"), LocalizedString("CalculateTileAreaWidthTitle"), LocalizedString("CalculateTileAreaLengthTitle")]
        areaLabelMaxWidth = FTTileAreaInfoCalculateTableViewCell.maxWidth(of: titles, for: self.traitCollection)
        tableView.reloadData()
    }
    
    fileprivate func registerForKeyboardNotifications() {
        let notification = NotificationCenter.default
        notification.addObserver(self, selector: #selector(self.onKeyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        notification.addObserver(self, selector: #selector(self.onKeyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    fileprivate func deregisterForKeyboardNotifications() {
        let notification = NotificationCenter.default
        notification.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        notification.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func prepareCellPattarn() {
        pattarns.removeAll()
        
        if let _ = calculate.tile {
            pattarns.append(FTTileSizeInfoCalculateTableViewCell.self)
        } else {
            pattarns.append(FTTileSizeCalculateTableViewCell.self)
        }
        
        pattarns.append(FTTileAreaCalculateTableViewCell.self)
        switch calculate.area {
        case .square_metre(_):
            pattarns.append(FTTileAreaInfoCalculateTableViewCell.self)
        case .metre(_):
            pattarns.append(FTTileAreaInfoCalculateTableViewCell.self)
            pattarns.append(FTTileAreaInfoCalculateTableViewCell.self)
        }
        
        pattarns.append(FTTileExtraCalculateTableViewCell.self)
        switch calculate.extra {
        case .none:
            pattarns.append(FTTileRemarkCalculateTableViewCell.self)
        case .percent(_):
            pattarns.append(FTTileExtraInfoCalculateTableViewCell.self)
        }
        
        pattarns.append(FTTileConfirmCalculateTableViewCell.self)
    }
    
    private func setupNavigationItem() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_clear"), style: .plain, target: self, action: #selector(self.onResetCalculate))
    }
    
    @objc internal func onResetCalculate() {
        if view.isUserInteractionEnabled == true {
            calculate = TileCalculate()
            calculate.tile = preferTile
            prepareCellPattarn()
            tableView.reloadData()
        }
    }
    
    @objc internal func onCancelTile(_ button: UIButton) {
        calculate.tile = nil
        let haveBox = resetCalculateBox()
        prepareCellPattarn()
        
        let indexPaths: [IndexPath]
        if haveBox {
            indexPaths = [IndexPath(row: 0, section: 0), IndexPath(row: pattarns.count-1, section: 0)]
        } else {
             indexPaths = [IndexPath(row: 0, section: 0)]
        }
        tableView.reloadRows(at: indexPaths, with: .fade)
    }
    
    @objc internal func onSelectTileSize(_ button: UIButton) {
        
        guard let tileSizes = self.tileSizes else {
            return
        }
        
        let isInchUnit = calculate.sizeUnit == .inch
        unitSegmentView = FTSegmentControl(items: [LocalizedString("CalculateTileUnitInchTitle"), LocalizedString("CalculateTileUnitCentimaterTitle")])
        unitSegmentView?.sizeToFit()
        unitSegmentView?.selectedSegmentIndex = isInchUnit ? 0 : 1
        unitSegmentView?.addTarget(self, action: #selector(self.onUnitSegmentValueChanged(_:)), for: .valueChanged)
        
        let controller = FTMenuPickerViewController(menus: tileSizes)
        controller.popoverPresentationController?.sourceView = button
        controller.popoverPresentationController?.sourceRect = button.bounds
        if let index = tileSizes.firstIndex(where: { $0 == calculate.size }) {
            controller.preferSelectIndexs = [index]
        } else {
            let cell = button.firstSuperView(class: FTTileSizeCalculateTableViewCell.self)
            calculate.size = tileSizes.first
            cell?.setElementOfCell(calculate)
        }
        controller.delegate = self
        controller.topView = unitSegmentView
        self.present(controller, animated: true, completion: nil)
        
        if resetCalculateBox() {
            tableView.reloadRows(at: [IndexPath(row: pattarns.count-1, section: 0)], with: .fade)
        }
    }
    
    @objc internal func onSelectTileCatalog() {
        let catalogViewController = FTCatalogListViewController(style: .full)
        catalogViewController.delegate = self
        catalogViewController.navigationItem.leftBarButtonItem = UIBarButtonItem(title: LocalizedString("cancel"), style: .done, target: self, action: #selector(self.onCancelCatalog))
        let navigation = FTNavigationController(navigationBarClass: FTNavigationBar.self, toolbarClass: nil)
        navigation.viewControllers = [catalogViewController]
        self.present(navigation, animated: true, completion: nil)
    }
    
    @objc internal func onTitleSizeChange(_ segment: FTSegmentControl) {
        
        guard let cell = segment.firstSuperView(class: UITableViewCell.self), let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        
        let oldArea = calculate.area
        switch segment.selectedSegmentIndex {
        case 0: calculate.area = .square_metre(size: 0)
        case 1: calculate.area = .metre(width: 0, length: 0)
        default:break
        }
        
        tableView.beginUpdates()
        let lastIndexPath: IndexPath?
        if resetCalculateBox() {
            lastIndexPath = IndexPath(row: pattarns.count-1, section: 0)
        } else {
            lastIndexPath = nil
        }
        prepareCellPattarn()
        
        let indexPaths: [IndexPath]
        if let lastIndexPath = lastIndexPath {
            indexPaths = [IndexPath(row: indexPath.row+1, section: 0), lastIndexPath]
        } else {
            indexPaths = [IndexPath(row: indexPath.row+1, section: 0)]
        }
        tableView.reloadRows(at: indexPaths, with: .fade)
        if case .square_metre(_) = oldArea, case .metre(_) = calculate.area {
            tableView.insertRows(at: [IndexPath(row: indexPath.row+2, section: 0)], with: .fade)
        } else if case .metre(_) = oldArea, case .square_metre(_) = calculate.area {
            tableView.deleteRows(at: [IndexPath(row: indexPath.row+2, section: 0)], with: .fade)
        }
        tableView.endUpdates()
    }
    
    @objc internal func onTileSizeTextChange(_ textField: UITextField) {
        
        switch calculate.area {
        case .square_metre(_):
            let value = Double(textField.text ?? "0") ?? 0
            calculate.area = .square_metre(size: value)
        case .metre(_, let length) where textField.tag == 0:
            let value = Double(textField.text ?? "0") ?? 0
            calculate.area = .metre(width: value, length: length)
        case .metre(let width, _) where textField.tag == 1:
            let value = Double(textField.text ?? "0") ?? 0
            calculate.area = .metre(width: width, length: value)
        default:break
        }
        if resetCalculateBox() {
            tableView.reloadRows(at: [IndexPath(row: pattarns.count-1, section: 0)], with: .fade)
        }
    }
    
    @objc internal func onTileExtraChange(_ segment: FTSegmentControl) {
        
        guard let cell = segment.firstSuperView(class: UITableViewCell.self), let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        
        switch segment.selectedSegmentIndex {
        case 0: calculate.extra = .percent(value: 10)
        case 1: calculate.extra = .none
        default:break
        }
        
        tableView.beginUpdates()
        let haveBox = resetCalculateBox()
        prepareCellPattarn()
        let indexPaths: [IndexPath]
        if haveBox {
            indexPaths = [IndexPath(row: indexPath.row+1, section: 0), IndexPath(row: pattarns.count-1, section: 0)]
        } else {
            indexPaths = [IndexPath(row: indexPath.row+1, section: 0)]
        }
        tableView.reloadRows(at: indexPaths, with: .fade)
        tableView.endUpdates()
    }
    
    @objc internal func onTileExtraValueChange(_ button: UIButton) {
        
        let cell = button.firstSuperView(class: FTTileExtraInfoCalculateTableViewCell.self)
        cell?.unselectedButtons()
        button.isSelected = true
        
        switch button.tag {
        case 0: calculate.extra = .percent(value: 5)
        case 1: calculate.extra = .percent(value: 10)
        case 2: calculate.extra = .percent(value: 15)
        default: break
        }
        if resetCalculateBox() {
            tableView.reloadRows(at: [IndexPath(row: pattarns.count-1, section: 0)], with: .fade)
        }
    }
    
    @objc internal func onConfirm(_ button: UIButton) {
        DLog("calculate : \(calculate)")
        
        guard let cell = button.firstSuperView(class: FTTileConfirmCalculateTableViewCell.self), let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        
        self.view.isUserInteractionEnabled = false
        
        let storeTitle = button.title
        button.title = "   "
        let downloadView = UIActivityIndicatorView(style: .white)
        downloadView.prepareForAutoLayout()
        cell.contentView.addSubview(downloadView)
        [downloadView.centerX == button.centerX,
         downloadView.centerY == button.centerY].activated()
        downloadView.startAnimating()
        
        AppService.call({ try $0.calculate(self.calculate) }) { (result, error) in
            downloadView.stopAnimating()
            downloadView.removeFromSuperview()
            button.title = storeTitle
            self.view.isUserInteractionEnabled = true
            if let result = result {
                self.calculate.box = result.box
                self.prepareCellPattarn()
                self.tableView.beginUpdates()
                self.tableView.reloadRows(at: [indexPath], with: .fade)
                self.tableView.endUpdates()
            } else {
                DLog("error : \(String(describing: error))")
                let alert = UIAlertController(title: LocalizedString("error"), message: LocalizedString("CalculateSeriveError"), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: LocalizedString("ok"), style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @objc internal func onBuyTile(_ button: UIButton) {
        
        if let tile = calculate.tile {
            let url = "https://www.homepro.co.th/p/\(tile.id)?utm_source=fittile&utm_medium=bn_buynow"
            let controller = SFSafariViewController(url: URL(string: url)!)
            self.present(controller, animated: true, completion: nil)
        } else {
            let url = "https://www.homepro.co.th"
            let controller = SFSafariViewController(url: URL(string: url)!)
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    @objc internal func onCancelCatalog() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc internal func onUnitSegmentValueChanged(_ segment: FTSegmentControl) {
        calculate.sizeUnit = segment.selectedSegmentIndex == 0 ? .inch : .centimetre
        
        if let pickerController = self.presentedViewController as? FTMenuPickerViewController {
            calculate.size = pickerController.currentValue as? TileSize
            pickerController.reloadPicker()
            tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
        }
    }
    
    @objc internal func onKeyboardWillShow(_ notification: Notification) {
        keyboardShow(true, for: notification)
    }
    
    @objc internal func onKeyboardWillHide(_ notification: Notification) {
        keyboardShow(false, for: notification)
    }
    
    func keyboardShow(_ show: Bool, for notification: Notification) {
        if let view = activeTextField {
            let frame = view.convert(view.bounds, to: self.view)
            tableView.scrollRectToVisible(frame, animated: true)
        }
    }
    
    private func resetCalculateBox() -> Bool {
        if let _ = calculate.box {
            calculate.box = nil
            return true
        }
        return false
    }
}

extension FTCalculateViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pattarns.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellClass = pattarns[indexPath.row]
        let cell = tableView.dequeueReusableCell(class: cellClass.self, for: indexPath)
        if let cell = cell as? FTTileSizeInfoCalculateTableViewCell {
            if let tile = calculate.tile {
                cell.setElementOfCell(tile)
            }
            cell.addAction(#selector(self.onCancelTile(_:)), for: self)
            cell.cancalTileButton.isHidden = preferTile != nil
        } else if let cell = cell as? FTTileSizeCalculateTableViewCell {
            cell.setElementOfCell(calculate)
            cell.addAction(#selector(self.onSelectTileSize(_:)), for: self)
            cell.addCatalogAction(#selector(self.onSelectTileCatalog), for: self)
        } else if let cell = cell as? FTTileAreaCalculateTableViewCell {
            switch calculate.area {
            case .square_metre(_):
                cell.segmentView.selectedSegmentIndex = 0
            case .metre(_):
                cell.segmentView.selectedSegmentIndex = 1
            }
            cell.addAction(#selector(self.onTitleSizeChange(_:)), for: self)
        } else if let cell = cell as? FTTileAreaInfoCalculateTableViewCell {
            
            let formatter = NumberFormatter()
            formatter.allowsFloats = true
            switch calculate.area {
            case .square_metre(let size):
                cell.textField.text = formatter.string(from: NSNumber(value: size))
                cell.titleLabel?.text = LocalizedString("CalculateTileAreaSizeTitle")
                cell.unitLabel?.text = LocalizedString("CalculateTileAreaUnitTitle")
            case .metre(let width, let length):
                let indexs = pattarns.filterIndexs({ $0 == FTTileAreaInfoCalculateTableViewCell.self })
                let isWidthCell = indexs.first == indexPath.row
                if isWidthCell {
                    cell.textField.text = formatter.string(from: NSNumber(value: width))
                    cell.titleLabel?.text = LocalizedString("CalculateTileAreaWidthTitle")
                    cell.lineView?.isHidden = true
                } else {
                    cell.textField.tag = 1
                    cell.textField.text = formatter.string(from: NSNumber(value: length))
                    cell.titleLabel?.text = LocalizedString("CalculateTileAreaLengthTitle")
                }
                cell.unitLabel?.text = LocalizedString("CalculateTileAreaUnitMeterTitle")
            }
            cell.setTitleLabelWidth(areaLabelMaxWidth)
            cell.addAction(#selector(self.onTileSizeTextChange(_:)), for: self)
        } else if let cell = cell as? FTTileExtraCalculateTableViewCell {
            switch calculate.extra {
            case .none:
                cell.segmentView.selectedSegmentIndex = 1
            case .percent(_):
                cell.segmentView.selectedSegmentIndex = 0
            }
            cell.addAction(#selector(self.onTileExtraChange(_:)), for: self)
        } else if let cell = cell as? FTTileRemarkCalculateTableViewCell {
            cell.titleLabel?.text = LocalizedString("CalculateTileExtraRemarkTitle")
            cell.titleLabel?.textColor = .appRed
        } else if let cell = cell as? FTTileExtraInfoCalculateTableViewCell {
            switch calculate.extra {
            case .percent(let value) where value == 5:
                cell.buttons.first?.isSelected = true
            case .percent(let value) where value == 10:
                cell.buttons[safe:1]?.isSelected = true
            case .percent(let value) where value == 15:
                cell.buttons[safe:2]?.isSelected = true
            default:break
            }
            cell.addAction(#selector(self.onTileExtraValueChange(_:)), for: self)
        } else if let cell = cell as? FTTileConfirmCalculateTableViewCell {
            
            if let box = self.calculate.box {
                cell.resultView.isHidden = false
                cell.resultBoxLabel.text = NumberFormatter.decimal(box)
                cell.addAction(#selector(self.onBuyTile(_:)), for: self)
                cell.confirmButton.title = LocalizedString("CalculateTileResultBuyTitle")
                cell.remarkLabel.text = LocalizedString("CalculateTileResultRemarkTitle")
            } else {
                cell.addAction(#selector(self.onConfirm(_:)), for: self)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        if let _ = tableView.cellForRow(at: indexPath) as? FTTileSizeInfoCalculateTableViewCell, preferTile == nil {
            onSelectTileCatalog()
        }
    }
}

extension FTCalculateViewController: FTCatalogListViewControllerDelegate {
    
    func catalogList(_ catalogList: FTCatalogListViewController, didSelectedTile tile: Tile) {
        calculate.box = nil
        calculate.tile = tile.copy()
        prepareCellPattarn()
        tableView.reloadData()
        catalogList.dismiss(animated: true, completion: nil)
    }
    
    func catalogListDidStartScanQRCode(_ catalogList: FTCatalogListViewController) {
        
    }
}

extension FTCalculateViewController: FTMenuPickerViewDelegate {
    
    func menuPicker(_ picker: FTMenuPickerViewController, textForData data: Any) -> String {
        if let tileSize = data as? TileSize {
            switch calculate.sizeUnit {
            case .inch:
                return String(format: LocalizedString("CalculateInchSizeTitle"), tileSize.inch.width, tileSize.inch.length)
            case .centimetre:
                return String(format: LocalizedString("CalculateCmSizeTitle"), tileSize.centimetre.width, tileSize.centimetre.length)
            }
        }
        return ""
    }
    
    func menuPicker(_ picker: FTMenuPickerViewController, valueChanged data: Any) {
        
        guard let tileSize = data as? TileSize else {
            return
        }
        let cell = tableView.cellForRow(at: IndexPath(item: 0, section: 0)) as? FTTileSizeCalculateTableViewCell
        calculate.box = nil
        calculate.size = tileSize
        cell?.setElementOfCell(calculate)
    }
    
    func menuPickerDidFinish() {
        
    }
}

extension FTCalculateViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.activeTextField = nil
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.isEmpty { return true }
        
        if string == " " && range.length == 0 && range.location == 0 {
            return true
        }
        
        if string.contains(".") && textField.text?.contains(".") == true {
            return false
        }
        
        let charecterSet = CharacterSet(charactersIn: "0123456789.")
        let charecterCondition = string.components(separatedBy: charecterSet.inverted).count == 1
        if charecterCondition {
            return true
        }
        return false
    }
}

