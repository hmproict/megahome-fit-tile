//
//  AppSetting.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 21/1/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//


import UIKit
import DGTEngineSwift

let kAppSettingDataFileName = "AppSettingData"

class AppSetting: NSObject {
    
//    struct Conmunity: OptionSet {
//
//        let rawValue: Int
//
//        static let none = Conmunity(rawValue: 0)
//        static let mail = Conmunity(rawValue: 1 << 1)
//        static let sms = Conmunity(rawValue: 1 << 2)
//        static let email = Conmunity(rawValue: 1 << 3)
//        static let all: Conmunity = [.mail, .sms, .email]
//    }
//
//    var allowBioticAuthen: Bool = false
//    var allowNotification: Bool = false
//    var allowCurrentLocation: Bool = false
//    var allowComunityChannnel: Conmunity = .all
    
    // MARK: -------------------------------------------------------
    // MARK: Singantle methods
    
    internal class var `default`: AppSetting {
        
        struct Static {
            static let instance: AppSetting = {
                let obj = AppSetting()
                
                NSKeyedArchiver.setClassName("AppSetting", for: AppSetting.self)
                NSKeyedUnarchiver.setClass(AppSetting.self, forClassName: "AppSetting")
                
//                let path = AppData.pathFile(kAppSettingDataFileName)
//                if let setting = AppData.decyptData(from: path) as? AppSetting {
//                    obj = setting
//                } else {
//                    obj = AppSetting()
//                }
                return obj
            }()
        }
        return Static.instance
    }
    
    override init() {
        super.init()
    }
    
//    func encode(with aCoder: NSCoder) {
//        aCoder.encode(allowBioticAuthen, forKey: "BCAllowBioticAuthen")
//        aCoder.encode(allowNotification, forKey: "BCAllowNotification")
//        aCoder.encode(allowCurrentLocation, forKey: "BCAllowCurrentLocation")
//        aCoder.encode(allowComunityChannnel.rawValue, forKey: "BCAllowComunityChannnel")
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        self.allowBioticAuthen = aDecoder.decodeBool(forKey: "BCAllowBioticAuthen")
//        self.allowNotification = aDecoder.decodeBool(forKey: "BCAllowNotification")
//        self.allowCurrentLocation = aDecoder.decodeBool(forKey: "BCAllowCurrentLocation")
//        let channnel = aDecoder.decodeInteger(forKey: "BCAllowComunityChannnel")
//        self.allowComunityChannnel = Conmunity(rawValue: channnel)
//    }
    
    func sync() {
//        let path = AppData.pathFile(kAppSettingDataFileName)
//        AppData.writeData(self, to: path)
    }
}
