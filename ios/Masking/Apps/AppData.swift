//
//  AppData.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 21/1/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import UIKit
import RealmSwift
import Async
import DGTEngineSwift

let kAppDeviceInfomationDataFileName = "DeviceInfomationData"
let kAppInfomationDataFileName = "AppInfomationData"

class AppData {
    
    fileprivate let _deviceInfo: DeviceInfo
    fileprivate var _infos: AppInfos?
    
    fileprivate let _realm: Realm
    static let queue = DispatchQueue(label: "com.digitopolis.masking")
    
    fileprivate var _tiles = [Tile]()
    fileprivate var _pageIndex: Int = 0
    fileprivate var _tileSizes = [TileSize]()
    
    init() {
        
        let deviceInfoPath = AppData.pathFile(kAppDeviceInfomationDataFileName)
        DLog("documentPath : \(deviceInfoPath)")
        
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: 4,

            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                DLog("oldSchemaVersion : \(oldSchemaVersion)")
                if oldSchemaVersion < 1 {
                    // Apply any necessary migration logic here.
                    migration.enumerateObjects(ofType: Tile.className()) { (_, newTile) in
                        newTile?["patternImage"] = ""
                    }
                } else if oldSchemaVersion < 2 {
                    // Apply any necessary migration logic here.
                    migration.enumerateObjects(ofType: TileProject.className()) { (_, newProject) in
                        newProject?["qrcodeURL"] = ""
                        newProject?["imageURL"] = ""
                        newProject?["imageFittileURL"] = ""
                    }
                } else if (oldSchemaVersion < 3) {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                    migration.enumerateObjects(ofType: Tile.className()) { (_, newTile) in
                        newTile?["type"] = ""
                    }
                    migration.enumerateObjects(ofType: TileProject.className()) { oldObject, newObject in
                        
                        let artboard = migration.create(TileArtboard.className())
                        artboard["perspective"] = oldObject?["perspective"]
                        artboard["scale"] = oldObject?["scale"]
                        artboard["perspectiveType"] = 0
                        artboard["_location"] = oldObject?["_location"]
                        artboard["_frame"] = oldObject?["_frame"]
                        artboard["_points"] = oldObject?["_points"]
                        
                        let rawTile = oldObject?["tile"] as? MigrationObject
                        if let tile = rawTile {
                            migration.delete(tile)
                        }
                        
                        let tile = migration.create(Tile.className())
                        tile["id"] = rawTile?["id"]
                        tile["shortDesc"] = rawTile?["shortDesc"]
                        tile["longDesc"] = rawTile?["longDesc"]
                        tile["width"] = rawTile?["width"]
                        tile["length"] = rawTile?["length"]
                        tile["random"] = rawTile?["random"]
                        tile["box"] = rawTile?["box"]
                        tile["feature"] = rawTile?["feature"]
                        tile["vendor"] = rawTile?["vendor"]
                        tile["pattern"] = rawTile?["pattern"]
                        tile["image"] = rawTile?["image"]
                        tile["url"] = rawTile?["url"]
                        tile["type"] = "Floor"
                        tile["patternImage"] = rawTile?["patternImage"]
                        artboard["tile"] = tile
                        newObject!["artboards"] = [artboard]
                    }
                } else if oldSchemaVersion < 4 {
                    // Apply any necessary migration logic here.
                    migration.enumerateObjects(ofType: Tile.className()) { (_, newTile) in
                        newTile?["floor"] = ""
                        newTile?["wall"] = ""
                        newTile?["leftWall"] = ""
                        newTile?["rightWall"] = ""
                        newTile?["widthInch"] = 0
                        newTile?["lengthInch"] = 0
                        newTile?["category"] = 0
                        newTile?["area"] = ""
                    }
                }
            })

        // Now that we've told Realm how to handle the schema change, opening the file
        // will automatically perform the migration
        self._realm = try! Realm(configuration: config)
        
        NSKeyedArchiver.setClassName("DeviceInfo", for: DeviceInfo.self)
        NSKeyedUnarchiver.setClass(DeviceInfo.self, forClassName: "DeviceInfo")
        NSKeyedArchiver.setClassName("AppSetting", for: AppSetting.self)
        NSKeyedUnarchiver.setClass(AppSetting.self, forClassName: "AppSetting")
        if let infomation = AppData.decyptData(from: deviceInfoPath, key: DeviceInfo.key) as? DeviceInfo {
            self._deviceInfo = infomation
        } else {
            self._deviceInfo = DeviceInfo()
            Async.background {
                AppData.saveDeviceData()
            }
        }
        
        let appInfoPath = AppData.pathFile(kAppInfomationDataFileName)
        let key = AppData.encyptKey(for: appInfoPath, uuid: self._deviceInfo.uuid)
        self._infos = AppData.decyptModel(class: AppInfos.self, from: appInfoPath, key: key) ?? AppInfos()
        if let clearCatalogSize = self._infos?.clearCatalogSize, !clearCatalogSize {
            let sizes = self._realm.objects(TileSize.self)
            AppData.write {
                self._realm.delete(sizes)
            }
            self._infos?.clearCatalogSize = true
        }
    }
    
    class var shared: AppData {
        struct Static {
            static let instance: AppData = AppData()
        }
        return Static.instance
    }
    
    class var realm: Realm {
        return self.shared._realm;
    }
    
    class func setup() {
        let _ = self.shared
    }
    
    class var deviceInfo: DeviceInfo {
        return shared._deviceInfo
    }
    
    class var appInfos: AppInfos {
        return shared._infos ?? AppInfos()
    }
    
    class var tiles: [Tile] {
        return shared._tiles
    }
    
    class var tilePageIndex: Int {
        return shared._pageIndex
    }
    
    class var tileSizes: [TileSize] {
        return shared._tileSizes
    }
    
    class func write(_ handler: @escaping () -> Void) {
        Async.main {
            self.queue.sync {
                try! self.realm.write {
                    handler()
                }
            }
        }
    }
    
    class func save(_ project: TileProject) {
        
        self.write {
            realm.add(project, update: .modified)
            NotificationCenter.default.post(name: .MyProjectsSaveChanged, object: nil)
        }
    }
    
    class func loadAllProjects() -> [TileProject] {
        let result = realm.objects(TileProject.self)
        return Array(result)
    }
    
    class func load() -> TileProject? {
        guard let project = realm.objects(TileProject.self).first else {
            return nil
        }
        return project.copy()
    }
    
    class func saveDeviceData() {
        let path = self.pathFile(kAppDeviceInfomationDataFileName)
        self.writeData(shared._deviceInfo, to:path, with:DeviceInfo.key)
    }
    
    class func saveInfosData() {
        let path = self.pathFile(kAppInfomationDataFileName)
        self.writeModel(shared._infos, to:path)
    }
    
    class func prepareTileStarterPack(completion: ((_ success: Bool) -> Void)? = nil) {
        let tiles = AppData.realm.objects(Tile.self)
        if tiles.isEmpty {
            
            AppService.call({ try $0.tileList(["size": 20]) }) { (result, error) in
                
                if let tiles = result?.list {
                    AppData.write {
                        AppData.realm.add(tiles, update: .modified)
                    }
                    completion?(true)
                } else {
                    do {
                        guard let url = Bundle.main.url(forResource: "StarterKit", withExtension: "json") else {
                            throw NSError(code: 999, description: "Can't find Tile Starter Kit pack file")
                        }
                        let data = try Data(contentsOf: url)
                        let tiles = try JSONDecoder().decode([Tile].self, from: data)
                        AppData.shared._tiles = tiles
                        AppData.write {
                            AppData.realm.add(tiles)
                        }
                        completion?(true)
                    } catch {
                        completion?(false)
                    }
                }
            }
        } else {
            completion?(true)
        }
    }
    
    class func prepareTileSize(completion: ((_ sizes: [TileSize]) -> Void)? = nil) {
        if !tileSizes.isEmpty {
            completion?(tileSizes)
            return;
        }
        
        let sizes = Array(AppData.realm.objects(TileSize.self))
        AppService.call({ try $0.tileSize() }) { (result, error) in
            if let list = result {
                AppData.write {
                    AppData.realm.add(list.list, update: .modified)
                }
                AppData.shared._tileSizes = list.list
                completion?(list.list);
            } else {
                completion?(sizes);
            }
        }
    }
    
    class func loadDefaultTiles() {
        AppData.shared._tiles = Array(AppData.realm.objects(Tile.self)).map({ $0.copy() })
    }
    
    class func addTiles(_ tiles: [Tile], pageIndex: Int) {
        AppData.shared._tiles.append(contentsOf: tiles)
        DLog("addTiles index : \(pageIndex)")
        AppData.shared._pageIndex = pageIndex
    }
    
    class func pathFile(_ fileName: String, in folder: String? = nil) -> String {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        var filePath = documentsURL!
        if let folder = folder {
            filePath.appendPathComponent(folder, isDirectory: true)
        }
        filePath.appendPathComponent(fileName)
        return filePath.absoluteString
    }
    
    class func decyptData(from path: String, key: String? = nil) -> AnyObject? {
        guard let url = URL(string: path), let data = try? Data(contentsOf: url) else { return nil }
        let key = key ?? encyptKey(for: path)
        let decryptData = DGTEncryptData.aes256DecryptData(data, withKey: key)
        let result = NSKeyedUnarchiver.unarchiveObject(with: decryptData!) as AnyObject
        return result
    }
    
    class func decyptModel<T: Decodable>(`class`: T.Type, from path: String, key: String? = nil) -> T? {
        guard let url = URL(string: path), let data = try? Data(contentsOf: url) else { return nil }
        let key = key ?? encyptKey(for: path)
        if let decryptData = DGTEncryptData.aes256DecryptData(data, withKey: key) {
            do {
                return try JSONDecoder().decode(`class`, from: decryptData)
            } catch {
                DLog("error : \(error)")
            }
        }
        return nil
    }
    
    @discardableResult
    class func writeData(_ object: AnyObject, to path: String, with key: String? = nil) -> Bool {
        let data = NSKeyedArchiver.archivedData(withRootObject: object)
        let key = key ?? encyptKey(for: path)
        let encyptData = DGTEncryptData.aes256EncryptData(data, withKey: key)
        do {
            try encyptData?.write(to: URL(string: path)!, options: .atomic)
        } catch {
            DLog("write to file error \(error)");
            return false
        }
        return true
    }
    
    @discardableResult
    class func writeModel<T: Encodable>(_ object: T, to path: String, with key: String? = nil) -> Bool {
        do {
            let data = try JSONEncoder().encode(object)
            let key = key ?? encyptKey(for: path)
            let encyptData = DGTEncryptData.aes256EncryptData(data, withKey: key)
            try encyptData?.write(to: URL(string: path)!, options: .atomic)
        } catch {
            DLog("write to file error \(error)");
            return false
        }
        return true
    }
    
    class func encyptKey(for path: String, uuid: String? = nil) -> String {
        
        let fileName = URL(string: path)!.lastPathComponent
        let appIdentifier = Bundle.main.object(forInfoDictionaryKey: "CFBundleIdentifier") as! String
        let uuid = uuid ?? shared._deviceInfo.uuid
        return "\(fileName)##-ERwcgL\(uuid)*|*Hd\(appIdentifier)\\KKf"
    }
    
}
