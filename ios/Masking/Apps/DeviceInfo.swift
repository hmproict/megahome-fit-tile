//
//  DeviceInfo.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 21/1/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//


import Foundation

class DeviceInfo: NSObject, NSCoding {
    
    class var key: String {
        let appIdentifier = Bundle.main.object(forInfoDictionaryKey: "CFBundleIdentifier") as! String
        return "//>':;srto\(appIdentifier)UeqxX{--0}M+-%^&*#"
    }
    
    let uuid: String
    var pushToken: String
    var firebaseToken: String?
    var isTutorialShowed = false
    
    override init() {
        uuid = UUID().uuidString
        pushToken = UUID().uuidString
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        self.uuid = aDecoder.decodeObject(forKey: "AppUUID") as! String
        self.isTutorialShowed = aDecoder.decodeBool(forKey: "AppIsTutorailShowed")
        self.pushToken = aDecoder.decodeObject(forKey: "AppPushToken") as! String
        self.firebaseToken = aDecoder.decodeObject(forKey: "AppFirebasePushToken") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(uuid, forKey: "AppUUID")
        aCoder.encode(isTutorialShowed, forKey: "AppIsTutorailShowed")
        aCoder.encode(pushToken, forKey: "AppPushToken")
        aCoder.encode(firebaseToken, forKey: "AppFirebasePushToken")
    }
}
