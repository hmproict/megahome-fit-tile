//
//  AppStyle.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 21/1/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//


import Foundation
import UIKit
import DGTEngineSwift

struct AppUIStyle {
    
    let regular: Bool 
    init(regular: Bool = false) {
        self.regular = regular
    }
    
    var cornerRedius: CGFloat {
        return 8
    }
    
    var cornerInset: DGTCornerInset {
        return DGTCornerInset(radius: 8)
    }
    
    var space: CGFloat {
        return regular ? 20 : 10
    }
    
    var margin: UIEdgeInsets {
        let result: CGFloat = {
            let baseWidth: CGFloat = 320
            if AppDelegateObject.window!.bounds.width == baseWidth {
                return self.space
            }
            let multiply: CGFloat = 2
            return ((AppDelegateObject.window!.bounds.width * self.space) / baseWidth) * multiply
        }()
        return UIEdgeInsets(top: space, left: ceil(result), bottom: space, right: ceil(result))
    }
    
    var viewMargin: UIEdgeInsets {
        return UIEdgeInsets(redius: space)
    }
    
    var cellMargin: UIEdgeInsets {
        return UIEdgeInsets(top: space/2, left: space, bottom: space/2, right: space)
    }
    
    init(traitCollection: UITraitCollection) {
        self.regular = traitCollection.horizontalSizeClass == .regular
    }
    
    static func shadowImage(height: CGFloat) -> UIImage? {
        
        let size = CGSize(width: 3, height: height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        let currentContext = UIGraphicsGetCurrentContext()
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let gradientOption = CGGradientDrawingOptions(rawValue: 0)
        
        let startComponent = UIColor.black.components
        
        let componentCount: Int = 2
        let components: [CGFloat] = [
            startComponent.red, startComponent.green,   startComponent.blue,    0,
            startComponent.red, startComponent.green,   startComponent.blue,    0.5,
            ]
        
        let locations: [CGFloat] = [0, 1]
        let gradient = CGGradient(colorSpace: colorSpace, colorComponents: components, locations: locations, count: componentCount)
        
        currentContext?.drawLinearGradient(gradient!, start: CGPoint(x: 2, y: 0), end: CGPoint(x: 2, y: size.height), options: gradientOption)
        let gradientImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return gradientImage?.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 1, bottom: 0, right: 1))
    }
}

extension UIColor {
    
    class var appLightGreen: UIColor {
        return #colorLiteral(red: 0, green: 0.8117647059, blue: 0.7725490196, alpha: 1)
    }
    
    class var appGreen: UIColor {
        return #colorLiteral(red: 0, green: 0.5607843137, blue: 0.5333333333, alpha: 1)
    }
    
    class var appDarkGreen: UIColor {
        return #colorLiteral(red: 0, green: 0.3450980392, blue: 0.3294117647, alpha: 1)
    }
    
    class var appSuperDarkGreen: UIColor {
        return #colorLiteral(red: 0, green: 0.2509803922, blue: 0.231372549, alpha: 1)
    }
    
    class var appBGGreen: UIColor {
        return #colorLiteral(red: 0.8784313725, green: 0.9882352941, blue:0.9843137255, alpha: 1)
    }
    
    class var appLightGray: UIColor {
        return #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
    }
    
    class var appDarkGray: UIColor {
        return #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
    }
    
    class var appRed: UIColor {
        return #colorLiteral(red: 0.9333333333, green: 0.2235294118, blue: 0.1647058824, alpha: 1)
    }
}

enum AppFontStyle: Int {
    case H0
    case H1
    case H2
    case H3
    case H4
    case H5
    case H6
    case H7
    case H8
    case H9
}

struct AppTextStyle {
    
    let regular: Bool
    
    init(regular: Bool = false) {
        self.regular = regular
    }
    
    init(traitCollection: UITraitCollection) {
        self.regular = traitCollection.horizontalSizeClass == .regular
    }
    
    func font(style: AppFontStyle) -> UIFont {
        
        switch style {
        case .H0:
            return UIFont.customFont(regular ? 46:36, weight: .dgtRegular)
        case .H1:
            return UIFont.customFont(regular ? 46:30, weight: .dgtRegular)
        case .H2:
            return UIFont.customFont(regular ? 40:24, weight: .dgtBold)
        case .H3:
            return UIFont.customFont(regular ? 40:24, weight: .dgtMedium)
        case .H4:
            return UIFont.customFont(regular ? 40:24, weight: .dgtRegular)
        case .H5:
            return UIFont.customFont(regular ? 32:20, weight: .dgtMedium)
        case .H6:
            return UIFont.customFont(regular ? 30:20, weight: .dgtRegular)
        case .H7:
            return UIFont.customFont(regular ? 30:18, weight: .dgtRegular)
        case .H8:
            return UIFont.customFont(regular ? 24:16, weight: .dgtRegular)
        case .H9:
            return UIFont.customFont(regular ? 22:14, weight: .dgtRegular)
        }
    }
}

