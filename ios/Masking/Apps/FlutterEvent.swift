//
//  FlutterEvent.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 24/3/2564 BE.
//  Copyright © 2564 BE Aduldach Pradubyart. All rights reserved.
//

import Foundation
import Flutter

class AppFlutterEvent: NSObject, FlutterStreamHandler {
    
    var events: FlutterEventSink?
    
    func onListen(withArguments arguments: Any?, eventSink events: @escaping FlutterEventSink) -> FlutterError? {
        self.events = events
        return nil
    }
    
    func onCancel(withArguments arguments: Any?) -> FlutterError? {
        self.events = nil
        return nil
    }
    
    func backToMain() {
        self.events?("backToRoot")
    }
}
