//
//  AppService.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 21/1/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//


import Foundation
import DGTEngineSwift
import Async
import TUSKit

let kAppCompany = "mh"
let kQRApp = "megahome"

// Production
let kHomeProService = "https://api.homepro.co.th/hpg/prd"
let kServiceClienId = "40bbb073d96f196d3cfbe7834ad2f3cf"
let kServiceClienSecret = "37e041077817e509d28375d23ed6a123"

// Development
//let kHomeProService = "https://qas-api.homepro.co.th/hpg/qas"
//let kServiceClienId = "c0a5635f2c3903ed5e89988ce441b164"
//let kServiceClienSecret = "49acc8c80f6aeef5dea320a0d5c6e133"

enum AppError: Error {
    case invalidParameter
    case invalidResponse
    case unknow
    case custom(message: String)
    
    case tokenExpire
    
    case loginSessionExpire(message: String)
    case service(code: Int, message: String)
    
    case forceUpdate(url: String?)
    
    init(code: Int, message: String) {
        if code == -18000 {
            self = .loginSessionExpire(message: message)
        } else {
            self = .service(code: code, message: message)
        }
    }
    
    var localizedDescription: String {
        switch self {
        case .service(_, let message):
            return message
        case .loginSessionExpire(let message):
            return message
        case .custom(let message):
            return message
        default:
            return LocalizedString("UnknowErrorCondition")
        }
    }
}

class AppService {
    
    struct AppResponseStatus: Decodable {
        let status: Int
        let message: String
    }
    
    struct AppHTTPStatus: Decodable {
        let httpCode: String
        let httpMessage: String
        let moreInformation: String
    }
    
    lazy var uploadSession: TUSSession = {
        
        let fileManager = FileManager.default
        let folder: URL? = {
            do {
                let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
                let fileURL = documentDirectory.appendingPathComponent("Upload", isDirectory: true)
                if !fileManager.fileExists(atPath: fileURL.path) {
                    try fileManager.createDirectory(at: fileURL, withIntermediateDirectories: false, attributes: nil)
                }
                return fileURL
            } catch {
                DLog("Error : \(error)")
                return nil
            }
        }()
        
        let store = TUSFileUploadStore(url: folder ?? FileManager.default.urls(for: .applicationSupportDirectory, in: .userDomainMask).first!)!
        let url = "https://fittile.digitopolisstudio.com/upload"
        return TUSSession(endpoint: URL(string: url)!, dataStore: store, allowsCellularAccess: true)
    }()
    
    var authenToken: String?
    
    // MARK: -------------------------------------------------------
    // MARK: Services Async
    // MARK: -------------------------------------------------------
    
    class var call: AppService {
        struct Static {
            static let instance: AppService = AppService()
        }
        return Static.instance
    }
    
    class var authenHeader: [String: String] {
        let clientSecret = "fEyzFz94EDV3nt58TZrLqaKPG6vZn9yP6jxdZVF8"
        let authenData = ("fittile" + ":" + clientSecret).data(using: .utf8)!
        let authen = "Basic " + authenData.base64EncodedString()
        return ["Authorization": authen]
    }
    
    init() {
        NetworkService.serviceURL = kHomeProService + "/Master/"
//        NetworkService.isLogResponse = true
    }
    
    class func setup() {
        let _ = self.call
    }
    
    class func call<U>(_ function: @escaping (AppService) throws -> U, completion: @escaping ((_ result: U?, _ error: AppError?) -> Void)) {
        
        let queue: DispatchQueue = {
            if let currentQueue = OperationQueue.current?.underlyingQueue {
                return currentQueue
            }
            return Thread.isMainThread ? DispatchQueue.main : DispatchQueue.global(qos: .background)
        }()
        Async.background {
            do {
                let result = try function(self.call)
                queue.async {
                    completion(result, nil)
                }
            } catch let error as AppError {
                DLog("service error : \(error)")
                
                if case .tokenExpire = error {
                    self.call.authenToken = nil
                    do {
                        let result = try function(self.call)
                        queue.async {
                            completion(result, nil)
                        }
                    } catch {
                        DLog("service error : \(error)")
                        queue.async {
                            completion(nil, .unknow)
                        }
                    }
                } else {
                    queue.async {
                        completion(nil, error)
                    }
                }
            } catch let error as NSError {
                DLog("service error : \(error)")
                queue.async {
                    completion(nil, .unknow)
                }
            } catch {
                DLog("service error : \(error)")
                queue.async {
                    completion(nil, .unknow)
                }
            }
        }
    }
    
    class func async(_ handler: @escaping () throws -> Void, errorHandler: @escaping (AppError) -> Void) {
        Async.background {
            do {
                try handler()
            } catch let error as AppError {
                Async.main {
                    errorHandler(error)
                }
            } catch {
                Async.main {
                    errorHandler(AppError.unknow)
                }
            }
        }
    }
    
    // MARK: -------------------------------------------------------
    // MARK: Services
    // MARK: -------------------------------------------------------
    
    func token() throws -> String {
        
        if let token = authenToken {
            return token
        }
        
        let param = [
            "client_id": kServiceClienId,
            "client_secret": kServiceClienSecret,
            "scope": "Master",
            "grant_type": "client_credentials",
        ]

        let url = kHomeProService + "/homeprooaprovider/oauth2/token"
        let header = ["Content-Type": "application/x-www-form-urlencoded"]
        let request = try NetworkService.request(url: url, method: .POST, param: param, header: header)
        let data = try NetworkService.syncData(from: request)
        
        struct TokenResponse: Decodable {
            let tokenType: String
            let accessToken: String
            let scope: String
            let expiresIn: Int
        }
        
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        let result = try decoder.decode(TokenResponse.self, from: data)
      
        authenToken = result.accessToken;
        return result.accessToken;
    }
    
    func calculate(_ data: TileCalculate) throws -> (id: String, box: Int) {
        
        var params = [String: Any?]()
        if let id = data.tile?.id {
            params["id"] = id
        }
        params["company"] = kAppCompany
        if case .square_metre(let size) = data.area {
            params["area"] = String(format: "%f", size)
        } else if case .metre(let width, let length) = data.area {
            params["width"] = String(format: "%f", width)
            params["length"] = String(format: "%f", length)
        }
        if case .percent(let value) = data.extra {
            params["extra"] = String(format: "%d", value)
        }
        if let size = data.size {
            params["size"] = size.inch.width + "x" + size.inch.length
            params["unit"] = "inch"
            params["type"] = size.type == .floor ? "floor" : "wall"
        }
        
        let authenToken = try token()
        let header: [String: String] = [
            "Authorization": "Bearer " + authenToken,
            "X-IBM-Client-id": kServiceClienId,
        ]
        
        DLog("NetworkService.serviceURL : " + (NetworkService.serviceURL ?? "no url"))
        let request = try NetworkService.request(path: "GET_FITTILE_CALCULATE", param: params, header: header)
        let data = try NetworkService.syncData(from: request)

        let decoder = AppUtility.decoder
        
        if let httpStatus = try? decoder.decode(AppHTTPStatus.self, from: data), httpStatus.httpCode == "401" {
            throw AppError.tokenExpire
        }
        
        struct TileBox: Decodable {
            let id: String
            let box: String
        }
        let result = try decoder.decode(TileBox.self, from: data)
        return (id: result.id, box: Int(result.box) ?? 0)
    }
    
    func tile(id: String) throws -> Tile {
        
        let authenToken = try token()
        let header: [String: String] = [
            "Authorization": "Bearer " + authenToken,
            "X-IBM-Client-id": kServiceClienId,
        ]
        let request = try NetworkService.request(path: "GET_FITTILE_INFO", param: ["id": id, "company": kAppCompany], header: header)
        let data = try NetworkService.syncData(from: request)
        
        let decoder = AppUtility.decoder
        if let httpStatus = try? decoder.decode(AppHTTPStatus.self, from: data), httpStatus.httpCode == "401" {
            throw AppError.tokenExpire
        }
        
        let tile = try decoder.decode(Tile.self, from: data)
        return tile
    }
    
    func tileList(_ param: [String: Any?]? = nil) throws -> TileList {
        
        let authenToken = try token()
        let header: [String: String] = [
            "Authorization": "Bearer " + authenToken,
            "X-IBM-Client-id": kServiceClienId,
        ]
        
        var params = [String: Any?]()
        if let param = param {
            params = param;
        } else {
            params["size"] = "10"
        }
        params["company"] = kAppCompany
        
        let request = try NetworkService.request(path: "GET_FITTILE_DEFAULT2", param: params, header: header)
        let data = try NetworkService.syncData(from: request)
        
        let decoder = AppUtility.decoder
        if let httpStatus = try? decoder.decode(AppHTTPStatus.self, from: data), httpStatus.httpCode == "401" {
            throw AppError.tokenExpire
        }
        
        let tile = try decoder.decode(TileList.self, from: data)
        return tile
    }
    
    func tileSize(type: Tile.PlanType? = nil) throws -> TileSizeList {
        
        let authenToken = try token()
        let header: [String: String] = [
            "Authorization": "Bearer " + authenToken,
            "X-IBM-Client-id": kServiceClienId,
        ]
        
        var params = [String: Any?]()
        switch type {
        case .floor?:
            params["type"] = "floor"
        case .wall?:
            params["type"] = "wall"
        default:
            break
        }
        params["company"] = kAppCompany
        
        let request = try NetworkService.request(path: "GET_FITTILE_SIZE", param: params, header: header)
        let data = try NetworkService.syncData(from: request)
        
        let decoder = AppUtility.decoder
        if let httpStatus = try? decoder.decode(AppHTTPStatus.self, from: data), httpStatus.httpCode == "401" {
            throw AppError.tokenExpire
        }
        
        let tile = try decoder.decode(TileSizeList.self, from: data)
        return tile
    }
    
    func tileProject(transactionId: String) throws -> TileProject {
        
        let dgtServiceURL = "https://fittile.digitopolisstudio.com/api/project?transaction_id=\(transactionId)&app=\(kQRApp)"
        let header = AppService.authenHeader + ["Accept-Encoding": "gzip,deflate", "Content-Type": "application/x-www-form-urlencoded"]
        
        let request = try NetworkService.request(url: dgtServiceURL, method: .GET, header: header)
        let data = try NetworkService.syncData(from: request)
        
        let decoder = AppUtility.decoder
        struct ProjectRessult: Decodable {
            let status: Int
            let message: String
            let project: TileProject
        }
        do {
            let result = try decoder.decode(ProjectRessult.self, from: data)
            return result.project
        } catch {
            let result = try decoder.decode(AppResponseStatus.self, from: data)
            throw AppError.init(code: result.status, message: result.message)
        }
    }
    
    func createShareProject(project: TileProject) throws -> String {
        
        let dgtServiceURL = "https://fittile.digitopolisstudio.com/api/project"
        let header = AppService.authenHeader + ["Accept-Encoding": "gzip,deflate", "Content-Type": "application/x-www-form-urlencoded"]
        let request = try NetworkService.request(url: dgtServiceURL, method: .POST, param: ["transaction_id": project.id, "app": kQRApp], header: header)
        let data = try NetworkService.syncData(from: request)
        
        let decoder = AppUtility.decoder
        struct CreateProjectRessult: Decodable {
            let status: Int
            let message: String
            let url: String
        }
        
        do {
            let result = try decoder.decode(CreateProjectRessult.self, from: data)
            return result.url
        } catch {
            let result = try decoder.decode(AppResponseStatus.self, from: data)
            throw AppError.init(code: result.status, message: result.message)
        }
    }
    
    func editShareProject(project: TileProject) throws -> Bool {
        
        let dgtServiceURL = "https://fittile.digitopolisstudio.com/api/project/" + project.id
        let header = AppService.authenHeader + ["Accept-Encoding": "gzip,deflate", "Content-Type": "application/x-www-form-urlencoded"]
        let request = try NetworkService.request(url: dgtServiceURL, method: .POST, param: ["title": project.title, "date": project.date, "app": kQRApp], header: header)
        let data = try NetworkService.syncData(from: request)
        
        let decoder = AppUtility.decoder
        let result = try decoder.decode(AppResponseStatus.self, from: data)
        return result.status == 200
    }
    
    func deleteShareProject(project: TileProject) throws -> Bool {
        
        let dgtServiceURL = "https://fittile.digitopolisstudio.com/api/project"
        let header = AppService.authenHeader + ["Accept-Encoding": "gzip,deflate", "Content-Type": "application/x-www-form-urlencoded"]
        let request = try NetworkService.request(url: dgtServiceURL, method: .DELETE, param: ["transaction_id": project.id, "app": kQRApp], header: header)
        let data = try NetworkService.syncData(from: request)
        
        let decoder = AppUtility.decoder
        let result = try decoder.decode(AppResponseStatus.self, from: data)
        return result.status == 200
    }
}

