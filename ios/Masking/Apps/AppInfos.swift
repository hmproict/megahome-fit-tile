//
//  AppData.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 21/1/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import Foundation

class AppInfos: Codable {
    
    var tileDownloadIndex = 1
    var unknowRoomNameIndex = 1
    var albumIdentifier: String?
    var tutorialCompleted: Bool = false
    var clearCatalogSize: Bool? = false
}
