//
//  AppUtility.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 21/1/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import Foundation
import UIKit
import EventKit
import Async
import DGTEngineSwift
import Photos
import PhotosUI

extension DispatchQueue {
    
    class var current: DispatchQueue {
        if let currentQueue = OperationQueue.current?.underlyingQueue {
            return currentQueue
        }
        return Thread.isMainThread ? DispatchQueue.main : DispatchQueue.global(qos: .background)
    }
}

class AppUtility {

    enum PhotoAuthenStatus {
        case allow
        case limit
        case none
    }
    
    class var isRunOnDevice: Bool {
        #if targetEnvironment(simulator)
        return false
        #else
        return true
        #endif
    }
    
    class var defaultDateFormat: DateFormatter {
        struct Static {
            static let instance: DateFormatter = {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                formatter.locale = .us
                return formatter
            }()
        }
        return Static.instance
    }
    
    class var decoder: JSONDecoder {
        struct Static {
            static let instance: JSONDecoder = {
                let decoder = JSONDecoder()
                let format = DateFormatter()
                format.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                format.locale = .us
                decoder.dateDecodingStrategy = .formatted(format)
                return decoder
            }()
        }
        return Static.instance
    }
    
    class func isNoInternetConnectionError(_ error: NSError?) -> Bool {
        if error?.domain == NSURLErrorDomain && error?.code == NSURLErrorNotConnectedToInternet {
            return true
        }
        return false
    }
    
    class func parameters(for url: URL) -> [String: String] {
        var parameter = [String: String]()
        if let pairs = url.query?.components(separatedBy: "&") {
            for pair in pairs {
                
                let bits = pair.components(separatedBy: "=")
                if let key = bits.first?.removingPercentEncoding, let value =  bits.last?.removingPercentEncoding {
                    parameter[key] = value
                }
            }
        }
        return parameter
    }
    
    enum AppFolder {
        case none
        case image
        case screenShot
        
        func name() -> String {
            switch self {
            case .none: return ""
            case .image: return "images"
            case .screenShot: return "screenshots"
            }
        }
    }
    
    @discardableResult
    class func saveImageToDocument(_ image: UIImage, folder: AppFolder = .image) -> String? {
        
        let folderName = folder.name()
        let fileManager = FileManager.default
        
        let folder: URL? = {
            do {
                let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
                let fileURL = documentDirectory.appendingPathComponent(folderName, isDirectory: true)
                if !fileManager.fileExists(atPath: fileURL.path) {
                    try fileManager.createDirectory(at: fileURL, withIntermediateDirectories: false, attributes: nil)
                }
                return fileURL
            } catch {
                DLog("Error : \(error)")
                return nil
            }
        }()
        
        let uuid = UUID().uuidString
        let dateString = DateFormatter.string(format: "yyyy-MM-dd_HH-mm-ss", date: Date(), locale: .us)
        let name = uuid + dateString + ".jpg"
        if let imageURL = folder?.appendingPathComponent(name), let data = image.jpegData(compressionQuality: 1) {
            
            do {
                try data.write(to: imageURL)
            } catch {
                DLog("Error : \(error)")
            }
            return name
        }
        return nil
    }
    
    @discardableResult
    class func moveImageToDocument(url: URL, folder: AppFolder = .image) -> String? {
        
        let folderName = folder.name()
        let fileManager = FileManager.default
        
        let folder: URL? = {
            do {
                let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
                let fileURL = documentDirectory.appendingPathComponent(folderName, isDirectory: true)
                if !fileManager.fileExists(atPath: fileURL.path) {
                    try fileManager.createDirectory(at: fileURL, withIntermediateDirectories: false, attributes: nil)
                }
                return fileURL
            } catch {
                DLog("Error : \(error)")
                return nil
            }
        }()
        
        let uuid = UUID().uuidString
        let dateString = DateFormatter.string(format: "yyyy-MM-dd_HH-mm-ss", date: Date(), locale: .us)
        let name = uuid + dateString + ".jpg"
        if let imageURL = folder?.appendingPathComponent(name) {
            
            do {
                try fileManager.moveItem(at: url, to: imageURL)
            } catch {
                DLog("Error : \(error)")
            }
            return name
        }
        return nil
    }
    
    class func pathFile(_ fileName: String, in folder: AppFolder = .image) -> String {
        guard var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return ""
        }
        
        switch folder {
        case .none: break
        default: documentsURL.appendPathComponent(folder.name(), isDirectory: true)
        }
        documentsURL.appendPathComponent(fileName)
        return documentsURL.path
    }
    
    class func fileExists(_ fileName: String, in folder: AppFolder = .image) -> Bool {
        let path = self.pathFile(fileName, in: folder)
        return FileManager.default.fileExists(atPath: path)
    }
    
    class func authenPhotoLibraryAlbum(_ handler: @escaping (PhotoAuthenStatus) -> Void) {
        switch PHPhotoLibrary.authorizationStatus() {
        case .notDetermined:
            if #available(iOS 14, *) {
                PHPhotoLibrary.requestAuthorization(for: .readWrite) { status in
                    switch status {
                    case .authorized:
                        handler(.allow)
                    case .limited:
                        handler(.limit)
                    default:
                        handler(.none)
                    }
                }
            } else {
                PHPhotoLibrary.requestAuthorization( { status in
                    switch status {
                    case .authorized:
                        handler(.allow)
                    case .limited:
                        handler(.limit)
                    default:
                        handler(.none)
                    }
                })
            }
        case .authorized:
            handler(.allow)
        case .limited:
            handler(.limit)
        default:
            handler(.none)
        }
    }
    
    class func createPhotoLibraryAlbum(name: String, completion: @escaping ((_ collection: PHAssetCollection?) -> Void)) {
        
        if let identifier = AppData.appInfos.albumIdentifier {
            
            let fetchResult = PHAssetCollection.fetchAssetCollections(withLocalIdentifiers: [identifier], options: nil)
            guard let album: PHAssetCollection = fetchResult.firstObject else {
                // FetchResult has no PHAssetCollection
                completion(nil)
                return
            }
            completion(album)
        } else {
            var albumPlaceholder: PHObjectPlaceholder?
            PHPhotoLibrary.shared().performChanges({
                // Request creating an album with parameter name
                let createAlbumRequest = PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: name)
                // Get a placeholder for the new album
                albumPlaceholder = createAlbumRequest.placeholderForCreatedAssetCollection
            }, completionHandler: { success, error in
                if success {
                    guard let placeholder = albumPlaceholder else {
                        fatalError("Album placeholder is nil")
                    }
                    
                    let fetchResult = PHAssetCollection.fetchAssetCollections(withLocalIdentifiers: [placeholder.localIdentifier], options: nil)
                    guard let album: PHAssetCollection = fetchResult.firstObject else {
                        // FetchResult has no PHAssetCollection
                        completion(nil)
                        return
                    }
                    
                    // Saved successfully!
                    AppData.appInfos.albumIdentifier = placeholder.localIdentifier
                    DLog("type: \(album.assetCollectionType)")
                    completion(album)
                    AppData.saveInfosData()
                }
                else if let e = error {
                    // Save album failed with error
                    DLog("error : \(e.localizedDescription)")
                    completion(nil)
                }
                else {
                    // Save album failed with no error
                    completion(nil)
                }
            })
        }
    }
    
    class func saveVideos(url: URL, to album: PHAssetCollection, completion: @escaping ((_ success: Bool) -> Void)) {
        PHPhotoLibrary.shared().performChanges({
            
            let request = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url)
            // Request editing the album
            guard let albumChangeRequest = PHAssetCollectionChangeRequest(for: album) else {
                // Album change request has failed
                completion(false)
                return
            }
            // Get a placeholder for the new asset and add it to the album editing request
            guard let asset = request?.placeholderForCreatedAsset else {
                completion(false)
                return
            }
            
            albumChangeRequest.addAssets([asset] as NSArray)
        }, completionHandler: { success, error in
            if success {
                // Saved successfully!
                completion(true)
            }
            else if let e = error {
                // Save photo failed with error
                DLog("error : \(e.localizedDescription)")
                completion(false)
            }
            else {
                // Save photo failed with no error
                completion(false)
            }
        })
    }
    
    class func saveImage(urls: [URL], to album: PHAssetCollection, completion: @escaping ((_ success: Bool) -> Void)) {
        PHPhotoLibrary.shared().performChanges({
            
            let requests = urls.compactMap({ PHAssetChangeRequest.creationRequestForAssetFromImage(atFileURL: $0) })
            
            // Request editing the album
            guard let albumChangeRequest = PHAssetCollectionChangeRequest(for: album) else {
                // Album change request has failed
                completion(false)
                return
            }
            // Get a placeholder for the new asset and add it to the album editing request
            guard !requests.isEmpty else {
                completion(false)
                return
            }
            
            let assets = requests.compactMap({ $0.placeholderForCreatedAsset })
            albumChangeRequest.addAssets(assets as NSArray)
        }, completionHandler: { success, error in
            if success {
                // Saved successfully!
                completion(true)
            }
            else if let e = error {
                // Save photo failed with error
                DLog("error : \(e.localizedDescription)")
                completion(false)
            }
            else {
                // Save photo failed with no error
                completion(false)
            }
        })
    }
}

class AppStoryboardSegue: UIStoryboardSegue {
    
    override func perform() {
        if let navigation = source.navigationController {
            navigation.pushViewController(destination, animated: true)
        } else {
            source.present(destination, animated: true, completion: nil)
        }
    }
}


extension AppStoryboardIdentifier: Equatable {
    public static func ==(lhs: AppStoryboardIdentifier, rhs: AppStoryboardIdentifier) -> Bool {
        return lhs.rawValue == rhs.rawValue
    }
}

extension AppStoryboardIdentifier: Hashable {
    var hashValue: Int {
        return self.rawValue.hashValue
    }
}

func ==(lhs: String?, rhs: AppStoryboardIdentifier) -> Bool {
    return lhs == rhs.rawValue
}

func ==(lhs: AppStoryboardIdentifier, rhs: String?) -> Bool {
    return lhs.rawValue == rhs
}

enum AppSegueIdentifier: String {
  
    case location = "GotoLocationSegueIdentifier"
}

enum AppStoryboardIdentifier: String {
    
    case nointernet = "NoInternetConnectionStoryboardIdentifier"
}

extension AppSegueIdentifier: Equatable {
    public static func ==(lhs: AppSegueIdentifier, rhs: AppSegueIdentifier) -> Bool {
        return lhs.rawValue == rhs.rawValue
    }
}

func ==(lhs: String, rhs: AppSegueIdentifier) -> Bool {
    return lhs == rhs.rawValue
}

func ==(lhs: AppSegueIdentifier, rhs: String) -> Bool {
    return lhs.rawValue == rhs
}

extension UIViewController {
    func performSegue(withIdentifier identifier: AppSegueIdentifier, sender: Any?) {
        self.performSegue(withIdentifier: identifier.rawValue, sender: sender)
    }
}

public extension Notification.Name {
    
    static let MyProjectsSaveChanged = Notification.Name(rawValue:"FTMyProjectsSaveChanged")
}

extension UITapGestureRecognizer {
    
    func didTapAttributedText(in label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y);
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}
