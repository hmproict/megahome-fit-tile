//
//  AppDelegate.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 7/1/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import UIKit
import DGTEngineSwift
import RealmSwift
import Async
import Firebase
import Flutter
import FlutterPluginRegistrant

let AppDelegateObject = UIApplication.shared.delegate as! AppDelegate

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    lazy var flutterEngine = FlutterEngine()
    lazy var flutterEvent = AppFlutterEvent()
    
    var window: UIWindow?
    
    fileprivate var menuControllers: [String: UIViewController] = [:]
    
    var enableAutoOrientation = true
    var navigationController: FTTabbarViewController {
        return self.window!.rootViewController as! FTTabbarViewController
    }

    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        UIFont.registerFontFamily("DB Helvethaica X")
        return true
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        flutterEngine.run();
        // Used to connect plugins (only if you have plugins with iOS platform code).
        GeneratedPluginRegistrant.register(with: flutterEngine);
        
        let evnet = FlutterEventChannel(name: "com.homepro.megahome.flutter.event.default", binaryMessenger: flutterEngine.binaryMessenger)
        evnet.setStreamHandler(flutterEvent)
        
        DLog("Realm.Configuration.defaultConfiguration.fileURL : \(Realm.Configuration.defaultConfiguration.fileURL)")
        
        setApplicationData()
        setApplicationLanguage()
        setApplicationAppearance()
        setApplicationAnalytic()
        setApplicationService()
        
        AppData.prepareTileStarterPack()
                
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        AppData.saveDeviceData()
        AppData.saveInfosData()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        DLog("application open URL : \(url)")
        if let sheme = url.scheme, sheme == "fittile" {
            let parameters = AppUtility.parameters(for: url)
            switch url.host {
            case "project"? :
                let controller = controllerOfMenuItem(.camera) as? FTMyProjectViewController
                controller?.preferredProjectId = parameters["id"]
                changeToMenuItem(.camera)
            case "tiles"? :
                let controller = controllerOfMenuItem(.catalog) as? FTCatalogListViewController
                controller?.preferredTileId = parameters["id"]
                changeToMenuItem(.catalog)
            case "calculate"? :
                changeToMenuItem(.calculate)
            default:
                break
            }
            return true
        }
        return false
    }

    fileprivate func setApplicationLanguage() {
        
//        if DGTApplicationLanguage.current == DGTDefaultLanguage() {
//            if let language = Locale.current.languageCode, let _ = language.range(of: "th") {
//                DGTApplicationLanguage.setDefaultLangauge(.tha)
//            } else {
//                DGTApplicationLanguage.setDefaultLangauge(.eng)
//            }
//        }
//
//        NotificationCenter.default.addObserver(self, selector: #selector(self.onChangeAppLanguage), name: .dgtApplicationLanguageChange, object: nil)
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        if enableAutoOrientation {
            return DeviceType.IS_IPAD ? .all : .allButUpsideDown
        }
        return .portrait
    }
    
    fileprivate func setApplicationAppearance() {
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        let barbuttonItem = UIBarButtonItem.appearance()
        barbuttonItem.tintColor = .white
        barbuttonItem.setTitleTextAttributes([.font: UIFont.customFont(24)], for: .normal)
        barbuttonItem.setTitleTextAttributes([.font: UIFont.customFont(24)], for: .highlighted)
        UINavigationBar.appearance().titleTextAttributes = [.font: UIFont.customFont(24, weight: .dgtRegular), .foregroundColor: UIColor.white]
        
        let downloadView = DownloadView.appearance()
        downloadView.titleTextAttributes = [.font: AppTextStyle().font(style: .H4), .foregroundColor: UIColor.appGreen]
        downloadView.loadingColor = .appGreen
        downloadView.emptyContentText = LocalizedString("empty content")
        downloadView.loadingText = LocalizedString("loading")
        
        UITableViewCell.appearance().preservesSuperviewLayoutMargins = false
        UITableViewCell.appearance().contentView.preservesSuperviewLayoutMargins = false
    }
    
    fileprivate func setApplicationAnalytic() {

        FirebaseApp.configure()
    }
    
    fileprivate func setApplicationService() {
        AppService.setup()
    }
    
    fileprivate func setApplicationData() {
        AppData.setup()
    }
    
    func sendAppAnalytics(_ name: String) {
        
        let tracker = GAI.sharedInstance().defaultTracker
        tracker?.set(kGAIScreenName, value:  name)
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker?.send(builder?.build() as? [AnyHashable : Any])
    }
    
    @discardableResult
    func changeToMenuItem(_ item:FTMenuItem, animatd: Bool = true, completion: (() -> Void)? = nil) -> Bool {
        
        func goToMenuItem(_ item: FTMenuItem, completion: (() -> Void)?) {
            navigationController.selectedMenuIndex = item.rawValue
            
            let controller = controllerOfMenuItem(item)
            navigationController.setViewControllers([controller], animated: animatd)
            if let completion = completion {
                if animatd {
                    Async.main(after: 0.5) {
                        completion()
                    }
                } else {
                    completion()
                }
            }
        }
        
        if item == .room {
            flutterEvent.backToMain()
        }
        if let presentedViewController = navigationController.presentedViewController {
            presentedViewController.dismiss(animated: animatd, completion: {
                self.sendAppAnalytics(item.analyticScreen)
                goToMenuItem(item, completion: completion)
            })
        } else {
            sendAppAnalytics(item.analyticScreen)
            goToMenuItem(item, completion: completion)
        }
        return true
    }
    
    func displayLoadingView() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        let loadingBGView = UIView(frame: window!.bounds)
        loadingBGView.tag = 5555
        loadingBGView.backgroundColor = .clear
        loadingBGView.isUserInteractionEnabled = true
        
        let loading = DGTActivityIndicatorView(style: .whiteLarge)
        loading.backgroundColor = UIColor(grayscalCode: 0, alpha: 0.6);
        loadingBGView.addSubview(loading)
        loading.center = loadingBGView.centerPointOfView()
        
        window?.addSubview(loadingBGView)
        loading.startAnimating()
    }
    
    func hideLoadingView() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        window?.subviews.first(where: { $0.tag == 5555 })?.removeFromSuperview()
    }
    
    private func controllerOfMenuItem(_ item: FTMenuItem) -> UIViewController {
        let identifier = item.controller.storyboardIdentifier
        if let viewController = menuControllers[identifier] {
            viewController.navigationItem.hidesBackButton = true
            viewController.title = item.pageTitle
            return viewController;
        } else {
            if item == .room {
                let viewController = item.controller.init();
                viewController.title = item.pageTitle
                viewController.navigationItem.hidesBackButton = true
                menuControllers[identifier] = viewController
                return viewController;
            } else {
                if let viewController = navigationController.storyboard?.instantiateViewController(withIdentifier: identifier) {
                    
                    viewController.title = item.pageTitle
                    viewController.navigationItem.hidesBackButton = true
                    menuControllers[identifier] = viewController
                    return viewController;
                } else {
                    let viewController = item.controller.init();
                    viewController.title = item.pageTitle
                    viewController.navigationItem.hidesBackButton = true
                    menuControllers[identifier] = viewController
                    return viewController;
                }
            }
        }
    }
}
