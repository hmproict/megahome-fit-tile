//
//  TileCalculate.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 18/2/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import Foundation

class TileCalculate: Encodable {
    
    enum Area {
        case square_metre(size: Double)
        case metre(width: Double, length: Double)
    }
    
    enum Extra {
        case none
        case percent(value: Int)
    }
    
    var tile: Tile?
    var size: TileSize?
    var sizeUnit: TileSize.Unit = .inch
    var area: Area = .square_metre(size: 0)
    var extra: Extra = .percent(value: 10)
    var box: Int?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case area
        case width
        case length
        case extra
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        guard let tileId = tile?.id ?? size?.id else {
            throw AppError.invalidParameter
        }
        try container.encode(tileId, forKey: .id)
        
        if case .square_metre(let size) = area {
            try container.encode(String(format: "%f", size), forKey: .area)
        } else if case .metre(let width, let length) = area {
            try container.encode(String(format: "%f", width), forKey: .width)
            try container.encode(String(format: "%f", length), forKey: .length)
        }
        if case .percent(let value) = extra {
            try container.encode(String(format: "%d", value), forKey: .extra)
        }
        
    }
}
