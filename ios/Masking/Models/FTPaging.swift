//
//  FTPaging.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 24/2/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import Foundation
import DGTEngineSwift

class FTTilePaging: DownloadManager {
    
    var page: Int = 0
    
    func callService(_ completion: @escaping ((_ tiles: [Tile]?) -> Void)) {
        
        guard let service = serviceObject else {
            completion(nil)
            return
        }
        if stage.contains(.downloading) {return}
        
        self.stage = .downloading
        
        let fromIndex = max(0, page) * service.maxPerPage
        let pageSize = service.maxPerPage
        let param: [String: Any?]
        if var serviceParam = service.param {
            serviceParam["from"] = fromIndex
            serviceParam["size"] = pageSize
            serviceParam["company"] = "hp"
            param = serviceParam
        } else {
            param = ["from": fromIndex, "size": pageSize, "company": "hp"]
        }
        service.param = param
        
        AppService.call({ try $0.tileList(param) }) { [weak self] (tileList, error) in
            guard let weakSelf = self, weakSelf.stage.contains(.downloading) else {
                    return
                }
                
                if let tileList = tileList {
                    
                    let total = Int(tileList.total) ?? 0
                    guard total > 0 && tileList.list.count > 0 else {
                        if weakSelf.isSameServiceObject(service) {
                            self?.stage = [.downloaded, .emptyContent]
                            completion(tileList.list)
                        }
                        return
                    }
                    
                    if weakSelf.isSameServiceObject(service) {
                        if fromIndex + pageSize < total {
                            self?.stage = .downloadMore
                            self?.page += 1
                        } else {
                            self?.stage = .downloaded
                        }
                        completion(tileList.list)
                    }
                } else {
                    if weakSelf.isSameServiceObject(service) {
                        self?.error = error
                        self?.stage = [.error, .downloaded]
                        completion(nil)
                    }
                }
            }
        }
    
    func reloadPageService() {
        error = nil
        stage = .none
        page = 1
    }
}
