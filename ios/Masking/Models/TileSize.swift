//
//  TileSize.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 15/2/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

//    "id": "1061912",
//    "centimetre": {
//        "width": 15,
//        "length": 60
//    },
//    "inch": {
//        "width": 6,
//        "length": 24
//    }

import Foundation
import DGTEngineSwift
import RealmSwift
import Realm

class TileSize: Object, Copyable, Codable {
    
    enum Unit {
        case centimetre
        case inch
    }
    
    struct Size: Equatable {
        let width: String
        let length: String
    }
    
    @objc dynamic var id = ""
    @objc dynamic var sizeInch = ""
    @objc dynamic var sizeCm = ""
    @objc dynamic var boxSize = ""
    @objc dynamic var planType = ""
    
    var centimetre: Size {
        if let centimetre = _centimetre {
            return centimetre
        }
        prepareData()
        return _centimetre ?? Size(width: "0", length: "0")
    }
    var inch: Size {
        if let inch = _inch {
            return inch
        }
        prepareData()
        return _inch ?? Size(width: "0", length: "0")
    }
    var box: Double {
        if let box = _box {
            return box
        }
        prepareData()
        return _box ?? 0
    }
    var type: Tile.PlanType {
        if let type = _type {
            return type
        }
        prepareData()
        return _type ?? .floor
    }
    
    fileprivate var _centimetre: Size?
    fileprivate var _inch: Size?
    fileprivate var _box: Double?
    fileprivate var _type: Tile.PlanType?
    
    override var hash: Int {
        return id.hashValue
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case sizeInch = "size_inch"
        case sizeCm = "size_cm"
        case boxSize = "box"
        case planType = "type"
    }
    
    required convenience init(instance: TileSize) {
        self.init(inch: instance.sizeInch, cm: instance.sizeCm, box: instance.boxSize, type: instance.planType)
        self.id = instance.id
        self.sizeInch = instance.sizeInch
        self.sizeCm = instance.sizeCm
        self.boxSize = instance.boxSize
        self.planType = instance.planType
    }
    
    required convenience init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let sizeInch = try container.decode(String.self, forKey: .sizeInch)
        let sizeCm = try container.decode(String.self, forKey: .sizeCm)
        
        let boxSize = try container.decode(String.self, forKey: .boxSize)
        let planType = try container.decode(String.self, forKey: .planType)
        
        self.init(inch: sizeInch, cm: sizeCm, box: boxSize, type: planType)
        
        self.sizeInch = sizeInch
        self.sizeCm = sizeCm
        
        self.id = sizeInch + "|" + sizeCm + "|" + planType
        
        self.boxSize = boxSize
        self.planType = planType
    }
    
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(sizeInch, forKey: .sizeInch)
        try container.encode(sizeCm, forKey: .sizeCm)
        try container.encode(planType, forKey: .planType)
        try container.encode(box, forKey: .boxSize)
    }
    
    init(inch: String, cm: String, box: String, type: String) {
        let cmComponent = cm.uppercased().components(separatedBy: "X")
        self._centimetre = Size(width: cmComponent.first ?? "0", length: cmComponent.last ?? "0")
        
        let inchComponent = inch.uppercased().components(separatedBy: "X")
        self._inch = Size(width: inchComponent.first ?? "0", length: inchComponent.last ?? "0")
        
        self._box = Double(box) ?? 0.0
        
        switch type {
        case "Wall":
            self._type = .wall
        default:
            self._type = .floor
        }
    }
    
    required init() {
        super.init()
    }
    
    fileprivate func prepareData() {
        let cmComponent = sizeCm.uppercased().components(separatedBy: "X")
        _centimetre = Size(width: cmComponent.first ?? "0", length: cmComponent.last ?? "0")
        
        let inchComponent = sizeInch.uppercased().components(separatedBy: "X")
        _inch = Size(width: inchComponent.first ?? "0", length: inchComponent.last ?? "0")
        
        _box = Double(boxSize) ?? 0.0
        
        switch planType {
        case "Wall":
            _type = .wall
        default:
            _type = .floor
        }
    }
}

struct TileSizeList: Decodable {
    let list: [TileSize]
}
