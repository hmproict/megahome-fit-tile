//
//  FTMenuItem.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 7/2/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import Foundation
import DGTEngineSwift

enum FTMenuItem: Int {
    case catalog
    case camera
    case room
    case calculate
    
    var image: UIImage {
        switch self {
        case .catalog: return #imageLiteral(resourceName: "tabbar_catalog")
        case .camera: return #imageLiteral(resourceName: "icon_fittile")
        case .room: return #imageLiteral(resourceName: "icon_room")
        case .calculate: return #imageLiteral(resourceName: "tabbar_calculate")
        }
    }
    
    var controller: UIViewController.Type {
        switch self {
        case .catalog: return FTCatalogListViewController.self
        case .camera: return FTMyProjectViewController.self
        case .room: return FTRoomViewController.self
        case .calculate: return FTCalculateViewController.self
        }
    }
    
    var pageTitle: String {
        switch self {
        case .catalog: return LocalizedString("CatalogListPageTitle")
        case .camera: return LocalizedString("MyProjectPageTitle")
        case .room: return LocalizedString("RoomPageTitle")
        case .calculate: return LocalizedString("CalculatePageTitle")
        }
    }
    
    var tabberTitle: String {
        switch self {
        case .catalog: return LocalizedString("TabbarTitleCatalog")
        case .camera: return LocalizedString("TabbarTitleProjects")
        case .calculate: return LocalizedString("TabbarTitleCalculate")
        case .room: return LocalizedString("TabbarTitleRoom")
        }
    }
    
    var analyticScreen: String {
        switch self {
        case .catalog: return "Catalog"
        case .camera: return "Fittile"
        case .calculate: return "Calculate"
        case .room: return "Room"
        }
    }
}
