//
//  TileProject.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 21/1/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import UIKit
import DGTEngineSwift
import RealmSwift
import Realm

enum TileTiltStyle: Int {
    case floor = 0
    case left
    case right
}

class TileProject: Object, Copyable, Decodable {
    
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var title: String = "Room"
    @objc dynamic var imageName: String = ""
    @objc dynamic var screenshotImageName: String = ""
    @objc dynamic var date: Date = Date()
    @objc dynamic var completed: Bool = false
    let artboards = List<TileArtboard>()
    @objc dynamic var qrcodeURL: String = ""
    @objc dynamic var imageURL: String = ""
    @objc dynamic var imageFittileURL: String = ""
    
    var shareProject: Bool {
        return imageURL.isEmpty == false
    }
    
    var artboard: TileArtboard {
        if let artboard = artboards.last {
            return artboard
        }
        addArtboard()
        return artboards.last!
    }
    
    var noOriginImage: Bool {
        return imageURL.isEmpty && imageName.isEmpty
    }
    
    var shareProjectComplete: Bool {
        return shareProject && completed
    }
    
    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case imageName
        case screenshotImageName
        case date
        case stage
        case artboards
        case imageURL = "raw_image"
        case imageFittileURL = "fittile_image"
        case tileId = "tile_id"
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required init(from decoder: Decoder) throws {
        super.init()
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if container.contains(.id) {
            self.id = try container.decode(String.self, forKey: .id)
        }
        if container.contains(.title) {
            self.title = try container.decode(String?.self, forKey: .title) ?? "Shared Project"
        }
        if container.contains(.imageName) {
            self.imageName = try container.decode(String.self, forKey: .imageName)
        }
        if container.contains(.screenshotImageName) {
            self.screenshotImageName = try container.decode(String.self, forKey: .screenshotImageName)
        }
        if container.contains(.date) {
            self.date = try container.decode(Date?.self, forKey: .date) ?? Date()
        }
        if container.contains(.imageURL) {
            self.imageURL = try container.decode(String?.self, forKey: .imageURL) ?? ""
        }
        if container.contains(.imageFittileURL) {
            self.imageFittileURL = try container.decode(String?.self, forKey: .imageFittileURL) ?? ""
        }
        if container.contains(.stage) {
            let stage = try container.decode(String.self, forKey: .stage)
            self.completed = stage == "complete"
        }
        if container.contains(.artboards) {
            let element = try container.decode([TileArtboard].self, forKey: .artboards)
            self.artboards.removeAll();
            for artboard in element {
                self.artboards.append(artboard)
            }
        }
        if container.contains(.tileId), let tileId = try container.decode(String?.self, forKey: .tileId) {
            for id in tileId.components(separatedBy: ",") {
                let tile = Tile()
                tile.id = id
                if let _ = self.artboard.tile {
                    addArtboard()
                }
                self.artboard.tile = tile
            }
        }
    }
    
    required init(instance: TileProject) {
        super.init()
        self.id = instance.id
        self.title = instance.title
        self.imageName = instance.imageName
        self.screenshotImageName = instance.screenshotImageName
        self.date = instance.date
        self.qrcodeURL = instance.qrcodeURL
        self.imageURL = instance.imageURL
        self.completed = instance.completed
        self.imageFittileURL = instance.imageFittileURL
        for artboard in instance.artboards {
            self.artboards.append(artboard.copy())
        }
    }
    
    required init() {
        super.init()
    }
    
    func addArtboard() {
        let artboard = TileArtboard();
        self.artboards.append(artboard);
    }
}

class TileArtboard: Object, Copyable, Decodable {
    
    @objc dynamic var perspective: Float = 0.4
    @objc dynamic var perspectiveType: Int = 0
    @objc dynamic var scale: Float = 1
    @objc dynamic private var _location: String = "0,0"
    @objc dynamic private var _frame: String = "0,0,0,0"
    private let _points = List<String>()
    @objc dynamic var tile: Tile?
    
    var tiltType: TileTiltStyle {
        set {
            perspectiveType = newValue.rawValue
        }
        get {
            return TileTiltStyle.init(rawValue: perspectiveType) ?? .floor
        }
    }
    
    var location: CGPoint {
        set {
            _location = newValue.toString()
        }
        get {
            return CGPoint(string: _location) ?? .zero
        }
    }
    
    var frame: CGRect {
        set {
            _frame = newValue.toString()
        }
        get {
            return CGRect(string: _frame) ?? .zero
        }
    }
    
    var points: [CGPoint] {
        return _points.compactMap({ CGPoint(string: $0) })
    }
    
    func addPoint(_ point: CGPoint) {
        _points.append(point.toString())
    }
    
    func changeLocation(_ location: CGPoint, at index: Int) {
        _points[index] = location.toString()
    }
    
    func removePoint(at index: Int) {
        _points.remove(at: index)
    }
    
    private enum CodingKeys: String, CodingKey {
        case perspective
        case perspectiveType = "perspective_type"
        case scale
        case location
        case points
        case tile
    }
    
    required init(from decoder: Decoder) throws {
        super.init()
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if container.contains(.perspective) {
            self.perspective = try container.decode(Float.self, forKey: .perspective)
        }
        if container.contains(.perspectiveType) {
            self.perspectiveType = try container.decode(Int.self, forKey: .perspectiveType)
        }
        if container.contains(.scale) {
            self.scale = try container.decode(Float.self, forKey: .scale)
        }
        if container.contains(.location) {
            self._location = try container.decode(String.self, forKey: .location)
        }
        if container.contains(.points) {
            let element = try container.decode([String].self, forKey: .points)
            for point in element {
                self._points.append(point)
            }
        }
        if container.contains(.tile) {
            self.tile = try container.decode(Tile.self, forKey: .tile)
        }
    }
    
    required init(instance: TileArtboard) {
        self.perspective = instance.perspective
        self.perspectiveType = instance.perspectiveType
        self.scale = instance.scale
        self._location = instance._location
        self._frame = instance._frame
        for points in instance._points {
            self._points.append(points)
        }
        self.tile = self.tile?.copy()
    }

    required init() {
        super.init()
    }
}

extension CGPoint {
    
    func toString() -> String {
        return "\(self.x),\(self.y)"
    }
    
    init?(string: String) {
        let component = string.components(separatedBy: ",")
        guard let xValue = component[safe:0], let yValue = component[safe:1], let x = Double(xValue), let y = Double(yValue) else {
            return nil
        }
        self.init(x: x, y: y)
    }
}

extension CGRect {
    
    func toString() -> String {
        return "\(self.origin.x),\(self.origin.y),\(self.width),\(self.height)"
    }
    
    init?(string: String) {
        let component = string.components(separatedBy: ",")
        guard let xValue = component[safe:0], let yValue = component[safe:1], let widthValue = component[safe:2], let heightValue = component[safe:3] else {
            return nil
        }
        self.init(x: Double(xValue) ?? 0, y: Double(yValue) ?? 0, width: Double(widthValue) ?? 0, height: Double(heightValue) ?? 0)
    }
}
