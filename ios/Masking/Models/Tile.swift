//
//  Tile.swift
//  Masking
//
//  Created by Aduldach Pradubyart on 21/1/2562 BE.
//  Copyright © 2562 Aduldach Pradubyart. All rights reserved.
//

import UIKit
import DGTEngineSwift
import RealmSwift
import Realm

//    "id": "265147",
//    "short_desc": "ก/บพื้น 40X40cm ไม้ลานนา เบจ",
//    "long_desc": "<h3>กระเบื้อง พื้น 40X40CM ไม้ลานนา เบจ</h3><ul><li>กระเบื้องปูพื้นภายใน ไม้ลานนา</li><li>ขนาด 16X16 นิ้ว สีเบจ</li><li>พื้นผิว:ผิวมัน ดีไซน์กราฟฟิคต่อลาย</li><li>จำหน่ายเป็นกล่อง</li><li>การใช้งาน:ปูพื้นภายใน</li></ul>",
//    "width": "40",
//    "length": "40",
//    "ramdom": "0",
//    "box": "1M2",
//    "feature": "ลายไม้ภายใน",
//    "vendor": "tara",
//    "pattern": "1x1",
//    "image": "https://static.homepro.co.th/fittile/265147.jpg"
//    "floor": "",
//    "wall": "https://s3-ap-southeast-1.amazonaws.com/qas-public-bucket/homepro/FITTILE_IMAGE/02/367/236787/23042021_236787_W$Image.jpg",
//    "left_wall": "https://s3-ap-southeast-1.amazonaws.com/qas-public-bucket/homepro/FITTILE_IMAGE/02/367/236787/23042021_236787_L$Image.jpg",
//    "right_wall": "https://s3-ap-southeast-1.amazonaws.com/qas-public-bucket/homepro/FITTILE_IMAGE/02/367/236787/23042021_236787_R$Image.jpg",
//    "inch_width": "8",
//    "inch_length": "10",
//    "category": "1",
//    "area": "BR,DN,LV,FY,PA,WA"

class Tile: Object, Copyable, Codable {
    
    enum PlanType {
        case floor
        case wall
    }
    
    @objc dynamic var id = ""
    @objc dynamic var shortDesc = ""
    @objc dynamic var longDesc = ""
    @objc dynamic var width: Int = 0
    @objc dynamic var length: Int = 0
    @objc dynamic var random: Int = 0
    @objc dynamic var box = ""
    @objc dynamic var feature = ""
    @objc dynamic var vendor = ""
    @objc dynamic var pattern = ""
    @objc dynamic var image = ""
    @objc dynamic var url = ""
    @objc dynamic var patternImage = ""
    @objc dynamic var type = ""
    
    @objc dynamic var floor = ""
    @objc dynamic var wall = ""
    @objc dynamic var leftWall = ""
    @objc dynamic var rightWall = ""
    @objc dynamic var widthInch: Int = 0
    @objc dynamic var lengthInch: Int = 0
    @objc dynamic var category: Int = 0
    @objc dynamic var area = ""
    
    override var hash: Int {
        return id.hashValue
    }
    
    var drawImage: String {
        if patternImage != "" {
            return patternImage
        }
        return image
    }
    
    var planType: PlanType {
        switch type {
        case "Wall":
            return .wall
        default:
            return .floor
        }
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    private enum CodingKeys: String, CodingKey {
        case id
        case shortDesc = "short_desc"
        case longDesc = "long_desc"
        case width
        case length
        case random
        case box
        case feature
        case vendor
        case pattern
        case patternImage = "image_pattern"
        case image
        case url
        case type
        
        case floor
        case wall
        case leftWall = "left_wall"
        case rightWall = "right_wall"
        case widthInch = "inch_width"
        case lengthInch = "inch_length"
        case category
        case area
    }
    
    required init(instance: Tile) {
        super.init()
        self.id = instance.id
        self.shortDesc = instance.shortDesc
        self.longDesc = instance.longDesc
        self.width = instance.width
        self.length = instance.length
        self.random = instance.random
        self.box = instance.box
        self.feature = instance.feature
        self.vendor = instance.vendor
        self.pattern = instance.pattern
        self.image = instance.image
        self.patternImage = instance.patternImage
        self.url = instance.url
        self.type = instance.type
        
        self.floor = instance.floor
        self.wall = instance.wall
        self.leftWall = instance.leftWall
        self.rightWall = instance.rightWall
        self.widthInch = instance.widthInch
        self.lengthInch = instance.lengthInch
        self.category = instance.category
        self.area = instance.area
    }
    
    required init(from decoder: Decoder) throws {
        super.init()
        
        func parseInt(key: CodingKeys) -> Int {
            if let value = try? container.decode(String.self, forKey: key) {
                return Int(value) ?? 0
            } else if let value = try? container.decode(Int.self, forKey: key) {
                return value
            }
            return 0
        }
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decode(String.self, forKey: .id)
        self.shortDesc = try container.decode(String.self, forKey: .shortDesc)
        self.longDesc = try container.decode(String.self, forKey: .longDesc)
        
        self.width = parseInt(key: .width)
        self.length = parseInt(key: .length)
        
        if container.contains(.random) {
            let random = try container.decode(String.self, forKey: .random)
            self.random = Int(random) ?? 0
        } else {
            self.random = 0
        }
        
        if let box = try? container.decode(String.self, forKey: .box) {
            self.box = box
        } else if let box = try? container.decode(Int.self, forKey: .box) {
            self.box = String(box)
        } else {
            self.box = ""
        }
        self.feature = try container.decode(String.self, forKey: .feature)
        self.vendor = try container.decode(String.self, forKey: .vendor)
        self.pattern = try container.decode(String.self, forKey: .pattern)
        self.image = try container.decode(String.self, forKey: .image)
        
        if container.contains(.type) {
            self.type = try container.decode(String.self, forKey: .type)
        } else {
            self.type = "Floor"
        }
        
        if container.contains(.patternImage) {
            self.patternImage = try container.decode(String.self, forKey: .patternImage)
        }
        
        self.url = try container.decode(String.self, forKey: .url)
        
        self.floor = try container.decode(String.self, forKey: .floor)
        self.wall = try container.decode(String.self, forKey: .wall)
        self.leftWall = try container.decode(String.self, forKey: .leftWall)
        self.rightWall = try container.decode(String.self, forKey: .rightWall)
        self.widthInch = parseInt(key: .widthInch)
        self.lengthInch = parseInt(key: .lengthInch)
        self.category = parseInt(key: .category)
        self.area = try container.decode(String.self, forKey: .area)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(shortDesc, forKey: .shortDesc)
        try container.encode(longDesc, forKey: .longDesc)
        try container.encode(width, forKey: .width)
        try container.encode(length, forKey: .length)
        try container.encode(String(random), forKey: .random)
        try container.encode(Double(box), forKey: .box)
        try container.encode(feature, forKey: .feature)
        try container.encode(vendor, forKey: .vendor)
        try container.encode(pattern, forKey: .pattern)
        try container.encode(image, forKey: .image)
        try container.encode(url, forKey: .url)
        try container.encode(patternImage, forKey: .patternImage)
        try container.encode(type, forKey: .type)
        
        try container.encode(floor, forKey: .floor)
        try container.encode(wall, forKey: .wall)
        try container.encode(leftWall, forKey: .leftWall)
        try container.encode(rightWall, forKey: .rightWall)
        try container.encode(widthInch, forKey: .widthInch)
        try container.encode(lengthInch, forKey: .lengthInch)
        try container.encode(category, forKey: .category)
        try container.encode(area, forKey: .area)
    }
    
    required init() {
        super.init()
    }
}

struct TileList: Decodable {
    let total: String
    let list: [Tile]
}
