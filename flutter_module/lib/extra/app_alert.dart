import 'package:flutter/material.dart';
import 'package:flutter_module/element/app_button.dart';
import 'package:flutter_module/element/app_color.dart';

import 'app_layout.dart';

typedef AppAlertButtonBuilder = List<AppButton> Function(BuildContext context);

class AppAlert {
  final String title;
  final String? desc;
  final Text? descText;
  final Widget? header;
  final Widget? content;
  final AppAlertButtonBuilder button;
  final bool allowTouchToDismiss;
  final bool forceToVerticalButton;

  AppAlert({
    required this.title,
    required this.button,
    this.desc,
    this.descText,
    this.content,
    this.header,
    this.allowTouchToDismiss = false,
    this.forceToVerticalButton = false,
  });

  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  bool isDismiss = false;

  Widget _buildDialog(BuildContext context) {
    final layout = AppLayout(context);
    final theme = Theme.of(context);
    return Center(
      child: ConstrainedBox(
        constraints: BoxConstraints.expand(width: double.infinity, height: double.infinity),
        child: SafeArea(
          child: Center(
            child: SingleChildScrollView(
              child: AppPopupSize(
                child: ClipRRect(
                  borderRadius: layout.borderRadius,
                  child: Container(
                    color: AppColor.white,
                    child: Column(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(color: Theme.of(context).primaryColor),
                          child: Container(
                            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                            child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                              header ?? Container(),
                              Container(
                                padding: header != null ? EdgeInsets.only(top: 10) : EdgeInsets.zero,
                                child: Center(
                                  child: Text(
                                    title,
                                    style: theme.textTheme.headline5?.copyWith(color: Colors.white),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                              if (desc != null || descText != null)
                                Container(
                                  padding: EdgeInsets.only(top: 10),
                                  child: Center(
                                    child: descText ?? Text(desc!, style: theme.textTheme.bodyText2, textAlign: TextAlign.center),
                                  ),
                                ),
                            ]),
                          ),
                        ),
                        if (content != null) Container(padding: EdgeInsets.only(left: 20, right: 20, top: 20), child: content),
                        Container(padding: EdgeInsets.all(20), child: _getContent(context))
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _getContent(BuildContext context) {
    final buttons = button(context);
    List<Widget> _getButtons(BuildContext context, {bool addExpanded = true, bool vertical = false}) {
      final space = vertical ? SizedBox(height: 10) : SizedBox(width: 10);

      return buttons.fold(
        [],
        (o, v) => o.isEmpty ? o + [addExpanded ? Expanded(child: v) : v] : o + [space, addExpanded ? Expanded(child: v) : v],
      );
    }

    if ((buttons.isNotEmpty && buttons.length > 2) || forceToVerticalButton) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: _getButtons(context, addExpanded: false, vertical: true),
      );
    }
    return Row(
      children: _getButtons(context),
    );
  }

  /// Displays defined alert window
  Future<bool?> show(BuildContext context) async {
    return await showGeneralDialog(
        context: context,
        pageBuilder: (BuildContext buildContext, Animation<double> animation, Animation<double> secondaryAnimation) {
          return _buildDialog(buildContext);
        },
        barrierLabel: "Dismiss",
        barrierDismissible: allowTouchToDismiss,
        barrierColor: Colors.black45,
        transitionDuration: Duration(milliseconds: 200),
        transitionBuilder: (
          BuildContext context,
          Animation<double> animation,
          Animation<double> secondaryAnimation,
          Widget child,
        ) {
          return ScaleTransition(
            scale: Tween<double>(
              begin: 0.8,
              end: 1.0,
            ).animate(
              CurvedAnimation(
                parent: animation,
                curve: Interval(
                  0.50,
                  1.00,
                  curve: Curves.linear,
                ),
              ),
            ),
            child: FadeTransition(
              child: child,
              opacity: animation,
            ),
          );
        });
  }

  dismiss() async {
    isDismiss = true;
    final context = _keyLoader.currentContext;
    if (context != null) {
      Navigator.of(context).pop();
    } else {
      await Future.delayed(new Duration(milliseconds: 500));
      final currentContext = _keyLoader.currentContext;
      if (currentContext != null) {
        Navigator.of(currentContext).pop();
      }
    }
  }

  static Future<bool?> showAlert(
    String title,
    BuildContext context, {
    String? detail,
    String? buttonTitle,
  }) async {
    final style = Theme.of(context).textTheme;
    return await AppAlert(
      title: title,
      content: detail != null ? Text(detail, style: style.subtitle1, textAlign: TextAlign.center) : null,
      button: (alertContext) => [
        AppButton(
          title: buttonTitle ?? "Ok",
          color: AppColor.green,
          textStyle: Theme.of(context).textTheme.subtitle1?.copyWith(color: Colors.white),
          onPressed: () => Navigator.of(alertContext).pop(true),
        )
      ],
    ).show(context);
  }

  static Future<bool?> showConfirm(
    String title,
    BuildContext context, {
    String? detail,
    String? confirmTitle,
    String? cancelTitle,
  }) async {
    return await AppAlert(
        title: title,
        content: detail != null ? Text(detail, style: Theme.of(context).textTheme.subtitle1, textAlign: TextAlign.center) : null,
        button: (alertContext) => [
              AppButton.altConfirm(
                cancelTitle ?? "Cancel",
                onPressed: () => Navigator.of(alertContext).pop(false),
              ),
              AppButton(
                title: confirmTitle ?? "Confirm",
                color: AppColor.green,
                textStyle: Theme.of(context).textTheme.subtitle1?.copyWith(color: Colors.white),
                onPressed: () => Navigator.of(alertContext).pop(true),
              )
            ]).show(context);
  }
}
