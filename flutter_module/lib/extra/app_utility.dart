import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
// import 'package:set_app/model/mode.dart';
// import 'package:uuid/uuid.dart';

import 'dart:io';
// import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:path/path.dart' as path;

String formattingAmount(String text) {
  String cleanText = text.replaceAll(",", "");
  final splitTexts = cleanText.split(".");
  final _numberFormat = NumberFormat.decimalPattern();

  String _formatText(String text) {
    if (text.isNotEmpty) {
      final number = double.tryParse(text);
      if (number != null) {
        final amount = _numberFormat.format(number);
        return amount;
      }
    }
    return "";
  }

  if (splitTexts.length > 2) {
    return _formatText(splitTexts.first) + "." + splitTexts[1];
  }

  String suffixText = "";
  final indexOfQ = cleanText.indexOf(".");
  if (indexOfQ != -1) {
    cleanText = splitTexts.first;
    if (splitTexts.last.length > 2) {
      suffixText = "." + splitTexts.last.substring(0, 2);
    } else {
      suffixText = "." + splitTexts.last;
    }
  }
  return _formatText(cleanText) + suffixText;
}

bool isThaiLocale(BuildContext context) {
  return Localizations.localeOf(context).languageCode == "th";
}

void printWrapped(String text) {
  final pattern = new RegExp('.{1,800}'); // 800 is the size of each chunk
  pattern.allMatches(text).forEach((match) => print(match.group(0)));
}

class AppUtility {

  static String formatTime(int number) {
    return (number < 10 ? '0' : '') + number.toString();
  }

  static bool boolValueOf(dynamic? value, {bool defaultValue = false}) {
    if (value is bool) {
      return value;
    } else if (value is int) {
      return value == 1;
    } else if (value is double) {
      return value == 1;
    }
    return defaultValue;
  }

  static int intValueOf(dynamic? value, {int defaultValue = 0}) {
    if (value is bool) {
      return value ? 0 : 1;
    } else if (value is int) {
      return value;
    } else if (value is double) {
      return value.toInt();
    }
    return defaultValue;
  }

  static double doubleValueOf(dynamic? value, {double defaultValue = 0.0}) {
    if (value is bool) {
      return value ? 0 : 1;
    } else if (value is int) {
      return value.toDouble();
    } else if (value is double) {
      return value;
    }
    return defaultValue;
  }

  static ScrollPhysics get scrollPhysics => Platform.isAndroid ? const ClampingScrollPhysics() : const AlwaysScrollableScrollPhysics();

  static Color colorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');
    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }
    return Color(int.parse(hexColor, radix: 16));
  }

  static bool validURL(String url) {
    return Uri.parse(url).isAbsolute;
  }
}

extension AppDateTime on DateTime {
  static DateTime now() {
    final date = DateTime.now();
    return DateTime(date.year, date.month, date.day);
  }

  DateTime get noTime => DateTime(year, month, day);
  DateTime get firstDay => DateTime(year, month, 1);

  bool get isNoTime => this.hour == 0 && this.minute == 0 && this.second == 0;

  static DateTime parse(String formattedString) {
    final date = DateTime.parse(formattedString);
    return date.toLocal();
  }

  String toFullIso8601String() {
    final date = this.toLocal();
    final defaultText = date.toIso8601String();
    final timezone = date.timeZoneOffset;
    final hour = timezone.abs().inHours;
    final minute = timezone.abs().inMinutes - (hour * 60);
    return defaultText + (timezone.isNegative ? "-" : "+") + hour.toString().padLeft(2, "0") + minute.toString().padLeft(2, "0");
  }

  bool sameYearNMonth(DateTime date) {
    return date.year == year && date.month == month;
  }

  DateTime addDay(int days) {
    return this.add(Duration(days: days));
  }
}

List sorted<E>(Iterable input, {int compare(E a, E b)?, key, bool asc = true}) {
  List copy = new List.from(input);
  copy.sort((a, b) {
    if (compare == null && key == null) {
      return asc ? a.compareTo(b) : b.compareTo(a);
    }
    if (compare == null) {
      return asc ? key(a).compareTo(key(b)) : key(b).compareTo(key(a));
    }
    if (key == null) {
      return compare(a, b);
    }
    return compare(key(a), key(b));
  });
  return copy;
}

extension IndexedIterable<E> on Iterable<E> {
  Iterable<T> mapIndexed<T>(T Function(E e, int i) f) {
    var i = 0;
    return map((e) => f(e, i++));
  }
}
