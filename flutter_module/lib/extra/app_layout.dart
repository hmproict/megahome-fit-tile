import 'dart:math';

import 'package:flutter/cupertino.dart' as cupertino;
import 'package:flutter/material.dart';

class AppLayout {
  AppLayout(BuildContext context)
      : this.context = context,
        this.mediaQuery = MediaQuery.of(context);

  final BuildContext context;
  final MediaQueryData mediaQuery;

  Size get size => mediaQuery.size;
  double get textScaleFactor => mediaQuery.textScaleFactor;
  EdgeInsets get devicePadding => mediaQuery.padding;

  EdgeInsets padding({
    EdgeInsets? extra,
    bool bottom = false,
    bool top = false,
    bool left = true,
    bool right = true,
  }) {
    final padding = mediaQuery.padding;
    if (extra != null) {
      return EdgeInsets.only(
          top: top ? padding.top + extra.top : extra.top,
          bottom: bottom ? padding.bottom + extra.bottom : extra.bottom,
          left: left ? padding.left + extra.left : extra.left,
          right: right ? padding.right + extra.right : extra.right);
    }
    return EdgeInsets.only(
      top: top ? padding.top : 0,
      bottom: bottom ? padding.bottom : 0,
      left: left ? padding.left : 0,
      right: right ? padding.right : 0,
    );
  }

  double get space => 10.0;

  Radius get radius => Radius.circular(space);

  BorderRadius get borderRadius => BorderRadius.all(radius);

  double get paddingSize => isMobileStyle ? space : mediaQuery.size.width * 0.025;

  double get paddingBottom => mediaQuery.padding.bottom + mediaQuery.viewInsets.bottom;

  double get sheetSize {
    final margin = mediaQuery.size.width / 9.35;
    return max(mediaQuery.size.width - (margin * 2), 300);
  }

  double get sheetMargin => mediaQuery.size.width / 9.35;

  EdgeInsets get inBoxPadding => EdgeInsets.all(paddingSize);
  double get sheetMarginByScreen => isBigSizeStyle ? sheetMargin : paddingSize;

  EdgeInsets defaultPadding({bool bottom = false, bool top = false}) {
    final insets = EdgeInsets.symmetric(horizontal: paddingSize, vertical: paddingSize / 2);
    final padding = mediaQuery.padding;
    return EdgeInsets.only(
        top: top ? padding.top + insets.top : insets.top,
        bottom: bottom ? padding.bottom + insets.bottom : insets.bottom,
        left: insets.left + padding.left,
        right: insets.right + padding.right);
  }

  bool get isMobileStyle {
    final shortestSide = mediaQuery.size.shortestSide;
    final useMobileLayout = shortestSide < 600;
    if (useMobileLayout) {
      return true;
    }
    return false;
  }

  bool get isLandscapeStyle => isMobileStyle && mediaQuery.orientation == Orientation.landscape;
  bool get isBigSizeStyle => !isMobileStyle || isLandscapeStyle;

  double paddingBottomBy(TextStyle style, {EdgeInsets insets = EdgeInsets.zero}) {
    final textSpan = TextSpan(text: " ", style: style);
    final textPainter = TextPainter(text: textSpan, textDirection: TextDirection.ltr);
    textPainter.layout();
    final height = textPainter.height;
    if (mediaQuery.viewInsets.bottom > 200) {
      return max(mediaQuery.viewInsets.bottom - (height + insets.top + insets.bottom), 0);
    }
    return paddingBottom;
  }

  static SliverGridDelegateWithMaxCrossAxisExtent levelGridStyle(BuildContext context) {
    final media = MediaQuery.of(context);
    return SliverGridDelegateWithMaxCrossAxisExtent(
      maxCrossAxisExtent: 90.0 * media.textScaleFactor,
      mainAxisSpacing: 0.0,
      crossAxisSpacing: 0.0,
      childAspectRatio: 0.65,
    );
  }

  Size textSize(
    String text,
    TextStyle style, {
    double minWidth = 0,
    double maxWidth = double.infinity,
    double? textScaleFactor,
    int? maxLines,
    StrutStyle? strutStyle,
  }) {
    final textSpan = TextSpan(
      text: text,
      style: style,
    );
    final textPainter = TextPainter(
      text: textSpan,
      textAlign: TextAlign.left,
      textDirection: cupertino.TextDirection.ltr,
      textScaleFactor: textScaleFactor ?? mediaQuery.textScaleFactor,
      maxLines: maxLines,
      strutStyle: strutStyle,
    );
    textPainter.layout(minWidth: minWidth, maxWidth: maxWidth);
    return textPainter.size;
  }
}

class AppPopupSize extends StatelessWidget {
  AppPopupSize({required this.child});

  final Widget child;

  @override
  Widget build(BuildContext context) {
    final media = MediaQuery.of(context);
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      final width = min(400.0, constraints.maxWidth - (media.viewInsets.horizontal + 20));
      return SizedBox(width: width, child: child);
    });
  }
}

class AppAspectRatio extends StatelessWidget {
  AppAspectRatio({required this.aspectRatio, this.fullAspectRatio, this.child});

  final double aspectRatio;
  final double? fullAspectRatio;
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    final layout = AppLayout(context);
    if (layout.isBigSizeStyle && fullAspectRatio != null) {
      return AspectRatio(
        aspectRatio: fullAspectRatio!,
        child: child,
      );
    }
    return AspectRatio(
      aspectRatio: aspectRatio,
      child: child,
    );
  }
}
