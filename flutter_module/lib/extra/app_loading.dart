import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:loading_animations/loading_animations.dart';

// ignore: must_be_immutable
class AppLoading extends StatelessWidget {
  bool isDismiss = false;
  BuildContext? loadingContext;
  final Color? color;

  AppLoading({this.color});

  @override
  Widget build(BuildContext context) {
    SchedulerBinding.instance?.addPostFrameCallback((_) {
      if (isDismiss) {
        loadingContext = null;
        Navigator.of(context).pop();
      }
    });

    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: LoadingBouncingGrid.square(
        backgroundColor: color ?? Theme.of(context).primaryColor,
      ),
    );
  }

  dismiss() async {
    isDismiss = true;
    try {
      final context = loadingContext;
      if (context != null) {
        Navigator.of(context).pop();
      } else {
        await Future.delayed(new Duration(milliseconds: 500));
        final currentContext = loadingContext;
        if (currentContext != null) {
          Navigator.of(currentContext).pop();
        }
      }
    } catch (e) {
      await Future.delayed(new Duration(milliseconds: 500));
      dismiss();
    }
  }

  present(BuildContext context, {Color barrierColor = Colors.white54}) {
    showGeneralDialog(
        context: context,
        pageBuilder: (loadingContext, animation1, animation2) {
          this.loadingContext = loadingContext;
          return this;
        },
        barrierDismissible: false,
        transitionDuration: Duration(microseconds: 1),
        barrierColor: barrierColor,
        useRootNavigator: true);
  }

  static AppLoading show(BuildContext context, {Color? color, Color barrierColor = Colors.white54}) {
    final loading = AppLoading(color: color);
    showGeneralDialog(
        context: context,
        pageBuilder: (loadingContext, animation1, animation2) {
          loading.loadingContext = loadingContext;
          return loading;
        },
        barrierDismissible: false,
        transitionDuration: Duration(microseconds: 1),
        barrierColor: barrierColor,
        useRootNavigator: true);
    return loading;
  }
}