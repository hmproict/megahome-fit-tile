import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:path/path.dart' as path;
import 'package:image/image.dart' as image;

class AppImage {
  static var httpClient = new HttpClient()..badCertificateCallback = ((X509Certificate cert, String host, int port) => true);

  static String name(String name) {
    return "assets/images/" + name;
  }

  static String room(String name) {
    return "assets/images/previews/" + name;
  }

  static String roomUrl(String name) {
    return "https://cloud.digitopolisstudio.com/download/fittile/" + name;
  }

  static String icon(String name) {
    return "assets/images/icons/" + name;
  }

  static Future<ImageProvider> roomCacheImage(String name) async {
    final document = await path_provider.getApplicationDocumentsDirectory();
    final showcases = Directory(path.join(document.path, "showcases"));
    final showcasesImagePath = path.join(showcases.path, name);
    final file = File(showcasesImagePath);
    if (await file.exists()) {
      final imageData = await file.readAsBytes();
      return MemoryImage(imageData);
    }

    final showcasesExists = await showcases.exists();
    if (!showcasesExists) {
      await showcases.create();
    }

    final url = roomUrl(name);
    print("load room URL : $url");
    var request = await httpClient.getUrl(Uri.parse(url));
    var response = await request.close();
    var bytes = await consolidateHttpClientResponseBytes(response);
    await file.writeAsBytes(bytes);
    return MemoryImage(bytes);
  }

  static Future<ImageProvider> tileCacheImage(String url, {double? width, double? height}) async {
    bool _validURL = Uri.parse(url).isAbsolute;
    if (!_validURL) {
      throw PlatformException(code: "009");
    }

    String getFileName() {
      final baseName = path.basenameWithoutExtension(url);
      final extension = path.extension(url);
      if (width != null && height != null) {
        return baseName + width.toStringAsFixed(0) + height.toStringAsFixed(0) + extension;
      } else if (width != null) {
        return baseName + width.toStringAsFixed(0) + extension;
      } else if (height != null) {
        return baseName + height.toStringAsFixed(0) + extension;
      }
      return path.basename(url);
    }

    final document = await path_provider.getApplicationDocumentsDirectory();
    final tileFolder = Directory(path.join(document.path, "tiles"));
    final tileImagePath = path.join(tileFolder.path, getFileName());
    final file = File(tileImagePath);
    if (await file.exists()) {
      final imageData = await file.readAsBytes();
      return MemoryImage(imageData);
    }

    final showcasesExists = await tileFolder.exists();
    if (!showcasesExists) {
      await tileFolder.create();
    }

    var request = await httpClient.getUrl(Uri.parse(url));
    var response = await request.close();
    if (response.statusCode == 200) {
      var bytes = await consolidateHttpClientResponseBytes(response);
      if (width != null || height != null) {
        final tileImage = image.decodeImage(bytes)!;
        final resizeImage = image.copyResize(tileImage, width: width?.ceil(), height: height?.ceil());
        bytes = Uint8List.fromList(image.encodeJpg(resizeImage));
      }
      await file.writeAsBytes(bytes);
      return MemoryImage(bytes);
    }
    throw PlatformException(code: response.statusCode.toString());
  }
}
