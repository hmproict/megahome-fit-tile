// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tileSize.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TileSize _$TileSizeFromJson(Map<String, dynamic> json) {
  return $checkedNew('TileSize', json, () {
    final val = TileSize(
      sizeInch: $checkedConvert(json, 'size_inch', (v) => v as String),
      sizeCm: $checkedConvert(json, 'size_cm', (v) => v as String),
      box: $checkedConvert(json, 'box', (v) => (v as num).toDouble()),
      type: $checkedConvert(json, 'type', (v) => v as String),
    );
    $checkedConvert(json, 'select_cm', (v) => val.selectCm = v as bool?);
    return val;
  }, fieldKeyMap: const {
    'sizeInch': 'size_inch',
    'sizeCm': 'size_cm',
    'selectCm': 'select_cm'
  });
}

Map<String, dynamic> _$TileSizeToJson(TileSize instance) => <String, dynamic>{
      'size_inch': instance.sizeInch,
      'size_cm': instance.sizeCm,
      'box': instance.box,
      'type': instance.type,
      'select_cm': instance.selectCm,
    };
