// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Area _$AreaFromJson(Map<String, dynamic> json) {
  return $checkedNew('Area', json, () {
    final val = Area(
      $checkedConvert(json, 'id', (v) => v as String),
      $checkedConvert(json, 'name', (v) => v as String),
      $checkedConvert(json, 'name_th', (v) => v as String),
      $checkedConvert(json, 'image', (v) => v as String),
      $checkedConvert(
          json,
          'rooms',
          (v) => (v as List<dynamic>)
              .map((e) => Room.fromJson(e as Map<String, dynamic>))
              .toList()),
    );
    return val;
  }, fieldKeyMap: const {'nameTh': 'name_th'});
}

Map<String, dynamic> _$AreaToJson(Area instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'name_th': instance.nameTh,
      'image': instance.image,
      'rooms': instance.rooms,
    };

Room _$RoomFromJson(Map<String, dynamic> json) {
  return $checkedNew('Room', json, () {
    final val = Room(
      $checkedConvert(json, 'id', (v) => v as String),
      $checkedConvert(json, 'image', (v) => v as String),
      $checkedConvert(json, 'top_image', (v) => v as String?),
      $checkedConvert(json, 'preview', (v) => v as String),
      $checkedConvert(json, 'preview_height', (v) => (v as num).toDouble()),
      $checkedConvert(
          json,
          'floor',
          (v) => (v as List<dynamic>)
              .map((e) => (e as List<dynamic>).map((e) => e as String).toList())
              .toList()),
      $checkedConvert(
          json,
          'wall',
          (v) => (v as List<dynamic>)
              .map((e) => (e as List<dynamic>).map((e) => e as String).toList())
              .toList()),
      $checkedConvert(
          json,
          'right_wall',
          (v) => (v as List<dynamic>)
              .map((e) => (e as List<dynamic>).map((e) => e as String).toList())
              .toList()),
      $checkedConvert(
          json,
          'left_wall',
          (v) => (v as List<dynamic>)
              .map((e) => (e as List<dynamic>).map((e) => e as String).toList())
              .toList()),
    );
    return val;
  }, fieldKeyMap: const {
    'topImage': 'top_image',
    'previewHeight': 'preview_height',
    'rightWall': 'right_wall',
    'leftWall': 'left_wall'
  });
}

Map<String, dynamic> _$RoomToJson(Room instance) => <String, dynamic>{
      'id': instance.id,
      'image': instance.image,
      'top_image': instance.topImage,
      'preview': instance.preview,
      'preview_height': instance.previewHeight,
      'floor': instance.floor,
      'wall': instance.wall,
      'right_wall': instance.rightWall,
      'left_wall': instance.leftWall,
    };
