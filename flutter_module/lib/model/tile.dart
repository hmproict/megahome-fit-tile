import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'tile.g.dart';

@JsonSerializable()
class Tile extends Equatable {
  Tile({
    required this.id,
    required this.shortDesc,
    required this.longDesc,
    required this.width,
    required this.length,
    required this.random,
    required this.box,
    required this.feature,
    required this.vendor,
    required this.pattern,
    required this.type,
    required this.image,
    required this.url,
    required this.imagePattern,
    required this.floor,
    required this.wall,
    required this.leftWall,
    required this.rightWall,
    required this.inchWidth,
    required this.inchLength,
    required this.category,
    required this.area,
  });

  final String id;
  final String shortDesc;
  final String longDesc;
  final double width;
  final double length;
  final String? random;
  final double box;
  final String feature;
  final String vendor;
  final String pattern;
  final String type;
  final String image;
  final String url;
  final String imagePattern;

  final String floor;
  final String wall;
  final String leftWall;
  final String rightWall;
  final double inchWidth;
  final double inchLength;
  final int category;
  final String area;

  factory Tile.fromJson(Map<String, dynamic> json) => _$TileFromJson(json);
  Map<String, dynamic> toJson() => _$TileToJson(this);

  @override
  List<Object?> get props => [id];

  bool get isFloorPlan => type == "Floor";
}
