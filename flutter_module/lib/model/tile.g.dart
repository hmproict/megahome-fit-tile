// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tile.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Tile _$TileFromJson(Map<String, dynamic> json) {
  return $checkedNew('Tile', json, () {
    final val = Tile(
      id: $checkedConvert(json, 'id', (v) => v as String),
      shortDesc: $checkedConvert(json, 'short_desc', (v) => v as String),
      longDesc: $checkedConvert(json, 'long_desc', (v) => v as String),
      width: $checkedConvert(json, 'width', (v) => (v as num).toDouble()),
      length: $checkedConvert(json, 'length', (v) => (v as num).toDouble()),
      random: $checkedConvert(json, 'random', (v) => v as String?),
      box: $checkedConvert(json, 'box', (v) => (v as num).toDouble()),
      feature: $checkedConvert(json, 'feature', (v) => v as String),
      vendor: $checkedConvert(json, 'vendor', (v) => v as String),
      pattern: $checkedConvert(json, 'pattern', (v) => v as String),
      type: $checkedConvert(json, 'type', (v) => v as String),
      image: $checkedConvert(json, 'image', (v) => v as String),
      url: $checkedConvert(json, 'url', (v) => v as String),
      imagePattern: $checkedConvert(json, 'image_pattern', (v) => v as String),
      floor: $checkedConvert(json, 'floor', (v) => v as String),
      wall: $checkedConvert(json, 'wall', (v) => v as String),
      leftWall: $checkedConvert(json, 'left_wall', (v) => v as String),
      rightWall: $checkedConvert(json, 'right_wall', (v) => v as String),
      inchWidth:
          $checkedConvert(json, 'inch_width', (v) => (v as num).toDouble()),
      inchLength:
          $checkedConvert(json, 'inch_length', (v) => (v as num).toDouble()),
      category: $checkedConvert(json, 'category', (v) => v as int),
      area: $checkedConvert(json, 'area', (v) => v as String),
    );
    return val;
  }, fieldKeyMap: const {
    'shortDesc': 'short_desc',
    'longDesc': 'long_desc',
    'imagePattern': 'image_pattern',
    'leftWall': 'left_wall',
    'rightWall': 'right_wall',
    'inchWidth': 'inch_width',
    'inchLength': 'inch_length'
  });
}

Map<String, dynamic> _$TileToJson(Tile instance) => <String, dynamic>{
      'id': instance.id,
      'short_desc': instance.shortDesc,
      'long_desc': instance.longDesc,
      'width': instance.width,
      'length': instance.length,
      'random': instance.random,
      'box': instance.box,
      'feature': instance.feature,
      'vendor': instance.vendor,
      'pattern': instance.pattern,
      'type': instance.type,
      'image': instance.image,
      'url': instance.url,
      'image_pattern': instance.imagePattern,
      'floor': instance.floor,
      'wall': instance.wall,
      'left_wall': instance.leftWall,
      'right_wall': instance.rightWall,
      'inch_width': instance.inchWidth,
      'inch_length': instance.inchLength,
      'category': instance.category,
      'area': instance.area,
    };
