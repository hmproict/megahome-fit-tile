import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'tileSize.g.dart';

@JsonSerializable()
class TileSize extends Equatable {
  final String sizeInch;
  final String sizeCm;
  final double box;
  final String type;

  TileSize({
    required this.sizeInch,
    required this.sizeCm,
    required this.box,
    required this.type,
  });

  String get id => sizeInch + "|" + sizeCm;

  bool? selectCm = true;

  Size get inch {
    final texts = sizeInch.toUpperCase().split("X");
    try {
      return Size(double.tryParse(texts.first) ?? 0, double.tryParse(texts.last) ?? 0);
    } catch (e) {
      return Size.zero;
    }
  }

  Size get cm {
    final texts = sizeCm.toUpperCase().split("X");
    try {
      return Size(double.tryParse(texts.first) ?? 0, double.tryParse(texts.last) ?? 0);
    } catch (e) {
      return Size.zero;
    }
  }

  Size get selectedSize => selectCm == true ? cm : inch;

  factory TileSize.fromJson(Map<String, dynamic> json) => _$TileSizeFromJson(json);
  Map<String, dynamic> toJson() => _$TileSizeToJson(this);

  @override
  List<Object?> get props => [id];
}
