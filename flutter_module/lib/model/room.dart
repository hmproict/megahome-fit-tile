import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'room.g.dart';

@JsonSerializable()
class Area {
  final String id;
  final String name;
  final String nameTh;
  final String image;
  final List<Room> rooms;

  Area(this.id, this.name, this.nameTh, this.image, this.rooms);

  factory Area.fromJson(Map<String, dynamic> json) => _$AreaFromJson(json);
  Map<String, dynamic> toJson() => _$AreaToJson(this);

  String localizedName(BuildContext context) {
    final locale = Localizations.localeOf(context);
    return locale.languageCode == "th" ? nameTh : name;
  }
}

@JsonSerializable()
class Room {
  final String id;
  final String image;
  final String? topImage;
  final String preview;
  final double previewHeight;
  List<List<String>> floor;
  List<List<String>> wall;
  List<List<String>> rightWall;
  List<List<String>> leftWall;

  Room(this.id, this.image, this.topImage, this.preview, this.previewHeight, this.floor, this.wall, this.rightWall, this.leftWall);
  factory Room.fromJson(Map<String, dynamic> json) => _$RoomFromJson(json);
  Map<String, dynamic> toJson() => _$RoomToJson(this);
}
