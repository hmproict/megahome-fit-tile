import 'package:flutter/material.dart';

class AppColor {
  static final AppColor colors = AppColor();

  static const Color lightGreen = Color(0xFF00CFC5);
  static const Color darkGreen = Color(0xFF005854);
  static const Color superDarkGreen = Color(0xFF00403B);
  static const Color green = Color(0xFF008F88);
  static const Color text = Color(0xFF363636);
  static const Color lightBlack = Color(0xFF464646);
  static const Color black = Color(0xFF363636);
  static const Color darkGray = Color(0xFFAEAEAE);
  static const Color gray = Color(0xFFEEEFEF);
  static const Color line = Color(0xFFBCBCBC);
  static const Color white = Color(0xFFFFFFFF);
}
