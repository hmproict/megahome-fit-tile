import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_module/element/app_button.dart';
import 'package:flutter_module/element/app_color.dart';
import 'package:flutter_module/extra/app_layout.dart';
import 'package:flutter_module/model/tileSize.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:intl/intl.dart';

class TileSizeCollection extends StatefulWidget {
  TileSizeCollection(this.sizes, {this.preferSelected, this.topPadding});
  final List<TileSize> sizes;
  final TileSize? preferSelected;
  final double? topPadding;

  @override
  _TileSizeCollectionState createState() => _TileSizeCollectionState();
}

class _TileSizeCollectionState extends State<TileSizeCollection> {
  int _segmentedIndex = 0;
  final _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();

    if (widget.preferSelected != null) {
      _segmentedIndex = widget.preferSelected!.selectCm == true ? 0 : 1;
    }
  }

  @override
  Widget build(BuildContext context) {
    final localized = AppLocalizations.of(context);
    final format = NumberFormat.decimalPattern();
    format.maximumFractionDigits = 2;
    format.minimumFractionDigits = 0;
    final style = Theme.of(context).textTheme;

    final color = AppColor.darkGreen.withAlpha(175);

    final tileSized = [
      AppButton(
        keepButtonSize: false,
        padding: EdgeInsets.symmetric(vertical: 5, horizontal: 12),
        shape: AppButtonShape.tube,
        title: localized?.filterTileAll ?? "-",
        color: widget.preferSelected == null ? color.withAlpha(255) : color,
        textStyle: style.button?.copyWith(color: AppColor.white),
        onPressed: () {
          Navigator.of(context).pop("All");
        },
      ),
      ...widget.sizes.map((e) => AppButton(
            keepButtonSize: false,
            padding: EdgeInsets.symmetric(vertical: 5, horizontal: 12),
            shape: AppButtonShape.tube,
            title: _segmentedIndex == 0
                ? localized?.filterCm(format.format(e.cm.width), format.format(e.cm.height)) ?? "-"
                : localized?.filterInch(format.format(e.inch.width), format.format(e.inch.height)) ?? "-",
            color: widget.preferSelected == e ? color.withAlpha(255) : color,
            textStyle: style.button?.copyWith(color: AppColor.white),
            onPressed: () {
              e.selectCm = _segmentedIndex == 0;
              Navigator.of(context).pop(e);
            },
          ))
    ];

    return SafeArea(
      child: Container(
        decoration: BoxDecoration(
          color: AppColor.white,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
        ),
        padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
        margin: EdgeInsets.only(top: widget.topPadding ?? 0.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              localized?.filterTileTitle ?? "-",
              style: style.subtitle1,
            ),
            SizedBox(
              height: 10,
            ),
            CupertinoSlidingSegmentedControl<int>(
                groupValue: _segmentedIndex,
                children: {
                  0: Padding(
                    padding: EdgeInsets.symmetric(vertical: 3),
                    child: Text(
                      localized?.filterTileCm ?? "-",
                      style: style.subtitle1?.copyWith(color: _segmentedIndex == 0 ? AppColor.white : Colors.black),
                    ),
                  ),
                  1: Padding(
                      padding: EdgeInsets.symmetric(vertical: 3),
                      child: Text(
                        localized?.filterTileInch ?? "-",
                        style: style.subtitle1?.copyWith(color: _segmentedIndex == 1 ? AppColor.white : Colors.black),
                      ))
                },
                thumbColor: AppColor.darkGreen,
                padding: EdgeInsets.zero,
                onValueChanged: (i) {
                  setState(() {
                    _segmentedIndex = i!;
                  });
                }),
            SizedBox(
              height: 20,
            ),
            Flexible(
              child: Scrollbar(
                controller: _scrollController,
                showTrackOnHover: true,
                child: SingleChildScrollView(
                  controller: _scrollController,
                  padding: EdgeInsets.only(bottom: 20),
                  child: Wrap(
                    direction: Axis.horizontal,
                    spacing: 10,
                    runSpacing: 15,
                    children: tileSized,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}
