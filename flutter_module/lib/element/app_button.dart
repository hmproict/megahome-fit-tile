import 'package:flutter/material.dart';
import 'package:flutter_module/extra/app_layout.dart';
import 'app_color.dart';

enum AppButtonShape { rect, roundRect, tube, circle }

class AppButton extends StatelessWidget {
  AppButton({
    Key? key,
    this.color = Colors.white,
    this.shape = AppButtonShape.roundRect,
    this.leading,
    this.tailing,
    this.content,
    this.title,
    this.textStyle,
    this.textAlign = TextAlign.center,
    this.borderColor,
    this.padding,
    this.showShadow = false,
    this.showShadowWhenHighlight = false,
    this.customShape,
    this.onPressed,
    this.keepButtonSize = true,
    this.space = 10.0,
    this.numberOfLine,
  }) : super(key: key);

  final Color color;
  final Color? borderColor;
  final VoidCallback? onPressed;
  final AppButtonShape shape;
  final String? title;
  final TextStyle? textStyle;
  final TextAlign textAlign;
  final Widget? leading;
  final Widget? tailing;
  final Widget? content;
  final EdgeInsets? padding;
  final bool showShadow;
  final bool showShadowWhenHighlight;
  final bool keepButtonSize;
  final ShapeBorder? customShape;
  final double space;
  final int? numberOfLine;

  @override
  Widget build(BuildContext context) {
    final layout = AppLayout(context);
    final defaultStyle = Theme.of(context).textTheme.subtitle1;
    ShapeBorder buttonShape;
    if (customShape != null) {
      buttonShape = customShape!;
    } else {
      switch (shape) {
        case AppButtonShape.tube:
          if (borderColor != null) {
            buttonShape = StadiumBorder(side: BorderSide(color: borderColor!));
          } else {
            buttonShape = StadiumBorder();
          }
          break;
        case AppButtonShape.circle:
          if (borderColor != null) {
            buttonShape = CircleBorder(side: BorderSide(color: borderColor!));
          } else {
            buttonShape = CircleBorder();
          }
          break;
        case AppButtonShape.rect:
          if (borderColor != null) {
            buttonShape = RoundedRectangleBorder(borderRadius: BorderRadius.zero, side: BorderSide(color: borderColor!));
          } else {
            buttonShape = RoundedRectangleBorder(borderRadius: BorderRadius.zero);
          }
          break;
        default:
          if (borderColor != null) {
            buttonShape = RoundedRectangleBorder(borderRadius: layout.borderRadius, side: BorderSide(color: borderColor!));
          } else {
            buttonShape = RoundedRectangleBorder(borderRadius: layout.borderRadius);
          }
      }
    }

    Widget? child() {
      final space = SizedBox(width: this.space);
      final text = content ??
          (title != null
              ? Text(
                  title ?? "",
                  style: textStyle ?? defaultStyle,
                  textAlign: textAlign,
                  overflow: TextOverflow.fade,
                  maxLines: numberOfLine,
                )
              : null);

      if (leading != null && tailing != null) {
        if (text != null) {
          return Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[leading!, space, text, space, tailing!],
          );
        }
        return Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[leading!, space, tailing!],
        );
      }
      if (leading != null) {
        if (text != null) {
          return Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[leading!, space, text],
          );
        }
        return leading;
      }
      if (tailing != null) {
        if (text != null) {
          return Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[text, space, tailing!],
          );
        }
        return tailing;
      }
      return text;
    }

    return MaterialButton(
      minWidth: keepButtonSize ? null : (padding != null ? padding!.left + padding!.right : 0),
      height: keepButtonSize ? null : (padding != null ? padding!.top + padding!.bottom : 0),
      padding: keepButtonSize ? (padding ?? EdgeInsets.symmetric(horizontal: 10, vertical: 8)) : (padding ?? EdgeInsets.zero),
      onPressed: onPressed,
      elevation: showShadow ? 4 : 0,
      highlightElevation: showShadowWhenHighlight ? (showShadow ? 10 : 4) : 0,
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      color: color,
      disabledColor: color.withAlpha(150),
      shape: buttonShape,
      child: textAlign == TextAlign.center ? child() : SizedBox(width: double.infinity, child: child()),
    );
  }

  factory AppButton.confirm(String title, {VoidCallback? onPressed}) {
    return AppButton(
      color: AppColor.green,
      onPressed: onPressed,
      title: title,
      showShadowWhenHighlight: true,
    );
  }

  factory AppButton.altConfirm(String title, {VoidCallback? onPressed}) {
    return AppButton(
      borderColor: AppColor.green,
      onPressed: onPressed,
      title: title,
      showShadowWhenHighlight: true,
    );
  }
}
