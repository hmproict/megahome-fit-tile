import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_module/element/app_color.dart';
import 'package:flutter_module/page/router/router_back.dart';
import 'package:flutter_module/page/router/router_delegate.dart';
import 'package:flutter_module/page/router/router_parser.dart';

const MethodChannel default_channel = const MethodChannel("com.homepro.megahome.flutter");
const EventChannel default_event = const EventChannel("com.homepro.megahome.flutter.event.default");

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final _routerDelegate = AppRouterDelegate();
  final _routeInformationParser = AppRouteInformationParser();

  final textTheme = TextTheme(
    bodyText1: TextStyle(fontFamily: "DB Helvethaica X", fontSize: 20.0, fontWeight: FontWeight.normal, color: Colors.black),
    bodyText2: TextStyle(fontFamily: "DB Helvethaica X", fontSize: 24.0, fontWeight: FontWeight.normal, color: Colors.black),
    headline1: TextStyle(fontFamily: "DB Helvethaica X", fontSize: 60.0, fontWeight: FontWeight.normal, color: Colors.black),
    headline2: TextStyle(fontFamily: "DB Helvethaica X", fontSize: 48.0, fontWeight: FontWeight.normal, color: Colors.black),
    headline3: TextStyle(fontFamily: "DB Helvethaica X", fontSize: 36.0, fontWeight: FontWeight.normal, color: Colors.black),
    headline4: TextStyle(fontFamily: "DB Helvethaica X Med", fontSize: 30.0, fontWeight: FontWeight.normal, color: Colors.black),
    headline5: TextStyle(fontFamily: "DB Helvethaica X", fontSize: 30.0, fontWeight: FontWeight.normal, color: Colors.black),
    headline6: TextStyle(fontFamily: "DB Helvethaica X", fontSize: 28.0, fontWeight: FontWeight.normal, color: Colors.black),
    subtitle1: TextStyle(fontFamily: "DB Helvethaica X Med", fontSize: 24.0, fontWeight: FontWeight.normal, color: Colors.black),
    subtitle2: TextStyle(fontFamily: "DB Helvethaica X", fontSize: 24.0, fontWeight: FontWeight.normal, color: Colors.black),
    caption: TextStyle(fontFamily: "DB Helvethaica X", fontSize: 18.0, fontWeight: FontWeight.normal, color: Colors.black),
    button: TextStyle(fontFamily: "DB Helvethaica X Med", fontSize: 20.0, fontWeight: FontWeight.normal, color: Colors.black),
    overline: TextStyle(fontFamily: "DB Helvethaica X", fontSize: 16.0, fontWeight: FontWeight.normal, color: Colors.black),
  );

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'FIT TILE',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        appBarTheme: AppBarTheme(
          brightness: Brightness.dark,
          backgroundColor: AppColor.darkGreen,
          centerTitle: true,
          elevation: 20,
          shadowColor: Colors.black,
          textTheme: TextTheme(headline6: textTheme.headline6?.copyWith(color: Colors.white)),
        ),
        textTheme: textTheme,
        primaryTextTheme: textTheme,
        fontFamily: "DB Helvethaica X",
        brightness: Brightness.dark,
        primaryColor: AppColor.green,
        canvasColor: Colors.white,
        accentColor: Colors.blueAccent,
        scaffoldBackgroundColor: Colors.white,
        pageTransitionsTheme: PageTransitionsTheme(builders: {
          TargetPlatform.android: OpenUpwardsPageTransitionsBuilder(),
          TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
        }),
      ),
      debugShowCheckedModeBanner: false,
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      routerDelegate: _routerDelegate,
      routeInformationParser: _routeInformationParser,
      backButtonDispatcher: AppBackButtonDispatcher(_routerDelegate),
    );
  }
}
