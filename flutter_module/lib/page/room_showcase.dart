import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_module/element/app_color.dart';
import 'package:flutter_module/element/tile_element.dart';
import 'package:flutter_module/extra/app_loading.dart';
import 'package:flutter_module/model/tileSize.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:path/path.dart' as path;

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_module/element/app_button.dart';
import 'package:flutter_module/extra/app_images.dart';
import 'package:flutter_module/extra/app_layout.dart';
import 'package:flutter_module/main.dart';
import 'package:flutter_module/model/room.dart';
import 'package:flutter_module/model/tile.dart';
import 'package:shimmer/shimmer.dart';
import 'package:flutter_module/extra/app_utility.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:loading_animations/loading_animations.dart';
import 'package:flutter_module/extra/app_alert.dart';

enum RoomPart {
  floor,
  wall,
  wall_left,
  wall_right,
}

class RoomPartIndex extends Equatable {
  RoomPartIndex(this.part, this.index);

  final RoomPart part;
  final int index;

  @override
  List<Object> get props => [part, index];
}

class RoomPartIndexTile extends RoomPartIndex {
  RoomPartIndexTile(RoomPartIndex part, this.tile) : super(part.part, part.index);

  final Tile? tile;

  bool get haveTile => tile != null;

  @override
  List<Object> get props => tile != null ? [part, index, tile!] : super.props;
}

class RoomShowcase extends StatefulWidget {
  RoomShowcase(this.room, {this.onClose});

  final Room room;
  final VoidCallback? onClose;

  @override
  _RoomShowcaseState createState() => _RoomShowcaseState();
}

class _RoomShowcaseState extends State<RoomShowcase> {
  RoomPartIndex? _selectedPart;
  RoomPart? _animationPart = RoomPart.wall_right;

  ImageProvider? _imageProvider;
  ImageProvider? _topImageProvider;

  List<Tile>? _tiles;
  List<Tile>? _qrTiles;
  bool _downloaded = false;
  bool _isScrolling = false;

  TileSize? _filterSize;

  final _notifierTiles = Map<RoomPartIndex, ValueNotifier<RoomPartIndexTile>>();

  final _scrollController = ScrollController();
  final _panelController = PanelController();
  final _textController = TextEditingController();

  bool get _alreadySaveProject => _notifierTiles.values.where((element) => element.value.haveTile).isNotEmpty;
  bool get _shouldLoadMore {
    if (_scrollController.hasClients && !_downloaded) {
      final pixels = _scrollController.position.pixels + 80;
      final max = _scrollController.position.maxScrollExtent;
      if (!_isScrolling && pixels >= max) {
        return true;
      }
    }
    return false;
  }

  bool get _selectedFloorPart {
    if (_selectedPart != null) {
      switch (_selectedPart!.part) {
        case RoomPart.floor:
          return true;
        default:
          return false;
      }
    }
    return false;
  }

  @override
  void initState() {
    super.initState();
    _prepareNotifierTiles();
    _changeToBlackStatusBarStyle();
    _prepareImage();
    _loadTiles();
  }

  @override
  Widget build(BuildContext context) {
    SchedulerBinding.instance?.addPostFrameCallback((_) {
      if (_panelController.isAttached && _selectedPart == null) {
        _panelController.hide();
      }
      if (_shouldLoadMore) {
        _loadTiles();
      }
    });

    final localized = AppLocalizations.of(context);
    final theme = Theme.of(context);
    final layout = AppLayout(context);

    final format = NumberFormat.decimalPattern();
    format.maximumFractionDigits = 2;
    format.minimumFractionDigits = 0;

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.black,
      statusBarBrightness: Brightness.light,
    ));

    if (_imageProvider == null) {
      return WillPopScope(
          onWillPop: () async {
            if (widget.onClose != null) {
              widget.onClose!();
            }
            return true;
          },
          child: Scaffold(
              backgroundColor: Colors.white,
              body: SafeArea(
                  child: Center(
                child: LoadingBouncingGrid.square(
                  backgroundColor: theme.primaryColor,
                ),
              ))));
    }

    final List<Tile> tiles;
    final sameTiles = _tiles?.where((element) => _qrTiles?.contains(element) == true).toList() ?? [];
    if (sameTiles.length == _qrTiles?.length) {
      tiles = _tiles ?? [];
    } else if (sameTiles.isNotEmpty) {
      final qrTiles = _qrTiles?.where((element) => sameTiles.contains(element) == false).toList() ?? [];
      tiles = (_tiles ?? []) + qrTiles;
    } else {
      tiles = (_tiles ?? []) + (_qrTiles ?? []);
    }

    final List<Tile> filterTiles;
    if (_selectedPart != null) {
      final matchTiles = tiles.where((element) => _selectedPart!.part == RoomPart.floor ? element.isFloorPlan : true).toList();

      if (_filterSize != null) {
        filterTiles = matchTiles
            .where(
              (element) => element.width == _filterSize!.cm.width && element.length == _filterSize!.cm.height,
            )
            .toList();
      } else {
        filterTiles = matchTiles;
      }
    } else {
      filterTiles = [];
    }

    print("filterTiles : ${filterTiles.length}");

    return WillPopScope(
      onWillPop: () async {
        if (widget.onClose != null) {
          widget.onClose!();
        }
        return true;
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: SlidingUpPanel(
            minHeight: 30,
            maxHeight: layout.mediaQuery.size.height * 0.70,
            controller: _panelController,
            header: Container(
              width: layout.mediaQuery.size.width,
              height: 50,
              color: Colors.transparent,
              child: Stack(
                children: [
                  Center(
                    child: Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      alignment: Alignment.center,
                      decoration: ShapeDecoration(shape: StadiumBorder(), color: Colors.black38),
                      width: 80,
                      height: 5,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10, right: 10),
                    alignment: Alignment.centerRight,
                    child: AppButton(
                      shape: AppButtonShape.tube,
                      color: Colors.white,
                      content: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Image.asset(
                            AppImage.icon(_filterSize != null ? "filtered.png" : "filter.png"),
                            color: Colors.black,
                            width: 18,
                            height: 18,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            _filterSize != null
                                ? (_filterSize!.selectCm == true
                                    ? localized?.filterCm(format.format(_filterSize!.cm.width), format.format(_filterSize!.cm.height)) ?? "-"
                                    : localized?.filterInch(format.format(_filterSize!.inch.width), format.format(_filterSize!.inch.height)) ?? "-")
                                : localized?.filterTile ?? "-",
                            style: theme.textTheme.bodyText1?.copyWith(height: 1),
                          ),
                        ],
                      ),
                      onPressed: _onFilterTile,
                    ),
                  ),
                ],
              ),
            ),
            panel: Scrollbar(
              controller: _scrollController,
              child: NotificationListener<ScrollNotification>(
                onNotification: (scrollNotification) {
                  _isScrolling = !(scrollNotification is ScrollEndNotification);
                  final pixels = scrollNotification.metrics.pixels;
                  final max = scrollNotification.metrics.maxScrollExtent;
                  if (!_downloaded && pixels >= max) {
                    _loadTiles();
                  }
                  return true;
                },
                child: GridView.builder(
                  physics: AlwaysScrollableScrollPhysics(),
                  controller: _scrollController,
                  padding: EdgeInsets.only(left: 10, right: 10, top: 60, bottom: 0),
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 100,
                    childAspectRatio: 1,
                    mainAxisSpacing: 10,
                    crossAxisSpacing: 10,
                  ),
                  itemBuilder: (context, index) {
                    if (index == 0) {
                      return AppButton(
                        shape: AppButtonShape.rect,
                        color: AppColor.green,
                        content: Image.asset(AppImage.icon("QR_scanner.png")),
                        onPressed: _onQRCodeScanner,
                      );
                    }
                    if (filterTiles.isEmpty || index > filterTiles.length) {
                      return Center(
                        child: LoadingBouncingGrid.square(
                          backgroundColor: theme.primaryColor,
                        ),
                      );
                    }

                    final tile = filterTiles[index - 1];
                    return FutureBuilder<ImageProvider>(
                        future: AppImage.tileCacheImage(tile.image),
                        builder: (context, snapshot) {
                          if (snapshot.hasData && snapshot.data != null) {
                            return AppButton(
                              padding: EdgeInsets.zero,
                              showShadowWhenHighlight: true,
                              content: Ink(
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                  image: snapshot.data!,
                                  fit: BoxFit.cover,
                                )),
                              ),
                              onPressed: () => _onPressedTile(tile),
                            );
                          } else if (snapshot.hasError) {
                            return AppButton(
                              padding: EdgeInsets.zero,
                              showShadowWhenHighlight: true,
                              content: Container(color: Colors.white),
                              onPressed: () => _onPressedTile(tile),
                            );
                          }

                          return Center(
                            child: LoadingBouncingGrid.square(
                              backgroundColor: theme.primaryColor,
                            ),
                          );
                        });
                  },
                  itemCount: filterTiles.length + (_downloaded ? 0 : 1) + 1,
                ),
              ),
            ),
            panelSnapping: false,
            color: Colors.white54,
            defaultPanelState: PanelState.CLOSED,
            body: Align(
              alignment: Alignment.topCenter,
              child: AspectRatio(
                aspectRatio: 9 / 16,
                child: Stack(
                  fit: StackFit.expand,
                  children: [
                    ...widget.room.floor.mapIndexed((plan, index) => _roomPartContent(plan, RoomPart.floor, index)),
                    ...widget.room.leftWall.mapIndexed((plan, index) => _roomPartContent(plan, RoomPart.wall_left, index)),
                    ...widget.room.rightWall.mapIndexed((plan, index) => _roomPartContent(plan, RoomPart.wall_right, index)),
                    ...widget.room.wall.mapIndexed((plan, index) => _roomPartContent(plan, RoomPart.wall, index)),
                    IgnorePointer(
                      child: Image(
                        image: _imageProvider!,
                        fit: BoxFit.contain,
                        alignment: Alignment.topCenter,
                        loadingBuilder: (context, child, loading) {
                          if (loading == null) return child;

                          return LoadingBouncingGrid.square(
                            backgroundColor: theme.primaryColor,
                          );
                        },
                      ),
                    ),
                    Padding(
                        padding: const EdgeInsets.all(10),
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              FloatingActionButton(
                                mini: true,
                                elevation: 0.0,
                                onPressed: _onBackPage,
                                backgroundColor: Colors.white,
                                child: Icon(Icons.arrow_back, color: Colors.black),
                              ),
                              if (_alreadySaveProject)
                                AppButton(
                                  customShape: StadiumBorder(),
                                  onPressed: _onSaveProject,
                                  color: Colors.white,
                                  padding: EdgeInsets.symmetric(vertical: 8, horizontal: 15),
                                  title: localized?.roomSaveButton ?? "-",
                                  textStyle: theme.textTheme.button,
                                  showShadowWhenHighlight: true,
                                ),
                            ],
                          ),
                        )),
                    if (_selectedPart == null)
                      Center(
                        child: Text(
                          localized?.roomPartSelection ?? "-",
                          style: Theme.of(context).textTheme.bodyText1?.copyWith(color: Colors.white),
                        ),
                      ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void dispose() {
    _animationPart = null;
    _scrollController.dispose();
    super.dispose();
  }

  Widget _roomPartContent(List<String> plan, RoomPart part, int index) {
    final partIndex = RoomPartIndex(part, index);
    return ValueListenableBuilder<RoomPartIndexTile>(
        key: ValueKey(partIndex),
        valueListenable: _notifierTiles[partIndex]!,
        builder: (_, value, __) {
          return RoomPartPlan(
            plan,
            value.part,
            key: ValueKey(value.part),
            animation: _animationPart == value.part,
            selected: _selectedPart == value,
            onPressed: () => _onPressedRoomPart(partIndex),
            tile: value.tile,
          );
        });
  }

  Future<void> _shimmerAnimation() async {
    switch (_animationPart) {
      case RoomPart.floor:
        _animationPart = RoomPart.wall_left;
        break;
      case RoomPart.wall:
        _animationPart = RoomPart.wall_right;
        break;
      case RoomPart.wall_left:
        _animationPart = RoomPart.wall;
        break;
      case RoomPart.wall_right:
        _animationPart = RoomPart.floor;
        break;
      default:
        break;
    }
    await Future.delayed(Duration(milliseconds: 1500));
    if (_animationPart != null) {
      setState(() {});
      _shimmerAnimation();
    }
  }

  Future<void> _loadTiles() async {
    try {
      final result = await default_channel.invokeMethod("tileList", {
        "room_id": widget.room.id.split("-").first,
      });

      final tiles = List.from(result).where((element) => element != null).map((json) => Tile.fromJson(Map<String, dynamic>.from(json))).toList();
      setState(() {
        _tiles = tiles;
      });

      await Future.delayed(Duration(milliseconds: 500));

      if (_shouldLoadMore) {
        _loadTiles();
      }
    } on PlatformException catch (e) {
      if (e.code == "00") {
        _downloaded = true;
        final tiles = List.from(e.details).map((json) => Tile.fromJson(Map<String, dynamic>.from(json))).toList();
        setState(() {
          _tiles = tiles;
        });
      }
    } catch (e) {
      print("_loadTiles error : $e");
    }
  }

  Future<void> _changeToBlackStatusBarStyle() async {
    try {
      final _ = await default_channel.invokeMethod("changeToBlackStatusBarStyle");
    } catch (e) {
      print("changeToBlackStatusBarStyle error : $e");
    }
  }

  Future<void> _changeToWhiteStatusBarStyle() async {
    try {
      final _ = await default_channel.invokeMethod("changeToWhiteStatusBarStyle");
    } catch (e) {
      print("changeToWhiteStatusBarStyle error : $e");
    }
  }

  Future<void> _resetTileList() async {
    try {
      final _ = await default_channel.invokeMethod("resetTileList");
    } catch (e) {
      print("resetTileList error : $e");
    }
  }

  Future<String?> _defaultRoomNo() async {
    try {
      final result = await default_channel.invokeMethod("defaultRoomNo");
      return result as String?;
    } catch (e) {
      print("defaultRoomNo error : $e");
    }
  }

  Future<List<TileSize>> _prepareTileSizes() async {
    final result = await default_channel.invokeMethod("tileSizes", {"type": _selectedFloorPart ? "Floor" : "Wall"});
    final list = List.from(result)
        .where((element) => element != null)
        .map(
          (json) => TileSize.fromJson(Map<String, dynamic>.from(json)),
        )
        .toList();

    if (list.isNotEmpty) {
      return list;
    }

    throw PlatformException(code: "018");
  }

  void _onBackPage() async {
    if (widget.onClose != null) {
      widget.onClose!();
    }
    await _changeToWhiteStatusBarStyle();
  }

  void _prepareImage() async {
    try {
      final image = await AppImage.roomCacheImage(widget.room.image);

      final ImageProvider? topImage;
      if (widget.room.topImage != null) {
        topImage = await AppImage.roomCacheImage(widget.room.topImage!);
      } else {
        topImage = null;
      }
      setState(() {
        _imageProvider = image;
        _topImageProvider = topImage;
      });
      _shimmerAnimation();
    } catch (e) {
      final localized = AppLocalizations.of(context);
      AppAlert.showAlert(localized?.loadRoomImageError ?? "", context);
    }
  }

  _onPressedTile(Tile tile) {
    final _part = _selectedPart;
    if (_part == null) return;

    final localized = AppLocalizations.of(context);
    switch (_part.part) {
      case RoomPart.floor:
        if (!AppUtility.validURL(tile.floor)) {
          AppAlert.showAlert(localized?.noShowcaseTileURL ?? "", context);
          return;
        }
        break;
      case RoomPart.wall:
        if (!AppUtility.validURL(tile.wall)) {
          AppAlert.showAlert(localized?.noShowcaseTileURL ?? "", context);
          return;
        }
        break;
      case RoomPart.wall_left:
        if (!AppUtility.validURL(tile.leftWall)) {
          AppAlert.showAlert(localized?.noShowcaseTileURL ?? "", context);
          return;
        }
        break;
      case RoomPart.wall_right:
        if (!AppUtility.validURL(tile.rightWall)) {
          AppAlert.showAlert(localized?.noShowcaseTileURL ?? "", context);
          return;
        }
        break;
    }

    final alreadySaveProject = _alreadySaveProject;
    _notifierTiles[_part]?.value = RoomPartIndexTile(_part, tile);
    if (alreadySaveProject != _alreadySaveProject) {
      setState(() {});
    }

    switch (_part.part) {
      case RoomPart.floor:
        _panelController.close();
        break;
      default:
        if (_panelController.isPanelShown && _panelController.panelPosition > 0.5) {
          _panelController.animatePanelToPosition(0.5);
        }
        break;
    }
  }

  _onPressedRoomPart(RoomPartIndex index) async {
    if (_animationPart != null) {
      setState(() {
        _animationPart = null;
        _selectedPart = index;
      });
    } else {
      if (_selectedPart?.part == RoomPart.floor && index.part != RoomPart.floor ||
          _selectedPart?.part != RoomPart.floor && index.part == RoomPart.floor) {
        setState(() {
          _selectedPart = index;
          _notifierTiles[index]?.value = RoomPartIndexTile(_selectedPart!, null);
        });
      } else {
        _selectedPart = index;
        _notifierTiles[index]?.value = RoomPartIndexTile(_selectedPart!, null);
      }
    }

    if (!_panelController.isPanelShown) {
      await _panelController.show();
    }
    if (_panelController.isPanelClosed) {
      await _panelController.open();
    }
  }

  void _prepareNotifierTiles() {
    widget.room.floor.asMap().forEach((index, _) {
      final part = RoomPartIndex(RoomPart.floor, index);
      _notifierTiles[part] = ValueNotifier(RoomPartIndexTile(part, null));
    });
    widget.room.leftWall.asMap().forEach((index, _) {
      final part = RoomPartIndex(RoomPart.wall_left, index);
      _notifierTiles[part] = ValueNotifier(RoomPartIndexTile(part, null));
    });
    widget.room.rightWall.asMap().forEach((index, _) {
      final part = RoomPartIndex(RoomPart.wall_right, index);
      _notifierTiles[part] = ValueNotifier(RoomPartIndexTile(part, null));
    });
    widget.room.wall.asMap().forEach((index, _) {
      final part = RoomPartIndex(RoomPart.wall, index);
      _notifierTiles[part] = ValueNotifier(RoomPartIndexTile(part, null));
    });
  }

  void _onQRCodeScanner() async {
    final localized = AppLocalizations.of(context);
    try {
      final result = await default_channel.invokeMethod("qrCodeScanner");
      final tile = Tile.fromJson(Map<String, dynamic>.from(result));
      if (_tiles?.where((element) => element.id == tile.id) != null) {
        if (!tile.isFloorPlan && _selectedPart?.part == RoomPart.floor) {
          AppAlert.showAlert(localized?.incompatibleTile ?? "", context);
        } else {
          _onPressedTile(tile);
        }
      } else {
        if (_qrTiles == null) {
          _qrTiles = List<Tile>.empty();
        }
        _qrTiles?.add(tile);
        if (!tile.isFloorPlan && _selectedPart?.part == RoomPart.floor) {
          AppAlert.showAlert(localized?.incompatibleTile ?? "", context);
        } else {
          _onPressedTile(tile);
        }
      }
    } catch (e) {
      print("_onQRCodeScanner error : $e");
    }
  }

  void _onSaveProject() async {
    final localized = AppLocalizations.of(context);
    final style = Theme.of(context).textTheme;

    final defaultRoom = await _defaultRoomNo();
    if (defaultRoom != null) {
      _textController.text = localized?.roomNameDefault(defaultRoom) ?? "-";
    }

    bool? result = await AppAlert(
        title: localized?.roomNameTitle ?? "-",
        content: Material(
          type: MaterialType.transparency,
          child: Container(
            child: TextField(
              autofocus: true,
              controller: _textController,
              textInputAction: TextInputAction.done,
              cursorColor: Theme.of(context).primaryColor,
              style: style.bodyText1,
              decoration: InputDecoration(
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Theme.of(context).primaryColor),
                  borderRadius: BorderRadius.circular(5),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black),
                  borderRadius: BorderRadius.circular(5),
                ),
                contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                isCollapsed: true,
                hintText: localized?.roomNameHint ?? "-",
                hintStyle: style.bodyText1?.copyWith(color: AppColor.darkGray),
              ),
              textAlign: TextAlign.center,
            ),
            width: double.infinity,
          ),
        ),
        button: (alertContext) => [
              AppButton(
                title: localized?.roomNameCancel ?? "-",
                borderColor: AppColor.green,
                textStyle: style.button,
                onPressed: () => Navigator.of(alertContext).pop(false),
              ),
              AppButton(
                title: localized?.roomNameConfirm ?? "-",
                color: AppColor.green,
                textStyle: style.button?.copyWith(color: Colors.white),
                onPressed: () => Navigator.of(alertContext).pop(true),
              )
            ]).show(context);

    if (result != true) return;

    Future<File> saveImage(ui.Image image, String name) async {
      final folder = await path_provider.getTemporaryDirectory();
      final file = File(path.join(folder.path, DateTime.now().millisecond.toString() + name));

      final data = await image.toByteData(format: ui.ImageByteFormat.png);
      final byte = data?.buffer.asUint8List();
      if (byte == null) {
        throw PlatformException(code: "8");
      }

      await file.writeAsBytes(byte);

      final fileExits = await file.exists();
      if (!fileExits) {
        throw PlatformException(code: "8");
      }
      return file;
    }

    final loading = AppLoading.show(context);

    try {
      final tiles = _notifierTiles.values.map((e) => e.value.tile).where((e) => e != null).toList();
      final tilesText = JsonEncoder().convert(tiles);

      final image = await _generateImage(_imageProvider!);

      final imageFile = await saveImage(image, "TempImageProject.png");

      final File? topImageFile;
      if (_topImageProvider != null) {
        final topImage = await _generateImage(_topImageProvider!);
        topImageFile = await saveImage(topImage, "TempTopImage.png");
      } else {
        topImageFile = null;
      }

      final success = await default_channel.invokeMethod("saveProject", {
        "image": imageFile.path,
        "top_image": topImageFile?.path,
        "name": _textController.text,
        "tiles": tilesText,
      });
      await loading.dismiss();
      if (success == true) {
        Navigator.of(context).popUntil((route) => route.isFirst);
        await _changeToWhiteStatusBarStyle();
      }
    } catch (e) {
      await loading.dismiss();
      print("_onSaveProject error : $e");
    }
  }

  Future<ui.Image> _generateImage(ImageProvider imageProvider) async {
    Future<ui.Image> getImage(ImageProvider provider) async {
      final rawImage = provider as MemoryImage;
      final codec = await ui.instantiateImageCodec(rawImage.bytes);
      final frameInfo = await codec.getNextFrame();
      return frameInfo.image;
    }

    final pictureRecorder = new ui.PictureRecorder();
    final canvas = new Canvas(pictureRecorder);
    canvas.drawColor(AppColor.white, BlendMode.color);

    final image = await getImage(imageProvider);
    final size = Size(image.width.toDouble(), image.height.toDouble());

    for (final data in _notifierTiles.values) {
      if (data.value.haveTile) {
        final ImageProvider rawImage;
        final List<List<String>> points;
        switch (data.value.part) {
          case RoomPart.floor:
            rawImage = await AppImage.tileCacheImage(data.value.tile!.floor) as MemoryImage;
            points = widget.room.floor;
            break;
          case RoomPart.wall:
            rawImage = await AppImage.tileCacheImage(data.value.tile!.wall) as MemoryImage;
            points = widget.room.wall;
            break;
          case RoomPart.wall_left:
            rawImage = await AppImage.tileCacheImage(data.value.tile!.leftWall) as MemoryImage;
            points = widget.room.leftWall;
            break;
          case RoomPart.wall_right:
            rawImage = await AppImage.tileCacheImage(data.value.tile!.rightWall) as MemoryImage;
            points = widget.room.rightWall;
            break;
        }

        canvas.save();

        final point = points[data.value.index];
        final path = RoomTileClipper(point);
        canvas.clipPath(path.getClip(size));

        final tileImage = await getImage(rawImage);
        final width = tileImage.width.toDouble() * size.height / tileImage.height.toDouble();
        final x = (size.width - width) / 2;

        canvas.drawImageRect(
          tileImage,
          Rect.fromLTWH(0, 0, tileImage.width.toDouble(), tileImage.height.toDouble()),
          Rect.fromLTWH(x, 0, width, size.height),
          Paint(),
        );

        canvas.restore();
      }
    }

    canvas.drawImage(image, Offset.zero, Paint());

    final resultImage = await pictureRecorder.endRecording().toImage(image.width, image.height);
    return resultImage;
  }

  void _onFilterTile() async {
    final loading = AppLoading.show(context);
    final layout = AppLayout(context);

    try {
      final tileSizes = await _prepareTileSizes();
      await loading.dismiss();
      final result = await showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        builder: (context) => TileSizeCollection(
          tileSizes,
          preferSelected: _filterSize,
          topPadding: layout.devicePadding.top,
        ),
        isScrollControlled: true,
      );

      print("result : $result");
      setState(() {
        if (result is String) {
          _filterSize = null;
        } else if (result is TileSize) {
          _filterSize = result;
        }
      });
    } catch (e) {
      print("_onFilterTile error : $e");
      await loading.dismiss();
      final localized = AppLocalizations.of(context);
      AppAlert.showAlert(localized?.noFilterTileError ?? "", context);
    }
  }
}

class RoomTileClipper extends CustomClipper<Path> {
  RoomTileClipper(this.offsets);

  final List<String> offsets;

  @override
  Path getClip(Size size) {
    var path = Path();

    for (int i = 0; i < offsets.length; i++) {
      final points = offsets[i].split(",");

      final x = (double.parse(points.first) * size.width) / 1080.0;
      final y = (double.parse(points.last) * size.height) / 1920.0;

      if (i == 0) {
        path.moveTo(x, y);
      } else {
        path.lineTo(x, y);
      }
    }
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant RoomTileClipper oldClipper) {
    return true;
  }
}

class RoomPartPlan extends StatelessWidget {
  RoomPartPlan(
    this.clipPart,
    this.roomPart, {
    Key? key,
    this.animation = false,
    this.selected = false,
    this.onPressed,
    this.tile,
  }) : super(key: key);

  final RoomPart roomPart;
  final List<String> clipPart;
  final bool animation, selected;
  final VoidCallback? onPressed;
  final Tile? tile;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    Widget tileContent() {
      if (tile != null) {
        final String url;
        switch (roomPart) {
          case RoomPart.floor:
            url = tile?.floor ?? "";
            break;
          case RoomPart.wall:
            url = tile?.wall ?? "";
            break;
          case RoomPart.wall_left:
            url = tile?.leftWall ?? "";
            break;
          case RoomPart.wall_right:
            url = tile?.rightWall ?? "";
            break;
        }

        return FutureBuilder<ImageProvider>(
            future: AppImage.tileCacheImage(url),
            builder: (context, snapshot) {
              if (snapshot.hasData && snapshot.data != null) {
                return Image(
                  image: snapshot.data!,
                  fit: BoxFit.fitHeight,
                  alignment: Alignment.center,
                  loadingBuilder: (_, child, loading) {
                    if (loading == null) return child;

                    return Shimmer.fromColors(
                      baseColor: Colors.grey[400]!,
                      highlightColor: Colors.white,
                      child: Container(color: Colors.white),
                    );
                  },
                );
              } else if (snapshot.hasError) {
                return Container(color: Colors.white);
              }

              return Shimmer.fromColors(
                baseColor: Colors.grey[400]!,
                highlightColor: Colors.white,
                child: Container(color: Colors.white),
              );
            });
      }

      return Container(
        color: selected ? theme.primaryColor : Colors.grey[400]!,
      );
    }

    final content = ClipPath(
      clipper: RoomTileClipper(clipPart),
      child: InkWell(onTap: onPressed, child: tileContent()),
    );

    return animation
        ? Shimmer.fromColors(
            baseColor: Colors.grey[400]!,
            highlightColor: Colors.white,
            loop: 0,
            child: content,
            enabled: animation,
          )
        : content;
  }
}
