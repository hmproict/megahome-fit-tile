import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_module/main.dart';
import 'package:flutter_module/page/area_list.dart';
import 'package:flutter_module/page/room_list.dart';
import 'package:flutter_module/page/router/router_path.dart';
import 'package:flutter_module/page/router/router_state.dart';

class AppRouterDelegate extends RouterDelegate<AppRoutePath> with ChangeNotifier, PopNavigatorRouterDelegateMixin<AppRoutePath> {
  final GlobalKey<NavigatorState> navigatorKey;

  StreamSubscription<dynamic>? _eventSubscription;
  RoomAppState appState = RoomAppState();

  AppRouterDelegate() : navigatorKey = GlobalKey<NavigatorState>() {
    appState.addListener(notifyListeners);
  }

  AppRoutePath get currentConfiguration {
    if (appState.selectedRoom != null) {
      return RoomPath(appState.selectedRoom!);
    } else if (appState.selectedArea != null) {
      return RoomListPath(appState.selectedArea!);
    } else {
      return RoomAreaPath();
    }
  }

  @override
  Widget build(BuildContext context) {

    if (appState.selectedArea != null && _eventSubscription == null) {
      _eventSubscription = default_event.receiveBroadcastStream("back").listen(_eventListener);
    }
    return Navigator(
      key: navigatorKey,
      pages: [
        MaterialPage(
          key: ValueKey('AreaPage'),
          child: AreaListPage(
            onSelectedArea: (area) {
              appState.selectedArea = area;
            },
          ),
        ),
        if (appState.selectedArea != null) RoomListPage(area: appState.selectedArea!)
      ],
      onPopPage: (route, result) {
        if (!route.didPop(result)) {
          return false;
        }

        print("route.settings : ${route.settings}");
        if (appState.selectedArea != null && route.settings is RoomListPage) {
          appState.selectedArea = null;
          _eventSubscription?.cancel();
          _eventSubscription = null;
        }
        notifyListeners();
        return true;
      },
    );
  }

  @override
  Future<void> setNewRoutePath(AppRoutePath path) async {
    print("setNewRoutePath : $path");
    if (path is RoomAreaPath) {
      appState.selectedArea = null;
      appState.selectedRoom = null;
    } else if (path is RoomListPath) {
      if (_eventSubscription == null) {
        _eventSubscription = default_event.receiveBroadcastStream("back").listen(_eventListener);
      }
      appState.selectedArea = null;
    } else if (path is RoomPath) {
      appState.selectedRoom = null;
    }
  }

  void _eventListener(dynamic event) {
    print("_eventListener : $event");
    if (event is String) {
      switch (event) {
        case "back":
          navigatorKey.currentState?.maybePop();
          break;
        case "backToRoot":
          navigatorKey.currentState?.popUntil((route) => route.isFirst);
          break;
        default:
          break;
      }
    }
  }

  @override
  void dispose() {
    _eventSubscription?.cancel();
    super.dispose();
  }
}
