import 'package:flutter/material.dart';
import 'package:flutter_module/page/router/router_path.dart';

class AppRouteInformationParser extends RouteInformationParser<AppRoutePath> {
  @override
  Future<AppRoutePath> parseRouteInformation(RouteInformation routeInformation) async {
    final uri = Uri.parse(routeInformation.location!);
    // Handle '/'
    if (uri.pathSegments.length == 0) {
      return RoomAreaPath();
    }

    // // Handle '/book/:id'
    // if (uri.pathSegments.length == 2) {
    //   if (uri.pathSegments[0] != 'book') return BookRoutePath.unknown();
    //   var remaining = uri.pathSegments[1];
    //   var id = int.tryParse(remaining);
    //   if (id == null) return BookRoutePath.unknown();
    //   return BookRoutePath.details(id);
    // }

    // Handle unknown routes
    return RoomAreaPath();
  }

  @override
  RouteInformation restoreRouteInformation(AppRoutePath path) {
    if (path is RoomAreaPath) {
      return RouteInformation(location: '/');
    }
    return RouteInformation(location: '/');
  }
}
