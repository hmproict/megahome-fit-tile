// Routes
import 'package:flutter_module/model/room.dart';

abstract class AppRoutePath {}

class RoomAreaPath extends AppRoutePath {}

class RoomListPath extends AppRoutePath {
  final Area area;

  RoomListPath(this.area);
}

class RoomPath extends AppRoutePath {
  final Room room;

  RoomPath(this.room);
}
