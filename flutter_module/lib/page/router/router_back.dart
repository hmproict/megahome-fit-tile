import 'package:flutter/material.dart';
import 'router_delegate.dart';

class AppBackButtonDispatcher extends RootBackButtonDispatcher {
  final AppRouterDelegate _routerDelegate;

  AppBackButtonDispatcher(this._routerDelegate)
      : super();

  @override
  Future<bool> didPopRoute() {
    print("didPopRoute : _routerDelegate");
    return _routerDelegate.popRoute();
  }
}
