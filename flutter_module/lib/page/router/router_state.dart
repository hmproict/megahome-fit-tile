import 'package:flutter/material.dart';
import 'package:flutter_module/model/room.dart';

class RoomAppState extends ChangeNotifier {
  Area? _selectedArea;
  Room? _selectedRoom;

  //
  // final List<Book> books = [
  //   Book('Stranger in a Strange Land', 'Robert A. Heinlein'),
  //   Book('Foundation', 'Isaac Asimov'),
  //   Book('Fahrenheit 451', 'Ray Bradbury'),
  // ];
  //

  RoomAppState();

  Area? get selectedArea => _selectedArea;

  set selectedArea(Area? area) {
    _selectedArea = area;
    _selectedRoom = null;
    notifyListeners();
  }

  Room? get selectedRoom => _selectedRoom;

  set selectedRoom(Room? room) {
    _selectedRoom = room;
    notifyListeners();
  }
}
