import 'dart:async';
import 'dart:ui' as ui;

import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_module/element/app_button.dart';
import 'package:flutter_module/extra/app_images.dart';
import 'package:flutter_module/model/room.dart';
import 'package:flutter_module/page/room_showcase.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class RoomListPage extends Page {
  final Area area;

  RoomListPage({
    required this.area,
  }) : super(key: ValueKey(area));

  Route createRoute(BuildContext context) {
    return MaterialPageRoute(
      settings: this,
      builder: (BuildContext context) {
        return RoomList(area);
      },
    );
  }
}

class RoomList extends StatefulWidget {
  RoomList(this.area);

  final Area area;

  @override
  _RoomListState createState() => _RoomListState();
}

class _RoomListState extends State<RoomList> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.area.localizedName(context)),
      ),
      body: SafeArea(
        child: StaggeredGridView.countBuilder(
          padding: const EdgeInsets.all(10),
          crossAxisCount: 4,
          itemCount: widget.area.rooms.length,
          itemBuilder: (BuildContext context, int index) => OpenContainer(
            closedBuilder: (_, closeContainer) {
              return LayoutBuilder(
                builder: (context, constraint) {
                  final width = constraint.maxWidth;
                  final height = width * 16 / 9;
                  final room = widget.area.rooms[index];

                  return AppButton(
                    content: Image.asset(
                      AppImage.room(room.preview),
                      fit: BoxFit.cover,
                      height: height * room.previewHeight,
                    ),
                    onPressed: closeContainer,
                    textAlign: TextAlign.left,
                    padding: EdgeInsets.zero,
                    showShadowWhenHighlight: true,
                  );
                },
              );
            },
            openBuilder: (_, openContainer) {
              final room = widget.area.rooms[index];
              return RoomShowcase(
                room,
                onClose: openContainer,
              );
            },
            closedColor: Colors.white,
            openColor: Colors.white,
            transitionType: ContainerTransitionType.fade,
            closedShape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          ),
          staggeredTileBuilder: (int index) => new StaggeredTile.fit(2),
          mainAxisSpacing: 10,
          crossAxisSpacing: 10,
        ),
      ),
    );
  }

  Future<Size> _imageSize(Room room) async {
    final name = AppImage.room(room.preview);
    final Image image = Image(image: AssetImage(name));
    Completer<ui.Image> completer = new Completer<ui.Image>();
    image.image.resolve(new ImageConfiguration()).addListener(new ImageStreamListener((ImageInfo image, bool _) {
      completer.complete(image.image);
    }));
    ui.Image info = await completer.future;
    return Size(info.width.toDouble(), info.height.toDouble());
  }
}
