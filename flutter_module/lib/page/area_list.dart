import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_module/element/app_button.dart';
import 'package:flutter_module/extra/app_images.dart';
import 'package:flutter_module/model/room.dart';

import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;

class AreaListPage extends StatefulWidget {
  AreaListPage({this.onSelectedArea});

  final ValueChanged<Area>? onSelectedArea;

  @override
  _AreaListPageState createState() => _AreaListPageState();
}

class _AreaListPageState extends State<AreaListPage> {
  List<Area>? areas;

  @override
  void initState() {
    super.initState();
    _prepareData().then((value) {
      setState(() {
        this.areas = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final localized = AppLocalizations.of(context);
    final style = Theme.of(context).textTheme;

    return Scaffold(
      appBar: AppBar(
        title: Text(localized?.roomPageTitle ?? "-"),
      ),
      body: SafeArea(
        child: areas == null
            ? Center(
                child: CircularProgressIndicator(),
              )
            : GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: 3 / 4,
                  crossAxisSpacing: 20,
                  mainAxisSpacing: 20,
                ),
                itemBuilder: (context, index) {
                  final area = areas![index];
                  return AppButton(
                    padding: EdgeInsets.zero,
                    shape: AppButtonShape.roundRect,
                    showShadowWhenHighlight: true,
                    showShadow: true,
                    content: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      child: Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                          image: new AssetImage(AppImage.room(area.image)),
                          fit: BoxFit.cover,
                        )),
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          color: Colors.white.withAlpha(125),
                          width: double.infinity,
                          child: Text.rich(
                            TextSpan(
                              text: area.nameTh,
                              style: style.caption?.copyWith(height: 1),
                              children: [
                                TextSpan(text: "\n"),
                                TextSpan(text: area.name, style: style.overline),
                              ],
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),
                    onPressed: () {
                      if (widget.onSelectedArea != null) {
                        widget.onSelectedArea!(area);
                      }
                    },
                  );
                },
                itemCount: areas!.length,
                padding: EdgeInsets.all(20),
              ),
      ),
    );
  }

  Future<List<Area>?> _prepareData() async {
    final rawRoom = await rootBundle.loadString('assets/room.json');
    final Map<String, dynamic> room = jsonDecode(rawRoom);
    final areas = room["area"].map((json) => Area.fromJson(json));
    return List<Area>.from(areas);
  }
}
